<?php
/**
 * Elgg index page for web-based applications
 *
 * @package Elgg
 * @subpackage Core
 */

/**
 * Start the Elgg engine
 */
require_once(dirname(__FILE__) . "/engine/start.php");

elgg_set_context('main');

// allow plugins to override the front page (return true to stop this front page code)
if (elgg_trigger_plugin_hook('index', 'system', null, FALSE) != FALSE) {
	exit;
}

if (elgg_is_logged_in()) {
	forward('/mystats/all');
}

//$login_box = elgg_view('core/account/login_box');
if (elgg_get_config('allow_registration')) {
	$register =  elgg_view('page/elements/register-welcome'); 
}

/*$paasage_icon = "<div class='welcome-wrapper'>" . elgg_view('output/img', array(
		'src' => '_graphics/icons/paasage-large-icon.png',
		'class' => 'paasage-large-icon'
	)) . "</div>"; */

$params = array(
		'content' => $register
);
$body = elgg_view_layout('one_column', $params);
echo elgg_view_page(null, $body);
