# Elgg API
Until Elgg create a stable API I begin to have the following API commands

* .../draw_app/api?*q=AppAndComp&key=Spec*
	Returns the similar applications and the artefacts of then if exists any.

* .../draw_app/api?*q=whatDoYouKnowAbout&key=...*
	Returns in human mode(no JSON) the *key* if is an app or an artifact. Or returns
	the number of node instances if the key is flexiant or amazon

# Node.js Natural Language Processing API

* ../word?*key=...*
Returns JSON: {word: ..., unknown: true or false}

#node installation on ubuntu
<pre>apt-get install nodejs npm</pre>
and inside the ./nlp run:
<pre>npm install natural </pre>
<pre>npm install express </pre>
#Configuration Files
There are three configuration files:
* /mod/draw_app/config.php, for php configuration
* /mod/draw_app/config.js, for javascript configuration

## To debug
* Suggest groups. IN MODALS
* VM connector to Clouds.

## To do
1. Component Type or Class 
2. Deployment Hints

###Scenario 1
1. Suggest from MDDB
2. Load schema from MDDB
3. User provide SLO
4. Suggest Deployment
5. Deploy (fake!)

###Scenario 3 
1. Cache apps, artifacts, clouds to the server in order for suggest.

###Scenario 4
1. refer to cloud vs application suggest..

###List 2
2. Deployment hints.

###List 3
1. Sidebar with suggestions when the user update some application.
2. Integrade Elgg with Rule DB.

###List 4
1. User can Right-click on words and provide information about that word.
2. Natural Language Processing 

## Done
1. Interaction with mdDB.
2. Suggested Groups when app created.
3.1 JS capture words of the user when is typing in posts.
3.2 when the user refer to flexiant in groups topics I saw the hint: `The MDDB contains 3456 instances of applications deployed on the Flexiant cloud. 
4. ScrollBar to draw_app
5. FeedBack To Groups From MDDB.
6. something that the system knows about.
7. mispelled words... (short demo with a few words)
8. Compoce app. (all the view or with no deployment hints)
9. timestamps on compose app.

##Changes
* I am changing the group plugin in order to suggest hints to user.
* I change the localhost to listen on nlplocal

#SLOs:
SLO for artifacts.

##IDE: 
* efarmogh, platforma, dedomena.
* user requirements, data
* usability.
* design:
* HCI 
* deliverable:
4. implementation of social network

## Meeting with HCI 24/1/2014
* see the already deployed applications.
* search for components.
* search for applications.
* *1280x800 analysis. scale.*
* feature category. vendor provided information.
* versions

## Meeting 03/02/2014
* Plugin for login
* Plugin jsPlumb to compose applications
* Application rating:
	-> successfull deployments, -> user rating (customer sat)
* Photo of applications
* eukolo tropo na allazoyme ta barh twn applications sto top app/new.

## Meeting 03/02/2014
* Remake the deliverable as follows: first, what we need and what we done, and second what applications and plugins we use.

## Meeting 09/05/14
* finalize button on application composition, use of a application plus plus

## Call 13/6/14
* library for the graphs.
* model with tosca. mapping tosca to chef??

## Meeting 25/6/14
* Green Button to run.
* Demo on November.
* Publication on 1 October.
* E-core, eclipse, emf <-- ?????

## Meeting 11/07/14 with HCI
* @todo implement cart in Elgg.
* @todo import to elgg the Chef components.


## To do 18/11
* mock numbers to draw_app/all or the first number from cdo server,
* uses and watches,
