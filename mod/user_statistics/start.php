<?php

elgg_register_event_handler('init', 'system', 'user_statistics_init', 1000);

function user_statistics_init() {
  elgg_register_page_handler('mystats', 'mystats_page_handler');
	elgg_register_page_handler('mytags', 'mytags_page_handler');
  user_stats_register_js();
  user_stats_register_libraries();
}

/**
 * The mystats page handler.
 * * mystats/all
 * 
 * @param String $page the requested page
 * @return boolean true if all goes well.
 * 
 */
function mystats_page_handler($page) {
  $base = elgg_get_plugins_path() . '/user_statistics';
  switch ($page[0]) {
    case 'all':
      gatekeeper();
      elgg_load_js('custom_css.profile.js');
      include $base . '/views/default/page/all.php';

      break;
    case 'activity':
      elgg_load_library('infinite.scroll.river_library');
      $offset = get_input('offset');
      echo show_activity($offset);

      break;
    default:
      break;
  }
  return TRUE;
}

function mytags_page_handler($page) {
	switch ($page[0]) {
		case 'skills':
		case 'interest':
			gatekeeper();
			
			echo elgg_view('page/mytags/all', array('user_guid' => $page[1]));
			
			break;
		default:
			break;
	}
	return TRUE;
}

function user_stats_register_js() {
  $url1 = 'mod/user_statistics/js/infinite-scroll.js';
  elgg_register_js('elgg.infinite.scroll.js', $url1, 'footer');
}

function user_stats_register_libraries() {
  $base = elgg_get_plugins_path() . 'user_statistics/lib/';

  elgg_register_library('infinite.scroll.river_library', $base . 'river_library.php');
  elgg_register_library('user.stats', $base . 'stats.php');  
  elgg_register_library('restAPI', $base . 'restAPI.php');
}
