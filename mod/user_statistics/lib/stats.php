<?php

/**
 * library = user.stats
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

/**
 * 
 * @param type $guid 
 * @return array 
 */
function user_stats_get_profile_completeness($guid) {
	$site = elgg_get_site_url();
	$edit = "{$site}my_profile/" . get_entity($guid)->username . "/edit";

	$result['count'] = 0;
	$result['hint'] = NULL;
	$skills = elgg_get_entities_from_relationship(array(
		'relationship' => 'has_skill',
		'relationship_guid' => $guid,
		'inverse_relationship' => FALSE
	));
	switch (count($skills)) {
		case 0:
			$result['hint'] = "<label for='skills' id='skillslabel'>add some skills</label>";
			break;
		case 1:
			$result['hint'] = "<label for='skills' id='skillslabel'>add two more skills</label>";
			$result['count'] += 10;
			break;
		case 2:
			$result['hint'] = "<label for='skills' id='skillslabel'>add one more skill</label>";
			$result['count'] += 20;
			break;
		default:
			$result['count'] += 30;
			break;
	}

	$interests = elgg_get_entities_from_relationship(array(
		'relationship' => 'has_interest',
		'relationship_guid' => $guid,
		'inverse_relationship' => FALSE
	));
	switch (count($interests)) {
		case 0:
			if ($result['hint'] == NULL) {
				$result['hint'] = "<label for='appendedInputButton' id='appendedInputButtonlabel'>add some interests</label>";
			}
			break;
		case 1:
			if ($result['hint'] == NULL) {
				$result['hint'] = "<label for='appendedInputButton' id='appendedInputButtonlabel'>add two more interests</label>";
			}
			$result['count'] += 10;
			break;
		case 2:
			if ($result['hint'] == NULL) {
				$result['hint'] = "<label for='appendedInputButton' id='appendedInputButtonlabel'>add one more interest</label>";
			}
			$result['count'] += 20;
			break;
		default:
			$result['count'] += 30;
			break;
	}

	/* Other Profile fields */
	$profile_fields = elgg_get_config('profile_fields');
	if (is_array($profile_fields) && count($profile_fields) > 0) {
		foreach ($profile_fields as $shortname => $valtype) {
			$metadata = elgg_get_metadata(array(
				'guid' => $guid,
				'metadata_name' => $shortname,
				'limit' => false
			));
			if ($metadata) {
				if (is_array($metadata)) {
					$value = '';
					foreach ($metadata as $md) {
						if (!empty($value)) {
							$value .= ', ';
						}
						$value .= $md->value;
					}
				} else {
					$value = $metadata->value;
				}
			}
			if ($value !== "") {
				$result['count'] += 10;
			} else {
				if ($result['hint'] == "") {
					$profile_fields_name = elgg_echo("profile:{$shortname}");
					if ($profile_fields_name === "Role") {
						$result['hint'] = "<a href='" . $edit . "'>What is your role in your Organization?</a>";
					} else if ($profile_fields_name === "Location") {
						$result['hint'] = "<a href='" . $edit . "'>Where are you based at?</a>";
					} else if ($profile_fields_name === "Organization") {
						$result['hint'] = "<a href='" . $edit . "'>Which organization are you member of?</a>";
					} else if ($profile_fields_name === "About") {
						$result['hint'] = "<a href='" . $edit . "'>Say a few things about yourself</a>";
					} else {
						$result['hint'] = "<a href='" . $edit . "'>Provide your " . $profile_fields_name . "</a>";
					}
				}
			}
			$value = ""; // reset value
		}
	}

	return $result;
}
