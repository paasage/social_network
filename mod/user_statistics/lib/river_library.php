<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function show_activity($offset) {
  $options = array('pagination' => false, 'offset' => $offset);
  return user_stats_list_river($options);
}

function user_stats_list_river(array $options = array()) {
  global $autofeed;
  $autofeed = true;

  $defaults = array(
      'offset' => (int) max(get_input('offset', 0), 0),
      'limit' => (int) max(get_input('limit', 20), 0),
      'pagination' => TRUE,
      'list_class' => 'elgg-list-river elgg-river', // @todo remove elgg-river in Elgg 1.9
  );

  $options = array_merge($defaults, $options);

  $options['count'] = TRUE;
  $count = elgg_get_river($options);

  $options['count'] = FALSE;
  $items = elgg_get_river($options);

  $options['count'] = $count;
  $options['items'] = $items;
  return elgg_view('page/components/list-activity', $options);
}

function user_stats_top_rated_apps($entities, $options) {
  $vars = draw_app_create_vars($entities, $options);

  return elgg_view('page/components/user-stats-list-models', $vars);
}

function user_stats_most_rated_apps($entities, $options) {
  $new_entities = array();
  
  foreach ($entities as $e) {
    if($e->runs > 0) {
      $new_entities[] = $e;
    }
  }
  
  $vars = draw_app_create_vars($new_entities, $options);
  $vars = draw_app_create_vars($entities, $options);

  return elgg_view('page/components/user-stats-list-models-most-runs', $vars);
}
