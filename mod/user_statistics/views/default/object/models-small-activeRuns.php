<?php
/**
 * The view for top contributions in top rated area.
 * 
 * @uses $vars['entity'] ElggEntity Application Model.
 * @uses $vars['most_run'] Now if called from top rated or most run.
 */
elgg_load_library('restAPI');

$app = $vars['entity'];

// get the time
$now = date("j M Y @ H:i");

// get the platform endpoint of the specific user
$currentUser = elgg_get_logged_in_user_entity();
$platformEndpoint = $currentUser->platformEndpoint;
$platformEndpointNoPort = substr($platformEndpoint, 0, -6);
$emailEndpoint = $currentUser->emailEndpoint;
$passwordEndpoint = $currentUser->passwordEndpoint;
$tenantEndpoint = $currentUser->tenantEndpoint;

// get the name of the model
$name = $app->appname;
$giud = $app->getGUID();

// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint);

// 2)retrieve the state of the paasage resource
$state = get_resource_state($platformEndpoint, $name, $token, $user_id);

// 3)retrieve the state of the paasage resource
$substate = get_resource_substate($platformEndpoint, $name, $token, $user_id);


//echo("<script> console.log('".$state."'); </script>");

?>

<dl>
    <!-- if the model is uploaded to Paasage platform -->
    <?php if ($state == 'CREATED' || $state == 'READY_TO_REASON' || $state == 'REASONING' || $state == 'READY_TO_DEPLOY' || $state == 'DEPLOYING' || $state == 'DEPLOYED' || $state == 'RUNNING') { 

      // these are the icon and the name of the application
      echo elgg_view('output/img', array('src' => '_graphics/model-icon-general.png'));
      echo elgg_view('output/url', array('href' => "draw_app/view/" . $app->getGUID(), 'text' => $app->appname));
      ?>
      <dd class="ratings"> <br> </dd>
      <dd class="ratings" id="state_<?= $name; ?>" style="margin-left: 25px">state: <?= $state; ?></dd>
      <dd class="ratings" id="substate_<?= $name; ?>" style="margin-left: 25px">subState: <?= $substate; ?></dd>
      <dd class="ratings" id="time_<?= $name; ?>" style="margin-left: 25px">information as of: <?= $now; ?></dd>
       
      <dd class="ratings"> <br> </dd>
      <dd class="ratings" id="platformEndpoint_<?= $name; ?>" style="margin-left: 25px">executionware UI: <a href="<?= $platformEndpointNoPort; ?>" target="_blank"> <?= $platformEndpointNoPort; ?> </a> </dd> 
      
      <dd class="ratings">
        <br>
        <button type="button" id="refresh_status" modelName="<?=$name;?>" >Refresh status</button> (automatically refeshing every 5 minutes)
      </dd>
    <?php } else { ?>
      
    <?php } ?>
</dl>
<hr>



