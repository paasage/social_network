<?php
/**
 * The view for top contributions in top rated area.
 * 
 * @uses $vars['entity'] ElggEntity Application Model.
 * @uses $vars['most_run'] Now if called from top rated or most run.
 */
$app = $vars['entity'];

$options_for_reviews = array(
    'metadata_names' => array('application_reference'),
    'metadata_values' => array($app->getGUID()),
    'limit' => 65000,
    'count' => TRUE
);

$count = elgg_get_entities_from_metadata($options_for_reviews);
$options_for_reviews['count'] = FALSE;
$reviews_context = elgg_get_entities_from_metadata($options_for_reviews);

$rate_number = 0;
if ($count) {
  foreach ($reviews_context as $r) {
    $rate_number += $r->rating;
  }
  $rate_number /= $count;
	$rate_number = number_format($rate_number , 2);
}

echo elgg_view('output/img', array('src' => '_graphics/model-icon-general.png'));
echo elgg_view('output/url', array('href' => "draw_app/view/" . $app->getGUID(), 'text' => $app->appname));
?>
<dl>
  <?php if ($vars['most_run']) { ?>
    <dd class="ratings">runs: <?= $app->runs; ?></dd>
  <?php } else { ?>
    <dd class="ratings">average rating: <?= $rate_number; ?>/5 (<?= $count; ?> reviews)</dd>
  <?php } ?>
</dl>


