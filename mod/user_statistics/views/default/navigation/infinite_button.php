<?php
/**
 * The button of infinite scroll
 */
$loading_icon = elgg_view('output/img', array(
		'src' => '_graphics/ajax_loader.gif'
));
?>
<div class="infinite-button">
	<span class="arrow-down">&gt;</span>
	<span class="loading_icon"><?php echo $loading_icon;?></span>
</div>