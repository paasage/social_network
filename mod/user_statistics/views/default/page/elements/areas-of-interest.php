<?php
/**
 * 
 */
$user_entity = get_entity($vars['guid']);

$annotations = elgg_get_entities_from_relationship(array('relationship' => 'has_interest', 'relationship_guid' => $vars['guid']));
$body = '';
foreach ($annotations as $interest) {
  $body .= '<div class="skill-tag" >
		<span class="skill-tag-name">' . $interest->name . '</span>
		<span class="skill-tag-delete-btn" data-uid="' . $interest->name . '">&times;</span>
	</div>';
}
?>

<div class="skill-tags-container" id="interest-tags-container">
  <?php echo $body; ?>
</div>

<?php
if (elgg_get_logged_in_user_guid() == $vars['guid']) {
  echo '<form onsubmit="return add_interest();" action="#" id="interest-form">'
  . '<input class="add-interest" id="appendedInputButton" type="text" placeholder="Add an Interest" name="interest" style="margin-left: 10px;">'
  . '<input class="skills-add-button" type="submit" value="Add" style="margin: 0;">'
  . '</form>';
}
?>