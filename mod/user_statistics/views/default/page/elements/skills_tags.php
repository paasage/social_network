<?php
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$user_entity = get_entity($vars['guid']);

?>
<div class="skill-tags-container" id="skill-tags-container">
<?php
$skills = elgg_get_entities_from_relationship(array('relationship' => 'has_skill', 'relationship_guid' => $vars['guid']));
foreach ($skills as $skill) {
  echo '<div class="skill-tag">
          <span class="skill-tag-name">' . $skill->name . '</span>
          <span class="skill-tag-delete-btn" data-uid="' . $skill->name. '">&times;</span>
        </div>';
}
?>
</div>
  <?php
  if (elgg_get_logged_in_user_guid() == $vars['guid']) {
    echo elgg_view('skills/add');
  }
  ?>

