<?php
/**
 * Shows a suggestion to profile completeness sidebar module.
 * 
 * @uses $vars['hint'] String | the hint.
 */
if($vars['hint'] == null) {
  return ;
}
?>
<div class="hint">
  <?= $vars['hint']; ?>
</div>


