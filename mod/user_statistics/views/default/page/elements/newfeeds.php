<?php
/**
 * The new feeds of the site.
 * 
 */
$options = array('pagination' => true);

elgg_load_js('elgg.infinite.scroll.js');
elgg_load_library('infinite.scroll.river_library');
?>

<div class="stats-sections" id="new-feeds">
	<h3>Live Feed</h3>
<?php 
	echo user_stats_list_river($options);
?>	
</div>