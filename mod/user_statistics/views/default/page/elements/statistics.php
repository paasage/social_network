<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$user_guid = elgg_get_logged_in_user_guid();

$friends = count(elgg_get_entities_from_relationship(array(
    'relationship' => 'friend',
    'relationship_guid' => $user_guid,
    'type' => 'user',
    'subtype' => '',
    'limit' => 5000,
    'order_by' => 'e.time_created desc',
  )));


$options = array(
    'count' => TRUE,
    'owner_guids' => array($user_guid),
    'types' => array('object'),
    'subtypes' => array('draw_application')
);

$options_contr = array(
        'relationship' => 'is_contributor_to',
        'relationship_guid' => $user_guid,
        'inverse_relationship' => FALSE,
        'count' => TRUE
    );


$models = elgg_get_entities($options) + elgg_get_entities_from_relationship($options_contr);

$options['subtypes'] = array('groupforumtopic');
$questions = elgg_get_entities($options);

$options['subtypes'] = array('software_component');
$components = elgg_get_entities($options);
if (!$components) {
  $components = 0;
}
$options_annotations = array(
    'annotation_owner_guids' => array($user_guid),
    'annotation_calculation' => 'count',
    'annotation_names' => array('group_topic_post')
);
$replies = elgg_get_annotations($options_annotations);
?>
<div class="row-fluid stats-topbar">
  Summary of activity for your models and components
</div>
<div class="row-fluid stats-sections">
  <div class="span4">
    <h3>Top Rated</h3>
    <?php echo elgg_view('page/elements/models/top-rated'); ?>
  </div>

  <div class="span4">
    <h3>Most Run</h3>
		<?php echo elgg_view('page/elements/models/most-run'); ?>
  </div>

  <div class="span4">
    <h3>Statistics Overview</h3>
    <ul class="stats-overview">
      <li>
        <span class="number"><?php echo $friends; ?></span>Friends
      </li>
      <li><span class="number"><?php echo $models; ?></span>Models</li>
      <li><span class="number"><?php echo $components; ?></span>Components</li>
      <li><span class="number"><?php echo $questions; ?></span>Questions</li>
      <li><span class="number"><?php echo $replies; ?></span>Replies</li>
    </ul>
  </div>
</div>
