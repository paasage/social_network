<?php

elgg_load_library('infinite.scroll.river_library');
$user_guid = elgg_get_logged_in_user_guid();
$top_contributions = elgg_list_entities(array(
	'type' => 'object',
	'subtype' => 'draw_application',
	'owner_guid' => $user_guid,
	'full_view' => false,
	'limit' => 2
	), 'elgg_get_entities', 'user_stats_top_rated_apps');

$options = array(
		'relationship' => 'is_contributor_to',
		'relationship_guid' => $user_guid,
		'inverse_relationship' => FALSE
);
$apps = elgg_list_entities(
	$options, 'elgg_get_entities_from_relationship', 'user_stats_top_rated_apps');
if($top_contributions == ""&& $apps == ""){
  echo '<div style="font-size: 1.1em;color: #9ea5b2;">Currently you don\'t have top rated application models</div>';
} else {
  echo $top_contributions . $apps;
}
