<?php
/**
 * View a list of items
 *
 * @package Elgg
 *
 * @uses $vars['items']       Array of ElggEntity or ElggAnnotation objects
 * @uses $vars['offset']      Index of the first list item in complete list
 * @uses $vars['limit']       Number of items per page. Only used as input to pagination.
 * @uses $vars['count']       Number of items in the complete list
 * @uses $vars['base_url']    Base URL of list (optional)
 * @uses $vars['pagination']  Show pagination? (default: true)
 * @uses $vars['position']    Position of the pagination: before, after, or both
 * @uses $vars['full_view']   Show the full view of the items (default: false)
 * @uses $vars['list_class']  Additional CSS class for the <ul> element
 * @uses $vars['item_class']  Additional CSS class for the <li> elements
 * 
 * @author Christos Papoulas
 */
//echo "<script> console.log('user-small-list-models'); </script>";


$items = $vars['items'];
$offset = elgg_extract('offset', $vars);
$limit = elgg_extract('limit', $vars);
$count = elgg_extract('count', $vars);
$base_url = current_page_url();
$pagination = elgg_extract('pagination', $vars, true);
$offset_key = elgg_extract('offset_key', $vars, 'offset');
$position = elgg_extract('position', $vars, 'after');

$page = elgg_extract('page', $vars);
$page2 = elgg_extract('page2', $vars);

$list_class = 'elgg-list';
if (isset($vars['list_class'])) {
	$list_class = "$list_class {$vars['list_class']}";
}

$item_class = 'elgg-item';
if (isset($vars['item_class'])) {
	$item_class = "$item_class {$vars['item_class']}";
}

$html = "";
$nav = "";

if (is_array($items) && count($items) > 0) {
	
	foreach ($items as $item) {

		// if we are at the "Active runs" page
		if ($page == "activeruns" || $page2 == "activeruns") {
			$li = elgg_view('object/models-small-activeRuns', array('entity' => $item));
		} else { // if we are at the "My models" page
			$li = elgg_view('object/models-small', array('entity' => $item));
		}
		echo "<script> console.log('$li'); </script>";
		if ($li) {
			if (elgg_instanceof($item)) {
				$id = "elgg-{$item->getType()}-{$item->getGUID()}";
			} else {
				$id = "item-{$item->getType()}-{$item->id}";
			}
			if (strpos($li, "ratings")) { // if the model is uploaded to a PaaSage platform
				$html .= '<div class="clearfix">';
				$html .= '<div class="activeruns">';
				$html .= '<div id="'. $id .'" class="app-small-object">'. $li . '</div>';
				$html .= '</div>';
				$html .= '</div>';
			}
		}
	}
}

echo $html;
