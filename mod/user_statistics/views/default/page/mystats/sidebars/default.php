<?php
elgg_load_library('user.stats');
$res = user_stats_get_profile_completeness(elgg_get_logged_in_user_guid());
$complete = $res['count']; 
$body = "Your Profile is {$complete}% complete<br/><br/>";
$body .= '<div class="complete-wrapper"><div class="complete-filled" style="width: ' . $complete . '%;"></div></div>';
$body .= elgg_view('page/elements/stats_suggestion', array('hint' => $res['hint'])); 

echo elgg_view_module('profile-complete', 'Profile Completeness', $body);

$skills = elgg_view('page/elements/skills_tags', array('guid' => elgg_get_logged_in_user_guid()));
$surl = elgg_view('output/url', array(
	'text' => 'View All (' . count( elgg_get_entities_from_relationship(array('relationship' => 'has_skill', 'relationship_guid' => elgg_get_logged_in_user_guid()))) . ')', 
	'class' => 'mystats_all', 
	'href' => 'mytags/skills/' . elgg_get_logged_in_user_guid())
);
echo elgg_view_module('profile-skills', 'Skills' . $surl, $skills, array('class' => 'mystats_skills'));

$interests = elgg_view('page/elements/areas-of-interest', array('guid' => elgg_get_logged_in_user_guid()));
$interests_count = count(elgg_get_entities_from_relationship(array('relationship' => 'has_interest', 'relationship_guid' => elgg_get_logged_in_user_guid())));
$surl = elgg_view('output/url', array(
	'text' => 'View All (' . $interests_count	. ')', 
	'class' => 'mystats_all', 
	'href' => 'mytags/interest/' . elgg_get_logged_in_user_guid())
);
echo elgg_view_module('profile-interests', 'Areas of interest' . $surl, $interests, array('class' => 'mystats_interests'));

echo elgg_view('groups/sidebars/elements/popular-groups');
?>

<?php
$suggested = elgg_view('page/elements/profile/suggest-friends', $vars);
$suggested .= '<div class="create"> <a href="' . $my_con_link . '">My Connections</a> </div>';
echo elgg_view_module('aside sidebar-section suggest-connections', 'Suggested Connections', $suggested);
