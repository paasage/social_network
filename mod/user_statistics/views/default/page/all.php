<?php
/**
 * 
 * 
 */

$sidebar = elgg_view( 'page/mystats/sidebars/default' );
$content =  elgg_view('page/elements/statistics');
$content .= elgg_view('page/elements/newfeeds');
$params = array('content' => $content, 'sidebar' => $sidebar);

$body = elgg_view_layout('one_sidebar', $params);
echo elgg_view_page('My Statistics', $body);
