<?php
/**
 * 
 * @uses $vars['user_guid'] User GUID
 */
if($vars['user_guid'] == NULL) {
	forward(REFERER);
}
$skills = elgg_get_entities_from_relationship(array('relationship' => 'has_skill', 'relationship_guid' => $vars['user_guid']));
?>
<div class="skill-tags-container all_tags" id="skill-tags-container" style="display: inline-table;">
	<h3>Skills</h3>
<?php
	foreach ($skills as $skill) {
  echo '<div class="skill-tag">
          <span class="skill-tag-name">' . $skill->name . '</span>
          <span class="skill-tag-delete-btn" data-uid="' . $skill->name. '">&times;</span>
        </div>';
	}
	if (elgg_get_logged_in_user_guid() == $vars['user_guid']) {
    echo elgg_view('skills/add', array('full_view' => TRUE));
  }
?>
</div>
<?php
$annotations = elgg_get_entities_from_relationship(array('relationship' => 'has_interest', 'relationship_guid' => $vars['user_guid']));
?>
<div class="skill-tags-container all_tags" id="interest-tags-container" style="display: inline-table;">
	<h3>Area of Interest</h3>
	<?php
	foreach ($annotations as $interest) {
  echo '<div class="skill-tag" >
		<span class="skill-tag-name">' . $interest->name . '</span>
		<span class="skill-tag-delete-btn" data-uid="' . $interest->name . '">&times;</span>
	</div>';
	}
	if (elgg_get_logged_in_user_guid() == $vars['user_guid']) {
  echo '<form onsubmit="return add_interest();" action="#">'
  . '<input class="add-interest" id="appendedInputButton" type="text" placeholder="Add an Interest" name="interest" style="margin-left: 10px;">'
  . '<input class="skills-add-button" type="submit" value="Add" style="margin: 0;">'
  . '</form>';
}
?>
</div>
<?php 

