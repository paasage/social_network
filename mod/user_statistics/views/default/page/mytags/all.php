<?php
/**
 * View all tags of interest and skills
 * 
 * @uses $vars['user_guid'] User GUID
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$sidebar = "";
$content =  elgg_view('page/mytags/body', $vars);

$params = array('content' => $content, 'sidebar' => $sidebar);

$body = elgg_view_layout('one_sidebar', $params);
echo elgg_view_page('My Tags', $body);