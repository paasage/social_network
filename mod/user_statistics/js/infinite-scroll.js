
$( 'document' ).ready( function() {
	var page_offset = 20;
	window.isAjaxCalled = false;
	console.log('infinite-scrool.js');
	$(window).scroll(function () {
		if($(window).scrollTop() + $(window).height() > getDocHeight() - 100) {
			if(!window.isAjaxCalled) {
				ajax_get_more_content(page_offset);
				page_offset = page_offset +10;
			}
		}
	});
} );

function ajax_get_more_content(offset) {
	elgg.get('mystats/activity?offset=' + offset, {
		beforeSend: function () {
			$('#new-feeds .infinite-button .arrow-down').hide();
			$('#new-feeds .infinite-button .loading_icon').css('display', 'block');
			window.isAjaxCalled = true;
		},
		success: function(resultText, success, xhr) {
			console.log( resultText );
			if(resultText) {
				$('#new-feeds ul.elgg-list.elgg-list-river.elgg-river').append( resultText );
				$('#new-feeds .infinite-button .loading_icon').css('display', 'none');
				$('#new-feeds .infinite-button .arrow-down').show();
			} else {
				$('#new-feeds .infinite-button').hide();
			}
			window.isAjaxCalled = false;
		}
	});
}

function getDocHeight() {
	var D = document;
	return Math.max(
		Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
		Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
		Math.max(D.body.clientHeight, D.documentElement.clientHeight)
	);
}