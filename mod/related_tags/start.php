<?php

/**
* Run Button plugin
*/

elgg_register_event_handler('init', 'system', 'related_tags_init');

function related_tags_init() {

  // register to receive requests that start with 'related_tags'
  elgg_register_page_handler('related_tags', 'related_tags_page_handler');

}

function related_tags_page_handler($page, $identifier) {

  $plugin_path = elgg_get_plugins_path();
  $base_path = $plugin_path . 'related_tags/pages/related_tags';

  if($identifier == "related_tags") {
    require "$base_path/related_tags.php";
  }


  return true;
}
