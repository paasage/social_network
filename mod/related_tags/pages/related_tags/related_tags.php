<?php
/**
* related tags page
*/

$title = "related tags page";

// get the current app
$current_app = get_entity($page[0]);
$current_app_tags = $current_app->getAnnotations('models_tags');

// get all Elgg apps
$options = array(
  'type' => 'object',
  'subtype' => 'draw_application',
  'full_view' => false,
  'limit' => false
);
$allapps = elgg_get_entities($options);

// get related apps
$related_apps = array();
$related_apps_index = 0;

foreach ($allapps as $app) {

	// do not include tags of current app
	if($app == $current_app) {
		continue;
	}

	$tags = $app->getAnnotations('models_tags');
	foreach ($tags as $tag) {

		foreach ($current_app_tags as $current_app_tag) {

			//echo "<script> console.log('tag: ".$tag->value."'); </script>";
			//echo "<script> console.log('current tag: ".$current_app_tag->value."'); </script>";

			if($current_app_tag->value == $tag->value) {
				$related_apps[$related_apps_index] = $app;
				$related_apps_index++;
			}
		}
		//echo "<script> console.log('".$tag->value."'); </script>";
	}
  	//echo "<script> console.log('".$app->getSubtype()."'); </script>";
}

// remove duplicated values from the array
//$related_apps_nodoubles = array_unique($related_apps);
foreach($related_apps as $k=>$v) {
	if( ($kt=array_search($v,$related_apps))!==false and $k!=$kt ) { 
		unset($related_apps[$kt]);  
 		$related_apps_nodoubles[]=$v; 
    }
}

// use the elgg view related_tags_sections
$params = array(
    title => 'Related Models',
    results => $related_apps,
    key => $page[0]
);
$content = elgg_view('related_tags/related_tags_sections', $params);

$vars = array('content' => $content);
$body = elgg_view_layout('one_sidebar', $vars);

echo elgg_view_page($title, $body);

