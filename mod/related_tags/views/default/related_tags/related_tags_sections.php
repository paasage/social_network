<?php
/**
 * 
 * @uses $vars['title'] 
 * @uses $vars['results'] 
 * @uses $vars['key'] 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$results = $vars['results'];
if($vars['results'] == "") {
	$results = "No {$vars['title']} named {$vars['key']}";
}
		
		$options = array();
        $options['full_view'] = false;

        if (!is_int($offset)) {
			$offset = (int) get_input('offset', 0);
		}

		$defaults = array(
			'items' => $results,
			'list_class' => 'elgg-list-entity',
			'full_view' => true,
			'pagination' => true,
			'limit' => false,
			'list_type' => $list_type,
			'list_type_toggle' => false,
			'offset' => $offset,
		);

		$options = array_merge($defaults, $options);

?>

<div class="row-fluid stats-topbar">
	<?= $vars['title']; ?>
</div>

<div class="row-fluid search-content" id="applications-views">
	<?= elgg_view('related_tags/list-models', $options); ?>
</div>
