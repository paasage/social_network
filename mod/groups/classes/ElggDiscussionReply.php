<?php

/**
 * Description of ElggDiscussionReply
 *
 * @author chris
 */
class ElggDiscussionReply extends ElggComment{
	
	/**
	 * Set subtype
	 */
	protected function initializeAttributes() {
		parent::initializeAttributes();

		$this->attributes['subtype'] = "discussion_reply";
	}
}

