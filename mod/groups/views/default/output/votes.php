<?php
/**
 * 
 * 
 * @uses $vars['guid'] The guid of Entity.
 */

elgg_load_js( 'custom_css.discussion.js' );

$discussion = get_entity($vars['guid']);
$relationships = get_entity_relationships($vars['guid']);
$upvotes = 0;
$downvotes = 0;
foreach ($relationships as $r) {
  $r_type = $r->get('relationship');
  if($r_type == 'up_vote') {
    $upvotes += 1;
  } else if ($r_type == 'down_vote') {
    $downvotes += 1;
  }
}

$up_icon = elgg_view('output/img', array('src' => '_graphics/groups/vote_inactive.jpg', 'class' => 'upvote'));
$down_icon = elgg_view('output/img', array('src' => '_graphics/groups/vote_inactive.jpg', 'class' => 'downvote'));

$upaction = elgg_view('output/img', array('src' => '_graphics/groups/vote_action.jpg', 'class' => 'upvote', 'data-guid' => $vars['guid']) );
$down_action = elgg_view('output/img', array('src' => '_graphics/groups/vote_action.jpg', 'class' => 'downvote', 'data-guid' => $vars['guid']));
?>
<div class="votes-stasts-wrapper">
  <div class="info">
    <span id="up-<?=$vars['guid'];?>"><?= $upvotes ?></span> <?= $up_icon ?> / 
		<span id="down-<?=$vars['guid'];?>"><?= $downvotes ?></span> <?= $down_icon ?> 
  </div>
  <div class="action">
    <?= $upaction ?> <?= $down_action ?> 
  </div>
</div>
