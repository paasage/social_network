<?php
/**
 * Discussion topic add/edit form body
 * 
 */

$title = elgg_extract('title', $vars, '');
$desc = elgg_extract('description', $vars, '');
$status = elgg_extract('status', $vars, '');
$tags = elgg_extract('tags', $vars, '');
$access_id = elgg_extract('access_id', $vars, ACCESS_DEFAULT);
$container_guid = elgg_extract('container_guid', $vars);
$guid = elgg_extract('guid', $vars, null);

//elgg load JS library for suggest to user.
elgg_load_js('elgg.autosuggest.to.topic');
//elgg load all other necessary js & css libraries 
elgg_load_js('elgg.modals');
elgg_load_js('elgg.jquery.impromptu');

$urls = array('no link');
$names = array('no link');
$App_option_values = array(0);
$allAppEntities = elgg_get_entities(array(
		'type' => 'object',
    'subtype' => 'draw_application',
		'owner_guids' => elgg_get_logged_in_user_guid()
));

foreach ($allAppEntities as $appEntity){
	$urls[] = get_entity_url($appEntity->guid);
	$names[] = $appEntity->appname;;
}

?>

<div class="control-group">
	<label class="control-label" for="inputTitle"><?php echo elgg_echo('title'); ?></label>
	<div class="controls">
		<?php echo elgg_view('input/text', array('name' => 'title', 'value' => $title, 'id' => 'inputTitle')); ?>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="#"><?php echo elgg_echo('groups:topicmessage'); ?></label>
	<div class="controls">
	<?php 
		echo elgg_view('input/div_textarea', array('class' => 'textarea-div', 'id' => 'editablediv', 'value' => $desc,));
    echo elgg_view('input/longtext', array(
        'name' => 'description',
        'id' => 'typing_area',
        'class' => 'discussion-topic-body',
        'style' => 'display: none'));
	?>
	</div>
</div>

<div class="control-group">
	<div class="controls">
	<?=elgg_view('page/elements/groups/discussion/associations', array('topic' => $guid));?>
	</div>
</div>

<div class="body-form-discussion"><div class="main"></div></div> <!-- for working associated apps -->

<div class="control-group">
  <label class="control-label" for="dropStatus"><?php echo elgg_echo("groups:topicstatus"); ?></label>
	<div class="controls">
	<?php
		echo elgg_view('input/dropdown', array(
			'name' => 'status',
			'value' => $status,
			'id' => 'dropStatus',
			'options_values' => array(
			'open' => elgg_echo('groups:topicopen'),
			'closed' => elgg_echo('groups:topicclosed'),
			),
		));
	?>
	</div>
</div>
	
<div class="control-group">
	<label class="control-label" for="dropAccess"><?php echo elgg_echo('access'); ?></label>
	<div class="controls">
	<?php echo elgg_view('input/access', array('name' => 'access_id', 'value' => $access_id, 'id' => 'dropAccess')); ?>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<button type="submit" class="btn"><?=elgg_echo('save');?></button>
	</div>
</div>

<div class="elgg-foot">
<?php
	echo elgg_view('input/hidden', array('name' => 'container_guid', 'value' => $container_guid));
	echo elgg_view('input/hidden', array('name' => 'writer_guid', 'value' => elgg_get_logged_in_user_guid()));
	if ($guid) {
		echo elgg_view('input/hidden', array('name' => 'topic_guid', 'value' => $guid));
	}
?>

</div>
