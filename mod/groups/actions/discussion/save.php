<?php

/**
 * Topic save action
 */
// Get variables
$title = htmlspecialchars(get_input('title', '', false), ENT_QUOTES, 'UTF-8');
$desc = get_input("description");
$status = get_input("status");
$access_id = (int) get_input("access_id");
$container_guid = (int) get_input('container_guid');
$guid = (int) get_input('topic_guid');

$tags = get_input("app-guids"); // apps

$execution_ids = get_input("execution-ids");
$execution_ref_apps = get_input("execution-guids");


//$writer_guid = get_input('writer_guid');
$writer_guid = elgg_get_logged_in_user_guid();
elgg_make_sticky_form('topic');

// validation of inputs
if (!$title || !$desc) {
  register_error(elgg_echo('discussion:error:missing'));
  forward(REFERER);
}

$container = get_entity($container_guid);
if (!$container || !$container->canWriteToContainer(0, 'object', 'groupforumtopic')) {
  register_error(elgg_echo('discussion:error:permissions'));
  forward(REFERER);
}

// check whether this is a new topic or an edit
$new_topic = true;
if ($guid > 0) {
  $new_topic = false;
}

if ($new_topic) {
  $topic = new ElggObject();
  $topic->subtype = 'groupforumtopic';
} else {
  // load original file object
  $topic = new ElggObject($guid);
  if (!$topic || !$topic->canEdit()) {
    register_error(elgg_echo('discussion:topic:notfound'));
    forward(REFERER);
  }
}

$topic->title = $title;
$topic->description = $desc;
$topic->status = $status;
$topic->access_id = $access_id;
$topic->container_guid = $container_guid;
$topic->writer_guid = $writer_guid;

$result = $topic->save();


foreach ($tags as $tag) { // associate app
  $topic->addRelationship($tag, 'question_associated_app');
}

$i = 0;
foreach ($execution_ids as $exec_id) { // associate execution
  $exec = new ElggObject();
  $exec->type = 'object';
  $exec->subtype = 'execution_of_app';
  if( !$execution_ref_apps[$i] ) {
    error_log('error in save new topic');
    forward(REFERER);
    return ;
  }
  $exec->appguid = $execution_ref_apps[$i];
  $exec->execid = $exec_id;
  $exec->access_id = ACCESS_PUBLIC;
  $execRelationGuid = $exec->save();
  if(!$execRelationGuid) {
    error_log('error in save new relation');
    forward(REFERER);
    return ;
  }
  add_entity_relationship($topic->getGUID(), 'question_associated_execution', $execRelationGuid);
  $i++;
}

if (!$result) {
  register_error(elgg_echo('discussion:error:notsaved'));
  forward(REFERER);
}

// topic saved so clear sticky form
elgg_clear_sticky_form('topic');


// handle results differently for new topics and topic edits
if ($new_topic) {
  system_message(elgg_echo('discussion:topic:created'));
  add_to_river('river/object/groupforumtopic/create', 'create', elgg_get_logged_in_user_guid(), $topic->guid);
  
  // nlp
  elgg_load_library('draw_app.nlp');
  manipulate_post($desc, $result);
} else {
  system_message(elgg_echo('discussion:topic:updated'));
}

// send a message to inform all the members of the group
$topicTitle = $topic->title;
$topic_link = elgg_view('output/url', array(
    href => elgg_get_site_url() . "discussion/view/{$topic->guid}",
    text => $topicTitle
));
$body .= " The {$topic_link} topic has been created.";

// get all the members of the group
$members = $container->getMembers();

foreach($members as $member) {
    //echo "<script> console.log('$member->name'); </script>";
    if ($member->guid == elgg_get_logged_in_user_guid()) { // do not send notification to yourself

    } else {
        messages_send("A new discussion topic has been created", $body, $member->guid, elgg_get_logged_in_user_guid());
    }
}

forward($topic->getURL());
