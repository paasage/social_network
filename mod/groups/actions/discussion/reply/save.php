<?php
/**
 * Post a reply to discussion topic
 *
 */

// Get input
$topic_guid = (int) get_input('entity_guid'); // the guid of discussion 
$text = get_input('group_topic_post'); // the text of anwser
/**
 * @todo Change this in order to support edit on comments
 */
//$annotation_id = (int) get_input('annotation_id');
$reply_guid = (int) get_input('reply_guid');

// reply cannot be empty
if (empty($text)) {
	register_error(elgg_echo('grouppost:nopost'));
	forward(REFERER);
}

$topic = get_entity($topic_guid);
if ($topic_guid) {
	$topic = get_entity($topic_guid);
	if(!elgg_instanceof($topic, 'object', 'groupforumtopic')){
		register_error(elgg_echo('grouppost:nopost'));
		forward(REFERER);
	}
	
	$group = $topic->getContainerEntity();
	if(!$group->canWriteToContainer()) {
		register_error(elgg_echo('groups:notmember'));
		forward(REFERER);
	}
}

$user = elgg_get_logged_in_user_entity();

if($reply_guid) {
	
} else { // new reply
	$r = new ElggDiscussionReply();
	$r->description = $text;
	$r->access_id = $topic->access_id;
	$r->container_guid = $topic->getGUID();
	$r->owner_guid = $user->getGUID();
	$r_guid = $r->save();
	if($r_guid == FALSE) {
		register_error(elgg_echo('groupspost:failure'));
		forward(REFERER);
	}
	/*
	 * @todo make river event
	elgg_create_river_item(array(
		'view' => 'river/object/discussion_reply/create',
		'action_type' => 'reply',
		'subject_guid' => $user->guid,
		'object_guid' => $reply->guid,
		'target_guid' => $topic->guid,
	));
	 ($view, $action_type, $subject_guid, $object_guid, $access_id = "",
$posted = 0, $annotation_id = 0)	 */
	add_to_river(
		'river/object/discussion_reply/create',
		'reply', 
		$user->guid, $r->guid
	);
	system_message(elgg_echo('groupspost:success'));

	/*
	// send a message to inform the creator of the topic
	$topicTitle = get_entity($topic_guid)->title;
	$topic_link = elgg_view('output/url', array(
		href => elgg_get_site_url() . "discussion/view/{$topic_guid}",
		text => $topicTitle
	));
	$body .= " I added a reply for the {$topic_link} question.";
	messages_send(
		"There is a reply for your question", $body, $topic->writer_guid, $user->getGUID()
	);
	*/


// send a message to inform all the members of the group
$topicTitle = get_entity($topic_guid)->title;
$topic_link = elgg_view('output/url', array(
	href => elgg_get_site_url() . "discussion/view/{$topic_guid}",
	text => $topicTitle
));
$body .= " I added a reply for the {$topic_link} question.";

$members = $group->getMembers(1000);

foreach($members as $member) {
    //echo "<script> console.log('$member->name'); </script>";
    if ($member->guid == elgg_get_logged_in_user_guid()) {

    } else {
        messages_send("A new reply has been added in the topic {$topicTitle} of the {$group->name} group", $body, $member->guid, $user->getGUID());
    }
    
}

}





forward(REFERER);

/*
// if editing a reply, make sure it's valid
// previous implementation
if ($annotation_id) {
	$annotation = elgg_get_annotation_from_id($annotation_id);
	if (!$annotation->canEdit()) {
		register_error(elgg_echo('groups:notowner'));
		forward(REFERER);
	}

	$annotation->value = $text;
	if (!$annotation->save()) {
		system_message(elgg_echo('groups:forumpost:error'));
		forward(REFERER);
	}
	system_message(elgg_echo('groups:forumpost:edited'));
} else {
	// add the reply to the forum topic
	$reply_id = $topic->annotate('group_topic_post', $text, $topic->access_id, $user->guid);
	if ($reply_id == false) {
		system_message(elgg_echo('groupspost:failure'));
		forward(REFERER);
	}

	add_to_river('river/annotation/group_topic_post/reply', 'reply', $user->guid, $topic->guid, "", 0, $reply_id);
	system_message(elgg_echo('groupspost:success'));
}*/

