<?php
/**
 * 
 * The action for searching for models according to the model's name.
 */

$name = get_input('model_name');
forward('draw_app/all/' . $name);