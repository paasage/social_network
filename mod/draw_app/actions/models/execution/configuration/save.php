<?php
/**
 * @uses 'execution_id'
 * @uses 'model_id'
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$execution_id = get_input('execution_id');
$model_id = get_input('model_id');
if(!isset($model_id) || !isset($execution_id)) {
	return ;
}

$exec = new ElggObject();
$exec->type = 'object';
$exec->subtype = 'execution_of_app';
$exec->appguid = $model_id;
$exec->execid = $execution_id;
$exec->access_id = ACCESS_PUBLIC;
$exec->owner_guid = elgg_get_logged_in_user_guid();
$execRelationGuid = $exec->save();
forward('myarea/configuration');

