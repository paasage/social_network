<?php

/**
 * action: models/upload
 * 
 * @uses appid The application guid.
 * @uses access_id int
 */
$app_guid = get_input('appid');
$app = get_entity($app_guid);
$app_name = $app->appname;

$access_id = (int) get_input("access_id");


if (!isset($app_guid)) {
	return FALSE;
}

elgg_load_library('elgg.draw_app_lib');
$container_guid = elgg_get_logged_in_user_guid();

$target_path = elgg_get_plugins_path() . "draw_app/lib/XMIsToBeUploaded/";

//$target_path = $target_path . $app_guid . ".xmi";

$target_path = $target_path . $app_name .".xmi";

system_message($_FILES['upload']['tmp_name']);

if (move_uploaded_file($_FILES['upload']['tmp_name'], $target_path)) {
	draw_app_upload_xmi($app_guid);
	system_message("xmi uploaded successfully", "success");
} else {
	system_message("Error in Uploading file", "error");
}