<?php

/**
 * action: model/watch
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$user = elgg_get_logged_in_user_guid();
$app = get_input('id', NULL);
if ($app == NULL) {
  return;
}
$entity = get_entity($app);
$data[watch] = TRUE;
if (add_entity_relationship($user, 'watching', $app) == TRUE) {
  if (isset($entity->views)) {
    $entity->views = $entity->views + 1;
  } else {
    $entity->views = 1;
  }
  
} else {
  remove_entity_relationship($user, 'watching', $app);
  $data[watch] = FALSE;
}
echo json_encode($data);