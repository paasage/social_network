<?php
/**
 * action: models/filters
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

// get the selected values
$deployed = get_input("Deployed", NULL);

$tags = get_input("tags", NULL);
$tags_query = http_build_query(array('tags' => $tags));

$clouds = get_input("clouds", NULL);
$clouds_query = http_build_query(array('clouds' => $clouds));

$geographies = get_input("geographies", NULL);
$geographies_query = http_build_query(array('geographies' => $geographies));

// forward to the new page using the selected values
forward("draw_app/filters?deployed={$deployed}&" . $tags_query . "&" . $clouds_query . "&" . $geographies_query);
