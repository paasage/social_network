<?php

/**
 * Adds a new app model with name, description and version
 * Action: editor/add and editor/add_with_components
 */
$name = get_input('name');
$description = get_input('description');
$v = get_input('version');
$edit_guid = get_input('guid', FALSE);

if (!isset($name) || !isset($description) || $name == '' || $description == '') {
	forward(REFERER);
	return ;
}

if($edit_guid) {
	$app = get_entity($edit_guid);
} else {
	$app = new ElggObject();
	$app->subtype = 'draw_application';
	$app->shares = (int) 0;
	$app->runs = (int) 0;
	$app->views = (int) 0;
	$app->running = FALSE;

	//echo "<script> console.log('manos manos manos'); </script>";
}

$app->json = $json;
$app->appname = $name;
$app->description = $description;
$app->version = $v;

if(!$edit_guid) {
	$app->owner_guid = elgg_get_logged_in_user_guid();
}
$app->access_id = ACCESS_PUBLIC;
$app_guid = $app->save();
	
if ($app_guid && !$edit_guid) {
	add_to_river('object/draw_application', 'create', elgg_get_logged_in_user_guid(), $app_guid);
	// code for message notification for the user
//	suggestGroupsRelatedTo($app_name, $user); // creates error
	add_to_river('river/draw_app/create', 'create', elgg_get_logged_in_user_guid(), $app_guid);
	
} 
forward(elgg_get_site_url() . 'draw_app/all');