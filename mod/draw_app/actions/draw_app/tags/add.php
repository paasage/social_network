<?php
/**
 * 
 * 
 */

$model_guid = get_input('model_id');
$tag = get_input('tag');

if(!isset($model_guid)) return FALSE;

$app_entity = get_entity($model_guid);
$app_entity->annotate('models_tags', $tag, ACCESS_PUBLIC, $model_guid, 'string');

$annotations = $app_entity->getAnnotations(
								'models_tags'
							);
foreach ($annotations as $annotation) {
	if($annotation->value == $tag){
		echo $annotation->id;
		return TRUE;
	}
}

return FALSE;