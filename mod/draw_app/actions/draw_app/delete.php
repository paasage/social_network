<?php
/**
 * The delete action of draw_application object
 * The guid mush be provided as parameter ?quid='number'
 * 
 * @author Christos Papoulas
 */

$guid = (int) get_input('guid');
if (!$guid) {
  return FALSE;
}

$entity = get_entity($guid);

if (!$entity->canEdit()) {
  register_error('You can not delete this entity');
  forward(REFERER);
}

if ($entity) {
  // delete entity
  if ($entity->delete()) {
    system_message(elgg_echo('Application deleted!'));
  } else {
    register_error(elgg_echo('Can not delete the entity'));
  }
} else {
  register_error(elgg_echo('The are is no such entity'));
}

forward(elgg_get_site_url() . "draw_app/all");