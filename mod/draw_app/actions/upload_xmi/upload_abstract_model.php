<?php 
/**
 * 
 * @uses $vars['model_guid'] The model guid
 *
 */
//elgg.system_message("upload_abstract_model.php run successfully!");


// use the name of the model in the social network as name of the paasage resource 
$model_guid = get_input('model_guid');
$model = get_entity($model_guid);
$name = $model->appname; 

elgg_load_library('elgg.draw_app_lib');
$container_guid = elgg_get_logged_in_user_guid();

$target_path = elgg_get_plugins_path() . "draw_app/lib/XMIsToBeUploaded/";
$target_path = $target_path . $name .".xmi";

$data = $_REQUEST['val'];
$fp = fopen($target_path,'w'); //Prepends timestamp to prevent overwriting
fwrite($fp, $data);
fclose($fp);

// use the cdo-client to communicate with cdo-server
$success = cdo_socket_client_upload_xmi($name);

$success = rtrim($success);

echo $success;


function cdo_socket_client_upload_xmi($model_name) {
  error_log("cdo_socket_client called with param: " + $model_name);
  $address = '127.0.0.1';
  //$address = '139.91.92.47';
  $port = 9009;
  $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket == false) {
    error_log("socket_create failed: " . socket_strerror(socket_last_error()) . "\n");
    return false;
  }
  $result = socket_connect($socket, $address, $port);
  if ($result === false) {
    error_log("socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n");
    return false;
  }

  $action = "upload";

  $in = "GET /$model_name /$action HTTP/1.1\r\n";
  $out = '';
  socket_write($socket, $in, strlen($in));
  socket_write($socket, NULL, strlen(EOF));
  while ($res = socket_read($socket, 2048)) {
    $out .= $res;
  }

  socket_close($socket);
  error_log("cdo_socket_client finished");
  
  return $out;
}

?>