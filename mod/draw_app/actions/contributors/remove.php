<?php
/**
 * Action: contributors/remove
 * removes a ElggUser as contributor to an application model.
 * 
 * @uses 'user' User GUID
 * @uses 'app' Application GUID
 */

$user_guid = get_input('user');
$app_guid = get_input('app');

if(!$user_guid || !$app_guid) {
	return FALSE;
}

return remove_entity_relationship($user_guid, 'is_contributor_to', $app_guid);