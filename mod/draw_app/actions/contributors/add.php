<?php

/**
 * Action: contributors/add
 * add a ElggUser as contributor to an application model.
 * 
 * @uses 'user' User GUID
 * @uses 'app' Application GUID
 */
$user_guid = get_input('user');
$app_guid = get_input('app');

if (!$user_guid || !$app_guid) {
	return FALSE;
}

$appname = get_entity($app_guid)->appname;
$app_link = elgg_view('output/url', array(
	href => elgg_get_site_url() . "draw_app/view/{$app_guid}",
	text => $appname
));
$body .= " I added you as a contributor to the {$app_link} application model.";
$res = add_entity_relationship($user_guid, 'is_contributor_to', $app_guid);

messages_send(
	"You were added as a contributor", $body, $user_guid, elgg_get_logged_in_user_guid()
);
return TRUE;
