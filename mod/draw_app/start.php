<?php

elgg_register_event_handler('init', 'system', 'draw_app_init');

function draw_app_init() {
    // libraries
    $base = elgg_get_plugins_path();
    draw_app_register_libraries($base);
    myarea_register_libraries($base);
    contributors_register_libraries($base);
    // actions
    draw_app_register_actions();
    //JS libraries
    draw_app_register_js();

    elgg_load_library('draw_app.all_apps');

    draw_app_register_page_handlers();

    draw_app_register_topbar_icon();

    $css_url = 'mod/draw_app/views/default/css/form.css';
    elgg_register_css('draw_app_form_css', $css_url);
}

function draw_app_register_page_handlers() {
    elgg_register_page_handler('draw_app', 'draw_app_page_handler');
    elgg_register_page_handler('myarea', 'myarea_page_handler');
    elgg_register_page_handler('contributors', 'contributors_page_hander');
}

function myarea_page_handler($page) {
    elgg_load_library('myarea.library');
    gatekeeper();
    switch ($page[0]) {
        default:
            echo myarea_default_page($page[0]);
            break;
    }
    return TRUE;
}

/**
 * pages:
 * * /contributors/{app_guid}
 * 
 * @param type $page The page requested
 * @return boolean TRUE if success.
 */
function contributors_page_hander($page) {
    gatekeeper();
    if (!isset($page)) {
        return false;
    }
    elgg_load_library('draw_app.contributors.library');
    switch ($page) {
        default:
            show_contributors($page[0]);
            break;
    }
    return TRUE;
}

/**
 * Pages:
 * * draw_app/download
 * * draw_app/save
 * * draw_app/all
 * * draw_app/api
 * * draw_app/search_group
 * * draw_app/view
 * * draw_app/myapps
 * * draw_app/edit
 * * draw_app/upload
 * * draw_app/editor
 * * draw_app/share
 * * draw_app/filters
 * 
 * @param string $page
 * @return boolean
 */
function draw_app_page_handler($page) {
    $base = elgg_get_plugins_path();
    switch ($page[0]) {
        case 'download':
            gatekeeper();
            set_input('appid', $page[1]);
            include $base . "draw_app/pages/draw_app/download_xmi.php";
            break;
        case 'all': // list all composed applications
            //gatekeeper();
            elgg_load_css('component_css'); //css for user_interface plugin
            elgg_load_css('draw_app.bootstrap.tags.css'); // css for tags
            elgg_load_js('draw_app.myscripts');
            elgg_load_library('draw_app.all_apps');
           
            show_all_apps($page[1]);
            break;
        case 'api':
            include $base . 'draw_app/pages/mddb/querymddb.php';
            break;
        case 'search_group':
            elgg_load_library('elgg.draw_app_lib');
            $tmp = suggestGroupsRelatedTo("MongoDB", NIL);
            break;
        case 'view':
            //gatekeeper();
            elgg_load_library('draw_app.all_apps');
            elgg_load_library('elgg.draw_app_lib');
            
            elgg_load_js('draw_app.myscripts');
            elgg_load_js('draw_app.filters');
            if ($page[2] == NULL) {
                $page[2] = 'runs';
            }
            
            echo get_application_view(
                    $page[1], $page[2], isset($page[3]) ? $page[3] : NULL, NULL
            );

            break;
        case 'myapps':
            include $base . 'draw_app/pages/draw_app/my_draw_app.php';
            break;
        case 'edit':
            elgg_load_library('elgg.draw_app_lib');
            echo edit_draw_app($page[1]);
            break;
        case 'editor':
            gatekeeper();
            elgg_load_library('draw_app.all_apps');
            elgg_load_js('draw_app.myscripts');
            // draw_app_load_editor_js();

            error_log('editor');
            draw_app_show_editor($page[1]);
            break;
        case 'share':
            gatekeeper();
            elgg_load_library('elgg.draw_app_lib');
            draw_app_share_appluication($page[1]);
            break;
        case 'filters':
            gatekeeper();

            elgg_load_js('draw_app.myscripts');
            $deployed = get_input('deployed', NULL);
            $tags = get_input('tags', NULL);
            $clouds = get_input('clouds', NULL);
            $geographies = get_input('geographies', NULL);

            echo show_filtered_apps($deployed, $tags, $clouds, $geographies);
            break;
        default:
            error_log('draw_app: The requested page doesn\'t exist');
            return false;
    }

    return true;
}

/**
 * Send Notification if a Application exists with the same name of the Group
 */
function send_notification_group_created() {
    elgg_load_library('elgg.draw_app_lib');
    send_notification_if_app_is_similar_to_group();
}

function draw_app_register_js() {
    $url1 = 'mod/draw_app/vendors/draw_app/autosuggest_to_topic.js';
    elgg_register_js('elgg.autosuggest.to.topic', $url1, 'footer');

    $url2 = 'mod/draw_app/iframe_draw_app/jquery-impromptu.js';
    elgg_register_js('elgg.jquery.impromptu', $url2, 'footer');

    $url3 = 'mod/draw_app/iframe_draw_app/modals.js';
    elgg_register_js('elgg.modals', $url3, 'footer');

    $my_scripts1 = 'mod/draw_app/js/models-library.js';
    elgg_register_js('draw_app.myscripts', $my_scripts1);

    $my_scripts2 = 'mod/draw_app/js/form.js';
    elgg_register_js('draw_app.form', $my_scripts2);
    
    $my_scripts3 = 'mod/draw_app/js/filters.js';
    elgg_register_js('draw_app.filters', $my_scripts3);

    // js to upload abstract xmi
    $upload_abstract_model = 'mod/draw_app/js/upload_abstract_model.js';
    elgg_register_js('draw_app.upload_abstract_model.js', $upload_abstract_model, 'footer');
    elgg_load_js('draw_app.upload_abstract_model.js');
}

function draw_app_register_actions() {
    $base = elgg_get_plugins_path();

    elgg_register_action("draw_app/tags/add", $base . "draw_app/actions/draw_app/tags/add.php");
    elgg_register_action("draw_app/tags/delete", $base . "draw_app/actions/draw_app/tags/delete.php");

    elgg_register_action("draw_app/delete", $base . "draw_app/actions/draw_app/delete.php");

    //elgg_register_action("models/filter-metrics", $base . "draw_app/actions/models/filter-metrics.php");
    elgg_register_action("models/search", $base . "draw_app/actions/models/search.php");
    elgg_register_action('models/filters', $base . "draw_app/actions/models/filters.php");

    elgg_register_action("contributors/add", $base . "draw_app/actions/contributors/add.php");
    elgg_register_action("contributors/remove", $base . "draw_app/actions/contributors/remove.php");

    elgg_register_action('models/upload', $base . 'draw_app/actions/models/upload_xmi.php');

    elgg_register_action('editor/add_with_components', $base . 'draw_app/actions/models/add.php');

    elgg_register_action('execution/configuration/save', $base . 'draw_app/actions/models/execution/configuration/save.php');

    elgg_register_action('model/watch', $base . 'draw_app/actions/models/watch.php');


    // action to upload abstract xmi
    elgg_register_action("upload_xmi/upload_abstract_model", $base . "draw_app/actions/upload_xmi/upload_abstract_model.php");
}

/**
 * register all libraries for application models.
 */
function draw_app_register_libraries($base) {
    $l1 = '/draw_app/lib/draw_app_lib.php';
    elgg_register_library('elgg.draw_app_lib', $base . $l1);
    $l2 = '/draw_app/lib/all_apps.php';
    elgg_register_library('draw_app.all_apps', $base . $l2);
    $l3 = '/draw_app/lib/cdo_parser.php';
    elgg_register_library('draw_app.cdo_parser', $base . $l3);
    
    $l5 = '/draw_app/lib/autonlp.php';
    elgg_register_library('draw_app.nlp', $base . $l5);
}

/**
 * registers all libraries for myarea
 */
function myarea_register_libraries($base) {
    $l1 = '/draw_app/lib/myarea.php';
    elgg_register_library('myarea.library', $base . $l1);
}

/**
 * registers all libraries for contributors
 */
function contributors_register_libraries($base) {
    elgg_register_library('draw_app.contributors.library', $base . '/draw_app/lib/contributors.php');
}

function draw_app_register_topbar_icon() {
    if (!elgg_is_logged_in()) {
        return;
    }

    $text = elgg_view('output/img', array(
        'src' => elgg_get_site_url() . '_graphics/myarea-topbar-icon.png',
        'title' => 'My Area'));

    $params = array(
        'name' => 'myarea',
        'text' => $text,
        'href' => elgg_get_site_url() . 'myarea/mymodels',
        'link_class' => 'cart-link-no-display'
    );
    elgg_register_menu_item('topbar', $params);
}
