<?php

/**
 * Display all applications that draw_app has.
 * library name: draw_app.all_apps
 * 
 * @todo Make the /views/default/object to display the draw_app object
 * 
 * @author Christos Papoulas
 */

/**
 * 
 * @param type $model_guid
 */
function draw_app_show_editor($model_guid) {

	/* $body = elgg_view_layout('one_sidebar', array(
	  'content' => elgg_view_form('editor/add', array('class' => 'form-horizontal'), array('model_guid' => $model_guid)),
	  'sidebar' => '',
	  )); */
	$body = elgg_view_layout('one_sidebar', array(
		'content' => elgg_view_form('editor/add_with_components', array('class' => 'form-horizontal'), array('model_guid' => $model_guid)),
		'sidebar' => '',
	));
	if ($model_guid) {
		$body .= '<script>$(document).ready(function () { '
			. 'loadModelFromJson(' . get_entity($model_guid)->json . ');'
			. '});</script>';
	}

	echo elgg_view_page('Editor', $body);
}

/**
 * Shows all and specific applications
 * 
 * @param string $app_name The application name if we want to show
 * specific applications.
 *  
 */
function show_all_apps($app_name = NULL) {
	$title = "Models";
	$content = '';

	if ($app_name) {

		// get all the apps and choose those who include the search value in array $matchApps
		$options = array(
            'type' => 'object',
            'subtype' => 'draw_application',
            'full_view' => false,
            'limit' => false
        );
        $allapps = elgg_get_entities($options);

        $matchApps = array();
        for ($i = 0; $i < count($allapps); $i++) {
            if (strlen(stristr($allapps[$i]->appname, $app_name))>0) {
                $matchApps[$i] = $allapps[$i];
            }
        }

        // prepare the array $options that is needed for the list-models view
        $options2 = array();
        $options2['full_view'] = false;
        if (!is_int($offset)) {
            $offset = (int) get_input('offset', 0);
        }
        $defaults = array(
            'items' => $matchApps,
            'list_class' => 'elgg-list-entity',
            'full_view' => true,
            'pagination' => true,
            'limit' => false,
            'list_type' => $list_type,
            'list_type_toggle' => false,
            'offset' => $offset,
        );
        $options2 = array_merge($defaults, $options2);

		$content .= elgg_view('page/components/list-models', $options2);
		
		if ($content == '') {
			$content .= '<h2>There is not such application.</h2>';
		}
	} else {
		$content .= elgg_list_entities(array(
			'type' => 'object',
			'subtype' => 'draw_application',
			'full_view' => false,
			'pagination' => true,
			'limit' => (int) 20
			), 'elgg_get_entities', 'draw_app_view_entity_list');
	}
	// add the content for the sidebar from custom_css plugin
	$sidebar = elgg_view('page/sidebars/sidebar-models');

	// layout the page
	$body = elgg_view_layout('one_sidebar', array(
		'content' => $content,
		'sidebar' => $sidebar,
		'recommended' => elgg_view('draw_app/components/models-recommended')
	));

	echo elgg_view_page($title, $body);
}

/**
 * 
 * 
 * @param Bool $deployed
 * @param array $tags The selected tags
 * 
 * @return String the page
 */
function show_filtered_apps($deployed = NULL, $tags = NULL, $clouds = NULL, $geographies = NULL) {

	$sidebar = elgg_view('page/sidebars/sidebar-models');
	$options = array();
	$apps = array();

	if ($deployed) {
		if ($deployed === "TRUE") {
			$apps = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18,
				'metadata_name_value_pairs' => array(
					'name' => 'runs',
					'value' => 0,
					'operand' => '>'
				)
			));
		} else if ($deployed === "FALSE") {
			$apps = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18,
				'metadata_name_value_pairs' => array(
					'name' => 'runs',
					'value' => 0,
					'operand' => '='
				)
			));
		}
	} else {
		$apps = elgg_get_entities_from_metadata($options = array(
			'type' => 'object',
			'subtype' => 'draw_application',
			'full_view' => false,
			'pagination' => true,
			'limit' => (int) 18
		));
	}

	if ($tags != NULL) {
		if (strnatcasecmp($tags[0], "All") == 0) {
			$apps_with_tags = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18
			));
		} else {
			$apps_with_tags = elgg_get_entities_from_annotations(array(
				'annotation_names' => array('models_tags'),
				'annotation_values' => $tags
			));
		}
	}

	if ($clouds != NULL) {
		if (strnatcasecmp($clouds, "All") == 0) {
			$apps_with_clouds = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18
			));
		} else {
			$apps_with_clouds = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18,
				'metadata_name_value_pairs' => array(
					'name' => 'cloud',
					'value' => $clouds,
					'operand' => '=',
					'case_sensitive' => false
				)
			));
		} 
	}

	if ($geographies != NULL) {
		if (strnatcasecmp($geographies, "All") == 0) {
			$apps_with_geographies = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18
			));
		} else {
			if (strnatcasecmp($geographies, "Europe") == 0) {
				$geographies = array("DE", "UK", "EU");
			}
			$apps_with_geographies = elgg_get_entities_from_metadata($options = array(
				'type' => 'object',
				'subtype' => 'draw_application',
				'full_view' => false,
				'pagination' => true,
				'limit' => (int) 18,
				'metadata_name_value_pairs' => array(
					'name' => 'geography',
					'value' => $geographies,
					'operand' => '=',
					'case_sensitive' => false
				)
			));
		}
	}


	// merge the results in one array called $shown_apps
	//$shown_apps = array_keep_same_values($apps, $apps_with_tags);
	$shown_apps = array_keep_same_values($apps, $apps_with_clouds);
	$shown_apps = array_keep_same_values($shown_apps, $apps_with_geographies);
	$shown_apps = array_keep_same_values($shown_apps, $apps_with_tags);
	//$shown_apps = array_intersect($apps, $apps_with_clouds);

	//$shown_apps = array_intersect($apps_with_tags, $apps_with_clouds);
	
	//$shown_apps = array_merge($shown_apps, $apps_with_geographies);

	$vars = draw_app_create_vars($shown_apps, $options);

	$content = elgg_view('page/components/list-models', $vars);
	$body = elgg_view_layout('one_sidebar', array(
		'content' => $content,
		'sidebar' => $sidebar,
		'recommended' => elgg_view('draw_app/components/models-recommended')
	));
	return elgg_view_page($title, $body);
}

function array_keep_same_values($ar1, $ar2) {
	$c1 = count($ar1);
	$c2 = count($ar2);
	if ($c1 > $c2) {
		$tar1 = $ar1;
		$tar2 = $ar2;
	} else {
		$tar1 = $ar2;
		$tar2 = $ar1;
	}
	$res = array();
	foreach ($tar1 as $v1) {
		foreach ($tar2 as $v2) {
			if ($v1 === $v2) {
				$res[] = $v1;
			}
		}
	}
	return $res;
}

function draw_app_create_vars($entities, $options) {
	if (!is_int($offset)) {
		$offset = (int) get_input('offset', 0);
	}

	if (is_array($options)) {
		// new function
		$defaults = array(
			'items' => $entities,
			'list_class' => 'elgg-list-entity',
			'full_view' => true,
			'pagination' => true,
			'limit' => (int) 20,
			'list_type' => $list_type,
			'list_type_toggle' => false,
			'offset' => $offset,
		);

		$options = array_merge($defaults, $options);
	}
	return $options;
}

function draw_app_view_entity_list($entities, $options) {
	$vars = draw_app_create_vars($entities, $options);

	return elgg_view('page/components/list-models', $vars);
}

function draw_app_view_entity_carousel_list($entities, $options) {
	$vars = draw_app_create_vars($entities, $options);
	return elgg_view('page/components/models/carousel-list', $vars);
}
