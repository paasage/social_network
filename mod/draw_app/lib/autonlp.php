<?php

/**
 * lib: draw_app.nlp
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
function send_nlp_request($description) {
    $getdata =  $description;
    /* Script URL */
    $url = 'socialnetwork.paasage.eu:3001/words';
    /* $_GET Parameters to Send */
    $params = array('sentense' => $getdata);
    /* Update URL to container Query String of Paramaters */
    $url .= '?' . http_build_query($params);
    /* cURL Resource */
    $ch = curl_init();
    /* Set URL */
    curl_setopt($ch, CURLOPT_URL, $url);
    /* Tell cURL to return the output */
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    /* Tell cURL NOT to return the headers */
    curl_setopt($ch, CURLOPT_HEADER, false);
    /* Execute cURL, Return Data */
    $data = curl_exec($ch);
    /* Check HTTP Code */
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    /* Close cURL Resource */
    curl_close($ch);

    return $data;
}

function create_reply($topic_guid, $text) {
    $topic = get_entity($topic_guid);
    if($topic == FALSE) {return ;}
    $r = new ElggDiscussionReply();
    $r->description = $text;
    $r->access_id = $topic->access_id;
    $r->container_guid = $topic->getGUID();
    $r->owner_guid = 2035; // Automaton
    $r_guid = $r->save();
    if ($r_guid == FALSE) {
        error_log("Error in automated reply to post " . $topic_guid);
    }
}

function manipulate_post($description, $topic_guid) {
    $json_s = send_nlp_request($description);
    $json = json_decode($json_s);
    error_log($json_s);
    // check if is classified as cost effectiveness query
    if (strcmp($json->{'classified'}, "cost_effectiveness") == 0) {
        create_reply($topic_guid, "The most cost effectiveness configuration of SPEC JEntreprise2010 is: " .
            elgg_view('output/url', array(
                'href' => elgg_get_site_url() . '/draw_app/view/1865/runs/jEnterprise18F', 
                'text' => 'jEnterprise18F'
                )
            ) . ". This is an aytomatically generated response based on historical data."
        );
    } else {
        error_log("topic " . $topic_guid . " not talking about cost effectiveness");
    }
}
