<?php

/**
 * library name: elgg.draw_app_lib
 * 1. The basic functions for save an app.
 * 
 * @author Christos Papoulas
 */

/**
 * UNUSED
 * this function called from JS to save the app in Elgg.
 * 
 * @todo Pass the html Dom as string. Array is not accepted.
 * @param dom_view The HTML DOM view of the app.
 * @return string the content of html 
 */
function save_app() {
    $html = get_input('key');
    $app_name = get_input('appnane');
    $user = elgg_get_logged_in_user_guid();

    $html_simplifier = $html[0];

    $app = new ElggObject();
    $app->subtype = 'draw_application';
    $app->appname = $app_name;
    $app->html = $html_simplifier;

    $app->owner_guid = elgg_get_logged_in_user_guid();
    $app->access_id = ACCESS_PUBLIC;
    $app_guid = $app->save(); // finally save the object

    if ($app_guid) {
        add_to_river('object/draw_application', 'create', elgg_get_logged_in_user_guid(), $app_guid);
        // code for message notification for the user
        suggestGroupsRelatedTo($app_name, $user);
        add_to_river('river/draw_app/create', 'create', elgg_get_logged_in_user_guid(), $app_guid);
        echo json_encode('Done');
    } else {
        error_log("The application description with id " . $app_guid . "could not be saved");
    }
}

/**
 * Searches the ElggGroup to find similar namas as the $key. And send notification
 * message to user that creates the application.
 * 
 * @param string $key The name of the group that we want to find.
 * @param int $user_guid The user GUID to send it a message if the function find 
 * something similar to the $key
 * @return boolean $res if success true else false.
 */
function suggestGroupsRelatedTo($key, $user_guid) {
    if ($user_guid < 0)
        return false;

    $similar_groups = searchGroup($key);

    if (count($similar_groups) > 0) {
        $group_list = "";
        foreach ($similar_groups as $v) {
            $group_list .= elgg_view('output/url', array(
              'href' => '/groups/profile/' . $v[0],
              'text' => $v[1],
              'is_trusted' => true
            ));
        }
        $notif_body = "There are similar groups like your application:"
          . $group_list;
        messages_send("Group Notification", $notif_body, $user_guid, $v[0]);
    }
    return true;
}

/**
 * Get a string $key as parameter and searches the group entities for similar 
 * groups names like $key. If finds any returns a array of (group_id, name)
 *  
 * @param String $key The name of a group
 * @return array $similar_groups 
 */
function searchGroup($key) {
    $allGroups = elgg_get_entities(array(
      'type' => 'group',
      'subtype' => 0,
      'limit' => 5
    ));
    $similar_groups = array();
    $gr_guid = -1;

    foreach ($allGroups as $group) {
        //$name = $group->attributes->name;
        foreach ($group as $keyt => $value) {
            if ($keyt == 'guid') {
                $gr_guid = $value;
            } elseif ($keyt == 'name') {
                if ($value == $key) {
                    if ($gr_guid != -1) {
                        $similar_groups[] = array(
                          $gr_guid,
                          $value
                        );
                    }
                    $gr_guid = -1;
                }
            }
        }
    }
    return $similar_groups;
}

/**
 * Send notification to user if exists a application with the same name as the 
 * group.
 * 
 * @todo To function properly
 * 
 * @param int $user_id The creator of the group
 * @param int $group_id The guid of the groop
 */
function send_notification_if_app_is_similar_to_group($user_id = null, $group_id = null) {
    
}

/**
 * View of a single application (with side bar).
 * 
 * @param int $guid The GUID of the inten
 * @param string $submenu runs | components | discussion | reviews | models
 * @param string $graph uptime | rt | throughput | cost
 */
function get_application_view($guid, $submenu = NULL, $graph = NULL, $graph_params = NULL, $providers = NULL) {
    $return = array();

    if (!$guid) {
        return FALSE;
    }
    $appEntity = get_entity($guid);
   
    if (!elgg_instanceof($appEntity, 'object', 'draw_application')) {
        register_error(elgg_echo('You try to view something that is not an application object'));
        $_SESSION['last_forward_from'] = current_page_url();
        forward('');
    }

    $return['content'] = draw_app_view_entity($appEntity, array('full_view' => true));
    
    $return['sidebar'] = elgg_view('page/sidebars/sidebar-model-specific', array('appGUID' => $guid));

    $res = elgg_view_page("Application", elgg_view_layout('application_full_view', array(
      'content' => $return['content'],
      'sidebar' => $return['sidebar'],
      'model_guid' => $guid,
      'page-submenu' => $submenu,
      'graph' => $graph,
      'euro_from' => $graph_params['euro_from'],
      'euro_to' => $graph_params['euro_to'],
      'rt_value' => $graph_params['rt_value'],
      'rt_metric' => $graph_params['rt_metric'],
      'providers' => $providers
    )));
    return $res;
}

function edit_draw_app($appguid) {
    forward('draw_app/editor/' . $appguid);
}

/**
 * Returns a string of a rendered entity.
 *
 * Entity views are either determined by setting the view property on the entity
 * or by having a view named after the entity $type/$subtype.  Entities that have
 * neither a view property nor a defined $type/$subtype view will fall back to
 * using the $type/default view.
 *
 * The entity view is called with the following in $vars:
 *  - ElggEntity 'entity' The entity being viewed
 *
 * Other common view $vars paramters:
 *  - bool 'full_view' Whether to show a full or condensed view.
 *
 * @tip This function can automatically appends annotations to entities if in full
 * view and a handler is registered for the entity:annotate.  See {@trac 964} and
 * {@link elgg_view_entity_annotations()}.
 *
 * @param ElggEntity $entity The entity to display
 * @param array      $vars   Array of variables to pass to the entity view.
 *                           In Elgg 1.7 and earlier it was the boolean $full_view
 * @param boolean    $bypass If false, will not pass to a custom template handler.
 *                           {@see set_template_handler()}
 * @param boolean    $debug  Complain if views are missing
 *
 * @return string HTML to display or false
 * @link http://docs.elgg.org/Views/Entity
 * @link http://docs.elgg.org/Entities
 * @todo The annotation hook might be better as a generic plugin hook to append content.
 */
function draw_app_view_entity(ElggEntity $entity, $vars = array(), $bypass = true, $debug = false) {

    // No point continuing if entity is null
    if (!$entity || !($entity instanceof ElggEntity)) {
        return false;
    }

    global $autofeed;
    $autofeed = true;

    $defaults = array(
      'full_view' => false,
    );

    if (is_array($vars)) {
        $vars = array_merge($defaults, $vars);
    } 
    
    $vars['entity'] = $entity;

    // if this entity has a view defined, use it
    $view = $entity->view;
    if (is_string($view)) {
        return elgg_view($view, $vars, $bypass, $debug);
    }

    $entity_type = $entity->getType();

    $subtype = $entity->getSubtype();
    if (empty($subtype)) {
        $subtype = 'default';
    }

    $contents = '';
    if (elgg_view_exists("$entity_type/$subtype")) {
        $contents = elgg_view("$entity_type/$subtype", $vars, $bypass, $debug);
    }
    if (empty($contents)) {
        $contents = elgg_view("$entity_type/default", $vars, $bypass, $debug);
    }

    // Marcus Povey 20090616 : Speculative and low impact approach for fixing #964
    if ($vars['full_view']) {
        $annotations = elgg_view_entity_annotations($entity, $vars['full_view']);

        if ($annotations) {
            $contents .= $annotations;
        }
    }
    return $contents;
}

/**
 * This function called when user shares an application model. 
 * 
 * @param int $model_guid The model GUID.
 * @return void
 */
function draw_app_share_appluication($model_guid) {
    $app = get_entity($model_guid);
    if (!isset($app->shares)) {
        $app->shares = 1;
    } else {
        $app->shares++;
    }
    $app->save();
    add_to_river(
      'river/draw_app/share', 'share', elgg_get_logged_in_user_guid(), $model_guid
    );
    forward(REFERER);
}

/**
 * This function called when user uploads an xmi file. 
 * 
 * @param int $model_guid The model GUID.
 * @return void
 */
function draw_app_upload_xmi($model_guid) {
    $app = get_entity($model_guid);
    add_to_river(
      'river/draw_app/uploadxmi', 'upload_xmi', elgg_get_logged_in_user_guid(), $model_guid
    );
    forward(REFERER);
}

function draw_app_get_app_scores($app) {
    $options_for_reviews = array(
      'metadata_names' => array('application_reference'),
      'metadata_values' => array($app->getGUID()),
      'limit' => 65000,
      'count' => TRUE
    );

    $count = elgg_get_entities_from_metadata($options_for_reviews);
    $options_for_reviews['count'] = FALSE;
    $reviews_context = elgg_get_entities_from_metadata($options_for_reviews);

    $rate_number = 0;
    if ($count) {
        foreach ($reviews_context as $r) {
            $rate_number += $r->rating;
        }
        round($rate_number /= $count, 2);
    }
    $score = elgg_view('object/elements/models/score', array(
      'rate_number' => $rate_number,
      'model_guid' => $app->getGUID()
    ));
    return $score;
}
