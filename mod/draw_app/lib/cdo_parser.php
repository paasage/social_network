<?php


function cdo_get_providers($key) {
  $providers = array();
  $str = '';
  foreach ($key['vms'] as $vms) {
    $providers[] = $vms['provider'];
  }
  $providers_u = array_unique($providers);
  foreach ($providers_u as $s) {
    $str .= $s . " ";
  }
  return $str;
}

function cdo_get_uptime($start_t, $end_t) {
  $s_t = strtotime($start_t);
  $e_t = strtotime($end_t);
  return ($e_t - $s_t);
}

/**
 * 
 * @param type $vms Must be an array but is string
 * @return string
 */
function cdo_get_the_vms($vms) {
  $str = '';
  /*  foreach ($vms as $vm) {
    $str = $str . $vm['type'] . " ";
    if($vm['heapsize']) {
    $str = $str . 'app server heap: ' . $vm['heapsize'] . "<br>";
    }
    if($vm['buffer']) {
    $str = $str . 'db buffer size: ' . $vm['buffer'] . "<br>";
    }
    }
   */
  return $str;
}

function cdo_get_the_geography($vms) {
  $geography = array();
  foreach ($vms as $vm) {
    $geography[] = $vm['geography'];
  }
  $geography_u = array_unique($geography);
  return implode(' ', $geography_u);
}

function cdo_get_the_providers_rates($res) {
  $providers = array();
  $amazon = 0;
  $azure = 0;
  $google = 0;
  foreach ($res as $k) {
    foreach ($k['vms'] as $vm) {
      switch ($vm['provider']) {
        case 'Amazon':
          $amazon++;
          break;
        case 'Azure':
          $azure++;
          break;
        case 'Google':
          $google++;
          break;
        default:
          break;
      }
    }
  }
  $total = $amazon + $azure + $google;
  $providers['amazon'] = ($amazon / $total) * 100;
  $providers['azure'] = ($azure / $total) * 100;
  $providers['google'] = ($google / $total) * 100;
  return $providers;
}

function cdo_socket_client($model_name) {
  error_log("cdo_socket_client called with param: " + $model_name);
  $address = '127.0.0.1';
  //$address = '139.91.92.47';
  $port = 9009;
  $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket == false) {
    error_log("socket_create failed: " . socket_strerror(socket_last_error()) . "\n");
    return false;
  }
  $result = socket_connect($socket, $address, $port);
  if ($result === false) {
    error_log("socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n");
    return false;
  }

  $in = "GET /$model_name HTTP/1.1\r\n";
  $out = '';
  socket_write($socket, $in, strlen($in));
  socket_write($socket, NULL, strlen(EOF));
  while ($res = socket_read($socket, 2048)) {
    $out .= $res;
  }

  socket_close($socket);
  error_log("cdo_socket_client finished");
  
  return $out;
}

function cdo_socket_client_port_9010($model_name) {
  error_log("cdo_socket_client called with param: " + $model_name);
  $address = '127.0.0.1';
  //$address = '139.91.92.47';
  $port = 9010;
  $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket == false) {
    error_log("socket_create failed: " . socket_strerror(socket_last_error()) . "\n");
    return false;
  }
  $result = socket_connect($socket, $address, $port);
  if ($result === false) {
    error_log("socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n");
    return false;
  }

  $action = "download";
  //$action = "";

  $in = "GET /$model_name /$action HTTP/1.1\r\n";
  $out = '';
  socket_write($socket, $in, strlen($in));
  socket_write($socket, NULL, strlen(EOF));
  while ($res = socket_read($socket, 2048)) {
    $out .= $res;
  }

  socket_close($socket);
  error_log("cdo_socket_client finished");
  
  return $out;
}

function json_get_providers($jkey) {
  $vms = $jkey->{'vms'};
  $providers = array();
  foreach ($vms as $vm) {
    $providers[] = $vm->{'provider'};
  }
  return get_string_with_unique_elements($providers);
}

function json_get_geography($jkey) {
  $vms = $jkey->{'vms'};
  $geography = array();
  foreach ($vms as $vm) {
    $geography[] = $vm->{'location'};
  }
  return get_string_with_unique_elements($geography);
}

function json_get_providers_rates($jarray) {
  $checkArray2 = array();

  // create an associative array called $checkArray2 where the key is the name of the provider 
  // and the value is the number of occurrences
  foreach ($jarray as $k) {
    $jk = json_decode($k);
    foreach ($jk->{'vms'} as $vm) {

     $checkArray2NotInside = 1;
      foreach($checkArray2 as $name => $name_value) {
        if ($name == $vm->{'provider'}) {
          $checkArray2[$name]++;
          $checkArray2NotInside = 0;
          //$message = "inside " . $name . $name_value;
          //echo "<script type='text/javascript'>console.log('$message');</script>";
        }
      } 

      if ($checkArray2NotInside == 1) {
        $checkArray2[$vm->{'provider'}] = 1;
      }

    }
  }

  // change the values in the $checkArray2 from absolute values to rates
  $total_occurrences = 0;
  foreach ($checkArray2 as $key => $value) {
    $total_occurrences = $total_occurrences + $value;
  }
  foreach ($checkArray2 as $key => $value) {
    if ($total_occurrences > 0) {
      $checkArray2[$key] = ($value / $total_occurrences) * 100;
    } else {
      $checkArray2[$key] = 0;
    }
  }

  // sort the array, high to low
  arsort($checkArray2);

  // create the array $checkArray with key 'other' and value $other_rates
  // that substitutes all the providers except the first two
  $other_rates = 0;
  $not_count = 0;
  foreach ($checkArray2 as $key => $value) {
    if ($not_count == 0 || $not_count == 1) {
      ;
    } else {
      $other_rates = $other_rates + $value;
    }
    $not_count++;
  }
  $checkArray = array();
  $count = 0;
  foreach ($checkArray2 as $key => $value) {
    $checkArray[$key] = $value;
    if ($count == 1) {
      break;
    }
    $count++;
  }
  $checkArray['other'] = $other_rates;
  
  // return the associative array called $checkArray2
  return $checkArray;
}

function json_get_providers_rates_filtersEdition($jarray) {
  $checkArray2 = array();

  // create an associative array called $checkArray2 where the key is the name of the provider 
  // and the value is the number of occurrences
  foreach ($jarray as $k) {
    $jk = json_decode($k);
    foreach ($jk->{'vms'} as $vm) {

     $checkArray2NotInside = 1;
      foreach($checkArray2 as $name => $name_value) {
        if ($name == $vm->{'provider'}) {
          $checkArray2[$name]++;
          $checkArray2NotInside = 0;
          //$message = "inside " . $name . $name_value;
          //echo "<script type='text/javascript'>console.log('$message');</script>";
        }
      } 

      if ($checkArray2NotInside == 1) {
        $checkArray2[$vm->{'provider'}] = 1;
      }

    }
  }

  // change the values in the $checkArray2 from absolute values to rates
  $total_occurrences = 0;
  foreach ($checkArray2 as $key => $value) {
    $total_occurrences = $total_occurrences + $value;
  }
  foreach ($checkArray2 as $key => $value) {
    if ($total_occurrences > 0) {
      $checkArray2[$key] = ($value / $total_occurrences) * 100;
    } else {
      $checkArray2[$key] = 0;
    }
  }

  // sort the array, high to low
  arsort($checkArray2);
  
  // return the associative array called $checkArray2
  return $checkArray2;
}

function json_get_location_filtersEdition($jarray) {
  $checkArray2 = array();

  // create an associative array called $checkArray2 where the key is the name of the provider 
  // and the value is the number of occurrences
  foreach ($jarray as $k) {
    $jk = json_decode($k);
    foreach ($jk->{'vms'} as $vm) {

     $checkArray2NotInside = 1;
      foreach($checkArray2 as $name => $name_value) {
        if ($name == $vm->{'location'}) {
          $checkArray2[$name]++;
          $checkArray2NotInside = 0;
          //$message = "inside " . $name . $name_value;
          //echo "<script type='text/javascript'>console.log('$message');</script>";
        }
      } 

      if ($checkArray2NotInside == 1) {
        $checkArray2[$vm->{'location'}] = 1;
      }

    }
  }
  
  // return the associative array called $checkArray2
  return $checkArray2;
}

function get_string_with_unique_elements($ar) {
  $str = '';
  $ar_u = array_unique($ar);
  foreach ($ar_u as $s) {
    $str .= $s . " ";
  }
  return $str;
}
