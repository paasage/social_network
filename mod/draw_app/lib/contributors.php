<?php
/**
 * library name: draw_app.contributors.library
 * 
 * 
 */


function show_contributors($app_guid) {
  $title = "Edit Contributors";
  $content = '';

  $content .= elgg_view('page/contributors/all', array('app_guid' => $app_guid));

  $sidebar = '';

  // layout the page
  $body = elgg_view_layout('one_sidebar', array(
      'content' => $content,
      'sidebar' => $sidebar,
  ));

  echo elgg_view_page($title, $body);
}


/**
 * Get all friends of users. Not those that are contributors
 * 
 * @param ElggUser $user ElggUser
 * @param integer  $app_guid The guid of the applications
 * @return type
 */
function contributions_get_all_friends($user, $app_guid) {
  $options_friends = array(
      'relationship' => 'friend',
      'relationship_guid' => $user->getGUID(),
      'inverse_relationship' => FALSE,
      'type' => 'user',
  );
  
  $options_contr = array(
      'relationship' => 'is_contributor_to',
      'relationship_guid' => $app_guid,
      'inverse_relationship' => TRUE,
      'type' => 'user',
  );
  
  $friends = elgg_get_entities_from_relationship($options_friends);
  $contributors = elgg_get_entities_from_relationship($options_contr);
  $friends_view = '';
  $contributors_view = '';
  $add_as_contributor = NULL;
  foreach ($friends as $f) {
    $add = TRUE;
		if(get_entity($app_guid)->getOwnerGUID() == $f->getGUID()){
			continue;
		}
			
    foreach ($contributors as $c) {
      if($c === $f){
        $add = FALSE;
      }
    }
    if($add) {
      $friends_view .= elgg_view('user/view-friend', array(
        'entity'   => $f, 
        'add_as_contributor' => TRUE,
        'app_guid' => $app_guid
      ));
    }
  }
  $res['friends'] = $friends_view;
	$res['contributors'] = "";
	foreach ($contributors as $c) {
		$contributors_view .= elgg_view('user/view-friend', array(
				'entity' => $c,
				'app_guid' => $app_guid,
				'remove_from_contributors' => TRUE
				)); 
	}
  $res['contributors'] = $contributors_view;
  return $res;
}

/**
 * 
 * 
 * @param int $app_guid
 * @param int $user_guid
 * @return boolean TRUE if is contributor
 */
function iscontributor($app_guid, $user_guid) {
	if(!isset($app_guid) || !isset($user_guid)) {
		return FALSE;
	}
	return check_entity_relationship($user_guid, 'is_contributor_to', $app_guid);
}