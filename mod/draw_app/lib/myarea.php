<?php
/**
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

/**
 * The page handler of myarea
 * 
 * @param String $page The page requested.
 */
function myarea_default_page($page) {
	$sidebar = elgg_view('page/sidebars/myarea/default');
	$content = elgg_view('myarea/topbar', array('link' => $page));
	switch ($page) {
		case 'mymodels':
			$content .= elgg_view('myarea/models');
			break;
		case 'activeruns':
			$content .= elgg_view('myarea/active_runs');
			break;
		case 'mycomponents':
			$content .= elgg_view('myarea/components');
			break;
		case 'credentials':
			$content .= elgg_view('myarea/credentials');
			break;
		case 'configuration':
			$content .= elgg_view('myarea/configuration');
			break;
		case 'watching':
			$content .= elgg_view('myarea/watching');
			break;
	}
	$params = array('content' => $content, 'sidebar' => $sidebar);
	$body = elgg_view_layout('one_sidebar', $params);
	return elgg_view_page('My Area', $body);
}

function myarea_view_entity_list($entities, $vars = array(), $offset = 0, $limit = 10, $full_view = true,
$list_type_toggle = true, $pagination = true) {
	if (!is_int($offset)) {
		$offset = (int)get_input('offset', 0);
	}

	if (is_array($vars)) {
		// new function
		$defaults = array(
			'items' => $entities,
			'list_class' => 'elgg-list-entity',
			'full_view' => true,
			'pagination' => true,
			'list_type' => $list_type,
			'list_type_toggle' => false,
			'offset' => $offset,
		);

		$vars = array_merge($defaults, $vars);

	} else {
		// old function parameters
		elgg_deprecated_notice("Please update your use of elgg_view_entity_list()", 1.8);

		$vars = array(
			'items' => $entities,
			'count' => (int) $vars, // the old count parameter
			'offset' => $offset,
			'limit' => (int) $limit,
			'full_view' => $full_view,
			'pagination' => $pagination,
			'list_type' => $list_type,
			'list_type_toggle' => $list_type_toggle,
			'list_class' => 'elgg-list-entity',
		);
	}

	return elgg_view('page/components/models/list-my-models', $vars);
}