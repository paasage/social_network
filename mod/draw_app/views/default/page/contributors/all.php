<?php
/**
 * 
 * Magage contributors of applications.
 * 
 * @uses $vars['app_guid'] The app_guid of the application.
 */
$app = get_entity($vars['app_guid']);

$user = elgg_get_logged_in_user_entity();

elgg_load_js('custom_css.view.friend.js');
elgg_load_js('elgg.javascript.config');


$res = contributions_get_all_friends($user, $vars['app_guid']);
$friends_view = $res['friends'];
$contributors_view = $res['contributors'];
?>



<div class="row-fluid color-body mymodels-view-apps">
  <h4>My Friends</h4>
  <div class="suggested-members-all" id="friends">
    <?php
    if ($friends_view) {
      echo $friends_view;
    } else {
      echo "<h5>Add some friends first.</h5>";
    }
    ?>
  </div>
</div>

<div class="row-fluid color-body mymodels-view-apps" >
  <h4>Contributors of <?php echo $app->appname; ?></h4>
	<div class="suggested-members-all" id="contibutors">
    <?php
    if ($contributors_view) {
      echo $contributors_view;
    } else {
      echo "<h5>You have no define any contributor.</h5>";
    }
    ?>
  </div>
</div>