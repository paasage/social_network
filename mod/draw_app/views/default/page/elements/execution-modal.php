<?php
/**
 * 
 * @uses $vars['id'] the row id.
 * @uses $vars['execution'] the execution id.
 * @uses $vars['providers'] The provider's name.
 * @uses $vars['vms']  The VMs as object.
 * @uses $vars['model_guid']  The model guid of this execution.
 */

//////////////////////////////////////////////
// Additional code for the review 4/12/15.  //
//////////////////////////////////////////////
$model_guid = $vars['model_guid'];
$model = get_entity($model_guid);
$name = $model->appname;


if($name == "Service Management Application") {  
    $modal_id = "modaltr" . $vars['id'];
    $list = '';
    $list .= '<li>Location: ' . $vars['vms'] .
                '<br>Provider: ' . $vars['providers'] . 
                '</li>';

} 
/////////////////////////////////////////////
/////////////////////////////////////////////
else {
    $modal_id = "modaltr" . $vars['id'];
    $vms = $vars['vms'];
    $list = '';
    foreach ($vms as $vm) {
        $list .= '<li>Type: ' . $vm->{'vm'} . 
                '<br>Location: ' . $vm->{'location'} .
                '<br>Provider: ' . $vm->{'provider'} . 
                '</li>';
    }
}
?>
<!-- Modal -->
<div id="<?= $modal_id; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">execution: <?= $vars['execution']; ?></h3>
    </div>
    <div class="modal-body">
        <p>
            Configuration:
        <ul>
            <?= $list; ?>
        </ul>
        </p>
        <p>
            <?php
            echo elgg_view_form('execution/configuration/save', array(), array(
                'execution_id' => $cdo_id,
                'model_id' => $vars['model_guid']
            ));
            ?>
            <button type="submit" class="btn btn-info">contact execution contributor</button>
        </p>
    </div>

</div>