<?php
/**
 * The components of a specific application
 * 
 * @author Christos Papoulas
 * 
 * @uses $vars['page-submenu'] The submenu option: reviews, models, component
 * default is discussion.
 * @uses $vars['model_guid'] The guid of the model.
 * @uses $vars['graph'] The metric for the graphs. uptime | rt | throughput | cost
 * @uses $vars['users_rates'] The number of users rates
 * @uses $vars['rating_mean'] The mean of user rates.
 */
error_log('specific app php');
?>
<div class="post item model" style=" display: inline; float: left;">
<?php
echo elgg_view_entity(get_entity(/*1521*/ 1767), array(
	'full_view' => FALSE,
	'specific_app' => TRUE));
?>
</div>
<div class="post item model" style=" display: inline; float: left;">
<?php
echo elgg_view_entity(get_entity(/*567*/ 792), array(
	'full_view' => FALSE,
	'specific_app' => TRUE));
?>
</div>