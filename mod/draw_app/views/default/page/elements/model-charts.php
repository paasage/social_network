<?php
/**
 * 
 * @uses $vars['data'] The json format of executions.
 * @uses $vars['graph'] Description
 * @uses $vars['form_options'] TRUE if has form options
 * @uses $vars['model_guid'] The model GUID.
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

//////////////////////////////////////////////////
// added code for the review 4/12/15            //
//////////////////////////////////////////////////

// get the name of the application
$model_guid = $vars['model_guid'];
$model = get_entity($model_guid);
$name = $model->appname;

if($name == "Service Management Application") {   
     
$txt_file    = file_get_contents('/var/www/elgg-min/mod/draw_app/views/default/page/elements/FullDeploymentBewan2ExecutionModels.xmi');
$rows        = explode("\n", $txt_file);
array_shift($rows);

$executionModels = 0;
$inExecutionModel = 0;
$executionModelsArray = array();
$executionModelsArrayIndex = 0;

$executionContexts = 0;
$inExecutionContext = 0;
$executionContextsArray = array();

$measurements = 0;
$inMeasurement = 0;

$countries = 0;
$inCountries = 0;

$provider = 0;
$inProvider = 0;

foreach($rows as $row => $data)
{
    // show the rows    
    //echo $data;
    //echo '<br />';
    
    if(strpos($data, "executionModels")) {
        $executionModels += 1;
        $inExecutionModel = 1;
    }
    if(strpos($data, "/>")) {
        $inExecutionModel = 0;
    }
    
    
    // retrive name, startTime, endTime and totalCost of an executionContext
    if(strpos($data, "executionContexts")) {
        $executionContexts += 1;
        $inExecutionContext = 1;
    }
    if(strpos($data, "/>")) {
        $inExecutionContext = 0;
    }
    if($inExecutionContext == 1 && strpos($data, "name")){
        $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $nameArray = array();
        $nameArray[0] = "executionContext";
        $nameArray[1] = substr($data, strpos($data, "\"")+1, $name_length);
        $executionContextsArray[0] = $nameArray;
        
    }
    if($inExecutionContext == 1 && strpos($data, "startTime")){
        $startTime_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $startTimeArray = array();
        $startTimeArray[0] = "startTime";
        $startTimeArray[1] = substr($data, strpos($data, "\"")+1, $startTime_length);
        $executionContextsArray[1] = $startTimeArray;
        
    }
    if($inExecutionContext == 1 && strpos($data, "endTime")){
        $endTime_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $endTimeArray = array();
        $endTimeArray[0] = "endTime";
        $endTimeArray[1] = substr($data, strpos($data, "\"")+1, $endTime_length);
        $executionContextsArray[2] = $endTimeArray;
        
    }
    if($inExecutionContext == 1 && strpos($data, "totalCost")){
        $totalCost_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $totalCostArray = array();
        $totalCostArray[0] = "totalCost";
        $totalCostArray[1] = substr($data, strpos($data, "\"")+1, $totalCost_length);
        $executionContextsArray[3] = $totalCostArray;
        
    }
    
    $executionContextsArray[4][0] = "status";
    $executionContextsArray[4][1] = "undefined";    
    
    // retrieve geography from the first loactionModel
    if(strpos($data, "<countries")) {
        $countries += 1;
        $inCountries = 1;
    }
    if(strpos($data, "/>")) {
        $inCountries = 0;
    }
    if($inCountries == 1 && strpos($data, "id") && $countries == 1){
        $countries_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $countriesArray = array();
        $countriesArray[0] = "geography";
        $countriesArray[1] = substr($data, strpos($data, "\"")+1, $countries_length);
        for ($x = 0; $x < count($executionModelsArray); $x++) {
            $executionModelsArray[$x][5] = $countriesArray;
        }
        
    }
    
    // retrieve cloudProvider from the last organisationModel
    if(strpos($data, "<provider")) {
        $provider += 1;
        $inProvider = 1;
    }
    if(strpos($data, ">")) {
        $inProvider = 0;
    }
    if($inProvider == 1 && strpos($data, "name")){
        $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $nameArray = array();
        $nameArray[0] = "cloudProvider";
        $nameArray[1] = substr($data, strpos($data, "\"")+1, $name_length);
        for ($x = 0; $x < count($executionModelsArray); $x++) {
            $executionModelsArray[$x][6] = $nameArray;
        }
        
    }
    
    // retrieve all the measurements of an executionContext
    if(strpos($data, "measurements")) {
        $measurements += 1;
        $index = 6 + $measurements;
        $inMeasurement = 1;
    }
    if(strpos($data, "/>")) {
        $inMeasurement = 0;
    }
    if($inMeasurement == 1 && strpos($data, "name")){
        $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $measurementsArray = array();
        $measurementsArray[0] = substr($data, strpos($data, "\"")+1, $name_length);
    }
    if($inMeasurement == 1 && strpos($data, "value")){
        $value_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $measurementsArray[1] = substr($data, strpos($data, "\"")+1, $value_length);
        $executionContextsArray[$index] = $measurementsArray;
        
    }
    
    if (strpos($data, "</executionModels>")) {
    $executionModelsArray[$executionModelsArrayIndex] = $executionContextsArray;
    $executionModelsArrayIndex++;
    $measurements = 0;
    }

}
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


$socket_res_json = $vars['data'];
$provider_rates = json_get_providers_rates($socket_res_json);
$run_url = elgg_get_site_url() . 'draw_app/view/' . $vars['model_guid'] . '/runs/';

$js_param_for_metric = 'cost';
if ($vars['graph'] == 'rt') {
  $js_param_for_metric = 'rt';
} else if ($vars['graph'] == 'throughput') {
  $js_param_for_metric = 'throughput';
} else if ($vars['graph'] == 'uptime') {
  $js_param_for_metric = 'uptime';
}
?>
<div class="models-run-runs">
  <?php
  $graph = $vars['graph'];
  if ($graph == "") {
    $graph = 'cost';
  }
  ?>
  <div id="chartContainer" class="chart-graph">
    <?php
    //////////////////////////////////////////////////
    // added code for the review 4/12/15            //
    // && $name != "Service Management Application" //
    //////////////////////////////////////////////////
    if ($socket_res_json[0] === "no such application" && $name != "Service Management Application") {
      echo "<div class='no-exechistory-message'>"
      . "No execution history found"
      . "</div>";
    }
    ?>
    <div style="bottom: -35px; position: absolute; width: 100%">
      <div class="chart-submenu" <?php
      if ($graph == 'uptime') {
        echo 'style="background-color: red;"';
      }
      ?>>
        <a href="<?php echo $run_url . 'uptime'; ?>">Uptime</a>
      </div>

      <?php
      if ($socket_res_json[0] === "no such application" && $name != "Service Management Application") {
        ;
      } 
      //////////////////////////////////////////////////
      // added code for the review 4/12/15            //
      // else if ...                                  //
      //////////////////////////////////////////////////
      elseif ($name == "Service Management Application") {
        
        $measurementsNames = array();
        $measurementsValues = array();
        for ($x = 7; $x < count($executionModelsArray[0]); $x++) {
          $measurementsNames[$x] = $executionModelsArray[0][$x][0];
          $measurementsValues[$x] = $executionModelsArray[0][$x][1];
        ?>
        
        <div class="chart-submenu" 
        <?php 
          if ($graph == $measurementsNames[$x]) {
              echo 'style="background-color: red;"';
          }
        ?>
        ><a href="<?php echo $run_url . $measurementsNames[$x]; ?>"><?= $measurementsNames[$x] ?></a></div>
        
        <?php
        }

      } 
      ///////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////
      else {
        foreach ($socket_res_json as $key) {
          $jkey = json_decode($key);
          $count_options = 0;
          foreach ($jkey->{'measurements'} as $meas => $value) {
            if (++$count_options > 6) {
              break;
            }
            ?>
            <div class="chart-submenu" <?php
            if ($graph == $meas) {
              echo 'style="background-color: red;"';
            }
            ?>>
              <a href="<?php echo $run_url . $meas; ?>"><?= $meas ?></a>
            </div>
            <?php
          }
          break;
        }
      }
      ?>

      <div class="chart-submenu" <?php
      if ($graph == 'cost') {
        echo 'style="background-color: red;"';
      }
      ?>>
        <a href="<?php echo $run_url . 'cost'; ?>">Cost</a>
      </div>

    </div> 

  </div>

  <div class="chartProviders">
    <canvas id="GeographyChart" width="268" height="240"></canvas>
    <?php 
    //////////////////////////////////////////////////
    // added code for the review 4/12/15            //
    // if...                                        //
    //////////////////////////////////////////////////
    if($name == "Service Management Application") {  
      echo '<span class="inside-cart-text" style="left: 20%"><div class="colored-other"> </div>' . $executionModelsArray[0][6][1] . '</span>';
    } else { 	
      $colors = array("#41c0c2", "#fff", "#2b3143", "#432b31");
      $colors_index = 0;
  
      foreach($provider_rates as $provider_name => $name_value) {
        echo '<span class="inside-cart-text" style="left:' . ($colors_index*30+10) . '%"><div class="colored-white" style="background-color: ' . $colors[$colors_index] .'"> </div>' . $provider_name . '</span>';
        //echo '{value:' . $name_value . ',color:"' . $colors[$colors_index] . '"},'; 
        $colors_index++;
      }
      //echo '<span class="inside-cart-text" style="left: 20%"><div class="colored-white"> </div>Amazon</span>';
      //echo '<span class="inside-cart-text" style="right: 20%"><div class="colored-other"> </div>Azure</span>';
    //////////////////////////////////////////////
    //////////////////////////////////////////////
    }?>
    <div style="height: 20px;"></div>
  </div>

</div>

<script  type="text/javascript">
  $(document).ready(function () {
    console.log('embedded js on runs');
    $("#app-runs-table").tablesorter(); // for sorting table

    // data is the input of the circle chart
    <?php
    $colors = array("#41c0c2", "#fff", "#2b3143", "#432b31");
    $colors_index = 0;
    echo 'var data = [';
    foreach($provider_rates as $provider_name => $name_value) {
        echo '{value:' . $name_value . ',color:"' . $colors[$colors_index] . '"},'; 
        $colors_index++;
    }
    echo '];';
    ?>
    console.log (data);

    options = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //The percentage of the chart that we cut out of the middle.
      percentageInnerCutout: 80,
      //Boolean - Whether we should animate the chart	
      animation: true,
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Function - Will fire on animation completion.
      onAnimationComplete: null,
      labelFontFamily: "Arial",
      labelFontStyle: "normal",
      labelFontSize: 24,
      labelFontColor: "#fff"
    }
    new Chart(document.getElementById("GeographyChart").getContext("2d")).Doughnut(data, options);


    var chartDataSource = new Array();

<?php
$i = 0;
$j = 0;
$jsdata = "";

//////////////////////////////////////////////////
// added code for the review 4/12/15            //
// if ...                                       //
//////////////////////////////////////////////////
if($name == "Service Management Application") {
  foreach ($executionModelsArray as $executionContext) {
    for ($x = 0; $x < count($executionContext); $x++) {
      if ($graph == 'cost') {
        $graph = 'totalCost';
      }
      //if ($graph == 'uptime') {
      //  $graph = 'Uptime';
      //}
      if ($graph == $executionContext[$x][0]) {
        $value = $executionContext[$x][1];
        $jsdata .= "chartDataSource[$j] = {execution: $j, value: $value};\n";
      }
    }
    $j++;
  }
} 
//////////////////////////////////////////////////
//////////////////////////////////////////////////
else {
foreach ($socket_res_json as $jkey) {
  $jkey = json_decode($jkey);
  if ($graph == 'cost') {
    $cost = $jkey->{'cost'} === 0.0 ? 0.03 : $jkey->{'cost'};
    $jsdata .= "chartDataSource[$i] = {execution: $i, value: $cost};\n";
  } else if ($graph == 'uptime') {
    $uptime = cdo_get_uptime($jkey->{'start_time'}, $jkey->{'end_time'});
    $jsdata .= "chartDataSource[$i] = {execution: $i, value: $uptime};\n";
  } else {
    $meas_value = $jkey->{'measurements'}->{$graph};
    if ($meas_value == NULL) {
      $meas_value = 0;
    }
    $jsdata .= "chartDataSource[$i] = {execution: $i, value: $meas_value };\n";
  }
  $i++;
}
}
echo $jsdata;
?>

    $(function () {
      $("#chartContainer").dxChart({});
    });

    $(function () {
      $("#chartContainer").dxChart({
        dataSource: chartDataSource,
        commonSeriesSettings: {
          argumentField: 'execution',
          type: 'bar'
        },
        legend: {
          visible: false,
        },
        tooltip: {
          enabled: true,
          percentPrecision: 2,
          customizeTooltip: function (value) {
            $('tr').css('background-color', '#fff');
            $('#tr' + value.argument).css('background-color', '#2d3043');
            console.log('value.argument' + value.argument);
            return {
              text: value.value
            };
          }
        },
        series: [
          {valueField: 'value'}
        ]
      });
    });
  });
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.17.1/jquery.tablesorter.js">
</script>