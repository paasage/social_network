<?php
/**
 * called by models-run-filter
 * 
 * @uses $vars['cdo_res'] The json from cdo.
 * @uses $vars['model_guid'] The GUID of the application.
 * @uses $vars['form_options'] TRUE if has form options
 */
$socket_res_json = $vars['cdo_res'];

//////////////////////////////////////////////
// Additional code for the review 4/12/15.  //
// It parses a local xmi file and retrieves //
// info from the execution models.          //
//////////////////////////////////////////////
$model_guid = $vars['model_guid'];
$model = get_entity($model_guid);
$name = $model->appname;


if($name == "Service Management Application") {   
     
$txt_file    = file_get_contents('/var/www/elgg-min/mod/draw_app/views/default/page/elements/FullDeploymentBewan2ExecutionModels.xmi');
$rows        = explode("\n", $txt_file);
array_shift($rows);

$executionModels = 0;
$inExecutionModel = 0;
$executionModelsArray = array();
$executionModelsArrayIndex = 0;

$executionContexts = 0;
$inExecutionContext = 0;
$executionContextsArray = array();

$measurements = 0;
$inMeasurement = 0;

$countries = 0;
$inCountries = 0;

$provider = 0;
$inProvider = 0;

foreach($rows as $row => $data)
{
    // show the rows    
    //echo $data;
    //echo '<br />';
    
    if(strpos($data, "executionModels")) {
        $executionModels += 1;
        $inExecutionModel = 1;
    }
    if(strpos($data, "/>")) {
        $inExecutionModel = 0;
    }
    
    
    // retrive name, startTime, endTime and totalCost of an executionContext
    if(strpos($data, "executionContexts")) {
        $executionContexts += 1;
        $inExecutionContext = 1;
    }
    if(strpos($data, "/>")) {
        $inExecutionContext = 0;
    }
    if($inExecutionContext == 1 && strpos($data, "name")){
        $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $nameArray = array();
        $nameArray[0] = "executionContext";
        $nameArray[1] = substr($data, strpos($data, "\"")+1, $name_length);
        $executionContextsArray[0] = $nameArray;
        
    }
    if($inExecutionContext == 1 && strpos($data, "startTime")){
        $startTime_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $startTimeArray = array();
        $startTimeArray[0] = "startTime";
        $startTimeArray[1] = substr($data, strpos($data, "\"")+1, $startTime_length);
        $executionContextsArray[1] = $startTimeArray;
        
    }
    if($inExecutionContext == 1 && strpos($data, "endTime")){
        $endTime_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $endTimeArray = array();
        $endTimeArray[0] = "endTime";
        $endTimeArray[1] = substr($data, strpos($data, "\"")+1, $endTime_length);
        $executionContextsArray[2] = $endTimeArray;
        
    }
    if($inExecutionContext == 1 && strpos($data, "totalCost")){
        $totalCost_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $totalCostArray = array();
        $totalCostArray[0] = "totalCost";
        $totalCostArray[1] = substr($data, strpos($data, "\"")+1, $totalCost_length);
        $executionContextsArray[3] = $totalCostArray;
        
    }
    
    $executionContextsArray[4][0] = "status";
    $executionContextsArray[4][1] = "undefined";    
    
    // retrieve geography from the first loactionModel
    if(strpos($data, "<countries")) {
        $countries += 1;
        $inCountries = 1;
    }
    if(strpos($data, "/>")) {
        $inCountries = 0;
    }
    if($inCountries == 1 && strpos($data, "id") && $countries == 1){
        $countries_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $countriesArray = array();
        $countriesArray[0] = "geography";
        $countriesArray[1] = substr($data, strpos($data, "\"")+1, $countries_length);
        for ($x = 0; $x < count($executionModelsArray); $x++) {
            $executionModelsArray[$x][5] = $countriesArray;
        }
        
    }
    
    // retrieve cloudProvider from the last organisationModel
    if(strpos($data, "<provider")) {
        $provider += 1;
        $inProvider = 1;
    }
    if(strpos($data, ">")) {
        $inProvider = 0;
    }
    if($inProvider == 1 && strpos($data, "name")){
        $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $nameArray = array();
        $nameArray[0] = "cloudProvider";
        $nameArray[1] = substr($data, strpos($data, "\"")+1, $name_length);
        for ($x = 0; $x < count($executionModelsArray); $x++) {
            $executionModelsArray[$x][6] = $nameArray;
        }
        
    }
    
    // retrieve all the measurements of an executionContext
    if(strpos($data, "measurements")) {
        $measurements += 1;
        $index = 6 + $measurements;
        $inMeasurement = 1;
    }
    if(strpos($data, "/>")) {
        $inMeasurement = 0;
    }
    if($inMeasurement == 1 && strpos($data, "name")){
        $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $measurementsArray = array();
        $measurementsArray[0] = substr($data, strpos($data, "\"")+1, $name_length);
    }
    if($inMeasurement == 1 && strpos($data, "value")){
        $value_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
        $measurementsArray[1] = substr($data, strpos($data, "\"")+1, $value_length);
        $executionContextsArray[$index] = $measurementsArray;
        
    }
    
    if (strpos($data, "</executionModels>")) {
    $executionModelsArray[$executionModelsArrayIndex] = $executionContextsArray;
    $executionModelsArrayIndex++;
    $measurements = 0;
    }

}
}
/////////////////////////////////////////////
/////////////////////////////////////////////

?>

<div class="row-fluid models-run-table" style="background-color: #fff;">
    <table id="app-runs-table">

        <thead>
            <tr>
              <!-- <th>#</th> -->
                <th title="Start Time">Start</th>
                <th title="End Time">End</th>
                <th title="Uptime">Uptime</th>
                <?php

                if($name == "Service Management Application") {
            
                    $measurementsNames = array();
                    for ($x = 7; $x < count($executionModelsArray[0]); $x++) {
                        $measurementsNames[$x] = $executionModelsArray[0][$x][0];
                    }

                    foreach ($measurementsNames as $value) {
                        echo "<th title='blabla'>" . $value . "</th>";
                    }

                } else {
                    foreach ($socket_res_json as $key) {
                        $jkey = json_decode($key);

                        // show the measurements in alphabetic order
                        $applicationMeasurements = array();
                        foreach ($jkey->{'measurements'} as $key => $value) {
                            $applicationMeasurements[$key] = $value;
                        }
                        ksort($applicationMeasurements);

                        // the first measurement must be Workload
                        echo "<th title='WorkloadRawMetric: \nHTTP requests per second'>" . elgg_get_excerpt("WorkloadRawMetric", 10) . "</th>";
                        
                        // the rest measurements
                        foreach ($applicationMeasurements as $key => $value) {
                            if ($key != "WorkloadRawMetric") {
                                echo "<th title='$key'>" . elgg_get_excerpt($key, 10) . "</th>";
                            }
                        }
                        break;
                    }
                }
                ?>

                <th title="Cost">Cost</th>
                <th title="Cloud Providers"><?= elgg_get_excerpt("Cloud Providers", 10); ?></th>
                <th title="Geography">Geography</th>
                <!-- <th title="Cost Effectiveness"><?= elgg_get_excerpt("Cost Effectiveness", 10); ?></th> -->
                <th title="Status">Status</th>
                <!-- <th style="display: none;"></th> -->
            </tr>
        </thead>

        <tbody>
            <?php

            if($name == "Service Management Application") {

                $view = "";
                $j = 0;
                foreach ($executionModelsArray as $executionContext) {
                    $executionContextName = $executionContext[0][1];
                    $startTime = $executionContext[1][1];
                    $endTime = $executionContext[2][1];
                    
                    $cost = $executionContext[3][1];
                    if ($endTime == "") {
                        $status = "running";
                        $uptime = strtotime($startTime) % 10500;
                    } else {
                        $status = "completed";
                        $uptime = strtotime($endTime) - strtotime($startTime);
                    }
                    $uptime = $uptime . " sec";
                    $costEffectiveness = $uptime / $cost;
                    $costEffectiveness = number_format((float)$costEffectiveness, 2, '.', '');
                    $geography = $executionContext[5][1];
                    $provider = $executionContext[6][1];

                    $view .= '<tr id="tr' . $j . '" class="blabla">'
                        . '<td>' . $startTime . '</td>'
                        . '<td>' . $endTime . '</td>'
                        . '<td>' . $uptime . '</td>';

                    $measurementsValues = array();
                    for ($x = 7; $x < count($executionContext); $x++) {
                        $measurementsValues[$x] = $executionContext[$x][1];
                        $view .= "<td>$measurementsValues[$x]</td>";
                    }

                    

                    $view .='<td>' . $cost . '</td>'
                        . '<td>' . $provider . '</td>'
                        . '<td>' . $geography . '</td>'
                        . '<td>' . $costEffectiveness . '</td>'
                        . '<td>' . $status . '</td>'
                        . '</tr>';


                    $view .= elgg_view('page/elements/execution-modal', 
                        array(
                            id => $j, 
                            execution => $executionContextName,
                            providers => $provider, 
                            vms => $geography,
                            model_guid => $vars['model_guid']
                    ));
                    $j++;

                }

                $app = get_entity($vars['model_guid']);
                $app->runs = $j;
                $app->save();
                echo $view;

            } else {

            $view = "";
            $i = 0;
            $avgCostEffect = 0;
            $uptime = array();
            $provider_rates = json_get_providers_rates($socket_res_json);
            foreach ($socket_res_json as $key) {

                $jkey = json_decode($key);
                $providers = json_get_providers($jkey);
                $geography = json_get_geography($jkey);
                $uptime[] = cdo_get_uptime($jkey->{'start_time'}, $jkey->{'end_time'});
                $cost = $jkey->{'cost'} === 0.0 ? 0.03 : $jkey->{'cost'};
                
                $view .= '<tr id="tr' . $i . '" class="' . $jkey->{'id'} . '">'
                //. '<td>' . $i . '</td>'
                . '<td>' . $jkey->{'start_time'} . '</td>'
                . '<td>' . $jkey->{'end_time'} . '</td>'
                . '<td>' . $uptime[$i] . ' sec</td>';

                // show the measurements in alphabetic order
                $applicationMeasurements2 = array();
                foreach ($jkey->{'measurements'} as $key => $value) {
                    $applicationMeasurements2[$key] = $value;
                }
                ksort($applicationMeasurements2);

                // the first measurement to be shown is WorkloadRawMetric
                foreach ($applicationMeasurements2 as $key => $value) {
                    if ($key == "WorkloadRawMetric") {
                        $workloadValue = $value;
                    }
                }
                $formattedValue = number_format($workloadValue, 2);
                $view .= "<td>$formattedValue</td>";

                // the rest measurements
                foreach ($applicationMeasurements2 as $key => $value) {
                    // show the names of WorkloadMix and not the values
                    if (strpos($key, "WorkloadMix") !== false) {
                        // for JMeter
                        if (strpos($key, "read") !== false) {
                            $writePosition = strpos($key, "write");
                            $workloadMixPosition = strpos($key, "WorkloadMix");
                            $wlmLength = ($writePosition+6) - ($workloadMixPosition+12);

                            $wlm = substr($key, strpos($key, "WorkloadMix")+11, $wlmLength);
                        } else { // for YSCB
                            $wlm = substr($key, strpos($key, "WorkloadMix"), 12);
                        }
                        // we show the name of this measurement
                        $view .= "<td>$wlm</td>";
                    } elseif ($key != "WorkloadRawMetric") {
                        $formattedValue = number_format($value, 2);
                        $view .= "<td>$formattedValue</td>";
                    }
                }


                if ($vars['model_guid'] == 1837) { // Scalarm
                    $cost_effectiveness = round($jkey->{'measurements'}->{'Meas'} / $cost, 2);
                } else {
                    $cost_effectiveness = round($jkey->{'measurements'}->{AverageThroughputCompositeMetric} / $cost, 2);
                }

                if ($avgCostEffect == 0) {
                    $avgCostEffectg = $cost_effectiveness;
                } else {
                    $avgCostEffectg = ($avgCostEffectg + $cost_effectiveness) / 2;
                }
                
                $view .= '<td>' . $cost . '</td>'
                . '<td>' . $providers . '</td>'
                . '<td>' . $geography . '</td>'
                //. '<td>' . $cost_effectiveness . '</td>'
                . '<td>' . (isset($jkey->{'end_time'}) ? "completed" : "undef") . '</td>'
                //. '<td class="expand-execution-row"><span onclick="execution_row_toggle($(this))">&gt;</span></td>'
                . '</tr>';

                /*$view .= elgg_view('object/elements/models/expand-execution-row', array(
                  'model_guid' => $vars['model_guid'],
                  'providers' => $providers,
                  'vms' => '', //$key['vms'],
                  'id' => $i,
                  'id_cdo' => $jkey->{'id'}
                ));*/

		        $view .= elgg_view('page/elements/execution-modal', 
                    array(
                        id => $i, execution => $jkey->{'id'},
                        providers => $providers, vms => $jkey->{'vms'},
                        model_guid => $vars['model_guid']
                ));
                $i++;
            }

            $app = get_entity($vars['model_guid']);
            $app->avgCostEffectiveness = $avgCostEffectg;
            $app->save();
            echo $view;
           }
            ?> 
        </tbody>
    </table>
</div>
