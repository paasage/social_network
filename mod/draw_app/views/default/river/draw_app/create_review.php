<?php
/**
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$performed_by = get_entity($vars['item']->subject_guid);
$object = get_entity($vars['item']->object_guid);

$url = "<a href=\"{$performed_by->getURL()}\">{$performed_by->name}</a>";
$site = elgg_get_site_url();
$app = "<a href=\"{$site}draw_app/view/{$object->getGUID()}/reviews\">" . $object->appname . "</a>";
$string = $url . " added a review to " . $app;

echo elgg_view('river/elements/layout', array(
	'item' => $vars['item'],
	'message' => $string,
	//'responses' => $responses,
));
