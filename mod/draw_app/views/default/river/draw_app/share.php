<?php
/**
 * The river view of the draw_application shared object.
 * 
 * @author Christos Papoulas
 */

$performed_by = get_entity($vars['item']->subject_guid);
$object = get_entity($vars['item']->object_guid);

$url = "<a href=\"{$performed_by->getURL()}\">{$performed_by->name}</a>";

$string = "<a href=\"" . $object->getURL() . "\">" . $object->appname . "</a>";
$string .= "<br/>Application shared by " . $url;

echo $string;
