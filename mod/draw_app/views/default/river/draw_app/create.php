<?php
/**
 * The river view of the draw_application object.
 * 
 * @author Christos Papoulas
 */
$performed_by = get_entity($vars['item']->subject_guid);
$object = get_entity($vars['item']->object_guid);
$site = elgg_get_site_url();
$url = "<a href=\"{$performed_by->getURL()}\">{$performed_by->name}</a>";

$string = "<a href='$site" . "draw_app/view/" . $object->getGUID() . "'>" . $object->appname . "</a>";
$string .= "<br/>Application created by " . $url;

echo elgg_view_entity_icon($performed_by, 'small') . $string;
