<?php

$performed_by = get_entity($vars['item']->subject_guid);
$object = get_entity($vars['item']->object_guid);
$site = elgg_get_site_url();
$img = elgg_view('output/img', array(
	src => $site . "_graphics/models/xml.png"
	));
$user = "<a href=\"{$performed_by->getURL()}\">{$performed_by->name}</a>";
$app_url = $site . "draw_app/view/" . $vars['item']->object_guid;
$app = "<a href='{$app_url}'>{$object->appname}<a>";
$string = "<br/>An xmi file uploaded to {$app} by {$user}";
?>

<div class="river-upload-xmi">
	<?= $img; ?>
	<div>
		<?= $string; ?>
	</div>
</div>
