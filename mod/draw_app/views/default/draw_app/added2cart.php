<?php
/**
 * 
 * @uses $vars['model_guid'] The GUID of the model.
 * @uses $vars['model_name'] The name of the model that added to cart.
 */

$topbar_icon = elgg_get_site_url() . '_graphics/models-features-cart-topbar.png';
?>
<div class="add2cart" data-uid="<?php echo $vars['model_guid'];?>">
	<p> The Model <?php echo $vars['model_name']; ?> was added to your list </p>
	<img src="<?php echo $topbar_icon;?>"> 
</div>