<?php
/*
 * CSS view of input tags
 * @author Christos Papoulas
 */

$tmp ="test if program goes here";
?>

.window
{
  background-color: white;
  border: 3px solid #346789;
  color: black;
  font-family: helvetica;
  font-size: 0.8em;
  height: 12em;
  opacity: 0.8;
  padding: 0.5em;
  position: absolute;
  width: 14em;
  z-index: 20;
  text-align: center;
}

.dragActive
{
  border: 10px solid red;
}

.suggestfromMDDB 
{
	border:2px solid #a1a1a1;
	padding:10px 10px; 
	background:#dddddd;
	border-radius:20px;
}

.lexical
{
	border:2px solid #a1a1a1;
	padding:10px 5px; 
	background:#FAFAD2;
	border-radius:5px;
	display:inline-block;
}