<?php
/**
 * The casouler of models
 * 
 * @author Christos Papoulas
 */
$user_entity = elgg_get_logged_in_user_entity();
if ($user_entity) {
    $annotations = $user_entity->getAnnotations(
      'user_interests', 5
    );
}
$interests = '';

$items = 4;
$content = elgg_list_entities(array(
  'type' => 'object',
  'subtype' => 'draw_application',
  'limit' => (int) $items,
  'full_view' => false,
  'pagination' => false
  ), 'elgg_get_entities', 'draw_app_view_entity_carousel_list');
?>
<h3>Models</h3>
<div class="row-fluid recommended-models" id="recommended-models-collapsed"style ="padding-left: 1cm; padding: initial;">
    <div class="recommended-expand">Expand<img src="<?php echo elgg_get_site_url(); ?>_graphics/models/models-expand.png"></div>
    <h4>Recommended for you</h4>
</div>

<div class="row-fluid recommended-models-expanded" id="recommended-models-expanded">
    <div class="recom-model-expand-wrapper"> 

        <div class="recom-model-expand-submenu">
            <h4>Recommended for you</h4>
            <div>
                <p>Areas of Interest</p>
                <p><input type="checkbox"/> All</p>
                <?php
                if ($annotations) {
                    foreach ($annotations as $interest) {
                        echo "<p><input type=\"checkbox\"/>$interest->value</p>";
                    }
                }
                ?>
                <p><a href="#">View All</a></p>
            </div>
        </div>

        <div class="recom-model-exp-item-wrapper">
            <?php echo $content; ?>
            <span class="controls arrow-left">&lt;</span>
            <span class="controls arrow-right">&gt;</span>
        </div>

        <div class="recommended-collapse">Collapse<img src="<?php echo elgg_get_site_url(); ?>_graphics/models/models-collapse.png"></div>

    </div>
</div>
