<?php
/**
 * 
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$app_exec = elgg_extract('entity', $vars, FALSE);
if(!$app_exec) {
	return ;
}
$url = elgg_get_site_url() . "draw_app/view/" . $app_exec->appguid . "/runs/" . $app_exec->execid;
$aref = elgg_view('output/url', array(
	'text' => $app_exec->execid,
	'href' => $url
));
$ex_icon = elgg_view('output/img', array(
	'src' => '_graphics/models/execution.png',
));
?>
<div>
	<?= $ex_icon; ?>
	<?= $aref; ?>
</div>