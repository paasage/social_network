<?php

/**
 * The object view of draw_application object.
 * 
 * 
 * @author Christos Papoulas
 */
$full_view = elgg_extract('full_view', $vars, FALSE);
$app = elgg_extract('entity', $vars, FALSE);
$specific_userGUID = elgg_extract('myapps', $vars, FALSE);
$view_source = $vars['view_source'];
if (!$app) {
    return FALSE;
}

if ($view_source === "watching") {
    echo elgg_view('object/models-small', array(entity => $app, most_run => NULL));
    return TRUE;
}

if ($specific_userGUID != false) {
    $ownerOfAppGUID = $app->getOwnerGUID();
} else {
    $ownerOfAppGUID = false;
}
$app_guid = $app->getGUID();

$ownerOfApp = get_user($app->getOwnerGUID());

$appName = $app->appname;

$owner_icon = elgg_view_entity_icon($ownerOfApp, 'tiny');
$owner_link = elgg_view('output/url', array(
  'href' => "my_profile/{$ownerOfApp->getGUID()}",
  'text' => $ownerOfApp->name,
  'is_trusted' => true,
  ));
$author_text = elgg_echo('byline', array($owner_link));
$date = elgg_view_friendly_time($app->time_created);
$metadata = elgg_view_menu('entity', array(// only for full view
  'entity' => $vars['entity'],
  'handler' => 'draw_app', 
  'sort_by' => 'priority',
  'class' => 'elgg-menu-hz',
  ));
$subtitle = "$author_text Created: $date";
$refToApp = elgg_view('output/url', array(
  'href' => 'draw_app/view/' . $vars['entity']->getGUID(),
  'text' => $appName,
  'is_trusted' => TRUE
  ));


if (!$full_view) {

    elgg_load_library("elgg.draw_app_lib");
    $score = draw_app_get_app_scores($app);

    $body = '';
    $params = array(
      'entity' => $app,
      'metadata' => $score,
      'title' => $refToApp,
      'subtitle' => $subtitle,
    );

    $params = $params + $vars;
    $summary = elgg_view('object/elements/summary-models', $params);
    $body .= elgg_view('object/elements/models/models-information', array(
      'entity_guid' => $app->getGUID(),
      'summary' => $summary
      )
    );
    // add the div for add to cart
    $body .= elgg_view(
      'draw_app/added2cart', array(
      'model_name' => $appName,
      'model_guid' => $app->getGUID()
    ));
    if ($specific_userGUID != false) { //show only the apps of a specific user.
        if ($specific_userGUID == $ownerOfApp->getGUID()) {
            echo elgg_view('object/elements/full', array(
              'summary' => $summary,
              'icon' => $owner_icon,
              'body' => $body,
            ));
        }
    } else {
        echo elgg_view('object/elements/full', array(
          'summary' => $summary,
          'icon' => '<img src="' . elgg_get_site_url() . '_graphics/models/models-general.png">',
          //'icon' => $owner_icon,
          'body' => "<div class='description'>" . elgg_get_excerpt($app->description, 150) . "</div>" . $body,
        ));
    }
} else { //full view 

    $test = "<script> console.log('draw_application full view'); </script>";
    echo $test;

    $img_link = elgg_view('output/img', array("src" => "_graphics/models/models-general.png"));

    $params = array(
      'entity' => $app,
      'title' => $refToApp,
      'metadata' => $metadata,
      'subtitle' => $subtitle,
    );

    $params = $params + $vars;
    $summary = elgg_view('object/elements/summary-models', $params);

    echo elgg_view('object/elements/full', array(
      'summary' => $summary,
      'icon' => $img_link,
      'body' => $body
    ));
} //end full view
