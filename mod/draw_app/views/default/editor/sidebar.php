<?php
/**
 * 
 * @uses $vars['appGUID'] The GUID of the application
 */

$app = get_entity($vars['appGUID']);
$name = '';
$description = '';
$version = '';
$clouds  = '';
$tags = array();

if($app) {
	$name = $app->appname;
	$description = $app->description;
	$version = $app->version;
	$clouds  = $app->clouds;
	$tags = array();	
}
?>

<div class="models-sidebar" style="margin-top: 8px; margin-left: 10px;">
	<h3 class="models-filter-header">Details</h3>
	<hr>
	<div class="sidebar-section">
		Name: <input type="text" id='app_title_input' value='<?php echo $name;?>'>
		<br/>
		<textarea rows="5" cols='50' id='description_appl' placeholder="Add a description" style="width: 260px;"><?php echo $description;?></textarea>
		<br/>
		Version: <input type="text" id='version' value="<?php echo $version;?>">
	</div> 

	<div class="sidebar-section deployed-query">
		<h3>Clouds</h3>
		<p>	
			<input type="checkbox" value="All"> All <br/>
			<input type="checkbox" value="Amazon"> Amazon <br/>
			<input type="checkbox" value="Flexiant"> Flexiant <br/>
		</p>
		<hr>
	</div>

	<div class="sidebar-section deployed-query">
		<h3>Geography</h3>
		<p>
			<input type="checkbox" value="All"> All <br/>
			<input type="checkbox" value="NorthAmerica"> North America <br/>
			<input type="checkbox" value="SouthAmerica"> South America <br/>
			<input type="checkbox" value="Europe"> Europe <br/>
			<input type="checkbox" value="Asia"> Asia <br/>
			<input type="checkbox" value="Africa"> Africa <br/>
			<input type="checkbox" value="Australia"> Australia <br/>				
		</p>
		<hr>
	</div>

	<div class="sidebar-section deployed-framework">
		<h3>Modelling Frameworks</h3>
		<p>
			<input type="checkbox" value="All"> All <br/>
			<input type="checkbox" value="Chef"> Chef <br/>
			<input type="checkbox" value="CloudML" checked> CloudML <br/>				
		</p>
		<hr>
	</div>

	<div class="sidebar-section">
		<h3>Tags</h3>
		<?php echo elgg_view('page/elements/models/model-tags', $vars);?>
	</div>
</div>