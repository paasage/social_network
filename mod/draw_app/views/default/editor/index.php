<?php


?>

<!-- LOAD DEPLOYMENT MODEL Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">Select your deployment model</h4>
		</div>
		<div class="modal-body">
		<div>
			<h3>From a file</h3><br/>
			<div class="span4">
				<input type="file" class="input-file" style="width:360px">
			</div>
			<div class="span4 offset4">
				<div class=" progress progress-striped active" hidden>
					<div id="progressBar" class="bar" style="width: 0%;"></div>
				</div>
			</div>
			<div>
				<a class="btn" data-dismiss="modal" onclick="loadFile('myModal');">Load</a>
			</div><br/>
			<h3>Connect to a CloudML Server</h3><br/>
			<div class="span4">
				<div class="col-lg-6">
					<div class="input-group">
						<input type="text" id="url" class="form-control" placeholder="URL">
						<span class="input-group-btn">
							<button class="btn btn-default" data-dismiss="modal" onclick='connect($("#url").val());send("!listenToAny");' type="button">Go!</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	</div>
</div>

<!-- RESET Modal -->
<div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="resetModalLabel">Reset and restart with a new deployment model?</h4>
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="reset();">Reset</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		</div>
	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- CONNECT Modal -->
<div class="modal fade" id="connectModal" tabindex="-1" role="dialog" aria-labelledby="connectModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="connectModalLabel">Connect to a CloudML Server</h4>
		</div>
		<div class="modal-body">
			<div class="span4">
				<div class="col-lg-6">
					<div class="input-group">
						<input type="text" id="urlWS" class="form-control" placeholder="URL">
						<span class="input-group-btn">
							<button class="btn btn-default" data-dismiss="modal" onclick='connect($("#urlWS").val());send("!listenToAny");' type="button">Go!</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		</div>
	</div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->


<!-- CREATE VM Modal -->
<div class="modal fade" id="createVMModal" tabindex="-1" role="dialog" aria-labelledby="createVMModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="createVMModalLabel">Create a VM Type</h4>
		</div>
		<div class="modal-body">
			<div class="span4">
				<div class="col-lg-6">
					<div class="input-group">
					<span class="input-group-addon">Name</span>
					<input type="text" id="name" class="form-control" placeholder="name"><br />
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">MinCores</span>
					<input type="text" id="minCores" class="form-control" placeholder="minCore"><br />
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">MaxCores</span>
					<input type="text" id="maxCores" class="form-control" placeholder="maxCore"><br />
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">MinRam</span>
					<input type="text" id="minRam" class="form-control" placeholder="minRam"><br />
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">MaxRam</span>
					<input type="text" id="maxRam" class="form-control" placeholder="maxRam"><br />
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">MinStorage</span>
					<input type="text" id="minStorage" class="form-control" placeholder="maxDisk">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">MaxStorage</span>
					<input type="text" id="maxStorage" class="form-control" placeholder="maxDisk">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">Location</span>
					<input type="text" id="location" class="form-control" placeholder="location">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">OS</span>
					<input type="text" id="OS" class="form-control" placeholder="OS">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">sshKey</span>
					<input type="text" id="sshKey" class="form-control" placeholder="sshKey">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">privateKey</span>
					<input type="text" id="privateKey" class="form-control" placeholder="privateKey">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">Is64</span>
					<input type="checkbox" id="Is64" class="form-control" placeholder="Is64">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">Provider</span>
					<input type="text" id="provider" class="form-control" placeholder="Provider">
					</div><br />
					<div class="input-group">
					<span class="input-group-addon">Image Id</span>
					<input type="text" id="imageId" class="form-control" placeholder="imageId">
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Next</button>
		</div>
	</div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->

<!-- Instantiate InternalComponent MODAL-->
<div class="modal fade" id="instantiateInternalComponentModal" tabindex="-1" role="dialog" aria-labelledby="instantiateInternalComponentModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="instantiateInternalComponentModalLabel">Instantiate an Internal Component</h4>
			</div>
			<div class="modal-body">
				<div class="span4">
					<div class="col-lg-6">
						<div class="input-group">
						<span class="input-group-addon">Name</span>
						<input type="text" id="name" class="form-control" placeholder="name"><br />
						</div><br />
						<div class="input-group">
						<span class="input-group-addon">Type</span>
						<select class="form-control" id="ICtype">
						</select>
						</div><br />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="">Instantiate</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Instantiate ExternalComponent MODAL-->
<div class="modal fade" id="instantiateExternalComponentModal" tabindex="-1" role="dialog" aria-labelledby="instantiateExternalComponentModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="instantiateExternalComponentModalLabel">Instantiate an External Component</h4>
			</div>
			<div class="modal-body">
				<div class="span4">
					<div class="col-lg-6">
						<div class="input-group">
						<span class="input-group-addon">Name</span>
						<input type="text" id="name" class="form-control" placeholder="name"><br />
						</div><br />
						<div class="input-group">
						<span class="input-group-addon">Type</span>
						<select class="form-control" id="ECtype">
						</select>
						</div><br />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="">Instantiate</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Instantiate ExternalComponent MODAL-->
<div class="modal fade" id="instantiateVMModal" tabindex="-1" role="dialog" aria-labelledby="instantiateVMModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="instantiateVMModalLabel">Instantiate a VM</h4>
			</div>
			<div class="modal-body">
				<div class="span4">
					<div class="col-lg-6">
						<div class="input-group">
						<span class="input-group-addon">Name</span>
						<input type="text" id="name" class="form-control" placeholder="name"><br />
						</div><br />
						<div class="input-group">
						<span class="input-group-addon">Type</span>
						<select class="form-control" id="VMtype">
						</select>
						</div><br />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="">Instantiate</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Provide SLO Modal-->
<div class="modal fade" id="ProvideSLO" tabindex="-1" role="dialog" aria-labelledby="instantiateVMModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="provideSLOLabel">Provide SLO</h4>
			</div>
			<div class="modal-body">
				<label>SLOs: 
					<select name="slo">
						<option value="browse_sla" selected=""> browse_sla </option>
						<option value="manage_sla"> manage_sla </option>
						<option value="purchase_sla"> purchase_sla </option>
					</select><br>
					response time (ms): <input type="text" name="response_time" value=""><br>
					load (TxRate): <input type="text" name="load" value="">
				</label>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="">Add</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- The menu: -->
<?php echo elgg_view('editor/submenu'); ?>
<!-- The editor: -->
<div id="main-editor"> 
	<h3 id="title"></h3>
		<div class="demo flowchart-demo" id="flowchart-demo"></div>
</div>

<script>
	$(document).ready(function () {
		if(!("WebSocket" in window)){  
			alertMessage("error","Your browser does not support WebSockets!",8000);
		}
	});
</script>

<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js'></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js'></script>
<script type='text/javascript' src='http://jsplumbtoolkit.com/lib/jquery.ui.touch-punch.min.js'></script>