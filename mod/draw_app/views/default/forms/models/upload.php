<?php
/**
 * action: models/upload
 * 
 * for uploading xmi file
 * @uses $vars['appid'] int the app GUID
 */
?>

<div id="modal-body" class="modal-body">
	<p><input type="file" name="upload"></p>
	<p><input id="hidden_input" type="hidden" name="appid" value="<?=$vars['appid'];?>"></p>
	<img id="upload_xmi_gif" src="<?php elgg_get_root_path();?>/_graphics/ajax_loader.gif" style="display: none"/>
</div>
<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	<button id="upload_abstract_model" data-uid="<?=$vars['appid'];?>" class="btn btn-primary">Upload</button> 
</div>