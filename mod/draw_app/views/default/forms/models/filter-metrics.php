<?php
/**
 * @uses $vars['data'] The json format of executions.
 * @uses $vars['graph'] The metric for the graphs. uptime | rt | throughput | cost
 * @uses $vars['model_guid'] The model GUID.
 */
$graph = $vars['graph'];
$model_guid = $vars['model_guid'];

$socket_res_json = $vars['data'];
$provider_rates = json_get_providers_rates_filtersEdition($socket_res_json);
$locations = json_get_location_filtersEdition($socket_res_json);

// save the cloud providers of the application locally at the social network
$app = get_entity($model_guid); 
$app->cloud = array_keys($provider_rates);
//print_r($app->cloud);

$app->geography = array_keys($locations);
//print_r($locations);

?>

<div class="models-run-filter">
    <h3 class="header"> Filters </h3>

    <div class="submodel-menu-filter">
        <div class="control-group">
            <label class="control-label">Cost:</label>
            <div class="controls">
                <input class="appendedInputButton" id="costFrom" name="euro_from" type="text" placeholder="&euro;" value="0">
                to 
                <input class="appendedInputButton" id="costTo" name="euro_to" type="text" placeholder="&euro;" value="0">
                <select class="form-control">
                    <option>&euro;</option>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Date:</label>
            <div class="controls">
                <input 
                    type="date" 
                    id="dateFrom"
                    name="date-from" 
                    placeholder="from (e.g. 2000-01-01)" 
                    value=""
                    class="appendedInputButton dateclass placeholderclass"
                    style="max-width: 150px;"
                    onClick="$(this).removeClass('placeholderclass')"><br/>
                <input 
                    type="date"
                    id="dateTo" 
                    name="date-to" 
                    placeholder="to (e.g. 2000-01-01)"
                    value=""
                    class="appendedInputButton dateclass placeholderclass" 
                    style="max-width: 150px;"
                    onClick="$(this).removeClass('placeholderclass')"><br/>
            </div>
        </div>
    </div>

    <div class="submodel-menu-filter">
        <div class="control-group">
            <label class="control-label" for="inputUptime">
                Uptime greater than:
            </label>
            <div class="controls" >
                <input class="appendedInputButton" id="inputUptime" type="text" placeholder="%" value="0%">
            </div>
        </div>

        <div class="control-group" id="addMetric">
            <label class="control-label" for="inputResponse">
                Add up to 2 more metrics:
            </label>
            <div class="controls">
                <select class="cloud-select" name="cloud-metrics" id="cloud-metrics" style="max-width: 150px;">
                <?php
                foreach ($socket_res_json as $key) {
                    $jkey = json_decode($key);
                    $count_options = 0;
                    foreach ($jkey->{'measurements'} as $meas => $value) {
                        if (++$count_options > 6) {
                            break;
                        }
                        echo '<option value="' . $meas . '">' . $meas . '</option>';
                    }
                    break;
                }
                ?>
                </select>
            </div> 
            <div class="controls">
                <button class="addMetric">Add</button>
            </div>
        </div>

        <!--
        <div class="control-group">
            <label class="control-label" for="inputResponse">
                Response Time less than:
            </label> 
            <div class="controls">
                <input class="appendedInputButton" id="inputResponse" type="text" name="rt_value" placeholder="ms">
                <select class="form-control time-select" name="rt_metric">
                    <option>ms</option>
                    <option>sec</option>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="inputThroughput">
                Throughput greater than: 
            </label>
            <div class="controls">
                <input class="appendedInputButton" id="inputThroughput" type="text" placeholder="ms">
            </div>
        </div>
        -->
    </div>

    <div class="submodel-menu-filter">
        <div class="control-group">
            <label class="control-label" for="inputGeography">
                Geography:
            </label> 
            <div class="controls" id="geography_selected">
                <select class="geography-select" name="geography" id="geography">
                <?php
                echo '<option value="All">All</option>';
                foreach ($locations as $key => $value) {
                    echo '<option value="' . $key . '">' . $key . '</option>';
                }
                ?>
                </select>
                <!--
                <input type="checkbox" value="All" id="inputGeography" checked> All <br/>
                <input type="checkbox" value="NorthAmerica" disabled> North America <br/>
                <input type="checkbox" value="SouthAmerica" disabled> South America <br/>
                <input type="checkbox" value="Europe" disabled> Europe <br/>
                <input type="checkbox" value="Asia" disabled> Asia <br/>
                <input type="checkbox" value="Africa" disabled> Africa <br/>
                <input type="checkbox" value="Australia" disabled> Australia <br/>	
                -->
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="inputProviders">
                Clouds:
            </label>
            <div class="controls" id="cloud_selected">
                <select class="cloud-select" name="clouds" id="clouds">
                <?php
                echo '<option value="All">All</option>';
                foreach ($provider_rates as $key => $value) {
                    echo '<option value="' . $key . '">' . $key . '</option>';
                }
                ?>
                </select>
            </div>
        </div>
    </div>

    <!--
    <div class="submodel-menu-filter">
        <div class="control-group" id="workloadType">
            <label class="control-label" for="inputWorkload">
                Workload type:
            </label>
            <div class="controls" id="workload_selected">
                <select class="workload-select" name="workloads" id="workloads">
                <option value="None">All</option>
                <option value="JMeter">JMeter</option>
                <option value="YCSB">YCSB</option>
                <option value="Other">Other</option>
                </select>
            </div>
        </div>
    </div>
    -->

    <div class="submodel-menu-filter"> 
        <div class="control-group">
            <label class="control-label" for="inputOwners">By:</label>
            <div class="controls">
                <input type="checkbox" value="AllUsers" id="inputOwners" checked> All Users <br/>
                <input type="checkbox" value="Me" disabled> Me <br/>
                <input type="checkbox" value="MyNetwork" disabled> My Network <br/>	
            </div>
        </div>
    </div>

    <input style="display: none;" type="text" name="model_guid" value="<?php echo $model_guid; ?>">
    <input style="display: none;" type="text" name="graph" value="<?php echo $graph; ?>">
    <input style="display: none;" type="text" name="curr_page" value="<?php echo $curr_page; ?>">

    <button class="models-button-filter">Apply</button>
</div>

<script type="text/javascript">
    //when add button is clicked
    var numberOfClicks = 0;
    $("button.addMetric").click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        numberOfClicks++;
        if (numberOfClicks > 2) {
            ;
        } else {
            var $contnt = $('div#addMetric');
            var htmlcont = "";
            htmlcont += "<div class='control-group'>";
            htmlcont +=     "<label class='control-label' for='inputResponse' id='labelInputResponse"
                            + numberOfClicks
                            + "' >" 
                            + $("select.cloud-select option:selected").val() 
                            + " less than: </label>";
            htmlcont +=     "<div class='controls'>";
            htmlcont +=         "<input class='appendedInputText' id='inputResponse" 
                                + numberOfClicks 
                                + "' type='text'>";
            htmlcont +=     "</div>"; 
            htmlcont +=     "<label class='control-label' for='inputResponse' id='labelInputResponse"
                            + numberOfClicks
                            + ".5' >" 
                            + $("select.cloud-select option:selected").val() 
                            + " more than: </label>";
            htmlcont +=     "<div class='controls'>";
            htmlcont +=         "<input class='appendedInputText' id='inputResponse" 
                                + numberOfClicks 
                                + ".5' type='text'>";
            htmlcont +=     "</div>";
            htmlcont += "</div>";
            $contnt.append(htmlcont);
        }
    });

    // when we select a value at workload 
    var numberOfTimes = 0;
    $("#workloads").change(function (e) {

        numberOfClicks++;

        if(numberOfClicks == 1) {
            if (document.getElementById("workloads").value == "JMeter") {
                var $contnt = $('div#workloadType');
                var htmlcont = "";

                htmlcont += "<br />"
                htmlcont += "<div class='control-group' id='workloadType'>";
                htmlcont +=    "<label class='control-label' for='inputWorkloadMix'> Workload mix: </label>";
                htmlcont +=    "<div class='controls' id='workloadMix_selected'>";
                htmlcont +=        "<select class='workloadMix-select' name='workloadMixs' id='workloadMixs'>";
                htmlcont +=        "<option value='All'>All</option>";
                htmlcont +=        "<option value='JMeterWorkloadMix1'>JMeterWorkloadMix1</option>";
                htmlcont +=        "<option value='JMeterWorkloadMix2'>JMeterWorkloadMix2</option>";
                htmlcont +=        "<option value='JMeterWorkloadMix3'>JMeterWorkloadMix3</option>";
                htmlcont +=        "</select>";
                htmlcont +=    "</div>";
                htmlcont += "</div>";

                $contnt.append(htmlcont);
            } else if (document.getElementById("workloads").value == "YCSB") {
                var $contnt = $('div#workloadType');
                var htmlcont = "";

                htmlcont += "<br />"
                htmlcont += "<div class='control-group' id='workloadType'>";
                htmlcont +=    "<label class='control-label' for='inputWorkloadMix'> Workload mix: </label>";
                htmlcont +=    "<div class='controls' id='workloadMix_selected'>";
                htmlcont +=        "<select class='workloadMix-select' name='workloadMixs' id='workloadMixs'>";
                htmlcont +=        "<option value='All'>All</option>";
                htmlcont +=        "<option value='YCSBWorkloadMix1'>YCSBWorkloadMix1</option>";
                htmlcont +=        "<option value='YCSBWorkloadMix2'>YCSBWorkloadMix2</option>";
                htmlcont +=        "<option value='YCSBWorkloadMix3'>YCSBWorkloadMix3</option>";
                htmlcont +=        "</select>";
                htmlcont +=    "</div>";
                htmlcont += "</div>";

                $contnt.append(htmlcont);
            }

        } else if (numberOfClicks > 1) {
            if (document.getElementById("workloads").value == "JMeter") {
                document.getElementById("workloadMixs").innerHTML = "<option value='JMeterWorkloadMix1'>JMeterWorkloadMix1</option> <option value='JMeterWorkloadMix2'>JMeterWorkloadMix2</option> <option value='JMeterWorkloadMix3'>JMeterWorkloadMix3</option>";
            } else if (document.getElementById("workloads").value == "YCSB") {
                document.getElementById("workloadMixs").innerHTML = "<option value='YCSBWorkloadMix1'>YCSBWorkloadMix1</option> <option value='YCSBWorkloadMix2'>YCSBWorkloadMix2</option> <option value='YCSBWorkloadMix3'>YCSBWorkloadMix3</option>";
            }
        }
    });

    //when workload JMeter is selected
    function workloadMix() {
        if (document.getElementById("workloads").value == "JMeter") {
        
            var $contnt = $('div#workloadType');
            var htmlcont = "";

            htmlcont += "<br />"
            htmlcont += "<div class='control-group' id='workloadType'>";
            htmlcont +=    "<label class='control-label' for='inputWorkloadMix'> Workload mix: </label>";
            htmlcont +=    "<div class='controls' id='workloadMix_selected'>";
            htmlcont +=        "<select class='workloadMix-select' name='workloadMixs' id='workloadMixs'>";
            htmlcont +=        "<option value='WorkloadMix1'>WorkloadMix1</option>";
            htmlcont +=        "<option value='WorkloadMix2'>WorkloadMix2</option>";
            htmlcont +=        "<option value='WorkloadMix3'>WorkloadMix3</option>";
            htmlcont +=        "</select>";
            htmlcont +=    "</div>";
            htmlcont += "</div>";

            $contnt.append(htmlcont);
        }
    }
</script>