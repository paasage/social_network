<?php 

/**
 * for action: models/filters
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>  
 */

$op = array('annotation_names' => array('models_tags'), 'limit' => 65000);
$numOftags = count(elgg_get_annotations($op));
?>
<div class="sidebar-section-show">
	<h3>Status</h3>
	<div>
		<label><input type="radio" value="TRUE" class="status elgg-input-radio" name="Deployed" /> Deployed</label>
		<label><input type="radio" value="FALSE" class="status elgg-input-radio" name="Deployed"/> Not yet deployed</label>
	</div>
	<hr>
</div>

<div class="sidebar-section deployed-query">
	<h3>Clouds</h3>
	<p>	
		<select name="clouds">
			<option value="All" selected> All </option>
			<option value="Amazon"> Amazon </option>
			<option value="Flexiant"> Flexiant </option>
			<option value="Azure"> Microsoft Azure </option>
			<option value="Google"> Google Compute Cloud </option>
		</select>
		<!--
		<input type="checkbox" name="clouds[]" value="All" checked> All <br/>
		<input type="checkbox" name="clouds[]" value="Amazon"> Amazon <br/>
		<input type="checkbox" name="clouds[]" value="Flexiant"> Flexiant <br/>
		<input type="checkbox" name="clouds[]" value="Azure"> Microsoft Azure <br/>
		<input type="checkbox" name="clouds[]" value="Google"> Google Compute Cloud <br/>
		-->
	</p>
	<hr>
</div>

<div class="sidebar-section deployed-query">
	<h3>Geography</h3>
	<p>
		<select name="geographies">
			<option value="All" selected> All </option>
			<option value="NorthAmerica"> NorthAmerica </option>
			<option value="SouthAmerica"> SouthAmerica </option>
			<option value="Europe"> Europe </option>
			<option value="Asia"> Asia </option>
			<option value="Africa"> Africa </option>
			<option value="Australia"> Australia </option>
		</select>
		<!--
		<input type="checkbox" name="geographies[]" value="All" checked> All <br/>
		<input type="checkbox" name="geographies[]" value="NorthAmerica"> North America <br/>
		<input type="checkbox" name="geographies[]" value="SouthAmerica"> South America <br/>
		<input type="checkbox" name="geographies[]" value="Europe"> Europe <br/>
		<input type="checkbox" name="geographies[]" value="Asia"> Asia <br/>
		<input type="checkbox" name="geographies[]" value="Africa"> Africa <br/>
		<input type="checkbox" name="geographies[]" value="Australia"> Australia <br/>				
		-->
	</p>
	<hr>
</div>

<div class="sidebar-section tags">
	<h3>Tags</h3>
	<a href="#AllTagsModal" role="button" class="span-view-all" data-toggle="modal">View All(<?= $numOftags; ?>)</a>
	<?php echo elgg_view('page/elements/models/all-tags', $vars); ?>
</div>

<input type="submit" class="models-button-filter" value="Apply">

<?php 
	echo elgg_view('modals/all-tags');