<?php
/**
 * 
 * The basic field for search models. This form is shown in models pages.
 * 
 */

?>
<div class="input-append">
		<input name="model_name" id="appendedInputButton" type="text" placeholder="Keyword">
		<button class="btn" type="submit">
			<span class="glyphicon glyphicon-search"></span>
		</button>
	</div>