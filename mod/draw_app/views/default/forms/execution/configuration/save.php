<?php
/**
 * @uses $vars['execution_id']
 * @uses $vars['model_id'] 
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

?>
<input type="hidden" name="execution_id" value="<?=$vars['execution_id'];?>">
<input type="hidden" name="model_id" value="<?=$vars['model_id'];?>">
<button type="submit" class="btn btn-primary">save this configuration</button>
