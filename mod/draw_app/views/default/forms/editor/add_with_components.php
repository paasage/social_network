<?php
/**
 * 
 * @uses $vars['model_guid'] The GUID of the application
 */
error_log("add_with_components");
$app = get_entity($vars['model_guid']);
$name = '';
$description = '';
$version = '';
$clouds = '';
$tags = array();

$buttom = 'Create new application description';
if ($app) {
	$name = $app->appname;
	$description = $app->description;
	$version = $app->version;
	$clouds = $app->clouds;
	$tags = array();
	$buttom = 'Save changes';
}
error_log('end some of php 22');
elgg_load_css('draw_app_form_css');
elgg_load_js('draw_app.form');
$components_url = elgg_get_site_url() . "components/all";

elgg_load_js('draw_app.downloadify');
elgg_load_js('draw_app.swfobject');
?>
<h3>New Application Model</h3>
<!--
<div class="wizard">
	<a id="menu_step_3" class="current">Import Model</a>
</div>
-->
<div id="step3" class="form-each-step">
	<div class="control-group">
		<label class="control-label" for="inputName">Name:</label>

		<div class="controls">
			<input type="text" id='inputName' name="name" value='<?php echo $name; ?>'>
			<span id="app_name_empty" class="glyphicon glyphicon-remove form-control-feedback" style="color: red; visibility: hidden">Please provide the name of application model</span>
			<p style="color:red"> Spaces are not supported, please replace with another character, such as _ </p>
		</div>
	</div>
	<br/>
	<div class="control-group">
		<label class="control-label" for="inputDescription">Description:</label>
		<div class="controls">
			<textarea rows="5" cols='50' name='description' id='inputDescription' placeholder="Add a description" style="width: 400px;"><?php echo $description; ?></textarea>
			<span id="description_empty" class="glyphicon glyphicon-remove form-control-feedback" style="color: red; visibility: hidden">Please provide more informative description </span>
		</div>
	</div>
	<br/>
	<div class="control-group">
		<label class="control-label" for="inputVersion">Version:</label>
		<div class="controls">
			<input type="text" name="version" id='inputVersion' value="<?php echo $version; ?>">
			<span id="version_empty" class="glyphicon glyphicon-remove form-control-feedback" style="color: red; visibility: hidden">Please provide the version of application model </span>
		</div>
	</div>
	<div class="control-group" id="XMIfieldInput">
		<label class="control-label" for="inputXMI">XMI file:</label>
		<div class="controls">
			<input type="file" name="xmiFile" id='inputXMI' value="" accept=".xmi">
		</div>
	</div>

	<div class="action-buttons">
		<button id="from3to2_1" class="action next btn btn-info">Previous</button>
		<button type="button" class="action btn btn-success" onclick="submit_form()">Save as draft</button>
		<button type="button" class="action btn btn-success" id="submit_app" onclick="submit_form()">Finalize</button>
	</div>
</div>

<?php
if ($app) {
	echo '<input type="hidden" name="guid" value="' . $vars['model_guid'] . '">';
}


