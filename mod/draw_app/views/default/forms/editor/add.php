<?php
/**
 * 
 * @uses $vars['model_guid'] The GUID of the application
 */
$app = get_entity($vars['model_guid']);
$name = '';
$description = '';
$version = '';
$clouds = '';
$tags = array();
$buttom = 'Create new application description';
if ($app) {
	$name = $app->appname;
	$description = $app->description;
	$version = $app->version;
	$clouds = $app->clouds;
	$tags = array();
	$buttom = 'Save changes';
}
?>

<div class="models-add">
	<h3 class="models-filter-header">New application</h3>
	<br/><hr>
		<div class="control-group">
			<label class="control-label" for="inputName">Name:</label>
			<div class="controls">
				<input type="text" id='inputName' name="name" value='<?php echo $name; ?>'>
			</div>
		</div>
		<br/>
		<div class="control-group">
			<label class="control-label" for="inputDescription">Description:</label>
			<div class="controls">
				<textarea rows="5" cols='50' name='description' id='inputDescription' placeholder="Add a description" style="width: 400px;"><?php echo $description; ?></textarea>
			</div>
		</div>
		<br/>
		<div class="control-group">
			<label class="control-label" for="inputVersion">Version:</label>
			<div class="controls">
				<input type="text" name="version" id='inputVersion' value="<?php echo $version; ?>">
			</div>
		</div>
	<?php 
		if($app) {
			echo '<input type="hidden" name="guid" value="'.$vars['model_guid'].'">';
		}
	?>
	<button class="models-button-filter"><?=$buttom;?></button>
</div>