<?php
/*
 * The page provides a fabricjs library to help users create/draw their
 * applications.
 *
 * @author Christos Papoulas
 */

gatekeeper();
$title = "Draw a new application architecture";
$content = elgg_view_title($title);
// add the canvas with the needed specific id

$content .= elgg_view('output/url', array(
  'href' => 'draw_app/all',
  'text' => 'All Applications',
  'value' => 'All Applications',
  'is_trusted' => true,
));
$content .= '<br/>';
$content .= elgg_view('output/url', array(
  'href' => 'draw_app/myapps',
  'text' => 'My own applications',
  'value' => 'My own applications',
  'is_trusted' => true,
));

global $MyConfig;
if($MyConfig == null ) {
		$MyConfig = include(elgg_get_plugins_path() . '/draw_app/config.php');
}
$content .= <<<EOT
<div class="draw_menu"> <!-- float:left; --> 
	<button id="aDD1" class="btn" onclick="$.prompt(addObject, null);" style="width:60px; height:60px; position:relative;" value="Add New Art">Add Object</button>
	<button id="aDD3" class="btn" onclick="saveApp();" style="width:90px; height:60px; position:relative;" value="saveApp">Save Application</button>
	<button id="aDD5" class="btn" onclick="addSLObox();" style="width:80px; height:60px; position:relative;" value="addSLO">Provide SLO</button>
	<button id="aDD7" class="btn" onclick="$.prompt(clearArea, null);" title="Clear the composition area" style="width:76px; height:60px; position:relative;" value="ClearArea">Clear Area</button>
	<button id="aDD4" class="btn" onclick="Suggest();" style="width:90px; height:60px; position:relative;" value="suggest">Look Up for Similar Apps</button>
	<button id="aDD6" class="btn" onclick="$.prompt(deployHint, null);" title="Optimizing Cost Effectiveness" style="width:90px; height:60px; position:relative;" value="deployHint">Deployment Hints</button>
</div>
<div id="composition_area" class="composition_area">

</div>
<div id="suggestion" class="suggestions"></div>
<div id="suggestion_artefact" style=""></div><br>
<div id="configuration" style="float:left;"></div>
EOT;
//$content .= elgg_view_form('app_description');
// throw the page to the client
$body = elgg_view_layout('one_column', array(
   'content' => $content,
));

echo elgg_view_page($title, $body);