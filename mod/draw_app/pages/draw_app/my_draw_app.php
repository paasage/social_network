<?php
/**
 * Display the application of a specific user
 * 
 * @author Christos Papoulas
 */
gatekeeper();
$title = "All My Composed Applications";
$content =  elgg_list_entities(array(
    'type' => 'object',
    'subtype' => 'draw_application',
    'myapps' => elgg_get_logged_in_user_guid(), 
    'full_view' => false
));

// optionally, add the content for the sidebar
$sidebar = elgg_view('output/url', array(
    'href' => 'draw_app/add',
    'text' => 'Compose an application',
    'is_trusted' => true
));

$sidebar .= '<br/>';

$sidebar .= elgg_view('output/url', array(
  'href' => 'draw_app/all',
  'text' => 'All Applications',
  'value' => 'All Applications',
  'is_trusted' => true,
));
// layout the page
$body = elgg_view_layout('one_sidebar', array(
   'content' => $content,
   'sidebar' => $sidebar
));


echo elgg_view_page($title, $body);