/* modal defines */
var VMconf = {
	state0: {
		title: 'VM configuration',
		html:'Cloud:<br><label><input type="radio" name="provider" value="Azure">Azure</label><br />'+
		'<label><input type="radio" name="provider" value="Flexiant">Flexiant</label>',
		buttons: { Next: 1 },
		//focus: ":input:first",
		submit:function(e,v,m,f){
			if(f.provider == 'Azure')
				$.prompt.goToState('state1');
			else
				$.prompt.goToState('state2')
			e.preventDefault();
		}	
	},
	state1: { //Azure images
		title: 'VM configuration',
		html:'Type<br><label><input type="radio" name="vm_type" value="m1.large">m1.large</label><br />'+
		'<label><input type="radio" name="vm_type" value="c1.medium">c1.medium</label><br />' +
		'<label><input type="radio" name="vm_type" value="m1.small">m1.small</label>',
		buttons: { Back: -1, Done: 1 },
		//focus: "input[name='fname']",
		submit:function(e,v,m,f){
			e.preventDefault();
			if(v == 1){
				//$('#'+window.idOfCaller).append('<p>' + f.vm_type + '</p>');
				var cl = f.provider == "Azure" ? "Windows_Azure" : "Flexiant";
				connect_vm2cloud(window.idOfCaller.replace('#', ''), cl);
				$.prompt.close();
			}
			else				
				$.prompt.goToState('state0');
		}
	},
	state2 :{ // Flexiant images
		title: 'VM configuration',
		html:'Type<br><label><input type="radio" name="vm_type" value="3c4m">3c4m</label><br />'+
		'<label><input type="radio" name="vm_type" value="1c2m">1c2m</label>',
		buttons: { Back: -1, Done: 1 },
		focus: 1,
		submit:function(e,v,m,f){
			e.preventDefault();
			if(v == 1){
				//$('#'+window.idOfCaller).append('<p>' + f.vm_type + '</p>');
				var cl = f.provider == "Azure" ? "Windows_Azure" : "Flexiant";
				connect_vm2cloud(window.idOfCaller.replace('#', ''), cl);
				$.prompt.close();
			}
			else				
				$.prompt.goToState('state0');
		}
	}
}; // end of VMconf


var ArtefactConf = {
	state0: {
		title: 'Conponent',
		html: '<p> If you deleted, you can not restore the component.</p>' +
						'<p> Query MDDB for applications with this artefact </p>',
		buttons: {'Query MDDB': 2, 'Configure': 1, 'Delete' : 0},
		submit: function(e,v,m,f) {
			e.preventDefault();
			if(v === 1) {
				$.prompt.goToState('state1');
			} else if (v == 0){
				var targetBoxId = window.idOfCaller;
				
				jsPlumb.detachAllConnections(window.idOfCaller.replace('#', ''));
				jsPlumb.removeAllEndpoints(window.idOfCaller.replace('#', ''));
				$(targetBoxId).remove();

				$.prompt.close();
			}
			else {
				$.post(ConfigJS.api, {key: $(window.idOfCaller).text(), q: 'AppsWithComp'})
				.done(function(data) { 
					var currentdate = new Date();
            var datetime = currentdate.getDay() + "/"+currentdate.getMonth() 
            + "/" + currentdate.getFullYear() + " @ " 
            + currentdate.getHours() + ":" 
            + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        $('#suggestion').html('<div class=\'mddbfd\'>' + datetime + ' MDDB feedback: <div>');
        $('#suggestion').append('<div class=suggestfromMDDB>'+ data + "</div><br>");

				$.prompt.close();
				} );
			}
		}
	},
	state1: {
		title: 'Component Configuration',
		html:'<label>Component name <input type="text" name="art_name" value=""></label><br/>',
		buttons: { Back: -1, Done: 1 },
		//focus: ":input:first",
		submit:function(e,v,m,f){
			e.preventDefault();
			if( v ===1 ) {
				if(f.art_name !== '')
					$(window.idOfCaller).html(f.art_name);
				$.prompt.close();
			}
			if( v === -1 ) $.prompt.goToState('state0');
		}	
	}
}; // end of ArtefactConf

var SaveCompotition = {
	state0: {
		title: 'Save Application?',
		html: '<p>Are you sure that you want to save the Application?</p>',
		buttons: {'Yes': true, 'No' : false},
		submit: function(e,v,m,f) {
			e.preventDefault();
			if(v) 
				storeAndPostApp();
			$.prompt.close();
		}
	}
}; // end of SaveCompotition

var AppSaved = {
	state0: {
		title: 'Application Saved',
		html: '<label>Your Application appears to be related to the following group(s). '+
					'Indicate whether you want to become a subscriber.</label>',
		buttons: {'OK' : true},
		submit: function (e,v,m,f) {
			$.prompt.close();
		}
	}
}; // end of AppSaved

var addSlo = {
	state0: {
		title: 'Select SLO',
		html: '<label>SLOs: '+
			'<select name="slo">' +
				'<option value="browse_sla" selected> browse_sla </option>'+
				'<option value="manage_sla"> manage_sla </option>'+
				'<option value="purchase_sla"> purchase_sla </option>'+
			'</select><br/>'+
			'response time (ms): <input type="text" name="response_time" value=""><br/>' +
			'load (TxRate): <input type="text" name="load" value="">' +
			'</label>',
		buttons: {'OK': true, 'Cancel' : false},
		submit: function(e,v,m,f) {
			e.preventDefault();
			if(v) {
				addSlo2App(f.slo, f.response_time, f.load, window.idOfCaller);
				$.prompt.close();
			} else {
				$.prompt.close();
			}
		}
	}
}; // end of addSlo

var addObject = {
	state0: {
		title: 'Select Object Type',
		html: '<label>Object types: <br/>'+
				'<select name="obj" multiple>' +
					'<option value="application" name="choice"> Application </option>' +
					'<option value="artifact" name="choice" selected> Artifact </option>' +
					'<option value="art_inst" name="choice"> Artifact Instance </option>'+
					'<option value="node_inst" name="choice"> Node Instance </option>'+
					'<option value="cd_vm_type" name="choice"> Virtual Machine </option>'+
				'</select></label>',
		buttons: {'OK': true, 'Cancel' : false},
		submit: function(e,v,m,f) {
				e.preventDefault();
				if(v) {
					addObjectByType(f.obj);
					$.prompt.close();
				} else {
					$.prompt.close();
				}
		}
	}
} // end of addObject

var deployHint = {
	state0: {
		title: 'Query MDDB for deployment hints',
		html: '<label>Criterion: Cost Effectiveness of deployment</label>',
		buttons: {'Query and Order': true, 'Cancel' : false},
		submit: function(e,v,m,f) {
				e.preventDefault();
				if(v) {
					var currentdate = new Date();
						var datetime = currentdate.getDay() + "/"+currentdate.getMonth() 
						+ "/" + currentdate.getFullYear() + " @ " 
						+ currentdate.getHours() + ":" 
						+ currentdate.getMinutes() + ":" + currentdate.getSeconds();
					$.post(ConfigJS.api, {
						q : 'cost',
						key : 'browse_resp_time'
					})
						.done(function (data) {
							$('.suggestions').append('<div class=suggestfromMDDB>' + data.concat('<br/>').concat(datetime) + '</div>');
						});

				} 
					$.prompt.close();
				
		}
	}
} // end of deployHint

var Appconf = {
	state0: {
		title: 'Application Configuration',
		html: '<label>Application Name <input type="text" name="app_name" value=""></label>',
		buttons: {'OK': true, 'Cancel' : false},
		submit: function(e,v,m,f) {
				e.preventDefault();
				if(v) {
					$('#app').html(f.app_name);
				} 
				$.prompt.close();	
		}
	}
} // end of Appconf

var clearArea = {
	state0: {
		title: 'Delete Application?',
		html: '<p>Are you sure that you want to clear the composition area?</p>',
		buttons: {'Yes': true, 'No' : false},
		submit: function(e,v,m,f) {
			e.preventDefault();
			if(v) 
				clear_area();
			$.prompt.close();
		}
	}
}

/* end modal defines */
