/**
 * This files contains the necessary JS to suggest when the user types in forms
 * at groups plugin.
 * 
 * requires the group plugin to be activated.
 * 
 * @author Christos Papoulas
 */

/**
 * Checks for zombies wrong words
 * @returns {nothing}
 */
function remove_non_existing_wrong_words(area){
	var wrong_words = $(".lexical");
	var allwords = $(area).val().replace(/[\s-]+$/, '').split(/[\s-]/);
	for (var i = 0; i < allwords.length; i++)
		allwords[i] = allwords[i].toLowerCase();
	
	for (var i = 0; i < wrong_words.length; i++) {
		if (allwords.indexOf(wrong_words[i].id) === -1) {
			$('#' + wrong_words[i].id).remove();
		}
	}
}

var previous_word = '';
$(document).ready(function() {
  $("#typing_area").bind("keyup", function(e) {
    if (e.keyCode === 32) { // white space!
      var word = $(this).val().replace(/[\s-]+$/, '').split(/[\s-]/).pop();
      if (word === previous_word)
				return;
      previous_word = word;
      console.log(word);
      if (word.length > 3) { // not for short words
	// query mddb
		$.post(ConfigJS.api,
			{
				q: 'whatDoYouKnowAbout',
				key: word.toLowerCase().replace('.', '')
			})
			.done(function(data) {
				var currentdate = new Date();
				var datetime = currentdate.getDay() + "/" + currentdate.getMonth()
					+ "/" + currentdate.getFullYear() + " @ "
					+ currentdate.getHours() + ":"
					+ currentdate.getMinutes() + ":" + currentdate.getSeconds();
				var disp = '';
				if (data != '""') {
					disp = data.replace(/'/g, '').replace(/"/g, '').concat(datetime);
					$('.elgg-sidebar').append("<div class='suggestfromMDDB'>" +
						disp +
						"</div>");
				}
			});
		//check if the word is known
		$.post(ConfigJS.node, {
			key: word
		})
			.done(function(data) {
				if (data.unknown == true) {
					$('#typing_area').after('<div id=' + data.word + ' class="lexical">' + data.word + '</div>')
						.fadeIn();
					$('#' + data.word).bind('click', function() {
						$.prompt({
				state0: {
					title: $('#' + data.word).text(),
					html: '<script>$("#entity_check").change( function (){if($(".select_hidden").css("display") == "none")$(".select_hidden").fadeIn();else $(".select_hidden").fadeOut();});</script><label>' +
						'PaaSage Entity: <input type="checkbox" id="entity_check"></br>' +
						'<select name="mddbentity" class="select_hidden" style="display: none;">' +
						'<option value="art"> Artifact </option>' +
						'<option value="app"> Application </option>' +
						'</select>' +
						'<br/>Definition <textarea rows="2" cols="10"></textarea>' +
						'</label><br />',
					focus: 1,
					position: {container: '#' + data.word, x: 40, y: 0, width: 300, arrow: 'lt'},
					buttons: {OK: 1, Cancel: 0},
					submit: function(e, v, m, f) {
						e.preventDefault();
						$(window.idOfCaller).fadeOut('slow').remove();
						$.prompt.close();
					}
				}}, '#' + data.word);
					});
				}
			});
			}
			remove_non_existing_wrong_words(this);
    }
    else if (e.keyCode === 190 || e.keyCode === 191) { //dot || ?
      var sentence = $(this).val().replace(/[.-]+$/, '').split(/[.-]/).pop();
			$.post(ConfigJS.sentence,
			{
				key: sentence
			}).done(function (data) {
					console.log(data);
					if(data.isQueryCloud){
						$.post(ConfigJS.api, {q : 'cloudproviders'}).done(function (data){
							var currentdate = new Date();
							var datetime = currentdate.getDay() + "/" + currentdate.getMonth()
								+ "/" + currentdate.getFullYear() + " @ "
								+ currentdate.getHours() + ":"
								+ currentdate.getMinutes() + ":" + currentdate.getSeconds();
							$('.elgg-sidebar').append("<div class='suggestfromMDDB'>" +
							'Known clouds:' + data.concat('<br/>').concat(datetime)+ "</div>");
						});
						
					}
			});
    }
  });
});

