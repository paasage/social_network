$(document).ready(function () {
var file;
var reader;

  $(':file').change(function(){
      file = this.files[0];
      reader = new FileReader();
      reader.readAsText(file, 'UTF-8');
  });

  console.log('upload_abstract_model.js');

  // when the upload button is clicked
  $('button#upload_abstract_model').on('click', function (event) {

    event.preventDefault();
    var model_guid = $(this).attr('data-uid');
    var formData = new FormData($('form')[0]);
    $('#error_message').remove();

    // show the loading image
    $('#upload_xmi_gif').show();
    //var model_name = $(this).attr('name');

    elgg.action('upload_xmi/upload_abstract_model', {
      data: {
        val: reader.result,
        model_guid: model_guid,
      },
      success: function (resultText) {

        console.log(resultText);
        //alert(resultText);

        if (resultText.output == true) {
          $('#upload-modal').modal('hide');
          location.reload();
        } else {
          document.getElementById("upload_xmi_gif").style.display = "none";
          document.getElementById("modal-body").insertAdjacentHTML('beforeend', '<div id="error_message">Something went wrong while storing the model.</div>');
        }

        return true;

      }
    });
  });

});