/*
 * lib: draw_app.filters
 */
console.log("filters.js");
$(document).ready(function () {
  //when apply button is selected
  $("button.models-button-filter").click(function (e) {
    e.preventDefault();
    e.stopPropagation();

    // input from Clouds filter in var $providers
    var input_tag = $('div#cloud_selected option:selected');
    var $providers = {}, i = 0;
    var allProviders = false;
    for (i = 0; i < input_tag.length; ++i) {
      $providers[i] = input_tag[i].value;
      if (input_tag[i].value === "All") {
        allProviders = true;
      }
    }

    // input from Geography filter in var $geographies
    var input_tag = $('div#geography_selected option:selected');
    var $geographies = {}, i = 0;
    var allGeographies = false;
    for (i = 0; i < input_tag.length; ++i) {
      $geographies[i] = input_tag[i].value;
      if (input_tag[i].value === "All") {
        allGeographies = true;
      }
    }

    // input from uptime filter in var uptime
    var uptime = document.getElementById("inputUptime").value;
    uptime = uptime.replace("%","");
    uptime = Number(uptime);

    // input from cost filter in vars costFrom and costTo
    var costFrom = document.getElementById("costFrom").value;
    costFrom = Number(costFrom);
    var costTo = document.getElementById("costTo").value;
    costTo = Number(costTo);

    var alllines = $("table#app-runs-table tbody tr");

    for (i = 0; i < alllines.length; ++i) {
      ($(alllines[i])).css('display', 'grid');
    }

    // input from date filter in vars dateFrom and dateTo
    var dateFrom = document.getElementById("dateFrom").value;
    var dateTo = document.getElementById("dateTo").value;

    // input from first metric in vars metric1LessThan metric1MoreThan
    if (document.getElementById("inputResponse1")) {
      var metric1LessThan = document.getElementById("inputResponse1").value;
    }
    if (document.getElementById("inputResponse1.5")) {
      var metric1MoreThan = document.getElementById("inputResponse1.5").value;
    }

    // input from second metric in vars metric1LessThan metric1MoreThan
    if (document.getElementById("inputResponse2")) {
      var metric2LessThan = document.getElementById("inputResponse2").value;
    }
    if (document.getElementById("inputResponse2.5")) {
      var metric2MoreThan = document.getElementById("inputResponse2.5").value;
    }


    // input from workloadMix filter in var $workloadMixs
    var input_tag = $('div#workloadMix_selected option:selected');
    var $workloadMixs = {}, i = 0;
    var allWorkloadMixs = false;
    for (i = 0; i < input_tag.length; ++i) {
      $workloadMixs[i] = input_tag[i].value;
      if (input_tag[i].value === "All") {
        allWorkloadMixs = true;
      }
    }
    

    var tableHead = $("table#app-runs-table thead th");
    var numberOfMeasurements = tableHead.length;

    /*
    // filter the executables according to given workloadMixs
    if (input_tag.length > 0 && allWorkloadMixs !== true) {
      // find the number of the column
      for (var k = 0; k < tableHead.length; k++) {
        var title = $($(tableHead[k])).attr('title').replace(' ', '');  
        if (title.indexOf("WorkloadMix") >= 0) {
          break;
        }
      }
      
      for (i = 0; i < alllines.length; ++i) {
        var wlm = $($(($(alllines[i])).find('td')[k])).text().replace(' ', '');
        var j;
        var key_length = Object.keys($workloadMixs).length;
        var toHide = true;
        for (j = 0; j < key_length; ++j) { 
          if ($workloadMixs[j].indexOf(wlm) >= 0) {
            toHide = false;
          }
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
      
    }
    */
    

    // filter the executables according to given clouds
    if (!allProviders) {
      // find the number of the column
      for (var k = 0; k < tableHead.length; k++) {
        var title = $($(tableHead[k])).attr('title').replace(' ', '');  
        if (title.localeCompare("CloudProviders") == 0) {
          break;
        }
      }

      for (i = 0; i < alllines.length; ++i) {
        var provider = $($(($(alllines[i])).find('td')[k])).text().replace(' ', '');
        var j;
        var key_length = Object.keys($providers).length;
        var toHide = true;
        for (j = 0; j < key_length; ++j) {
          if (provider === $providers[j]) {
            toHide = false;
          }
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

    // filter the executables according to given geography
    if (!allGeographies) {
      // find the number of the column
      for (var k = 0; k < tableHead.length; k++) {
        var title = $($(tableHead[k])).attr('title').replace(' ', '');
        if (title.localeCompare("Geography") == 0) {
          break;
        }
      }

      for (i = 0; i < alllines.length; ++i) {
        var geography = $($(($(alllines[i])).find('td')[k])).text().replace(' ', '');
        var j;
        var key_length = Object.keys($geographies).length;
        var toHide = true;
        for (j = 0; j < key_length; ++j) {
          if (geography === $geographies[j]) {
            toHide = false;
          }
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

    // filter the executables according to given uptime
    if (uptime > 0) {

      for (i = 0; i < alllines.length; ++i) {
        var startt = $($(($(alllines[i])).find('td')[0])).text().replace(' ', '');
        var endt = $($(($(alllines[i])).find('td')[1])).text().replace(' ', '');
        var upt = $($(($(alllines[i])).find('td')[2])).text().replace(' ', '');
        upt = upt.replace("sec","");
        upt = Number(upt);

        var toHide = true;
        var maxUptime = (dateToSeconds(endt) - dateToSeconds(startt));

        if (maxUptime > 0) {
          var uptpercentage = (100 * upt) / maxUptime;
          if (uptime < uptpercentage) {
            toHide = false;
          }
        } else {
          ;
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

    // filter the executables according to given cost
    if (costFrom != 0 && costTo != 0) {
      // find the number of the column
      for (var k = 0; k < tableHead.length; k++) {
        var title = $($(tableHead[k])).attr('title').replace(' ', '');
        if (title.localeCompare("Cost") == 0) {
          break;
        }
      }

      for (i = 0; i < alllines.length; ++i) {
        var cost = $($(($(alllines[i])).find('td')[k])).text().replace(' ', '');
        cost = Number(cost);

        var toHide = true;
        if (cost >= costFrom && cost <= costTo) {
          toHide = false;
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

    // filter the executables according to given dates
    if (dateFrom && dateTo) {
      for (i = 0; i < alllines.length; ++i) {
        var startt = $($(($(alllines[i])).find('td')[0])).text().replace(' ', '');
        var endt = $($(($(alllines[i])).find('td')[1])).text().replace(' ', '');

        var toHide = true;
        startt = configureDateString(startt);
        endt = configureDateString(endt);

        if (startt >= dateFrom && endt <= dateTo) {
          toHide = false;
        }

        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

    // filter the executables according to given first metric
    if (metric1LessThan && metric1MoreThan){
      var labelMetric1 = document.getElementById("labelInputResponse1").innerHTML;
      labelMetric1 = labelMetric1.replace(" less than:","");
      labelMetric1 = labelMetric1.trim();

      for (var k = 0; k < tableHead.length; k++) {
        var title = $($(tableHead[k])).attr('title').replace(' ', '');
        if (title.localeCompare(labelMetric1) == 0) {
          break;
        }
      }

      for (i = 0; i < alllines.length; ++i) {
        var metric1 = $($(($(alllines[i])).find('td')[k])).text().replace(' ', '');
        metric1 = Number(metric1);

        var toHide = true;
        if (metric1 >= metric1MoreThan && metric1 <= metric1LessThan) {
          toHide = false;
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

    // filter the executables according to given second metric
    if (metric2LessThan && metric2MoreThan){
      var labelMetric2 = document.getElementById("labelInputResponse2").innerHTML;
      labelMetric2 = labelMetric2.replace(" less than:","");
      labelMetric2 = labelMetric2.trim();

                // find the number of the column
      for (var k = 0; k < tableHead.length; k++) {
        var title = $($(tableHead[k])).attr('title').replace(' ', '');
        if (title.localeCompare(labelMetric2) == 0) {
          break;
        }
      }

      for (i = 0; i < alllines.length; ++i) {
        var metric2 = $($(($(alllines[i])).find('td')[k])).text().replace(' ', '');
        metric2 = Number(metric2);

        var toHide = true;
        if (metric2 >= metric2MoreThan && metric2 <= metric2LessThan) {
          toHide = false;
        }
        toHide && ($(alllines[i])).css('display', 'none');
      }
    }

  });

  // for cloud providers input tags
  $('div#cloud_selected input').change(function (e) {
    if (e.currentTarget.value !== "All" && e.delegateTarget.checked === true) {
      $('input#inputProviders').prop('checked', false);
    } else if (e.currentTarget.value === "All") {
      $('div#cloud_selected input').not(':first').prop('checked', false);
    }
  });


  function dateToSeconds(dateString) {
    
    var year = dateString.slice(24, 28);
    var month = dateString.slice(3, 6);
      
      switch (month) {
        case "Jan":
          month = "01";
          break;
        case "Feb":
          month = "02";
          break;
        case "Mar":
          month = "03";
          break;
        case "Apr":
          month = "04";
          break;
        case "May":
          month = "05";
          break;
        case "Jun":
          month = "06";
          break;
        case "Jul":
          month = "07";
          break;
        case "Aug":
          month = "08";
          break;
        case "Sep":
          month = "09";
          break;
        case "Oct":
          month = "10";
          break;
        case "Nov":
          month = "11";
          break;
        case "Dec":
          month = "12";
          break;
      }
      
    var day = dateString.slice(7, 9);
    var hour = dateString.slice(10, 18);

    // 2011-10-10T14:48:00
    var wholeDate3 = year + "-" + month + "-" + day + "T" + hour;

    var m = Date.parse(wholeDate3);
    return m / 1000;
  }

    function configureDateString(dateString) {
    
    var year = dateString.slice(24, 28);
    var month = dateString.slice(3, 6);
      switch (month) {
        case "Jan":
          month = "01";
          break;
        case "Feb":
          month = "02";
          break;
        case "Mar":
          month = "03";
          break;
        case "Apr":
          month = "04";
          break;
        case "May":
          month = "05";
          break;
        case "Jun":
          month = "06";
          break;
        case "Jul":
          month = "07";
          break;
        case "Aug":
          month = "08";
          break;
        case "Sep":
          month = "09";
          break;
        case "Oct":
          month = "10";
          break;
        case "Nov":
          month = "11";
          break;
        case "Dec":
          month = "12";
          break;
      }
    var day = dateString.slice(7, 9);
    var wholeDate = year + "-" + month + "-" + day;
    return wholeDate;
  }
});
