var components;
$(document).ready(function() {
	components = [];
	// for step 0
	$('.action-buttons .scratch').on('click', function(e) {
		$('#step0').hide('slide', {direction: 'left'}, 1000);

		setTimeout(function() {
			$('#step3').show('slide', {direction: 'right'}, 800);
		}, 1000);

		$('.wizard #menu_step_0').removeClass('current');
		$('.wizard #menu_step_3').addClass('current');
		$('#step3 .action-buttons #from3to2').remove();
		$('#step3 #notification_interm').hide();
		$('#XMIfieldInput').show();
	});

	$('.action-buttons .intermed').on('click', function(e) {
		if ($(this).hasClass('disabled'))
			return;
		$('#step0').hide('slide', {direction: 'left'}, 1000);
		setTimeout(function() {
			$('#step1').show('slide', {direction: 'right'}, 800);
		}, 1000);
		$('.wizard #menu_step_0').removeClass('current');
		$('.wizard #menu_step_1').addClass('current');
		$('#XMIfieldInput').hide();
	});

	// for step 1
	$('.action-buttons #from1to0').on('click', function(e) {
		$('#step1').hide('slide', {direction: 'right'}, 1000);

		setTimeout(function() {
			$('#step0').show('slide', {direction: 'left'}, 800);
		}, 1000);

		$('.wizard #menu_step_1').removeClass('current');
		$('.wizard #menu_step_0').addClass('current');
	});

	$('.action-buttons #from1to2').on('click', function(e) {
		components = [];
		$('#step1 input:checked').each(function() {
			components.push({name: this.name, id: this.value});
		});
		if (components.length == 0) {
			$('#step1 p.error').show();
			setTimeout(function() {
				$('#step1 p.error').hide();
			}, 5000);
		} else {
			$('#step1').hide('slide', {direction: 'left'}, 1000);
			setTimeout(function() {
				$('#step2').show('slide', {direction: 'right'}, 800);
			}, 1000);
			createComponents(components);
			$('#step2 select.deployed_on_options').val("VM");
			//$('#step2 input[value="VM"]').click();
			$('.wizard #menu_step_1').removeClass('current');
			$('.wizard #menu_step_2').addClass('current');
		}
	});

	//for step 2
	$('.action-buttons #from2to1').on('click', function(e) {
		$('#step2').hide('slide', {direction: 'right'}, 1000);

		setTimeout(function() {
			$('#step1').show('slide', {direction: 'left'}, 800);
		}, 1000);

		$('.wizard #menu_step_2').removeClass('current');
		$('.wizard #menu_step_1').addClass('current');
	});

	$('.action-buttons #from2to2_1').on('click', function(e) {

		$('#step2').hide('slide', {direction: 'left'}, 1000);
		setTimeout(function() {
			$('#step2_1').show('slide', {direction: 'right'}, 800);
		}, 1000);

		$('.wizard #menu_step_2').removeClass('current');
		$('.wizard #menu_step_2_1').addClass('current');
	});

	// for step 2_1
	$('.action-buttons #from2_1to2').on('click', function(e) {
		$('#step2_1').hide('slide', {direction: 'right'}, 1000);

		setTimeout(function() {
			$('#step2').show('slide', {direction: 'left'}, 800);
		}, 1000);

		$('.wizard #menu_step_2_1').removeClass('current');
		$('.wizard #menu_step_2').addClass('current');
	});

	$('.action-buttons #from2_1to3').on('click', function(e) {
		var none_selected = $('select.connfrom option[value="none"]:selected, select.connto option[value="none"]:selected')
		if (none_selected.length !== 0) {
			$('#components_error').show();
			setTimeout(function() {
				$('#components_error').hide();
			}, 5000);
			return;
		}
		$('#step2_1').hide('slide', {direction: 'left'}, 1000);
		setTimeout(function() {
			$('#step3').show('slide', {direction: 'right'}, 800);
		}, 1000);
		$('.wizard #menu_step_2_1').removeClass('current');
		$('.wizard #menu_step_3').addClass('current');
	});

	//for step 3
	$('.action-buttons #from3to2_1').on('click', function(e) {
		$('#step3').hide('slide', {direction: 'right'}, 1000);
		var toStep = '#step0';
		if (components.length !== 0) {
			toStep = '#step2_1'
		}
		setTimeout(function() {
			$(toStep).show('slide', {direction: 'left'}, 800);
		}, 1000);

		$('.wizard #menu_step_3').removeClass('current');
		if (components.length !== 0)
			$('.wizard #menu_step_2_1').addClass('current');
		else
			$('.wizard #menu_step_0').addClass('current');
	});
	//for table with components for step 2 
	$('#components_connections table').on('change', 'select.deployed_on_options'/*'input[type="radio"]'*/, function($e) {
		$(this).parent().children('input').attr('ischecked', 'no');
		$(this).attr('ischecked', 'yes');
		if (this.value !== "VM") {
			$($e.target.parentElement).next().find('select.providers').hide();
			$($e.target.parentElement).next().next().find('select').hide();
			$($e.target.parentElement).next().find('select.vms_selection').show();
		} else {
			$($e.target.parentElement).next().find('select.providers').show();
			$($e.target.parentElement).next().find('select.vms_selection').hide();
		}
	});

	$('#components_connections table').on('change', 'select.providers', function($e) {
		var optionSelected = $("option:selected", this);
		var valueSelected = this.value;
		if (valueSelected != "None") {
			$($e.target.parentElement).next().find('select').show();
		} else {
			$($e.target.parentElement).next().find('select').hide();
		}
	});

	// for adding connection in step 2_1
	$("#step2_1").on('click', '#add_connection', function() {
		var cmp = components;
		var $contnt = $('#step2_1 #components_communication table');
		var htmlcont = "";
		htmlcont += "<tr><td><select class='connfrom'>";
		htmlcont += "<option value='none' name='connection_from'>none</option>";
		$.each(cmp, function(k, v) {
			htmlcont += "<option value='" + v.id + "' name='connection_from'>"
							+ v.name + "</option>";
		});
		htmlcont += "</select></td>";
		htmlcont += "<td><select class='connto'>";
		htmlcont += "<option value='none' name='connection_from'>none</option>";
		$.each(cmp, function(k, v) {
			htmlcont += "<option value='" + v.id + "' name='connection_from'>"
							+ v.name + "</option>";
		});
		htmlcont += "</select></td>";
		htmlcont += "<td><input type='number' name='port'  min='1' max='65536' placeholder='1-65536' style='height: inherit;'></td>" +
						"<td><input type='checkbox' name='mandatory'></td>" +
						"<td><span class='glyphicon glyphicon-remove remove_button' aria-hidden='true' title='remove connection'></span></td>" +
						"</tr>";
		$contnt.append(htmlcont);
	});

	// for not connection is the same in to and from fields
	$("#step2_1").on('change', '.connfrom', function() {
		var optionSelected = $("option:selected", this).attr('value');
		$(this).parent().next().find('select.connto option').removeAttr('disabled');
		var $otherSelect = $(this).parent().next().find('select.connto option[value="' + optionSelected + '"]');
		if ($otherSelect.filter(':selected').length !== 0) {
			$otherSelect.filter(':selected').prop('selected', false);
		} else {
			if (optionSelected !== 'none')
				$otherSelect.attr('disabled', 'disabled');
		}
	});

	$("#step2_1").on('change', '.connto', function() {
		var optionSelected = $("option:selected", this).attr('value');
		$(this).parent().prev().find('select.connfrom option').removeAttr('disabled');
		var $otherSelect = $(this).parent().prev().find('select.connfrom option[value="' + optionSelected + '"]');
		if ($otherSelect.filter(':selected').length !== 0) {
			$otherSelect.filter(':selected').prop('selected', false);
		} else {
			if (optionSelected !== 'none')
				$otherSelect.attr('disabled', 'disabled');
		}
	});

	// for deleting a connection
	$("#step2_1").on('click', 'span.remove_button', function() {
		$(this).parent().parent().remove();
	});

});

//for action before submit
function submit_form() {
	if (!is_valid_form())
		return false;
	if (components.length === 0) {
		$('form.elgg-form-editor-add-with-components').submit();
		return true; // is not a baseline model
	}
	$('#step3').hide('slide', {direction: 'left'}, 1000);
	setTimeout(function() {
		$('#step_final').show('slide', {direction: 'right'}, 800);
	}, 1000);
	createJsonFromInput();
	return false;
}

function is_valid_form() {
	var is_valid = true;
	if ($("#step3 #inputName").val() === "") {
		$("#step3 #app_name_empty").css('visibility', 'visible');
		$("#step3 #inputName").focus();
		setTimeout(function() {
			$("#step3 #app_name_empty").css('visibility', 'hidden');
		}, 5000);
		is_valid = false;
	}
	if ($("#step3 #inputDescription").val().length < 50) {
		$("#step3 #description_empty").css('visibility', 'visible');
		setTimeout(function() {
			$("#step3 #description_empty").css('visibility', 'hidden');
		}, 5000);
		is_valid = false;
	}
	if ($("#step3 #inputVersion").val() === "") {
		$("#step3 #version_empty").css('visibility', 'visible');
		setTimeout(function() {
			$("#step3 #version_empty").css('visibility', 'hidden');
		}, 5000);
		is_valid = false;
	}
	return is_valid;
}

function createJsonFromInput() {
	var application = {};

	application.application = $("#step3 input#inputName").val();
	application.description = $("#step3 textarea#inputDescription").val();
	application.version = $("#step3 input#inputVersion").val();
	application.user = elgg.get_logged_in_user_entity().name;
	application.components = [];
	$.each(components, function(k, v) {
		var cmp = {};
		cmp.name = v.name;
		var deploy = $('#step2 table tr:not(:first-child)'); // get all step2 rows exept the header.
		$.each(deploy, function() {
			if ($('td:first', this).text() === v.name) {
				var where = $('td:nth-child(2) option:selected', this).attr("value");
				if (where === "VM") {
					cmp.cloud = $('td:nth-child(3) select.providers option:selected', this).text() // provider
					cmp.VM = $('td:nth-child(4) select.vm_types option:selected', this).text(); //vm type
				} else if (where === "Colocated") {
					cmp.colocated = $('td:nth-child(3) select.vms_selection option:selected', this).text(); //other component
				} else {
					cmp.deployed = $('td:nth-child(3) select.vms_selection option:selected', this).text(); //other component
				}
			}
		});
		// connections
		var connections = $('#step2_1 table tr:not(:first-child)');
		$.each(connections, function() {
			if (cmp.name === $('.connfrom option:selected', this).text()) {
				var connection_item = {};
				if (cmp.connected === undefined) {
					cmp.connected = [];
				}
				connection_item.communicationName = $('.connto option:selected', this).text();
				connection_item.mandatory =
								$('input[name="mandatory"]:checked', this).length >= 1 ? true : false;
				connection_item.port = $('input[name="port"]', this).val();
				cmp.connected.push(connection_item);
			}
		});
		application.components.push(cmp);
	});
	console.log('json created!!!');
	console.log(JSON.stringify(application));
	$.ajax({
		url: elgg.get_site_url() + "request_for_xmi.php",
		type: "POST",
		data: {app: application},
		success: function(data, textStatus, jqXHR) {
			console.log(data);
			download(data, application.application + '_baseline.xmi', 'text/xml');
			$("#step_final #processing_xmi" ).hide();
			$("#step_final img.waiting" ).hide();
			$("#step_final #done_xmi" ).show();
		
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log("error to communication for error")
		}
	});
	/*$.getJSON('139.91.92.47:6066', application, function(data) {
	 console.log(data);
	 });*/
}

// step 2
function createComponents(cmp) {
	$contnt = $('#components_connections table');
	$contnt.html('<tr><th>Component</th><th>Deployed on</th><th></th>' +
					'<th></th></tr>');
	var i = 0;
	$.each(cmp, function(k, v) {
		var providers = "<select style='' class='providers'>" +
						"<option value='None'>None</option>" +
						"<option value='Flexiant'>Flexiant</option>" +
						"<option value='Amazon'>Amazon</option>" +
						"</select>";
		var other_comp = "<select style='display: none;' class='vms_selection'>";
		$.each(cmp, function(k, w) {
			if (w.name !== v.name) {
				other_comp += "<option name='vm_selection' value=" + w.name + ">" + w.name + "</option>";
			}
		});
		other_comp += "</select>";
		var vm_types = "<select style='display: none;' class='vm_types'>" +
						"<option value='m1.small'>m1.small</option>" +
						"<option value='m1.large'>m1.large</option>" +
						"<option value='m1.xlarge'>m1.xlarge</option>" +
						"</select>";
		$contnt.append("<tr><td>" + v.name + "</td>" +
						"<td><select class='deployed_on_options'>" +
						"<option name='hosted" + i + "' value='VM'>VM</option>" +
						"<option name='hosted" + i + "' value='Component'>Component</option>" +
						"<option name='hosted" + i++ + "' value='Colocated'>Colocated</option>" +
						"</select></td>" +
						//"<td><input type='radio' name='hosted" + i + "' value='VM'>VM<br>" +
						//"<input type='radio' name='hosted" + i++ + "' value='Component'>Component</td>" +
						"<td>" + providers + other_comp + "</td>" +
						"<td>" + vm_types + "</td></tr>");
	});
}

function download(text, name, type) {
  var a = document.getElementById("download_baseline_xmi");
  var file = new Blob([text], {type: type});
  a.href = URL.createObjectURL(file);
  a.download = name;
}