/**
 * On model view hover on div show another div.
 * library: draw_app.myscripts
 * @author Christos Papoulas 
 */

var $review = 0;
var review_id = 0;

$(document).ready(function () {
    console.log("models-library.js");

    // focus on specific execution
    var url = window.location.pathname.split("/");
    var execution = url[url.length - 1];
    if (url[url.length - 2] === 'runs' && execution !== "") {
        $trfocus = $('.' + execution + '');
        $trfocus.css('background-color', '#aaeeea');
    }
    // code for expand and collapse div
    $(".model-expand-button").click(function (e) {
        e.stopPropagation();
        console.log("click function called");
        $('.model-expand-info').hide();
        $(this).parent().next().slideToggle();
    });
    $(".model-collapse-button").click(function (e) {
        e.stopPropagation();
        $(this).parent().slideToggle();
    });

    $('body').on('click', function () {
        $('.model-expand-info').hide();
    });

    // code for deployed/undeployed sidebar section
    $('input[type=radio][name=Deployed]').change(function () {
        if (this.value === 'TRUE')
            $('.sidebar-section.deployed-query').show();
        else
            $('.sidebar-section.deployed-query').hide();
    });

    //code for expand recommended
    $('.recommended-expand').click(function (e) {
        $('#recommended-models-collapsed').hide();
        $('#recommended-models-expanded').slideDown();
    });
    $('.recommended-collapse').click(function (e) {
        $('#recommended-models-expanded').hide();
        $('#recommended-models-collapsed').show();
    });

    //delete a tag
    $('#skill-tags-container .skill-tag .skill-tag-delete-btn').on('click', function () {
        var id = $(this).attr('data-uid');
        var $skill = $(this);
        elgg.action('draw_app/tags/delete', {
            data: {
                id: id
            },
            success: function () {
                $skill.parent().remove();
            },
            beforeSend: function () {
                $skill.parent().css('background-color', 'red');
            }
        });
    });

    // for upload model xmi with modal
    $('.files a.upload_file').on('click', function () {
        $('#upload-modal').modal();
    });

    // for watch a model in application full view
    $('.application-action-icons .one-icon .watch').on('click', function () {
        var id = $(this).attr('data-uid');
        elgg.action('model/watch', {
            data: {
                id: id
            },
            success: function (data) {
                if (data.output.watch === true) {
                    elgg.system_message("You start watching this app", 1000);
                    $(".application-action-icons .one-icon .watch .notwatching").hide();
                    $(".application-action-icons .one-icon .watch .watching").show();
                } else {
                    elgg.system_message("You stop watching this app", 1000);
                    $(".application-action-icons .one-icon .watch .notwatching").show();
                    $(".application-action-icons .one-icon .watch .watching").hide();
                }
            },
            beforeSend: function () {

            }
        });
    });
    $('.badge.appwatch').on('click', function () {
        var $app = $(this);
        var id = $(this).attr('data-uid');
        elgg.action('model/watch', {
            data: {
                id: id
            },
            success: function (data) {
                if (data.output.watch === true) {
                    elgg.system_message("You start watching this app", 1000);
                    $app.find(".notwatching").hide();
                    $app.find(".watching").show();
                } else {
                    elgg.system_message("You stop watching this app", 1000);
                    $app.find(".notwatching").show();
                    $app.find(".watching").hide();
                }
            },
            beforeSend: function () {

            }
        });
    });

    // for delete a review 
    $('.edit-review span a.delete-review').on('click', function () {
        $review = $(this).parent().parent().parent().parent();
        review_id = $(this).attr('data-uid');
        $('#review_delete').modal("show");
    });

    // for tags all, append selected to form
    $('#saveTags').on('click', function () {
        var ch = $("#AllTagsModal .modal-body input:checked").map(function () {
            return "<input type='checkbox' value='" + this.value + "' checked> " + this.value + "<br/>"
        }).get();
        $(".sidebar-section.tags .tags-container").html(ch);
        console.log(ch);
    });

    // for expand - collapse rows
    $('#app-runs-table tbody tr').on('click', function () {
        $('#modal' + $(this).attr('id')).modal('toggle');
//    $('tr.expand-execution-table-row').slideUp();
//    $(this).next().toggle();
    });
}); // end of document ready

//code for expand collapse execution row
/*function execution_row_toggle(e) {
 $('tr.expand-execution-table-row').slideUp();
 //$('.expand-execution-row span').css("-webkit-transform", "rotate(0deg)");
 
 var parent_id = e.parent().parent().attr('id');
 var $toshow = $('#e' + parent_id);
 e.parent().parent().after($toshow);
 $toshow.slideDown();
 e.css("-webkit-transform", "rotate(89deg)");
 }*/

// code for add tag form 
function add_tag(model_id) {
    var tag = $('.add-skill').val();
    if (tag === '' || typeof tag === 'undefined') {
        return false;
    }
    elgg.action('draw_app/tags/add', {
        data: {
            tag: tag,
            model_id: model_id
        },
        success: function (resultText, success, xhr) {
            $('#skill-tags-container').append('<div class="skill-tag" >' +
                    '<span class="skill-tag-name">' + tag + '</span>' +
                    '<span class="skill-tag-delete-btn" data-uid="' + resultText.output + '">&times;</span>' +
                    '</div>');
            $('#skills-form').css('display', 'block');
            $('.loading_icon').remove();
        }, beforeSend: function () {
            $('#skills-form').parent().append('<img src="../../_graphics/ajax_loader_bw.gif" class="loading_icon">');
            $('#skills-form').css('display', 'none');
        }
    });
    return false;
}

function delete_review() {
    elgg.action('reviews/delete', {
        data: {
            guid: review_id
        },
        success: function () {
            $review.hide(1000);
        }
    });
}
