<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


elgg_register_event_handler('init', 'system', 'tags_init', 1001);

function tags_init() {
  tags_register_actions();
  tags_register_libs();
}

function tags_register_actions() {
  $base = elgg_get_plugins_path() . 'tags/actions/';
  elgg_register_action('tags/add/skill', $base . 'add/skill.php');
  elgg_register_action('tags/delete/skill', $base . 'delete/skill.php');
  
  elgg_register_action('tags/add/interest', $base . 'add/interest.php');
  elgg_register_action('tags/delete/interest', $base . 'delete/interest.php');
}

function tags_register_libs() {
  $base = elgg_get_plugins_path() . '/tags/lib/';

  elgg_register_library('tags.tags', $base . '/tags.php');
}