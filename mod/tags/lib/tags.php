<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Creates a new tag skill (if not already exists) and returns the entity.
 * 
 * @param type $skill String the skill.
 * @return \ElggObject The ElggObject or NULL on failure.
 */
function tags_create_skill($skill, $subtype = 'skill') {
  $options = array(
    'types'     => 'object',
    'subtypes'  => $subtype,
  );
  $tags = elgg_get_entities($options);
  
  foreach ($tags as $t) {
    if($t->name === $skill) {
      return $t;
    }
  }
  // tag doesn't exist create new
  $new = new ElggObject();
  $new->type = 'object';
  $new->subtype = $subtype;
  $new->name = $skill;
  $new->owner_guid = elgg_get_logged_in_user_guid();
  $new->access_id = ACCESS_PUBLIC;
  $guid = $new->save();
  if($guid) {
    return $new;
  } else { // something went wrong
    return NULL;
  }
}
