<?php 
/**
 * action: tags/delete/interest
 * @uses skill String from input.
 */
$interest_s = get_input('id');
if($interest_s === NULL) {
  return FALSE;
}

$options = array(
  'types' => 'object',
  'subtypes' => 'interest',
);
$tags = elgg_get_entities($options);
$user_guid = elgg_get_logged_in_user_guid();
$relationship = 'has_interest';
$interest_e = NULL;
foreach ($tags as $t) {
  if ($t->name === $interest_s) {
    $interest_e = $t;
    break;
  }
}

if( $interest_e && check_entity_relationship( $user_guid, $relationship, $interest_e->getGUID() ) ) {
  remove_entity_relationship( $user_guid, $relationship, $interest_e->getGUID() );
  return TRUE;
}

return FALSE;

