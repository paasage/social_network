<?php

/**
 * action: tags/delete/skill
 * @uses skill String from input.
 */
$skill_s = get_input('id');
if($skill_s === NULL) {
  return FALSE;
}
$options = array(
  'types' => 'object',
  'subtypes' => 'skill',
);
$tags = elgg_get_entities($options);
$user_guid = elgg_get_logged_in_user_guid();
$relationship = 'has_skill';
$skill_e = NULL;
foreach ($tags as $t) {
  if ($t->name === $skill_s) {
    $skill_e = $t;
    break;
  }
}

if( $skill_e && check_entity_relationship( $user_guid, $relationship, $skill_e->getGUID() ) ) {
  remove_entity_relationship( $user_guid, $relationship, $skill_e->getGUID() );
  return TRUE;
}

return FALSE;