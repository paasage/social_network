<?php
/**
 * action: tags/add/interest
 */


$interest_s = get_input('interest');
if ($interest_s === '') {
  return FALSE;
}
elgg_load_library('tags.tags');
$relationship = 'has_interest';
$user_guid = elgg_get_logged_in_user_guid();

$interest_e = tags_create_skill($interest_s, 'interest');
if ($interest_e === null) {
  return FALSE;
} else {
  
  if(check_entity_relationship( $user_guid, $relationship, $interest_e->getGUID() ) === FALSE) {
    add_entity_relationship( $user_guid, $relationship, $interest_e->getGUID() );
    echo $interest_s;
    return TRUE;
  } else {
    return FALSE;
  }
}


return FALSE;
