<?php

/**
 * action: tags/add/skill
 * @uses skill String from input.
 */
$skill_s = get_input('skill');
if ($skill_s === '') {
  return FALSE;
}
elgg_load_library('tags.tags');
$relationship = 'has_skill';
$user_guid = elgg_get_logged_in_user_guid();
$user_entity = get_entity($user_guid);

$skill_e = tags_create_skill($skill_s);
if ($skill_e === null) {
  return FALSE;
} else {
  
  if(check_entity_relationship( $user_guid, $relationship, $skill_e->getGUID() ) === FALSE) {
    add_entity_relationship( $user_guid, $relationship, $skill_e->getGUID() );
    echo $skill_s;
    return TRUE;
  } else {
    return FALSE;
  }
  
}


return FALSE;
