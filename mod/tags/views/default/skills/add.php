<?php
/**
 * 
 * appendedInputButton
 * @uses $vars['full_view']
 */

?>
<form onsubmit="return add_skill();" action="#"<?php if(!$vars['full_view']) {?> id="skills-form" <?php } ?> >
	<input class="add-skill" id="skills" type="text" placeholder="Add a skill" name="skill" style="margin: 0;">
	<input class="skills-add-button" type="submit" value="Add" style="margin: 0;">
</form>