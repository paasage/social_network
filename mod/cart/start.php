<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

elgg_register_event_handler('init', 'system', 'initialize_cart_plugin');

function initialize_cart_plugin() {
	elgg_register_page_handler('cart', 'cart_page_handler');
	
	cart_register_actions();
	
	cart_register_library();
	
	cart_register_topbar_icon();
	
	cart_register_js();
	
	elgg_load_js('cart.cart.js');
}

function cart_page_handler($page) {
	elgg_load_library('cart.main_library.lib');
	$base = elgg_get_plugins_path();
	switch ($page[0]) {
		case 'add':
			gatekeeper();
			cart_add();
			break;
		case 'show':
			gatekeeper();
			cart_show();
			break;
		default:
			break;
	}
}

function cart_register_actions() {
	$base = elgg_get_plugins_path() . 'cart/actions/cart/';
	
	elgg_register_action("cart/deletecartitem", $base . "deletefromcart.php", 'logged_in');
	elgg_register_action('cart/add2cart', $base . 'add2cart.php', 'logged_in');
	elgg_register_action('cart/showcart', $base . 'show.php', 'logged_in');
}

function cart_register_library() {
	$base = elgg_get_plugins_path();
	
	$l1 = '/cart/lib/main_library.php';
  elgg_register_library('cart.main_library.lib', $base . $l1 );
}

function cart_register_topbar_icon() {
	if(!elgg_is_logged_in()) { return ; }
	$text = elgg_view('topbar/content');
	
	$params = array(
		'name' => 'cart',
		'text' => $text,
		'href' => 'myarea',
		'link_class' => 'cart-link-no-display'
	);
	elgg_register_menu_item('topbar', $params);
}

function cart_register_js() {
	$url1 = 'mod/cart/js/cart/cart_js.js';
  elgg_register_js('cart.cart.js', $url1, 'footer');
}
