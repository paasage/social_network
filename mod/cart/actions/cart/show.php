<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$user_guid = elgg_get_logged_in_user_guid();
$user_entity = elgg_get_logged_in_user_entity();
$annotations = $user_entity->getAnnotations(
        'cart'
);
$site = elgg_get_site_url();

$content = "<div class='list'>"
        . "<h4>Models list</h4>";
foreach ($annotations as $annotation) {
  
  $entity = get_entity($annotation->value);
  if ($entity) {
    $app_url = $entity->getURL();
    if ($entity->getSubtype() == "draw_application") {
      $model_icon = $site . '_graphics/model-icon-general.png';
      $content .= "<div class='cart-item'>";
      $content .= "<img src='" . $model_icon . "'>";
      $content .= "<a href='" . $app_url . "' class='app-name'>";
      $content .= $entity->appname;
      $content .= "</a>";
      $content .= "<a href=\"javascript:deleteElemet(" . $annotation->id . ");\" class='delete-app' data-uid='" . $annotation->id . "'>&times;</a>";
      $content .= '<a type="button" class="download_file btn-info" href="' . $site . 'draw_app/download/' . $entity->getGUID() . '">
					edit
				</a>';
      $content .= "</div>";
    } else if($entity->getSubtype() == 'software_component') {
      $app_url = $entity->getURL();
      $model_icon = $site . '_graphics/model-icon-general.png';
      $content .= "<div class='cart-item'>";
      $content .= "<img src='" . $model_icon . "'>";
      $content .= "<a href='" . $app_url . "' class='app-name'>";
      $content .= $entity->compname;
      $content .= "</a>";
      $content .= "<a href=\"javascript:deleteElemet(" . $annotation->id . ");\" class='delete-app' data-uid='" . $annotation->id . "'>&times;</a>";
      $content .= '<a type="button" class="download_file btn-info" href="' . $site . 'draw_app/download/' . $entity->getGUID() . '">
					edit
				</a>';
      $content .= "</div>";
    }
  }
}
$content .= "<div id='empty-cart-topbar'>" . "</div>";
$content .= elgg_view('output/url', array(
	href => $site . 'draw_app/editor',
	text => "<button class='btn-default btn'>Create Application Model</button>",
	style => '  position: absolute;  bottom: 0;  left: 25%;'
));
$content .= "</div>";



echo $content;
