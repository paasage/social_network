<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$model_guid = get_input('model_guid');
$user_guid = elgg_get_logged_in_user_guid();
$user_entity = elgg_get_logged_in_user_entity();

$user_entity->annotate('cart', $model_guid, ACCESS_PUBLIC, $user_guid, 'int');

$model = get_entity($model_guid);

if($model->use == NULL) {
  $model->use = 0;
}

$model->use = $model->use + 1;
$model->save();

forward('cart/show');