<?php
/**
 * 
 * library: cart.main_library.lib
 */

function cart_add() {
	$content = elgg_view_form('cart/add2cart');
	$body = elgg_view_layout('one_column', array('content' => $content));
	$title = '<h3>' . 'Add to cart' . '</h3>';
	echo elgg_view_page($title, $body);
}

function cart_show() {
	$user_guid = elgg_get_logged_in_user_guid();
	$user_entity = elgg_get_logged_in_user_entity();

	$annotations = $user_entity->getAnnotations(
		'cart'
	);
	$content = elgg_list_annotations(array('name' => 'cart', 'guid' => $user_guid));
	$body = elgg_view_layout('one_column', array('content' => $content));
	$title = '<h3>' . 'show cart' . '</h3>';
	echo elgg_view_page($title, $body);
}

function cart_view_annotation_list($annotations, array $vars = array()) {
	$defaults = array(
		'items' => $annotations,
		'list_class' => 'elgg-list-annotation',
		'full_view' => true,
		'offset_key' => 'annoff',
	);

	$vars = array_merge($defaults, $vars);

	return elgg_view('page/components/list-cart', $vars);
}


