$(document).ready(function () {
  console.log('cart_js.js');

  // add to cart in full view
  $('a#use').on('click', function () {
    var model_guid = $(this).attr('data-uid');
    var $actionDiv = $('.add2cart[data-uid="' + model_guid + '"]');

    elgg.action('cart/add2cart', {
      data: {
        model_guid: model_guid
      },
      success: function () {
        elgg.system_message("The application model added to your cart");
      },
      beforeSend: function () {
        $actionDiv.show("slow");
        setTimeout(function () {
          $actionDiv.hide();
        }, 5000);
      }
    });
  });

  // add action for cart
  $('.models-info .badge.addtocart').on('click', function () {
    var model_guid = $(this).attr('data-uid');
    var $actionDiv = $('.add2cart[data-uid="' + model_guid + '"]');

    elgg.action('cart/add2cart', {
      data: {
        model_guid: model_guid
      },
      success: function () {
        console.log(model_guid + ' is added');
      },
      beforeSend: function () {
        $actionDiv.show("slow");
        setTimeout(function () {
          $actionDiv.hide();
        }, 5000);
      }
    });
  });

  // show cart 
  $('.cart-topbar-img').on('click', function () {
    if ($('.cart-topbar-container').html() === '') {
      elgg.action('cart/showcart', {
        success: function (resultText, success, xhr) {
          $('.cart-topbar-container').html(resultText.output);
          $('.cart-topbar-container').show();
          $('li.elgg-menu-item-cart').css('background-color', '#475071');
        }
      });
    } else {
      hideCart();
    }
  });

  $(document).on('click', function (e) {
    if (!$(e.target).is('.delete-app')) {
      hideCart();
    }
  });
});

function hideCart() {
  $('.cart-topbar-container .list').remove();
  $('li.elgg-menu-item-cart').css('background-color', '#2b3042');
}

function deleteElemet(uid) {
  $cartItem = $('.cart-topbar-container .list .cart-item a[data-uid=' + uid + ']').parent();
  elgg.action('cart/deletecartitem', {
    data: {
      id: uid
    },
    success: function () {
      $cartItem.remove();
    },
    beforeSend: function () {
      $cartItem.css('background-color', 'red')
    }
  });
  return false;
}
