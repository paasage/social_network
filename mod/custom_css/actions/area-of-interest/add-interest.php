<?php
/**
 * 
 * Action
 */

$interest = get_input('interest');
$user_entity = elgg_get_logged_in_user_entity();
$user_guid = elgg_get_logged_in_user_guid();
$user_entity->annotate('user_interests', $interest, ACCESS_PUBLIC, $user_guid, 'string');

$annotations = $user_entity->getAnnotations(
								'user_interests'
							);
foreach ($annotations as $annotation) {
	if($annotation->value == $interest){
		echo $annotation->id;
		return TRUE;
	}
}

return FALSE;