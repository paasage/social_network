<?php
/**
 * 
 * Action for ajax request
 */

$annotation_id = get_input('id');
if(elgg_delete_annotation_by_id($annotation_id)) {
	echo TRUE;
} else {
	echo FALSE;
}