<?php

// UPLOAD the provider model to the paasage platrfom 
//elgg.system_message("credentials/uploadFlexiantProviderModel.php run successfully!");

// get the name of the cloud provider
$cloudProvider = get_input('cloudProvider');
//echo $cloudProvider;

// get the platform credentials
$currentUser = elgg_get_logged_in_user_entity();

$platformEndpoint = $currentUser->platformEndpoint;
$emailEndpoint = $currentUser->emailEndpoint;
$passwordEndpoint = $currentUser->passwordEndpoint;
$tenantEndpoint = $currentUser->tenantEndpoint;
//echo $platformEndpoint;
//echo("\n");
//echo $emailEndpoint;
//echo("\n");
//echo $passwordEndpoint;
//echo("\n");
//echo $tenantEndpoint;
//echo("\n");


// store the chosen provider model locally with the appropriate name
$target_path = elgg_get_plugins_path() . "draw_app/lib/XMIsToBeUploaded/";
$target_path = $target_path ."FlexiantProviderModel.xmi";
$data = $_REQUEST['val'];
$fp = fopen($target_path,'w'); //Prepends timestamp to prevent overwriting
fwrite($fp, $data);
fclose($fp);

// open the xmi file
$provider_xmifile = fopen("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/FlexiantProviderModel.xmi", "r") or die("Unable to open file!");
$provider_xmi = fread($provider_xmifile, filesize("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/FlexiantProviderModel.xmi"));
$provider_xmi_encoded = base64_encode($provider_xmi);
fclose($provider_xmifile);

//echo $provider_xmi_encoded;


// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint);
//echo("1: ");
//echo $token;
//echo("\n");
//echo $user_id;
//echo("\n");

// 2)create the paasage resource
$name = "CLOUD_PROVIDER";
if($cloudProvider == 'Flexiant') {
    $name = "FLEXIANT";
} else if ($cloudProvider == 'GWDG') {
    $name = "GWDG2";
} else if ($cloudProvider == 'AmazonEC2') {
    $name = "AmazonEC22";
} else if ($cloudProvider == 'Omistack') {
    $name = "Omistack2";
}

create_resource($platformEndpoint, $name, $token, $user_id);
//echo("2: ");
//echo("Resource created");
//echo("\n");

// 3)retrieve the primary key of the paasage resource
$resource_pk = get_resource_pk($platformEndpoint, $name, $token, $user_id);
//echo("3: ");
//echo($resource_pk);
//echo("\n");

// 4) upload the cloud provider xmi file to the paasage resource
if($cloudProvider == 'Flexiant') {
    upload_flexiant_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded);
    //echo("4: ");
    //echo upload_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded);
    //echo("\n");
} else if($cloudProvider == 'GWDG') {
    upload_GWDG_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded);
} else if($cloudProvider == 'AmazonEC2') {
    upload_amazonEC2_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded);
} else if($cloudProvider == 'Omistack') {
    upload_omistack_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded);
}

// 5) get the resourse state every 30 seconds until its value is not "UPLOADING_XMI"
do {
    
    sleep(4);

    //echo("5: ");
    $state2 = get_resource_state($platformEndpoint, $name, $token, $user_id);
    //echo($state2);
    //echo("\n");

} while (strcmp($state2, "UPLOADING_XMI") == 0);

echo($state2);



// functions
function authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint) {
    /**
    * a POST request to PaaSage platform API to retrieve: 
    *
    * 1) a token
    * 2) a userId
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/login";

    // this is the payload
    $data = array (
        'email' => $emailEndpoint,
        'password' => $passwordEndpoint,
        'tenant' => $tenantEndpoint
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    // execute the request
    $output = curl_exec($ch);

    // output the information - includes the header
    //echo($output);

    // retrieve token from the response
    $position = strpos($output, "token");
    $token = substr($output, $position+8, 205);
    //echo($token);

    // retrieve user_id from the response
    $position2 = strpos($output, "userId");
    $user_id = substr($output, $position2+8, 1);
    //echo($user_id);

    // close curl resource to free up system resources
    curl_close($ch);

    return array($token, $user_id);
}

function create_resource($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a POST request to PaaSage platform API to create paasage-resource 
    *
    * with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model";
    
    // this is the payload
    $data = array (
        "name" => $resource_name, 
        "state" => "CREATED", 
        "subState" => "empty", 
        "action" => "CREATE", 
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token, 
        'X-Auth-UserId: ' . $user_id, 
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function get_resource_pk($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the primary key 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;
    $url2 = $platformEndpoint . "api/model/";

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, $url2);
    $position2 = strpos($output, "rel");
    $resource_pk_length = ($position2-3) - ($position+strlen($url2));
    $resource_pk = substr($output, $position+strlen($url2), $resource_pk_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $resource_pk;
}

function upload_flexiant_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload a provider xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'upperware-models/fms/Flexiant',  //GWDG, AmazonEC2, Omistack
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $provider_xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function upload_GWDG_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload a provider xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'upperware-models/fms/GWDG',  //Felxiant, GWDG, AmazonEC2, Omistack
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $provider_xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function upload_amazonEC2_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload a provider xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'upperware-models/fms/AmazonEC2',  //Flexiant, GWDG, AmazonEC2, Omistack
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $provider_xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function upload_omistack_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload a provider xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'upperware-models/fms/Omistack',  //Flexiant, GWDG, AmazonEC2, Omistack
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $provider_xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function get_resource_state($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the state 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, "state");
    $position2 = strpos($output, "subState");
    $state_length = ($position2-3) - ($position+8);
    $state = substr($output, $position+8, $state_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $state;
}

?>