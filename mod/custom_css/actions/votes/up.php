<?php

/**
 *
 *  @uses topic the GUID of topic as input. 
 */
$topic_guid = get_input('topic');
$user_guid = elgg_get_logged_in_user_guid();
$result = array();
$result['downvote'] = 0;
$result['upvote'] = 0;

if ($user_guid == get_entity($topic_guid)->owner_guid) {

	echo json_encode($result);
}
$has_upvote = check_entity_relationship($topic_guid, 'up_vote', $user_guid);

if ($has_upvote === FALSE) {
	$has_downvote = check_entity_relationship($topic_guid, 'down_vote', $user_guid);
	if ($has_downvote !== FALSE) {
		remove_entity_relationship($topic_guid, 'down_vote', $user_guid);
		$result['downvote'] = -1;
	}
	add_entity_relationship($topic_guid, 'up_vote', $user_guid);
	$result['upvote'] = 1;
} else {
	remove_entity_relationship($topic_guid, 'up_vote', $user_guid);
	$result['upvote'] = -1;
}
echo json_encode($result);
return true;
