<?php
/**
 * Elgg add review action
 *
 * @package Elgg.Core
 * @subpackage Comments
 */

$entity_guid = (int) get_input('entity_guid');
$review_guid = get_input('review_guid');
$comment_text = get_input('generic_review');
$rating = get_input('rating');
if(!isset($review_guid)) {
  $rate_comment = new ElggObject();
  $rate_comment->type = 'object';
  $rate_comment->subtype = 'review_comment';
  
  $rate_comment->access_id = ACCESS_PUBLIC;
  $rate_comment->owner_guid = elgg_get_logged_in_user_guid();
  $rate_comment->application_reference = $entity_guid;
  //$rate_comment->container_guid = $entity_guid;
} else {
  $rate_comment = get_entity($review_guid);
}

$rate_comment->rating = ($rating == NULL) ? 0 : $rating;
$rate_comment->description = $comment_text;

$rate_comment_guid = $rate_comment->save();

if($rate_comment_guid) {
	system_message("Your rating post was saved");
        if(!isset($review_guid)) {
          elgg_trigger_event('notification', 'notification', $rate_comment);
          add_to_river('river/draw_app/create_review', 'create', $rate_comment->owner_guid, $rate_comment->application_reference);
        }
} else {
   register_error("The blog post could not be saved");
}

forward("draw_app/view/{$entity_guid}/reviews"); 
