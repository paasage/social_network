<?php

/**
 * Delete topic action
 *
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$entity_guid = (int) get_input('guid');

$entity = get_entity($entity_guid);
if (!$entity || !$entity->getSubtype() == "review_comment") {
  register_error("Try to delete something that is not a review_comment");
  forward(REFERER);
}
$user = elgg_get_logged_in_user_guid();
if (!($entity->owner_guid == $user) && !elgg_is_admin_user($user)) {
  register_error("No permissions to delete this reviews action");
  forward(REFERER);
}

$result = $entity->delete();
if ($result) {
  system_message("The review deleted");
} else {
  register_error("For some reason can't delete the review");
}


