<?php
/**
 * 
 */

elgg_load_library('custom_css.reviews.lib');

$user_guid = elgg_get_logged_in_user_guid();

$comment_guid = get_input('guid');
if(!isset($comment_guid )) {
	return FALSE;
}

$comment_entity = get_entity($comment_guid);
if(!$comment_entity){
	return FALSE;
}

if(can_vote_helpful($comment_guid, $user_guid)){
	$comment_entity->annotate('helpful_review', 'YES', ACCESS_PUBLIC, $user_guid, 'string');
}