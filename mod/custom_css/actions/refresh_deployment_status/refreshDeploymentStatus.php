<?php
elgg.system_message("refresh_deployment_status.php run successfully!");

elgg_load_library('restAPI');

// get the platform endpoint of the specific user
$currentUser = elgg_get_logged_in_user_entity();
$platformEndpoint = $currentUser->platformEndpoint;
$emailEndpoint = $currentUser->emailEndpoint;
$passwordEndpoint = $currentUser->passwordEndpoint;
$tenantEndpoint = $currentUser->tenantEndpoint;

// get the name of the model
$model_name = get_input('model_name');

// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint);

// 2)retrieve the state of the paasage resource
$state = get_resource_state($platformEndpoint, $model_name, $token, $user_id);
echo $state;
echo ";";

// 3)retrieve the state of the paasage resource
$substate = get_resource_substate($platformEndpoint, $model_name, $token, $user_id);
echo $substate;

?>