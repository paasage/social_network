<?php
/**
 * action: topbar/search
 * 
 * @param string search The search keywork
 */

$key = get_input('search');
if($key) {
	forward("search/{$key}");
}
forward('mystats/all');