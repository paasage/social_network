<?php
/**
 * 
 */


$skill = get_input('skill');
$user_guid = elgg_get_logged_in_user_guid();
$user_entity = get_entity($user_guid);
$user_entity->annotate('user_skills', $skill, ACCESS_PUBLIC, $user_guid, 'string');

$annotations = $user_entity->getAnnotations(
								'user_skills'
							);
foreach ($annotations as $annotation) {
	if($annotation->value == $skill){
		echo $annotation->id;
		return TRUE;
	}
}
