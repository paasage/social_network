<?php
/**
 * 
 * 
 */

/**
 * This function suggests users to logged in user.
 * 
 * @return array The suggested users.
 */
function suggest_friends() {
	global $CONFIG;
	$sfe_limit = 100;
	$guid = elgg_get_logged_in_user_guid();
	// retrieve all users friends
	$options = array(
		'type' => 'user',
		'relationship' => 'friend',
		'relationship_guid' => $guid,
		'wheres' => "u.banned = 'no'",
		'joins' => "INNER JOIN {$CONFIG->dbprefix}users_entity u USING (guid)",
		'order_by' => 'u.last_action DESC',
	  'limit' => 0,
	);
	$friends = elgg_get_entities_from_relationship($options);
	
	// generate a guids array
	$in = array($guid);
	if(is_array($friends) && count($friends) > 0){
		foreach ($friends as $friend) {
		  $in[] = $friend->guid;
		}
	}
	$in = implode(',', $in);

	$people = array();
	
	foreach ($friends as $friend) {
		// retrieve friends of each friend (discarding the users friends)
		$fof = elgg_get_entities_from_relationship(array(
			'type' => 'user',
			'relationship' => 'friend',
			'relationship_guid' => $friend->guid,
			'inverse_relationship' => TRUE,
			'wheres' => array(
				"e.guid NOT IN ($in)",
				"u.banned = 'no'"
			),
			'joins' => "INNER JOIN {$CONFIG->dbprefix}users_entity u USING (guid)",
			'order_by' => 'u.last_action DESC',
			'limit' => $sfe_limit
		));
		if (is_array($fof) && count($fof) > 0) {
			// populate $people
			foreach ($fof as $f) {
				if (isset($people[$f->guid])) {
					// if the current person is present in $people, increase the priority and attach the common friend entity
					$people[$f->guid]['mutuals'][] = $friend;
					++$people[$f->guid]['priority'];
				} else {
					$people[$f->guid] = array(
						'entity' => $f,
						'mutuals' => array($friend),
						'groups' => array(),
						'priority' => 0
					);
				}
			}
		}
	}
	
	return $people;
}