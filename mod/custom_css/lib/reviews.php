<?php

/**
 * 
 * 
 */
function custom_css_view_reviews($entity, $add_comment = true, array $vars = array()) {
  if (!($entity instanceof ElggEntity)) {
    return false;
  }

  $vars['entity'] = $entity;
  $vars['show_add_form'] = $add_comment;
  $vars['class'] = elgg_extract('class', $vars, "{$entity->getSubtype()}-reviews");

  $output = elgg_trigger_plugin_hook('reviews', $entity->getType(), $vars, false);
  if ($output) {
    return $output;
  } else {
    return elgg_view('page/elements/reviews', $vars);
  }
}

/**
 * Check if a user with $user_guid has mark a review with $comment_guid as 
 * helpfull
 * @param int $comment_guid The GUID of the review
 * @param int $user_guid The GUID of the user
 * @return boolean
 */
function can_vote_helpful($comment_guid, $user_guid) {
  $options = array(
      'guid' => $comment_guid,
      'annotation_names' => array('helpful_review'),
      'annotation_owner_guids' => array($user_guid)
  );
  $has_vote = elgg_get_annotations($options);
  if (empty($has_vote)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Counts how many users count the comment as helpful.
 * @param type $comment_guid The review / comment GUID
 * @return int 
 */
function count_helpful($comment_guid) {
  $options = array(
      'guid' => $comment_guid,
      'annotation_names' => array('helpful_review'),
      'annotation_calculation' => 'count'
  );
  return elgg_get_annotations($options);
}

/**
 * 
 * @param type $app_guid The GUID of the application
 * @return array if a useful review found.
 */
function get_most_helpful_review($app_guid) {
  $options = array(
      'metadata_names' => array('application_reference'),
      'metadata_values' => array($app_guid),
      'limit' => 10,
      'full_view' => FALSE
  );

  $reviews = elgg_get_entities_from_metadata($options);
  $most_useful = NULL;
  foreach ($reviews as $r) {
    $options = array(
        'guid' => $r->getGUID(),
        'annotation_names' => array('helpful_review'),
        'annotation_calculation' => 'count'
    );
    $count = elgg_get_annotations($options);
    if (!isset($most_useful)) {
      $most_useful['count'] = $count;
      $most_useful['review'] = $r;
    } else {
      if ($count > $most_useful['count']) {
        $most_useful['count'] = $count;
        $most_useful['review'] = $r;
      }
    }
  }
  return $most_useful;
}
