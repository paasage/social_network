<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function show_contact_list() {
  $title = "Contact List";

  $content = elgg_view('page/elements/contact_list');

  $sidebar = '';

  // layout the page
  $body = elgg_view_layout('one_sidebar', array(
      'content' => $content,
      'sidebar' => $sidebar,
  ));

  echo elgg_view_page($title, $body);
}
