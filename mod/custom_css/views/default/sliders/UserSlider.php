<?php
/**
 * 
 * @uses $vars['users'] 
 * @uses $vars['count'] 
 * @uses $vars['mutuals'] The array of mutuals friends for each user of array users.
 * @uses $vars['class-index']
 * @uses $vars['group-suggestions'] Bool | if is under groups suggestions shows the user
 * add button.
 */
$site_base = elgg_get_site_url();
$site = $site_base . '_graphics/user/';
$mutuals = $vars['mutuals'];
$message_icon = '<img src="' .
        $site_base .
        '_graphics/user/message-profile.png" class="profile-actions-image">';
$add_friend_icon = '<img src="' .
        $site_base .
        '_graphics/user/add-user-profile.png" class="profile-actions-image">';
$users = $vars['users'];
$count = $vars['count'];

$class = $vars['class-index'];
if ($users[0] == false) {
  return;
}
?>

<div class="connections-slider">
  <div class="connections-all-items" style="width: <?php echo $count; ?>px">
    <ul>
      <?php
      foreach ($users as $user) {
        echo '<li><img src="' . $user->getIconURL('small') . '" data-uid="' . $user->getGUID() . '"></li>';
      }
      ?>
    </ul>
  </div>
  <div class="controls-left"><img src="<?php echo $site; ?>slider-left.png"></div>
  <div class="controls-right"><img src="<?php echo $site; ?>slider-right.png"></div>

</div>

<div class="user-connection-info-div">
  <?php
  $i = 0;
  foreach ($users as $user) {
    $mes = $site_base . '/messages/compose?send_to=' . $user->getGUID();
    $add = $vars['group-suggestions'] ? elgg_view('output/url', array(
                'is_action' => true,
                'href' => "action/friends/add?friend={$user->getGUID()}",
                'text' => $add_friend_icon
            )) : '';
    echo '<div class="user-connection-info" data-uid="' . $user->getGUID() . '" style="display: none;">'
    . '<div class="friend-contributions-and-messages">'
    . '<a href="' . $user->getURL() . '"><h4>' . $user->name . '</h4></a>';
    if ($mutuals) {
      echo "<div class='mutuals'>{$mutuals[$i++]} mutuals</div>";
    }
    /* get user's models and components */
    $options = array(
        'count' => TRUE,
        'owner_guids' => array($user->getGUID()),
        'types' => array('object'),
        'subtypes' => array('draw_application')
    );

    $options_contr = array(
        'relationship' => 'is_contributor_to',
        'relationship_guid' => $user->getGUID(),
        'inverse_relationship' => FALSE,
        'count' => TRUE
    );

    $models_contri = elgg_get_entities_from_relationship($options_contr);

    $models = elgg_get_entities($options) + $models_contri;


    echo '<div><span>' . $models . ' models</span></div>'
    . '<div class="actions"><a href="' . $mes . '" >'
    . $message_icon
    . '</a>'
    . $add
    . '</div></div>'
    . '</div>';
  }
  ?>

</div>

<script>
  $(document).ready(function () {
    var width = <?php echo $count - 100; ?>;
    var curr = 0;
    var step = 115;
    var $container = $('.connections-slider .connections-all-items');
    $('.controls-right img').on('click', function () {
      if (curr - step <= -width)
        return;
      curr -= step;
      $container.css('left', curr + 'px');
      $('.user-connection-info').hide();
      $('.connections-slider .connections-all-items ul li img').attr('height', '40');
    });

    $('.controls-left img').on('click', function () {
      if (curr === 0)
        return;
      curr += step;
      $container.css('left', curr + 'px');
      $('.user-connection-info').hide();
      $('.connections-slider .connections-all-items ul li img').attr('height', '40');
    });

    $('.connections-slider .connections-all-items ul li img').on('click', function (e) {
      console.log('click on img: ' + this.getAttribute('data-uid'));
      $('.connections-slider .connections-all-items ul li img').attr('height', '40');
      $(this).attr('height', '50');
      $('.user-connection-info').hide();
      var uid = $(this).attr("data-uid");
      $('div[data-uid="' + uid + '"').show();
    });

  });
</script>