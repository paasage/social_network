<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 *
 * Elgg pageshell
 * The standard HTML page shell that everything else fits into
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['title']       The page title
 * @uses $vars['body']        The main content of the page
 * @uses $vars['sysmessages'] A 2d array of various message registers, passed from system_messages()
 */

// backward compatability support for plugins that are not using the new approach
// of routing through admin. See reportedcontent plugin for a simple example.
if (elgg_get_context() == 'admin') {
	if (get_input('handler') != 'admin') {
		elgg_deprecated_notice("admin plugins should route through 'admin'.", 1.8);
	}
	elgg_admin_add_plugin_settings_menu();
	elgg_unregister_css('elgg');
	echo elgg_view('page/admin', $vars);
	return true;
}

// render content before head so that JavaScript and CSS can be loaded. See #4032
$topbar = elgg_view('page/elements/topbar', $vars);
$messages = elgg_view('page/elements/messages', array('object' => $vars['sysmessages']));
$header = elgg_view('page/elements/header', $vars);
$body = elgg_view('page/elements/body', $vars);
$footer = elgg_view('page/elements/footer', $vars);

// Set the content type
header("Content-type: text/html; charset=UTF-8");

$lang = get_current_language();
global $MyConfig;
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<?php echo elgg_view('page/elements/head', $vars); ?>
<link href="<?php echo elgg_get_site_url() . "/mod/custom_css/main.css"; ?>" rel="stylesheet">
</head>
<body>
	<div class="elgg-page-messages">
		<?php echo $messages; ?>
		<?php echo $header; ?>
	</div>
	
	<div class="navbar navbar-fixed-top" role="navigation">
		<div class="navbar-inner">
		 <div class="container-fluid">
			 <?php 
					echo $topbar; 
				?>
		 </div>
		</div>
	</div>
	<?php echo $header; ?>

	<div class="container-fluid" style="padding: 70px 5px 0;" id="wrap">
            <?php echo $body; ?>
	</div>
    
        <div id="push"></div>
	<div class="elgg-page-footer" id="footer">
		<div class="elgg-inner">
			<?php echo $footer; ?>
		</div>
	</div>
	
<?php echo elgg_view('page/elements/foot'); ?>
</body>
</html>
