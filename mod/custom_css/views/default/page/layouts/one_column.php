<?php
/**
 * Elgg one-column layout
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['content'] Content string
 * @uses $vars['class']   Additional class to apply to layout
 * 
 * @author Christos Papoulas
 * 
 * This file have be edited and have removed almost all elgg classes.
 * The file is more simple now.
 */

$class = 'elgg-layout elgg-layout-one-column clearfix';
if (isset($vars['class'])) {
	$class = "$class {$vars['class']}";
}


if (isset($vars['title'])) {
	echo elgg_view_title($vars['title']);
}

echo $vars['content'];

