<?php
/**
 * This overwrites the default view of one_sidebar and adds a header of recom-
 * mended area
 * 
 * @author Christos Papoulas
 * 
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['content'] Content HTML for the main column
 * @uses $vars['sidebar'] Optional content that is displayed in the sidebar
 * @uses $vars['title']   Optional title for main content area
 * @uses $vars['class']   Additional class to apply to layout
 * @uses $vars['nav']     HTML of the page nav (override) (default: breadcrumbs)
 */

$class = '';//elgg-layout elgg-layout-one-sidebar clearfix';
if (isset($vars['class'])) {
	$class = "$class {$vars['class']}";
}

// navigation defaults to breadcrumbs
$nav = elgg_extract('nav', $vars, elgg_view('navigation/breadcrumbs'));

?>

<div  class="row ">
	<div class="span1 span_align">
		<?php
			echo $vars['recommended'];
		?>
	</div>
	<div class="span9 content">
		<?php
			echo $nav;

			if (isset($vars['title'])) {
				echo elgg_view_title($vars['title']);
			}
			if (isset($vars['content'])) {
				echo $vars['content'];
			}
		?>

	</div>
	<div class="span2">
		<?php
			echo $vars['sidebar'];
		?>
	</div>
</div>
