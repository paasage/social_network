<?php
/**
 * The full view of the application
 * 
 * @author Christos Papoulas
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['content'] Content HTML for the main column
 * @uses $vars['sidebar'] Optional content that is displayed in the sidebar
 * @uses $vars['title']   Optional title for main content area
 * @uses $vars['class']   Additional class to apply to layout
 * @uses $vars['nav']     HTML of the page nav (override) (default: breadcrumbs)
 * @uses $vars['model_guid'] The model GUID
 * @uses $vars['page-submenu'] The string id for the submenu
 * @uses $vars['graph'] The value of the metric for runs uptime | rt | throughput | cost.
 * @uses $vars['providers'] Filter based on providers seperated by comma
 * 
 */

$graph_params = $vars['euro_from'];

if (isset($vars['class'])) {
    $class = "$class {$vars['class']}";
}

// navigation defaults to breadcrumbs
$nav = elgg_extract('nav', $vars, elgg_view('navigation/breadcrumbs'));
$app = get_entity($vars['model_guid']);
$appname = $app->appname;
$version = (int) 0;
if (isset($app->version)) {
    $version = $app->version;
}
$runs = (int) 0;
if (isset($app->runs)) {
    $runs = $app->runs;
}
$views = (int) 0;
if (!isset($app->views)) {
    $app->views = (int) 0;
    $res = $app->save();
}
$views = $app->views;

$shares = (int) 0;
if (isset($app->shares)) {
    $shares = $app->shares;
}

$options_for_reviews = array(
    'metadata_names' => array('application_reference'),
    'metadata_values' => array($vars['model_guid']),
    'limit' => 65000,
    'count' => TRUE
);
$count = elgg_get_entities_from_metadata($options_for_reviews);
$options_for_reviews['count'] = FALSE;
$reviews_context = elgg_get_entities_from_metadata($options_for_reviews);

$rate_number = 0;
if ($count) {
    foreach ($reviews_context as $r) {
        $rate_number += $r->rating;
    }
    $rate_number /= $count;
    $rate_number = round($rate_number, 2);
}
$user_guid = elgg_get_logged_in_user_guid();
elgg_load_library('draw_app.contributors.library');
$can_upload = elgg_is_admin_logged_in() ||
        $app->getOwnerGUID() == $user_guid ||
        iscontributor($app->getGUID(), $user_guid);

echo elgg_view('modals/upload-xmi', array(appid => $vars['model_guid']));
?>

<div  class="row-fluid" id="application-full-view">
    <div class="row-fluid">
        <div class="span7 content full-application-view">
            <div class="application-full-with-description">
                <?php
                echo $nav;

                if (isset($vars['title'])) {
                    echo elgg_view_title($vars['title']);
                }
                if (isset($vars['content'])) {
                    echo $vars['content'];
                }
                echo $app->description;
                ?>
            </div>
            <div class=" application-mean-rate">
                <?php
                echo elgg_view(
                        'object/elements/models-rating-mean', array(
                    'users_number' => $count,
                    'rate_number' => $rate_number
                        )
                );
                echo elgg_view('page/elements/rating-slider', array(
                    'number_of_reviews' => $count,
                    'reviews_content' => $reviews_context
                        )
                );
                ?> 
            </div>
            <div class="files btn-group">
                <a  id="download_abstract_model" type="button" class="download_file btn-info" href="#" data-uid="<?=$vars['model_guid'];?>" name="<?=$appname;?>">
                    Download <i>abstract</i> model
                </a>
                <?php if ($can_upload) { ?>
                    <a  href="#" class="upload_file btn-warning" role="button" title="This is a first time upload. It cannot be used to overwrite an existed model. Use an online editor to overwrite an existed CAMEL model.">
                        Upload <i>abstract</i> model
                    </a>
                <?php } ?>
                <a  href="#" class="upload_file btn-warning" role="button" title="This is a first time upload of a deployable CAMEL model. It cannot be used to overwrite an existed model. Use an online editor to overwrite an existed CAMEL model.">
                        Upload <i>concrete</i> model
                </a>
            </div>
        </div>
        <div class="span2 application-action-icons">
            <?php
            echo elgg_view(
                    'page/elements/application-full-view-icons', array(
                'model_guid' => $vars['model_guid'],
                'version' => $version,
                'runs' => $runs,
                'views' => $views,
                'shares' => $shares
                    )
            );
            ?>
        </div>
        <div class="span3">	
            <?php
            echo $vars['sidebar'];
            ?>
        </div>
    </div>
    <div class="row-fluid">

        <?php
        echo elgg_view('navigation/models-submenu', array(
            'model_guid' => $vars['model_guid'],
            'page-submenu' => $vars['page-submenu'],
            'graph' => $vars['graph'],
            'users_rates' => $count,
            'rating_mean' => $rate_number,
            'providers' => $vars['providers']
        ));
        ?>
    </div>
</div>
