<?php
/**
 * This overwrites the default view of one_sidebar
 * 
 * @author Christos Papoulas
 * 
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['content'] Content HTML for the main column
 * @uses $vars['sidebar'] Optional content that is displayed in the sidebar
 * @uses $vars['title']   Optional title for main content area
 * @uses $vars['class']   Additional class to apply to layout
 * @uses $vars['nav']     HTML of the page nav (override) (default: breadcrumbs)
 * @uses $vars['recommended'] HTML for the top of the page
 * @uses $vars['carousel'] HTML for carousel of groups
 */

$class = '';//elgg-layout elgg-layout-one-sidebar clearfix';
if (isset($vars['class'])) {
	$class = "$class {$vars['class']}";
}

// navigation defaults to breadcrumbs
$nav = elgg_extract('nav', $vars, elgg_view('navigation/breadcrumbs'));
?>
<div  class="row-fluid">
	<?php 
		echo $vars['recommended'];
		echo $vars['carousel'];
	?>
	<div class="span9"  style="margin-left: 0">
		<?php
			echo $nav;

			if (isset($vars['title'])) {
				echo elgg_view_title($vars['title']);
			}
			if (isset($vars['content'])) {
				echo $vars['content'];
			}
		?>

	</div>
	<div class="span3">
		<?php
			echo $vars['sidebar'];
		?>
	</div>
</div>
