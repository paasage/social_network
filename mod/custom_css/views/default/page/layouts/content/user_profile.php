<?php
/**
 * 
 * Called by profile_page_handler.
 * 
 * The user profile layout.
 * 
 * @uses $vars['guid'] The GUID of User Entity.
 * 
 * @author Christos Papoulas
 */
gatekeeper();
$guid = $vars['guid'][0];
$entity = get_entity($guid);
$vars['size'] = 'medium';
$icon = elgg_view_entity_icon($entity, $size, $vars);
$site = elgg_get_site_url();
$user_name = $entity->username;

$options = array(
    'subject_guids' => $guid,
    'offset_key' => 'offset_activity',
    'offset_key' => 'offset_activity',
    'offset' => get_input('offset_activity')
);
$activity = elgg_list_river($options);

$group_option = array(
    'type' => 'object',
    'subtype' => 'groupforumtopic',
    'limit' => 5,
    'order_by' => 'e.last_action desc',
    'owner_guid' => $guid,
    'full_view' => false,
    'offset_key' => 'offset_discussion',
    'offset' => get_input('offset_discussion')
);

$discussions = elgg_list_entities($group_option);
if (!$discussions) {
  $discussions = 'No discussions';
}

$options = array(
    'type' => 'group',
    'relationship' => 'member',
    'relationship_guid' => $guid,
    'inverse_relationship' => false,
    'limit' => 3,
);
$my_groups = elgg_get_entities_from_relationship($options);
$my_groups_view = '';
if (!$my_groups) {
  $my_groups_view = "<p class='emphasized'>{$user_name} has not join any group yet.</p>";
} else {
  foreach ($my_groups as $g) {
    $my_groups_view .= elgg_view('groups/view_group', array('group_entity' => $g));
  }
}
elgg_load_js('draw_app.myscripts');

$top_contributions = elgg_list_entities(array(
    'type' => 'object',
    'subtype' => 'draw_application',
    'owner_guid' => $guid,
    'full_view' => false,
    'limit' => 3
        ), 'elgg_get_entities', 'draw_app_view_entity_list');
?>

<div  class="row-fluid" id="application-full-view">
  <div class="span9">

    <?php
    echo elgg_view('page/elements/profile/user-header', array(
        'icon' => $icon,
        'guid' => $guid
    ));
    ?>

    <div class="row-fluid"> <!-- Top contributions, discussions, activity -->
      <div class="row-fluid top-contributions-row">
        <h3>Top Contributions</h3>
        <h4><a href="#">My Contributions</a></h4>
      </div>
      <div class="profile-top-contributions"> <!-- Top contributions -->
        <?php
        echo $top_contributions;
        ?>
      </div>

      <div class="profile-top-discussions"> <!-- Top discussions -->
        <h3>Top Discussions</h3>
        <?php
        echo $discussions;
        ?>
      </div>

      <div class="profile-activity"> <!-- activity -->
        <h3>Activity Feed</h3>
        <?php echo $activity; ?>
      </div>

    </div>

  </div>

  <div class="span3 sidebar-user">

    <div class="user-stats-overview">
      <?php echo elgg_view('page/elements/user-stas', $vars); ?>
    </div>

    <div class="skills-overview">
      <h4>Skills</h4> <?php
				$surl = elgg_view('output/url', array(
					'text' => 'View All', 
					'class' => 'mystats_all', 
					'href' => 'mytags/skills/' . $guid)
				);
				echo $surl;
				echo elgg_view('page/elements/skills_tags', array('guid' => $guid)); 
		?>
    </div>

    <div class="areas-of-interest-overview">
      <h4>Areas Of Interest</h4>
      <?php 
			$surl = elgg_view('output/url', array(
					'text' => 'View All', 
					'class' => 'mystats_all', 
					'href' => 'mytags/interest/' . $guid)
				);
				echo $surl;
			echo elgg_view('page/elements/areas-of-interest', array('guid' => $guid)); 
			?>
    </div>

    <div class="my-connections-overview">
      <h4>Connections</h4>
      <span class="view-all-friends"><a href="<?php echo $site . 'friends/' . get_entity($guid)->username; ?>">View All</a></span>
      <?php
      echo elgg_view('page/elements/friend_connections', array('user_guid' => $guid));
      ?>
    </div>

    <div class="my-groups-overview">
      <h4>Groups</h4>
      <?php echo $my_groups_view; ?>
    </div>

  </div>
</div>
