<?php
/**
 * View a list of group items in carousel.
 *
 * @package Elgg
 *
 * @uses $vars['items']       Array of ElggEntity or ElggAnnotation objects
 * @uses $vars['offset']      Index of the first list item in complete list
 * @uses $vars['limit']       Number of items per page. Only used as input to pagination.
 * @uses $vars['count']       Number of items in the complete list
 * @uses $vars['base_url']    Base URL of list (optional)
 * @uses $vars['pagination']  Show pagination? (default: true)
 * @uses $vars['position']    Position of the pagination: before, after, or both
 * @uses $vars['full_view']   Show the full view of the items (default: false)
 * @uses $vars['list_class']  Additional CSS class for the <ul> element
 * @uses $vars['item_class']  Additional CSS class for the <li> elements
 * @uses $vars['count_px'] The pixels of carousel.
 */

$items = $vars['items'];
$offset = elgg_extract('offset', $vars);
$limit = elgg_extract('limit', $vars);
$count = elgg_extract('count', $vars);
$base_url = elgg_extract('base_url', $vars, '');
$pagination = elgg_extract('pagination', $vars, true);
$offset_key = elgg_extract('offset_key', $vars, 'offset');
$position = elgg_extract('position', $vars, 'after');

$list_class = 'elgg-list';
if (isset($vars['list_class'])) {
	$list_class = "$list_class {$vars['list_class']}";
}

$item_class = 'community-carousel-item';

$html = "";

if (is_array($items) && count($items) > 0) {
	foreach ($items as $item) {
		// show only groups that the user is not member
		if($item->isMember(elgg_get_logged_in_user_entity())){
			continue;
		}
		
		$li = view_group_entity($item, $vars);
		if ($li) {
			if (elgg_instanceof($item)) {
				$id = "elgg-{$item->getType()}-{$item->getGUID()}";
			} else {
				$id = "item-{$item->getType()}-{$item->id}";
			}
			$html .= "<div id=\"$id\" class=\"$item_class\">$li"
							. elgg_view('object/elements/groups/summery-hover-menu', array('group' => $item))
							. "</div>";
		}
	}
}

echo $html;

function view_group_entity($group, $vars) {
	$icon = elgg_view_entity_icon($group, 'small');
	// brief view
	$params = array(
		'entity' => $group,
		'metadata' => $metadata,
		'subtitle' => $group->briefdescription,
		'content' => elgg_get_excerpt($group->description, 300)
	);
	$params = $params + $vars;
	$list_body = elgg_view('object/elements/groups/summary', $params);
	
	return elgg_view_image_block($icon, $list_body, $vars);
}
?>

<script>
	$(document).ready(function(){
		var width = <?php echo $vars['count_px']*390;?>;
		var curr = 0;
		var step = 390;
		var $container = $('.community-inner-wrapper');
		$('.controls.arrow-right').on('click', function () {
			console.log('right clicked');
			if(curr - step <= -width)	return ;
			curr -= step;
			$container.css('margin-left', curr+'px');
		});
		
		$('.controls.arrow-left').on('click', function () {
			console.log('left clicked');			
			if(curr === 0) return ;
			curr += step;
			$container.css('margin-left', curr+'px');
		});
		
	});
</script>