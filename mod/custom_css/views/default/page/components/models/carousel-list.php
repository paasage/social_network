<?php
/**
 * View a list of items
 *
 * @package Elgg
 *
 * @uses $vars['items']       Array of ElggEntity or ElggAnnotation objects
 * @uses $vars['offset']      Index of the first list item in complete list
 * @uses $vars['limit']       Number of items per page. Only used as input to pagination.
 * @uses $vars['count']       Number of items in the complete list
 * @uses $vars['base_url']    Base URL of list (optional)
 * @uses $vars['pagination']  Show pagination? (default: true)
 * @uses $vars['position']    Position of the pagination: before, after, or both
 * @uses $vars['full_view']   Show the full view of the items (default: false)
 * @uses $vars['list_class']  Additional CSS class for the <ul> element
 * @uses $vars['item_class']  Additional CSS class for the <li> elements
 * 
 * @author Christos Papoulas
 */

$items = $vars['items'];
$offset = elgg_extract('offset', $vars);
$limit = elgg_extract('limit', $vars);
$count = elgg_extract('count', $vars);
$base_url = current_page_url();
$pagination = elgg_extract('pagination', $vars, true);
$offset_key = elgg_extract('offset_key', $vars, 'offset');
$position = elgg_extract('position', $vars, 'after');

$list_class = 'elgg-list';
if (isset($vars['list_class'])) {
	$list_class = "$list_class {$vars['list_class']}";
}

$item_class = 'elgg-item';
if (isset($vars['item_class'])) {
	$item_class = "$item_class {$vars['item_class']}";
}

$html = "";
$nav = "";

if ($pagination && $count) {
	$nav .= elgg_view('navigation/pagination_models', array(
		'base_url' => $base_url,
		'offset' => $offset,
		'count' => $count,
		'limit' => $limit,
		'offset_key' => $offset_key,
	));
}
$pixels = (count($vars['items']))*(300+7+15);
if (is_array($items) && count($items) > 0) {
	+$html .= '<div id="applications-carousel-container" class="clearfix" style="width:' . ($pixels + 100) . 'px;">';
	
	foreach ($items as $item) {
		$li = elgg_view_list_item($item, $vars);
		if ($li) {
			if (elgg_instanceof($item)) {
				$id = "elgg-{$item->getType()}-{$item->getGUID()}";
			} else {
				$id = "item-{$item->getType()}-{$item->id}";
			}
			$html .= '<div id="'. $id .'" class="post item model">'
							 . $li . '</div>';
		}
	}
	
	$html .= '</div>';
}

if ($position == 'before' || $position == 'both') {
	$html = $nav . $html;
}

if ($position == 'after' || $position == 'both') {
	$html .= $nav;
}

echo $html;

?>

<script>
$( document ).ready(function(){
		var width = <?php echo $pixels;?>;
		var curr = 0;
		var step = 326;
		var $container = $('#applications-carousel-container');
		$('.controls.arrow-right').on('click', function () {
			if(curr - step <= -width)	return ;
			curr -= step;
			$container.css('margin-left', curr+'px');
		});
		
		$('.controls.arrow-left').on('click', function () {
			if(curr === 0) return ;
			curr += step;
			$container.css('margin-left', curr+'px');
		});
		
	});
</script>