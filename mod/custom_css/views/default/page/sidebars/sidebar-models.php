<?php
/**
 * The basic sidebar for the Models page.
 * 
 * @author Christos Papoulas 
 */
?>

<div class="models-sidebar">
	<h3 class="models-filter-header">Filters</h3>
	<hr>

	<div class="sidebar-section">
		<?php
		echo elgg_view_form('models/search');
		echo elgg_view_form('models/filters')
		?>
	</div>

</div>