<?php
/**
 * 
 * @uses $vars['page'] The current page. 
 */
gatekeeper();

$content = elgg_view('page/sidebars/profile/elements/submenu', $vars);
echo elgg_view_module('aside', 'Settings', $content);