<?php
/**
 * 
 * @uses $vars['page'] The current page. string: edit | avatar | settings | notifications
 */
$username = elgg_get_logged_in_user_entity()->username;
?>

<div class="profile-submenu-item <?php echo $vars['page'] == 'edit' ? 'active' : '';?>">
	<?php echo elgg_view('output/url', array('text' => 'General Settings', 'href' => "my_profile/$username/edit")); ?>
</div>

<div class="profile-submenu-item <?php echo $vars['page'] == 'avatar' ? 'active' : '';?>">
	<?php echo elgg_view('output/url', array('text' => 'Avatar Settings', 'href' => "my_avatar/edit/$username")); ?>
</div>

<div class="profile-submenu-item <?php echo $vars['page'] == 'settings' ? 'active' : '';?>">
	<?php echo elgg_view('output/url', array('text' => 'Account Settings', 'href' => "settings/user/$username")); ?>
</div>

<div class="profile-submenu-item <?php echo $vars['page'] == 'notifications' ? 'active' : '';?>">
	<a href="#">Notifications</a>
</div>