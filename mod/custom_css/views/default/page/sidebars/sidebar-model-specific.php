<?php
/**
 * The sidebar of a full view application model and component.
 * 
 * @author Christos Papoulas
 */
$app = elgg_extract('appGUID', $vars, FALSE);
$app_entity = get_entity($app);


$ownerOfApp = get_entity($app_entity->getOwnerGUID());

$contributors = elgg_get_entities_from_relationship(array(
  relationship => 'is_contributor_to',
  relationship_guid => $app,
  inverse_relationship => TRUE
));
$contributors = array_merge(array($ownerOfApp), $contributors);

$contributors_count = count($contributors);
$logged_in_user = elgg_get_logged_in_user_guid();

$is_user_contributor = check_entity_relationship($logged_in_user, 'is_contributor_to', $app);

?>
<div class="model-specific-sidebar contributors">
    <h3 class="models-filter-header">Contributors (<?= $contributors_count; ?>)</h3> 
    <a href="#" class="view-all">View All</a><br>
<?php
echo elgg_view('sliders/UserSlider', array(
  'users' => $contributors,
  'count' => 1 * (40 + 5) + 100,
  'class' => 'contributors'));
?>
    <?php if (elgg_is_admin_logged_in() || $is_user_contributor || $ownerOfApp == elgg_get_logged_in_user_entity()) { ?>
        <a href="<?= elgg_get_site_url(); ?>contributors/<?= $app; ?>" class="add-link">Add/Remove Contributors</a>
    <?php } ?>
</div>

<div class="model-specific-sidebar model-specific-sidebar-tags">
    <h3 class="models-filter-header">Tags</h3> 
    </br>

    <!-- <a href="#" class="view-all">View All</a> -->
    <a href="#AllTagsModal" role="button" class="view-all" data-toggle="modal">View All</a>
  
    <?php echo elgg_view('page/elements/models/model-tags', array('appGUID' => $app)); ?>
</div>

<?php echo elgg_view('modals/tags-of-this-app', array('appGUID' => $app)); ?>