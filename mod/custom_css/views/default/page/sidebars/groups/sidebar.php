<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo elgg_view('groups/sidebar/find') . elgg_view('groups/sidebar/featured');
$my_con_link = elgg_get_site_url() . 'friends/' .  elgg_get_logged_in_user_entity()->username;
?>
<div class="groups-sidebar">
	
	<div class="sidebar-section my-groups">
		<h3>My Groups</h3>
		<?php echo elgg_view('page/elements/groups/my-groups');?>
		<div class="create"> <a href="add">Create Group</a> </div>
	</div>
	
	<div class="sidebar-section suggest-connections">
		<h3>Suggested Connections</h3>
		<?php echo elgg_view('page/elements/profile/suggest-friends', $vars); ?>
		<div class="create"> <a href="<?php echo $my_con_link; ?>">My Connections</a> </div>
	</div>
	
</div>