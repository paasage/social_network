<?php
/**
 * 
 * @author Christos Papoulas
 */
?>
<div class="category-sidebar">
	<h2 class="sidebar-title">Filters</h2>
	<hr>
	<form action="#">
		<div class="sidebar-section">
			<div class="input-append">
				<input class="span2" id="appendedInputButton" type="text" placeholder="keyword">
				<button class="btn" type="button">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</div>
		</div>
		<div class="sidebar-section">
			<h3>Cloud Automation</h3>
			<input type="checkbox" value="All">All <br/>
			<input type="checkbox" value="Chef">Chef <br/>
			<input type="checkbox" value="CloudML">CloudML <br/>
			<input type="checkbox" value="Puppet">Puppet <br/>
			<input type="checkbox" value="Salt">Salt <br/>
		</div>
		<div class="sidebar-section">
			<h3>Tags</h3>
		</div>
	</form>
	<?php //echo elgg_view_form('category/add'); ?>
</div>