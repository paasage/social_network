<?php
/**
 * Contains the execution, components and application association with 
 * a discussion.
 * 
 * @uses $vars['topic'] 
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$tags = get_entity_relationships($vars['topic']);
$apps = elgg_get_entities(array('types' => 'object', 'subtypes' => 'draw_application', 'limit' => 1000));
echo elgg_view('output/assosiated_apps', array('tags' => $tags));
?>
<!-- <div class="tags-wrapper"></div> -->

<a href="#" class="disc-small-icon" id="application-hint" title="Associate an application model">
	<img src="<?php echo elgg_get_site_url(); ?>_graphics/groups/model-small-icon.png">
	<div id="suggest-app-out-form">
		<input type="text" placeholder="search for application model"><hr>
		<div class="suggestions-list"></div>
	</div>
</a>

<a href="#" class="disc-small-icon" id="component-hint" title="Associate a component"><img src="<?php echo elgg_get_site_url(); ?>_graphics/groups/component-small-icon.png"></a>

<a href="#" class="disc-small-icon" id="execution-hint" title="Associate an application execution">
	<img src="<?php echo elgg_get_site_url(); ?>_graphics/models/execution.png">
	<div id="execution-association" >
		<input  type="text" placeholder="paste the execution id">
		of application model:
		<select  type="text" placeholder="add the application model" style="width: 66.8%;">
			<?php foreach ($apps as $app) { ?>
				<option value="<?= $app->getGUID(); ?>"><?= $app->appname; ?></option>
			<?php } ?>
		</select>
		<button onclick="associate();
						return false;" class="btn-small btn-success" style="width: 100%;">
			Associate execution
		</button>
	</div>
</a>