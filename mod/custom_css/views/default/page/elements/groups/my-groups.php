<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
elgg_load_library('custom_css.groups.lib');

$groups = elgg_list_entities(array(
		'type' => 'group',
		'relationship' => 'member',
		'relationship_guid' => elgg_get_logged_in_user_guid(),
		'inverse_relationship' => false,
		'full_view' => false,
		'limit' => 3,
		'pagination' => FALSE
	), 'elgg_get_entities_from_relationship', 'groups_view_entity_list');

$count_groups = elgg_get_entities_from_relationship(array(
		'type' => 'group',
		'relationship' => 'member',
		'relationship_guid' => elgg_get_logged_in_user_guid(),
		'inverse_relationship' => false,
		'count' => TRUE
	));

//<span class="span-view-all"><a href="#">View All</a></span>
echo $groups;
?>

