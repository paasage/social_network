<?php
/**
 * This file overrides the topbar (administrator, setting, log out).
 * @author Christos Papoulas
 */

// Elgg logo
echo elgg_view_menu('topbar', array('sort_by' => 'priority', array('elgg-menu-hz')));
echo elgg_view_menu('site');
