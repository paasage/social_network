<?php
/**
 * 
 * 
 * @uses $vars['icon'] The icon of the user.
 * @uses $vars['guid'] The guid of the user.
 * 
 */

?>

<div class="row-fluid profile-main"> <!-- the user icon and basic menu -->
	<?php
		echo $vars['icon'];
	?>
	<div class="myprofile-menu row-fluid">
		<?php 
			echo elgg_view('page/elements/profile/profile-menu-topbar', 
					$vars);
		?>				
	</div>

	<div class="profile-info-user">
		<?php echo elgg_view('page/elements/profile/information', $vars); ?>
	</div>

	<?php echo elgg_view('page/elements/profile/contributions', array('user_guid' => $vars['guid']));?>

</div>