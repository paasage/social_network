<?php
/**
 * 
 * @uses $vars['user_guid'] THe user GUID of the viewing Entity.
 */

$user_guid = $vars['user_guid'];
$options_contri = array(
      'relationship' => 'is_contributor_to',
      'relationship_guid' => $user_guid,
      'inverse_relationship' => FALSE,
      'count' => TRUE
  );
$options = array(
			'count' => TRUE,
			'owner_guids' => array($user_guid),
			'types' => array('object'),
			'subtypes' => array('draw_application')
		);
$models = elgg_get_entities($options)
        + elgg_get_entities_from_relationship($options_contri);
if(!$models) {
  $models = 0;
}
$options['subtypes'] = array('groupforumtopic');
$questions = elgg_get_entities($options);
if(!$questions) {
  $questions = 0;
}
$options['subtypes'] = array('software_component');
$components = elgg_get_entities($options);
if(!$components) {
  $components = 0;
}
$options_annotations = array(
		'annotation_owner_guids' => array($user_guid),
		'annotation_calculation' => 'count',
		'annotation_names' => array('group_topic_post')
		);
$replies_opt = array(
	'count' => TRUE,
	'type' => 'object',
	'subtype' => 'discussion_reply',
	'owner_guids' => array($user_guid)
);

$replies = elgg_get_annotations($options_annotations);
$replies += elgg_get_entities($replies_opt);
if(!$replies) {
  $replies = 0;
}
?>

<div class="contributions">
	<span class="number"><?php echo $models;?></span>
	<span class="text">models</span>
	<span class="number"><?php echo $components;?></span>
	<span class="text">components</span>
	<span class="number question"><?php echo $questions;?></span>
	<span class="text">questions</span>
	<span class="number question"><?php echo $replies;?></span>
	<span class="text">replies</span>
</div>