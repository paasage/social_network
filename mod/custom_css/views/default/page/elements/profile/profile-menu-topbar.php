<?php
/**
 * 
 * @uses $vars['guid'] The guid of the user owner of the view
 */
$guid = $vars['guid'];
$entity = get_entity($guid);
$user_name = $entity->username;

$site = elgg_get_site_url();
$__elgg_ts = time();
$__elgg_token = generate_action_token($__elgg_ts);
$action_img = $site . '_graphics/user/';
if(elgg_get_logged_in_user_entity()->isFriendsWith($guid)){
	$action = 'remove';
	$action_img .= 'remove-user-profile.png';
} else {
	$action = 'add';
	$action_img .= 'add-user-profile.png';
}
$message_icon = '<img src="' . $site . '_graphics/user/message-profile.png" class="profile-actions-image">';
$report_icon = '<img src="' . $site . '_graphics/user/report-profile.png" class="profile-actions-image">';
if(elgg_get_logged_in_user_guid() == $guid) { //is my profil
?> 
<a href="<?php echo "{$site}my_profile/{$user_name}/edit";?>" class="edit-profile-link">
	<img src="<?php echo $site . '_graphics/user/edit-profile-header.png';?>"/>
	Edit Profile
</a>
<?php } else { //is someone else profil 
	
	$remove_link = $site . 'action/friends/' . 
					$action . '?friend=' . $guid . '&__elgg_ts=' . $__elgg_ts
					. '&__elgg_token=' . $__elgg_token;
?>
<div class="remove-or-add-friend">
	<?php if(elgg_get_logged_in_user_entity()->isFriendsWith($guid)) {?>
	<a href="<?php echo $remove_link; ?>">
		<img src="<?php echo $action_img; ?>" class="profile-actions-image">Remove
	</a>
	<?php } else { ?>
	<a href="<?php echo $remove_link; ?>">
		<img src="<?php echo $action_img; ?>" class="profile-actions-image">
		Add
	</a>
	<?php } ?>
</div>
<div class="message-to-user">
	<a href="<?php echo $site;?>/messages/compose?send_to=<?php echo $guid;?>">
		<?php echo $message_icon; ?>Message
	</a>
</div>

<div class="report-user">
	<a href="#">
		<?php echo $report_icon;?>
		Report</a>
</div>
<?php
	}
?>