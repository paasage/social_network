<?php
/**
 * 
 * @uses $vars['guid'] The GUID of User Entity.
 */
$guid = $vars['guid'];
$entity = get_entity($guid);
?>

<h3><?php echo $entity->name; ?></h3>
<div class="information-profile">
	<?php
	$profile_fields = elgg_get_config('profile_fields');
	if (is_array($profile_fields) && count($profile_fields) > 0) {
		foreach ($profile_fields as $shortname => $valtype) {
			$metadata = elgg_get_metadata(array(
				'guid' => $guid,
				'metadata_name' => $shortname,
				'limit' => false
			));
			$value = '';
			if ($metadata) {
				if (is_array($metadata)) {
					foreach ($metadata as $md) {
						if (!empty($value)) {
							$value .= ', ';
						}
						$value .= $md->value;
					}
				} else {
					$value .= $metadata->value;
				}
			}
			if ($value !== "") {
				echo "<span class='profile_fields_name'>" .
				elgg_echo("profile:{$shortname}") .
				":</span> " . elgg_get_excerpt($value, 40) . " ";
			}
		}
	}
	?>
</div>