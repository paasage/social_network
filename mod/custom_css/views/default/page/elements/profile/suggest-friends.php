<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

elgg_load_library('custom_css.friends.lib');

$friends = suggest_friends();
$friends_array = array();
$i=0;
foreach ($friends as $f => $entity) {
	$friends_array[$i] = get_entity($f);
	$mutuals[$i++] = count($entity['mutuals']);
}
$count = count($friends)*(40 + 5) + 100;;
$options = array(
		'users' => $friends_array,
		'count' => $count,
		'mutuals' => $mutuals,
		'group-suggestions' => TRUE
		);

echo elgg_view('sliders/UserSlider', $options);