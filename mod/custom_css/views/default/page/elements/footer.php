<?php
/**
 * 
 * 
 */
$contact = elgg_view('output/url', array('href' => 'contact/list', 'text' => 'Contact'));
$terms = elgg_view('output/url', array('href' => '#', 'text' => 'Terms of Service'));
$privacy = elgg_view('output/url', array('href' => '#', 'text' => 'Privacy Policy'));
$europe = elgg_view('output/img', array('src' => '_graphics/europe.jpg'));

?>

<div class="row-fluid">
	<div class="span6 footer-links">
		<?php 
			echo $contact . $terms . $privacy;
		?>
	</div>
	
	<div class="span6 footer-icons">
		<?php 
			echo $europe;
		?>
	</div>
</div>