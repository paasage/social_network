<?php
/**
 * 
 * @uses $vars['guid'] The GUID of User Entity.
 */
elgg_load_library("user.stats");
$count = user_stats_get_profile_completeness($vars['guid'][0]);
$value = 0;
if ($count['count'] < 100) {
	$user_type = "New";
	$value = 10;
} else if ($count == 100) {
	$user_type = "Basic";
	$value = 40;
} else {
	$top_contributions = elgg_get_entities(array(
		'type' => 'object',
		'subtype' => 'draw_application',
		'owner_guid' => $vars['guid'][0],
		'full_view' => false,
		'limit' => 3,
		'count' => TRUE
	));

	$options = array(
		'relationship' => 'is_contributor_to',
		'relationship_guid' => $vars['guid'][0],
		'inverse_relationship' => FALSE,
		'count' => TRUE
	);
	$apps = $top_contributions + elgg_get_entities($options);
	if($apps > 0 ) {
		$user_type = "Expert";
		$value = 60;
	}
}
?>
<p>User Stats</p>
<div class="user-type-title"><?= $user_type; ?></div>
<canvas id="myChart" width="150" height="150"></canvas>
<script>
	$(document).ready(function() {
		var data = [
			{
				value: <?= $value; ?>,
				color: "#41c0c2"
			},
			{
				value: 100 -<?= $value; ?>,
				color: "#fff"
			}
		];
		options = {
			//Boolean - Whether we should show a stroke on each segment
			segmentShowStroke: true,
			//String - The colour of each segment stroke
			segmentStrokeColor: "#fff",
			//Number - The width of each segment stroke
			segmentStrokeWidth: 2,
			//The percentage of the chart that we cut out of the middle.
			percentageInnerCutout: 80,
			//Boolean - Whether we should animate the chart	
			animation: true,
			//Number - Amount of animation steps
			animationSteps: 100,
			//String - Animation easing effect
			animationEasing: "easeOutBounce",
			//Boolean - Whether we animate the rotation of the Doughnut
			animateRotate: true,
			//Boolean - Whether we animate scaling the Doughnut from the centre
			animateScale: false,
			//Function - Will fire on animation completion.
			onAnimationComplete: null,
			labelFontFamily: "Arial",
			labelFontStyle: "normal",
			labelFontSize: 24,
			labelFontColor: "#fff"
		};
		new Chart(document.getElementById("myChart").getContext("2d")).Doughnut(data, options);
	});
</script>