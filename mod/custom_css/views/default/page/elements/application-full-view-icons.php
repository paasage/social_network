<?php
/**
 * 
 * @uses $vars['model_guid'] The model guid
 * @uses $vars['version'] The version of application
 * @uses $vars['runs']  The number of the app runs (int)
 * @uses $vars['views'] The number of the page view (int) Deprecated
 * @uses $vars['shares'] The number of the times that this app is shared to the 
 * network
 */
$version = $vars['version'];
$runs = $vars['runs'];
$views = elgg_get_entities_from_relationship(array(
    relationship => 'watching', 
    relationship_guid => $vars['model_guid'],
    inverse_relationship => TRUE,
    count => TRUE
  ));
$shares = $vars['shares'];

$site = elgg_get_site_url();

$uses = get_entity($vars['model_guid'])->use;
if ($uses == NULL) {
  $uses = 0;
}
$version = get_entity($vars['model_guid'])->version;
if($version == NULL) {
	$version = 0.1;
}
$watching_r = check_entity_relationship(elgg_get_logged_in_user_guid(), 'watching', $vars['model_guid']);

// check if the logged_in user is the owner of the app
$show_buttons = false;
$app_owner_guid = get_entity($vars['model_guid'])->owner_guid;
$logged_in_user_guid = elgg_get_logged_in_user_guid();
if($app_owner_guid == $logged_in_user_guid) {
  $show_buttons = true;
}

// check if the logged_in user is contributor of the app
$options_contr = array(
    'relationship' => 'is_contributor_to',
    'relationship_guid' => $vars['model_guid'],
    'inverse_relationship' => TRUE,
    'type' => 'user',
);
$contributors = elgg_get_entities_from_relationship($options_contr);
foreach ($contributors as $c) {
  if($c->getGUID() == $logged_in_user_guid) {
    $show_buttons = true;
    break;
  }
}


echo elgg_view('modals/xmi-components', array(appid => $vars['model_guid']));
echo elgg_view('modals/xmi-components-deploy', array(appid => $vars['model_guid']));
?>
<div class="one-icon">
  <a href="#" title="Version">
    <?php
    echo elgg_view('output/img', array("src" => "_graphics/models/models-features-version-vbig.png"));
    ?>
  </a>
  <div class="details-for-app"><h4>v</h4><h3><?php echo $version; ?></h3></div>
</div>

<?php
if($show_buttons) {
?>
<div class="one-icon">
  <a href="#" id="run" data-uid="<?=$vars['model_guid'];?>" title="Reason this app" data-toggle="modal" data-target="#xmi-components-modal">
    <?php
    echo elgg_view('output/img', array("src" => "_graphics/models/models-features-play-vbig.png"));
    ?>
  </a>
  <div class="details-for-app"><h3><?php //echo $runs; ?></h3><h4>Reason</h4></div>
</div>

<div class="one-icon">
  <a href="#" id="deploy" data-uid="<?=$vars['model_guid'];?>" title="Deploy this app" data-toggle="modal" data-target="#xmi-components-modal-deploy">
    <?php
    echo elgg_view('output/img', array("src" => "_graphics/models/models-features-deploy-vbig.png"));
    ?>
  </a>
  <div class="details-for-app"><h3><?php //echo $runs; ?></h3><h4>Deploy</h4></div>
</div>
<?php
}
?>

<div class="one-icon" t>
  <a href="#" id="use" data-uid="<?=$vars['model_guid'];?>" title="Add to your cart">
  <?php
  echo elgg_view('output/img', array("src" => "_graphics/models/models-features-cart-vbig.png"));
  ?>
  </a>
  <div class="details-for-app"><h3><?= $uses; ?></h3><h4>Uses</h4></div>
</div>

<div class="one-icon">
  <a href="#" title="Watch this app" class="watch" data-uid="<?=$vars['model_guid'];?>">
    <?php
    if($watching_r ) {
      echo elgg_view('output/img', array("src" => "_graphics/models/models-features-eye-vbig-tick.png", "class" =>"watching"));
      echo elgg_view('output/img', array("src" => "_graphics/models/models-features-eye-vbig.png", style => "display: none;", "class" =>"notwatching"));
    } else {
      echo elgg_view('output/img', array("src" => "_graphics/models/models-features-eye-vbig-tick.png", style => "display: none;", "class" => "watching"));
      echo elgg_view('output/img', array("src" => "_graphics/models/models-features-eye-vbig.png", "class" =>"notwatching"));
    }
    ?>
  </a>
  <div class="details-for-app"><h3><?php echo $views; ?></h3><h4>Watches</h4></div>
</div>

<div class="one-icon">
  <a href='<?php echo $site; ?>draw_app/share/<?php echo $vars['model_guid'] ?>' title="Share this app">
    <?php
    echo elgg_view('output/img', array("src" => "_graphics/models/models-features-share-vbig.png"));
    ?>
  </a>
  <div class="details-for-app"><h3><?php echo $shares; ?></h3><h4>Shares</h4></div>
</div>
