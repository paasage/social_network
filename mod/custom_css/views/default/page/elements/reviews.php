<?php
/**
 * List comments with optional add form
 *
 * @uses $vars['entity']        ElggEntity
 * @uses $vars['show_add_form'] Display add form or not
 * @uses $vars['id']            Optional id for the div
 * @uses $vars['class']         Optional additional class for the div
 */
$show_add_form = elgg_extract('show_add_form', $vars, true);
$app_guid = $vars['entity']->guid;
$id = '';
elgg_load_js('custom_css.reviews.js');
$options = array(
  'metadata_names' => array('application_reference'),
  'metadata_values' => array($app_guid),
  'limit' => 10,
  'full_view' => FALSE
);

$reviews_count = elgg_get_entities_from_metadata($options + array('count' => TRUE));

if (isset($vars['id'])) {
  $id = "id='{$vars['id']}'";
}

$class = 'elgg-comments';
if (isset($vars['class'])) {
  $class = "$class {$vars['class']}";
}

// work around for deprecation code in elgg_view()
unset($vars['internalid']);

echo "<div $id class=\"$class\">";

if ($show_add_form) {
  echo elgg_view_form('reviews/add', array(), $vars);
}
?>

<div class="row-fluid reviews-fluid">
  <h3>Reviews (<?php echo $reviews_count; ?>)</h3>
  <select>
    <option value="volvo">Connections First</option>
    <option value="saab">Newest</option>
    <option value="opel">Most Helpful</option>
    <option value="audi">Most Critical</option>
  </select>
</div>

<?php
$rev = elgg_list_entities_from_metadata( $options );

echo $rev . '</div>';
?>
<div id="review_delete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Delete review</h3>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this review?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick="delete_review()">Yes</button>
    <button class="btn btn-primary" data-dismiss="modal">No</button>
  </div>
</div>