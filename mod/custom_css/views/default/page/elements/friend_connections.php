<?php
/**
 * 
 * 
 * @uses $vars['user_guid'] The user GUID
 */
$entity = get_entity($vars['user_guid']);
$friends = $entity->getFriends("", 10, 0 );
$count = count($friends)*(40 + 5) + 100;

$options = array(
		'users' => $friends,
		'count' => $count
		);
echo elgg_view('sliders/UserSlider', $options);