<?php
/**
 * 
 * @uses $vars['title'] 
 * @uses $vars['results'] 
 * @uses $vars['key'] 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$results = $vars['results'];
if($vars['results'] == "") {
	//$results = "No {$vars['title']} named {$vars['key']}";
	$results = 'No ' . $vars['title'] . ' named "' . $vars['key'] . '"';
}

?>
<div class="row-fluid stats-topbar">
	<?= $vars['title']; ?>
</div>
<div class="row-fluid search-content" id="applications-views">
	<?= $results; ?>
</div>