<?php
/**
 * 
 * 
 */
$options = array('annotation_names' => array('models_tags'),
				'limit' => 6);
$tags = elgg_get_annotations($options);

?>
<div class="tags-container">
	<input type="checkbox" name="tags[]" value="all" checked> All <br/>
	<?php
		foreach ($tags as $t) { 
			if($t->value == "")			continue;?>
			<input type="checkbox" name="tags[]" value="<?php echo $t->value;?>"> <?php echo $t->value;?> <br/>
	<?php } ?>
</div>