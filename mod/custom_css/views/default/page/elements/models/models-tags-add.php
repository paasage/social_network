<?php
/**
 * 
 * @uses $vars['appGUID'] The app guid.
 */
// get the tags of the app of this page
$app = get_entity($vars['appGUID']);
$app_tags = array();
$app_tags_Index = 0;

foreach ($app->getAnnotations('models_tags') as $app_tag) {
	$app_tags[$app_tags_Index] = $app_tag->value;
	$app_tags_Index++;
}

// get all the tags of the social network and put them in the array tags
$op = array('annotation_names' => array('models_tags'), 'limit' => 65000);

$tags = array();
$tagIndex = 0;
foreach (elgg_get_annotations($op) as $tag) {
	// check if the tag is already in the array
	$alreadyIn = 0;
	foreach ($tags as $tagg) {
		if ($tagg == $tag->value) {
			$alreadyIn = 1;
		}
	}
	// check if the tag is already tag of the app
	foreach ($app_tags as $taggg) {
		if ($taggg == $tag->value) {
			$alreadyIn = 1;
		}
	}
	// store the tag in the array
	if ($alreadyIn == 0) {
		$tags[$tagIndex] = $tag->value;
		$tagIndex++;
	}
}

?>
<form onsubmit="return add_tag(<?php echo $vars['appGUID'];?>);" action="#" id="skills-form" >
	<input class="add-skill" id="appendedInputButton" type="text" placeholder="Add a tag" name="tag" style="margin: 0;">
	<input class="skills-add-button" type="submit" value="Add" style="margin: 0;">
</form>

<script>
  $(function() {

  	// show suggestions of tags while the user types
  	var allTags = <?php echo json_encode($tags); ?>;
    $( "#appendedInputButton" ).autocomplete({
      source: allTags
    });
  });
</script>