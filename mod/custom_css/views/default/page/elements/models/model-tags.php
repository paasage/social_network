<?php
/**
 * 
 * @uses $vars['appGUID'] The app GUID of the viewing application.
 */

$app = get_entity($vars['appGUID']);

$user_guid = elgg_get_logged_in_user_guid();
if ($app) {
    $is_user_owner = $user_guid == $app->getOwnerGUID();
}
$contributor = check_entity_relationship($user_guid, 'is_contributor_to', $app->getGUID());
?>
<div class="skill-tags-container" id="skill-tags-container">
    <?php
    if ($app) {
        $models_tags = $app->getAnnotations('models_tags');
        
        foreach ($models_tags as $mt) {
            echo '<div class="skill-tag"><span class="skill-tag-name">' . $mt->value . '</span>';
            if ($is_user_owner) {
                echo '<span class="skill-tag-delete-btn" data-uid="' . $mt->id . '">&times;</span>';
            }
            echo '</div>';
        }
    }
    ?>
</div>
<?php
if ($is_user_owner || $contributor) {
    echo elgg_view('page/elements/models/models-tags-add', $vars);
}
