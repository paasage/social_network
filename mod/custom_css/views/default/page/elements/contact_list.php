<?php
/**
 * 
 * The contact list 
 */
$admin2 = elgg_view('user/view-contact-list', array(//manos
	'entity' => get_user_by_username("paputsakEmm"),
	'details' => "e-mail: paputsak@ics.forth.gr"
	));

$admin = elgg_view('user/view-contact-list', array(//chris
	'entity' => get_user(15),
	'details' => "e-mail: papoulas@ics.forth.gr"
	));
$others = elgg_view('user/view-contact-list', array(//kostas
	'entity' => get_user(53),
	'details' => 'e-mail: magoutis@ics.forth.gr'
	));

elgg_load_js('custom_css.view.friend.js');
?>

<div class="row-fluid suggested-members-all">

	<div class="stats-topbar"> 
		For support and technical matters please contact:
	</div>
	<?= $admin2; ?>
	<?= $admin; ?>
</div>

<div class="row-fluid suggested-members-all">
	<div class="stats-topbar"> 
		For all other matters please contact:
	</div>
	<?= $others ?>
</div>

<div class="row-fluid suggested-members-all">
	<div class="stats-topbar"> 
		or post a question to the Technical Support and Feedback discussion group
	</div>
</div>