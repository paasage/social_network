<?php
/**
 * 
 * @uses $vars['number_of_reviews'] The number of reviews
 * @uses $vars['reviews_content'] The content of reviews
 */

$site = elgg_get_site_url();
$number_of_reviews = (int) $vars['number_of_reviews'];
$reviews_content = $vars['reviews_content'];
$size_of_slider = $number_of_reviews*285;
?>

<div class="rating-slider">
	<div class="rating-all-items" style="width: <?php echo $size_of_slider + 100;?>px">
		<ul>
			<?php 
				foreach ($reviews_content as $r) {
					$owner_guid = $r->owner_guid;
					$rater = get_entity($owner_guid);
					$rater_url = $rater->getURL();
					$name = $rater->name;
					$date = elgg_view('output/date',array('value'=>$r->time_created));
					echo "<li class='ellipsis'>$r->description";
					echo "<div class='rate-info'><span><a href='$rater_url'>$name</a></span> <span style=\"margin-left: 100px;\">$date</span></div>";
					echo "</li>";
				}
			?>
		</ul>
	</div>
	<div class="controls-left" id="rate-controlers"><img src="<?php echo $site; ?>_graphics/user/slider-left.png"></div>
	<div class="controls-right" id="rate-controlers"><img src="<?php echo $site; ?>_graphics/user/slider-right.png"></div>
</div>


<script>
	$(document).ready(function(){
		var width = <?php echo $size_of_slider - 100;?>;
		var curr = 0;
		var step = 285;
		var $container = $('.rating-slider .rating-all-items');
		$('#rate-controlers.controls-right img').on('click', function () {
			if(curr - step <= -width)	return ;
			curr -= step;
			$container.css('left', curr+'px');
			console.log("right click:" + curr+'px');
		});
		
		$('#rate-controlers.controls-left img').on('click', function () {
			if(curr === 0) return ;
			curr += step;
			$container.css('left', curr+'px');
			console.log("left click:" + curr+'px');
		});
	});
</script>