<?php
/**
 * 
 * 
 */
?>

<div class="row-fluid welcome-register1">
	<div class="span7 description-wrapper">
		<div class="big-paragraph">Build application models that can run on multiple Cloud platforms</div>
		<p>
			Build or migrate old applications. View application execution statistics. 
			Manage resources and run applications on the Cloud.
		</p>
		<div class="big-paragraph">Collaborate with a powerful community</div>
		<p>Create your network. Find experts. Rate, review and discuss applications.</p>
	</div>
	
	<div class="span5">
	<?php 
		echo elgg_view_form('register');
	?>		
	</div>
	
</div>

<div class="row-fluid welcome-register2 description-wrapper">
	<div class="big-paragraph">Build and run application models</div>
	<p>Find applications, view their execution statistics and deploy 
		them directly or use them to create your own application model</p>
</div>

<div class="row-fluid welcome-register3 description-wrapper">
	<div class="big-paragraph">Join the community</div>
	<p>Find groups and users, connect with them and participate in discussions</p>
</div>