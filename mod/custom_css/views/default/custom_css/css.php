<?php 
/**
 * Changes the current css.
 * 
 * @author Christos Papoulas
 */
?>
/********************** begin custom logo **********************/
.elgg-page-header {
	background: #333333 url(<?php echo elgg_get_site_url(); ?>_graphics/toptoolbar_background.gif) repeat-x bottom left;
	border: 0px solid #999;
	padding: 2px 2px;
	border-bottom: 0px solid #000000;
}

.elgg-page-default .elgg-page-header > .elgg-inner {
  height: 80px;
}
.elgg-heading-site {
  color: white;
  font-family: Georgia,times,serif;
  font-style: italic;
  font-size: 1.2em;
  text-shadow: 1px 2px 4px #333333;
}

.elgg-sidebar {
	width: 290px;
}
/********************** end custom logo ************************/