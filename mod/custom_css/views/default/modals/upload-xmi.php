<?php
/**
 * 
 * @uses $vars['appid']	int The app GUID
 */
?>

<div id="upload-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Upload <i>CAMEL</i> model</h3>
	</div>
	<?php
		echo elgg_view_form(
			'models/upload',
			array(method => "post", enctype => "multipart/form-data"),
			array('appid' => $vars['appid'])
		);
	?>
</div>