<?php
/**
 * 
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$app = get_entity($vars['appid']);
$name = $app->appname;
//echo " <script> console.log('$name'); </script> ";

// create an array ($result_array) with the contents of the xmi file
$xmi3 = simplexml_load_file("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi");
$json_string = json_encode($xmi3);
$result_array = json_decode($json_string, TRUE);


?>


<!-- Modal -->
<div id="xmi-components-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <!--
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Choose the XMI components</h3>
  </div>
  <div class="modal-body">
    <p>
      <?php
      //echo print_r($result_array);
      /*echo " Deployment Models: </br>";
      foreach($result_array['deploymentModels'] as $deploymentModel) {

          if ($deploymentModel['name']) {
            echo "<input type='checkbox' name='xmi_components' value='" . $deploymentModel['name'] . "'> " . $deploymentModel['name'] . "<br/>";
            break;
          } else {
            echo "<input type='checkbox' name='xmi_components' value='" . $deploymentModel['@attributes']['name'] . "'> " . $deploymentModel['@attributes']['name'] . "<br/>";
          }
      }
      echo "</br>";

      echo " Requirement Models: </br>";
      foreach($result_array['requirementModels'] as $requirementModel) {
          
          if ($requirementModel['name']) {
            echo "<input type='checkbox' name='xmi_components' value='" . $requirementModel['name'] . "'> " . $requirementModel['name'] . "<br/>";
            break;
          } else {
            echo "<input type='checkbox' name='xmi_components' value='" . $requirementModel['@attributes']['name'] . "'> " . $requirementModel['@attributes']['name'] . "<br/>";
          }
      }
      echo "</br>";
      */
      ?>
    </p>
  </div> 
  -->

  <div id="modal-header" class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Reasoning phase</h3>
  </div>

  <div id="modal-body" class="modal-body">
    <!-- <p id="inputUpload"><input type="file" name="upload" ></p> -->
    <p id="message_to_user_deploy1">The reasoning phase has started, it usually takes less than 2 minutes.</p>
    <p>The user can be informed about the status of the application reasoning at <i><b>My Area</b></i> page, under <i><b>Active Runs</b></i> tab.</p>
    <p><input id="hidden_input" type="hidden" name="appid" value="<?=$vars['appid'];?>"></p>
    <div id="upload_xmi_gif" name="upload_xmi_gif_shown"><img id="upload_xmi_gif" src="<?php elgg_get_root_path();?>/_graphics/ajax_loader.gif"/></div>
    <div id="message_to_user"></div>
  </div>
  <div name="modal-footer-shown" class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <!-- <button id="reasonXMI" data-uid="<?=$vars['appid'];?>" class="btn btn-primary">Reason</button> -->
  </div>

</div>