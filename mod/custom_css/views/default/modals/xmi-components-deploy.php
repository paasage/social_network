<?php
/**
 * 
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$app = get_entity($vars['appid']);
$name = $app->appname;
//echo " <script> console.log('$name'); </script> ";

// create an array ($result_array) with the contents of the xmi file
$xmi3 = simplexml_load_file("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi");
$json_string = json_encode($xmi3);
$result_array = json_decode($json_string, TRUE);


?>


<!-- Modal -->
<div id="xmi-components-modal-deploy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Deploy your application</h3>
  </div>

  <div id="modal-body" class="modal-body">
    <p id="message_to_user_deploy1">The deployment of the application includes actions such as provisioning of VMs and downloading, configuration, and installation of software.</p> 
    <p id="message_to_user_deploy2">The user can be informed about the status of the application deployment at <i><b>My Area</b></i> page, under <i><b>Active Runs</b></i> tab.</p>
    <p><input id="hidden_input" type="hidden" name="appid" value="<?=$vars['appid'];?>"></p>
    <div id="upload_xmi_gif" name="upload_xmi_gif_hidden"><img id="upload_xmi_gif" src="<?php elgg_get_root_path();?>/_graphics/ajax_loader.gif"/></div>
    <div id="message_to_user_deploy"></div>
  </div>

  <div name="modal-footer-deploy-shown" class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button id="deployXMI" data-uid="<?=$vars['appid'];?>" class="btn btn-primary">Deploy</button>
  </div>

</div>