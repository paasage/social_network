<?php
/**
 * All tags modal
 * @todo Append the selected to the area with jquery
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$op = array('annotation_names' => array('models_tags'), 'limit' => 65000);
$tags = elgg_get_annotations($op);
?>
<!-- Modal -->
<div id="AllTagsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Tags</h3>
  </div>
  <div class="modal-body">
    <p>
			<?php
			foreach ($tags as $t) {
				if ($t->value == "")
					continue;
				?>
				<input type="checkbox" name="tags[]" value="<?php echo $t->value; ?>"> <?php echo $t->value; ?> <br/>
<?php } ?>
		</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" id="saveTags" data-dismiss="modal">Save</button>
  </div>
</div>