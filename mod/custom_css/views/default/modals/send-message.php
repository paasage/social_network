<?php
/**
 * 
 * 
 * @uses $vars['to'] The GUID of the receiver.
 * @uses $vars['from'] The GUID of the sender.
 */

$user = get_entity($vars['from']);
$options = array(
	'relationship' => 'friend',
	'relationship_guid' => $vars['from'],
	'inverse_relationship' => FALSE,
	'type' => 'user',
	'full_view' => FALSE
);

?>

<!-- Modal -->
<div id="send-message-<?php echo $vars['to']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Send a Message</h3>
  </div>
  <div class="modal-body">
    <?php 
		set_input('recipient_guid', $vars['to']);
			echo elgg_view_form('messages/send', array(
					'friends' => elgg_get_entities_from_relationship($options),
			) );
		?>
  </div>
</div>