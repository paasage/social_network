<?php
/**
 * Group profile summary for invitation
 *
 * Icon and profile fields
 *
 * @uses $vars['entity'] The group Entity
 */
elgg_load_js('custom_css.groups.invite.js');
if (!isset($vars['entity']) || !$vars['entity']) {
	echo elgg_echo('groups:notfound');
	return true;
}

$group = $vars['entity'];
$owner = $group->getOwnerEntity();
$site = elgg_get_site_url();
if (!$owner) {
	// not having an owner is very bad so we throw an exception
	$msg = elgg_echo('InvalidParameterException:IdNotExistForGUID', array('group owner', $group->guid));
	throw new InvalidParameterException($msg);
}


$friends = elgg_get_logged_in_user_entity()->getFriends('', 200);

$suggested_members = '';
foreach ($friends as $f) {
	if(!$group->isMember($f) && !check_entity_relationship($group->guid, 'invited', $f->guid)) {
		$suggested_members .= elgg_view('user/view-invite', array('entity' => $f, 'group_entity' => $group));
	}
}
?>

<div class="row-fluid group-overview">

		<div class="groups-profile-icon">
			<?php
				// we don't force icons to be square so don't set width/height
				echo elgg_view_entity_icon($group, 'medium', array(
					'href' => '',
					'width' => '',
					'height' => '',
				)); 
			?>
		</div>
		<div class="group-actions">
			<?php echo elgg_view('groups/elements/actions', $vars);?>
		</div>
		<div class="group-description">
		<?php echo "<h3>{$group->name}</h3>" . elgg_get_excerpt($group->description, 300);?>
		</div>
		<div class="group-information">
			<?php echo elgg_view('groups/elements/information', $vars); ?>
		</div>

</div>

<div class="row-fluid suggested-members-all">
	<h3>Suggested Members</h3>
	<?php echo $suggested_members; ?>
</div>