<?php
/**
 * Group profile summary
 *
 * Icon and profile fields
 *
 * @uses $vars['group']
 */

if (!isset($vars['entity']) || !$vars['entity']) {
	echo elgg_echo('groups:notfound');
	return true;
}

$group = $vars['entity'];
$owner = $group->getOwnerEntity();
$site = elgg_get_site_url();
if (!$owner) {
	// not having an owner is very bad so we throw an exception
	$msg = elgg_echo('InvalidParameterException:IdNotExistForGUID', array('group owner', $group->guid));
	throw new InvalidParameterException($msg);
}

?>

<div class="row-fluid group-overview">

		<div class="groups-profile-icon">
			<?php
				// we don't force icons to be square so don't set width/height
				echo elgg_view_entity_icon($group, 'medium', array(
					'href' => '',
					'width' => '',
					'height' => '',
				)); 
			?>
		</div>
		<div class="group-actions">
			<?php echo elgg_view('groups/elements/actions', $vars);?>
		</div>
		<div class="group-description">
		<?php echo "<h3>{$group->name}</h3>" . elgg_get_excerpt($group->description, 250);?>
		</div>
		<div class="group-information">
			<?php echo elgg_view('groups/elements/information', $vars); ?>
		</div>

</div>
<?php
	// make sure user has permissions to add a topic to container
	$container_guid = elgg_extract("container_guid", $vars);
	if ($group->canWriteToContainer(0, 'object', 'groupforumtopic')) {
		echo elgg_view_form('discussion/save-small', array('onsubmit' => 'getContextFromDiv()'), array());
	}

