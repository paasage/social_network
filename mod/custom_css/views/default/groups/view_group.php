<?php
/**
 * 
 * This view is for user profile page, under the section of GROUPS.
 * 
 * @uses $vars['group_entity'] The entity of the group. 
 */
$group_entity = $vars['group_entity'];
$icon = '<img src="' . $group_entity->getIconURL('small') . '">';
$name = $group_entity->get('name');
$url = $group_entity->getURL();
$link = elgg_view('output/url', array('text' => $name, 'href' => $url, 'is_trusted' => true, 'class' => 'group-profile-link'));

?>
<div class="group-entity-rapper">
	<?php 
		echo $icon . $link; 
	?>
</div>