<?php
/**
 * The informations of group.
 * 
 * @uses $vars['entity'] The group entity.
 */

$group = $vars['entity'];
$group_guid = $group->getGUID();

$members = $group->getMembers(0, 0, TRUE);

$options = array(
		'type' => 'object',
		'subtype' => 'groupforumtopic',
		'order_by' => 'e.last_action desc',
		'container_guid' => $group_guid,
		'count' => TRUE
	);

$questions = elgg_get_entities($options);

?>

<div class="contributions">
	<span class="number"><?php echo $members;?></span>
	<span class="text">members</span>
	<span class="number"><?php echo $questions;?></span>
	<span class="text">questions</span>
</div>