<?php
/**
 * The actions in topbar.
 * 
 * @uses $vars['entity'] The group entity.
 */
$group = $vars['entity'];
$owner = $group->getOwnerEntity();
$user = elgg_get_logged_in_user_guid();
$is_onwer = $owner->getGUID() == $user;
$is_member = $group->isMember($user);
$add_icon = elgg_view('output/img', array('src' => $site . '_graphics/groups/add-group-default.png'));
$report_icon = elgg_view('output/img', array('src' => $site . '_graphics/groups/report.png'));
$leave_icon = elgg_view('output/img', array('src' => $site . '_graphics/groups/leave-group-default.png'));
$edit_url = elgg_view('output/url', array('href' => $site . 'groups/edit/' . $group->getGUID(), 'text' => 'Edit Group')); 
$leave_url = elgg_view('output/url', array(
		'href' => 'action/groups/leave?group_guid='. $group->getGUID(), 
		'is_action' => TRUE,
		'text' => $leave_icon . 'Leave Group'));
$join_url = elgg_view('output/url', array(
		'href' => 'action/groups/join?group_guid='. $group->getGUID(), 
		'is_action' => TRUE,
		'text' => $add_icon . 'Join Group'));
$report_url = elgg_view('output/url', array('href' => '#', 'text' => $report_icon . 'Report'));
?>

<div class="group-action-links">
<?php if($is_onwer) { ?>
	<div class="group-actions-inside"><?php echo $edit_url; ?></div>
<?php } elseif ($is_member ) { ?>
	<div class="group-actions-inside join-leave"><?php echo $leave_url; ?></div>
	<div class="group-actions-inside report"><?php echo $report_url; ?></div>
<?php } else { ?>
	<div class="group-actions-inside join-leave"><?php echo $join_url; ?></div>
	<div class="group-actions-inside report"><?php echo $report_url; ?></div>
<?php } ?>
</div>