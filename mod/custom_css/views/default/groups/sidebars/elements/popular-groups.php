<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
elgg_load_library('custom_css.groups.lib');

$content = elgg_list_entities(array(
		'type' => 'group',
		'relationship' => 'member',
		'inverse_relationship' => false,
		'full_view' => false,
		'limit' => 3
	), 'elgg_get_entities_from_relationship_count', 'groups_view_entity_list');
if (!$content) {
	$content = elgg_echo('groups:none');
}

$content .= elgg_view('output/url', array('text' => 'View All', 'class' => 'link-view-all'));
echo elgg_view_module('aside', 'Popular Groups', $content, array('class' => 'sidebar-section my-groups'));
elgg_load_js('custom_css.groups.js');