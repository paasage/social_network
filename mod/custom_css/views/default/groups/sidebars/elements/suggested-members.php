<?php
/**
 * 
 * @uses $vars['entity'] The group Entity.
 */
$group = $vars['entity'];
$user = elgg_get_logged_in_user_entity();

if(!$group->isMember($user)) {  // only member can invite
	return ; 
}

$friends = $user->getFriends('', 200);

$suggestions = 0;
$content = '';
foreach ($friends as $f) {
	if($suggestions == 5){ break; }
	if(!$group->isMember($f) && !check_entity_relationship($group->guid, 'invited', $f->guid)) {
		$content .= elgg_view('user/view-item', array('entity' => $f, 'group_entity' => $group));
		$suggestions++;
	}
}

$content .= elgg_view('output/url', array(
							'text' => 'View All',
							'href' => "groups/my_invite/{$group->getGUID()}/{$group->name}",
							'class' => 'view-all-pos')
						);
echo elgg_view_module('aside', 'Suggested Members', $content);