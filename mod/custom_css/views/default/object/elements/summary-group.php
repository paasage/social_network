<?php
/**
 * Group summary
 *
 * Sample output
 * <ul class="elgg-menu elgg-menu-entity"><li>Public</li><li>Like this</li></ul>
 * <h3><a href="">Title</a></h3>
 * <p class="elgg-subtext">Posted 3 hours ago by George</p>
 * <p class="elgg-tags"><a href="">one</a>, <a href="">two</a></p>
 * <div class="elgg-content">Excerpt text</div>
 *
 * @uses $vars['entity']    ElggEntity
 * @uses $vars['title']     Title link (optional) false = no title, '' = default
 * @uses $vars['metadata']  HTML for entity menu and metadata (optional)
 * @uses $vars['subtitle']  HTML for the subtitle (optional)
 * @uses $vars['tags']      HTML for the tags (default is tags on entity, pass false for no tags)
 * @uses $vars['content']   HTML for the entity content (optional)
 */

$entity = $vars['entity'];

$title_link = elgg_extract('title', $vars, '');
if ($title_link === '') {
	if (isset($entity->title)) {
		$text = $entity->title;
	} else {
		$text = $entity->name;
	}
	$params = array(
		'text' => elgg_get_excerpt($text, 100),
		'href' => $entity->getURL(),
		'is_trusted' => true,
	);
	$title_link = elgg_view('output/url', $params);
}

$metadata = elgg_extract('metadata', $vars, '');
$subtitle = elgg_extract('subtitle', $vars, '');
$content = elgg_extract('content', $vars, '');
$edit = '';
if(elgg_is_admin_logged_in() || $entity->owner_guid == elgg_get_logged_in_user_guid()) {
	$edit .= elgg_view('output/url', array(
		href => elgg_get_site_url() . "discussion/edit/{$entity->getGUID()}",
		text => 'edit'
	));
}
?>

<div class="question-full-view">
	<div class="question-row"><h3>question</h3><?=$edit;?></div>
	<h3><?php echo $title;?></h3>
	<div class="elgg-subtext"><?php echo $subtitle;?></div>
	<?php 
		echo elgg_view('object/summary/extend', $vars);
		echo "<div class=\"elgg-content\">$content</div>";
	?>
</div>