<?php
/**
 * The reviews view of the application full view
 * 
 * called by custom_css/views/default/navigation/models-submenu
 * 
 * @uses $vars['model_guid'] The guid of the model.
 * @uses $vars['users_rates'] The number of users rates
 * @uses $vars['rating_mean'] The mean of user rates.
 * @author Christos Papoulas
 */
$model_guid = $vars['model_guid'];
elgg_load_library('custom_css.reviews.lib');
$reviews_add = custom_css_view_reviews(get_entity($model_guid));

$most_helpful = get_most_helpful_review($model_guid);
?>

<div class="models-review">
	<div class="row-fluid">
		<div class="span9">
			<?php echo $reviews_add;?>		
		</div>
		
		<div class="span3">
			<div class="span1" style="width: 280px; height: 200px; margin: 10px 0; background-color: #fff;">
				<h3>Summary</h3>
				<?php echo elgg_view('object/elements/reviews/summary', $vars);?>
			</div>
			
			<div class="span1" style="width: 280px; margin: 10px 0; background-color: #fff;">
				<h3>The most helpful favourable review</h3>
				<?php 
					echo elgg_view('object/elements/reviews/most_helpful', 
								array('count' => $most_helpful['count'], 'review' =>	$most_helpful['review']));
				?>
			</div>
			
			<div class="span1" style="width: 280px; height: 200px; margin: 10px 0; background-color: #fff;">
				<h3>The most helpful critical review</h3>
			</div>
		</div>
	</div>
	
</div>