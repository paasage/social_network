<?php
/**
 * 
 * @uses $vars['users_number'] The number of comments 
 * @uses $vars['rate_number'] The mean rate
 */

$url = elgg_get_site_url();
$users = $vars['users_number'];
$rate = $vars['rate_number'];
?>

<div class="reviews-mean">
	<div style="display: inline-block;">
		<img src="<?php echo $url;?>_graphics/models/star.png">
		<?php echo $rate; ?>/5 
	</div>

	<div style="display: inline-block; margin-left: 25px;">
		Rating from <?php echo $users;?> users
	</div>
</div>