<?php
/**
 * 
 * @uses  $vars['model_guid'] The model GUID.
 * @uses  $vars['reviews_array'] The array with all the reviews (maybe is limited). 
 * @todo Check if the array reviews_array is limited.
 */
$options_for_reviews = array(
    'type' => 'object',
    'subtype' => 'review_comment',
    'metadata_names' => array('application_reference'),
    'metadata_values' => array($vars['model_guid']),
    'limit' => 65000
);
$reviews = elgg_get_entities_from_metadata($options_for_reviews);
$five = (int) 0;
$four = (int) 0;
$three = (int) 0;
$two = (int) 0;
$one = (int) 0;
foreach ($reviews as $r) {
  switch ($r->rating) {
    case 5;
      $five++;
      break;
    case 4;
      $four++;
      break;
    case 3;
      $three++;
      break;
    case 2;
      $two++;
      break;
    case 1;
      $one++;
      break;
    default:
      error_log('Rating has no value');
      break;
  }
}
$all_reviews = $one + $two + $three + $four + $five;
$w1 = (($one + 0.5) / (float) $all_reviews) * 100;
$w2 = (($two + 0.5) / $all_reviews) * 100;
$w3 = (($three + 0.5) / $all_reviews) * 100;
$w4 = (($four + 0.5) / $all_reviews) * 100;
$w5 = (($five + 0.5) / $all_reviews) * 100;
?>
<div class="review-bars-wrapper">
  <span>5 stars</span><div class="review-bar"><div style="width: <?php echo $w5 <= 100 ? $w5 : 100; ?>%;"></div></div><span>(<?php echo $five; ?>)</span><br/>
  <span>4 stars</span><div class="review-bar"><div style="width: <?php echo $w4 <= 100 ? $w4 : 100; ?>%;"></div></div><span>(<?php echo $four; ?>)</span><br/>
  <span>3 stars</span><div class="review-bar"><div style="width: <?php echo $w3 <= 100 ? $w3 : 100; ?>%;"></div></div><span>(<?php echo $three; ?>)</span><br/>
  <span>2 stars</span><div class="review-bar"><div style="width: <?php echo $w2 <= 100 ? $w2 : 100; ?>%;"></div></div><span>(<?php echo $two; ?>)</span><br/>
  <span>1 stars</span><div class="review-bar"><div style="width: <?php echo $w1 <= 100 ? $w1 : 100; ?>%;"></div></div><span>(<?php echo $one; ?>)</span><br/>	
</div>

