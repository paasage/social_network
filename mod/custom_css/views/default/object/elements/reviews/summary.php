<?php

/**
 * 
 * @uses $vars['model_guid'] The guid of the model.
 * @uses $vars['users_rates'] The number of users rates
 * @uses $vars['rating_mean'] The mean of user rates.
 */
echo elgg_view(
        'object/elements/models-rating-mean', array(
    'users_number' => $vars['users_rates'],
    'rate_number' => $vars['rating_mean']
        )
);

echo elgg_view('object/elements/reviews/statistics', $vars + array('reviews_array' => $reviews_context));
?>
