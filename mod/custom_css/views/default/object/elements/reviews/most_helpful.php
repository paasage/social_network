<?php
/**
 * 
 * @uses $vars['count']
 * @uses $vars['review'] 
 */
if($vars['review']) {
	$rating = elgg_view('output/rate', array('checked' => $vars['review']->rating,
					'review_guid' => $vars['review']->getGUID()));
	$description = $vars['review']->description;
	$user = $vars['review']->getOwnerEntity();
	$time = elgg_view_friendly_time($vars['review']->time_created);
}
?>

<div>
	<div class="most-helpful-count-users"> 
		<?php 
			if($vars['count']) 
				echo $vars['count'] . " users found this review helpful";
			else
				echo "No reviews yet";
		?>   
	</div>

	<div class="review-wrapper">

		<div class="review-body-wrapper">
			<?php if($vars['review']) { 
				echo $rating;?>
			<p><?php echo $description;?></p>
			<p>Reviewed <?php echo $time;?><br/> by <a href="<?php echo $user->getURL();?>"><?php echo $user->name;?></a></p>
			<?php } ?> 
		</div>
	</div>
</div>