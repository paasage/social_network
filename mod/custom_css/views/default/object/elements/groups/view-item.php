<?php
/**
 * 
 * @uses $vars['group'] The displaying entity group.
 */
$site = elgg_get_site_url();
$group = $vars['group'];
$icon = elgg_view_entity_icon($group, 'small');
$hover_if = $group->isMember(elgg_get_logged_in_user_entity());
echo $icon;
$leave_group_icon_src = $site . '_graphics/groups/leave-group-default.png';
$join_group_icon_src = $site . '_graphics/groups/add-group-default.png';
$leave = elgg_view('output/url', array(
		'is_action' => TRUE,
		'href' => 'action/groups/leave?group_guid=' . $group->getGUID(),
		'text' => "<img src=" . $leave_group_icon_src . ">Leave Group"
	));

$join = elgg_view('output/url', array(
		'is_action' => TRUE,
		'href' => 'action/groups/join?group_guid=' . $group->getGUID(),
		'text' => "<img src=" . $join_group_icon_src . ">Join Group"
	));
$members_icon = elgg_view('output/img', array(
		'src' => $site . '_graphics/groups/members-group.png'));

$count_member = $group->getMembers(0, 0, TRUE) . " users";
?>

<div class="name">
	<h4><?php 
				echo elgg_view('output/url', array(
					'text' => $group->name,
					'href' => $group->getURL()
					));
			?>
	</h4>
</div>
	<div class="group-member">
		<?php echo $members_icon . "<span>" . $count_member . "</span>";?> 
	</div>
<?php if($hover_if) { 
	if($group->getOwnerGUID() != elgg_get_logged_in_user_guid()) { ?>
	<div class="group_hover action_color">
		<?php echo $leave;?>
	</div>
	<?php } 
	} else { ?>
	<div class="group_hover action_color">
		<?php echo $join;?>
	</div>
<?php } ?>