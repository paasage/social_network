<?php
/**
 * 
 * Display the menu onhover community item inside the carousel.
 * 
 * @uses $vars['group'] The displaying group.
 */
$user = elgg_get_logged_in_user_entity();

$info_content = $vars['group']->getMembers(0,0, true). ' users';
if($vars['group']->isMember($user)) {
	$action_content = elgg_view('output/url', 
							array(
									'text' => 'Leave Group',
									'href' => 'action/groups/leave?group_guid=' .  $vars['group']->getGUID(),
									'is_action' => TRUE
									)
							);
} else {
	$action_content = elgg_view('output/url', 
							array(
									'text' => 'Join Group',
									'href' => 'action/groups/join?group_guid=' .  $vars['group']->getGUID(),
									'is_action' => TRUE
									)
							);
}
?>

<div class="group-stats">
	<div class="info">
		<?php echo $info_content;?>
	</div>
	
	<div class="actions">
		<?php echo $action_content;?>
	</div>
</div>