<?php
/**
 * 
 * list friends
 * 
 * @uses $vars['friends'] The friend entities that will be displayed
 * @uses $vars['view_friendsOf_link'] TRUE if link have to be shown
 */
$user = elgg_get_logged_in_user_entity();

elgg_load_js('custom_css.view.friend.js');
elgg_load_js('elgg.javascript.config');

$friends_view = '';
foreach ($vars['friends'] as $f) {
	$friends_view .= elgg_view('user/view-friend', array('entity' => $f));
}

$site = elgg_get_site_url();
$inverse = $vars['view_friendsOf_link'];
?>

<div class="row-fluid suggested-members-all">
	<div class="wrapper-headers">
		<?php if(!$vars['view_friendsOf_link']) { ?>
			<h3>Friends with Me</h3>
		<?php } else { ?>
		<h3>My Friends</h3>
		<?php }
		if($vars['view_friendsOf_link']) { ?>
		<a href="<?=$site;?>friendsof/<?=$user->get('username');?>">View who is friend with me</a>
		<?php } ?>
	</div>
	<?php
		if($friends_view) {
			echo $friends_view; 
		} else {
			echo "<h4>No friends yet!</h4>";
		}
	?>
</div>
