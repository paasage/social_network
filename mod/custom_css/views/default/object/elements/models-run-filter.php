<?php

/**
 * 
 * @uses $vars['graph'] The metric for the graphs. uptime | rt | throughput | cost
 * @uses $vars['model_guid'] The model GUID.
 * @uses $vars['providers'] Filter based on providers seperated by comma
 */
//code for manipulte the form of runs
$from = get_input('from', "");
$to = get_input('to', "");
$rt_value = get_input('rt_value', "");
$rt_metric = get_input('rt_metric', "");
$date_from = get_input('date_from', "");
$date_to = get_input('date_to', "");
$providers = get_input('providers'); //get_input('providers_filter',"");
$has_form_options = !($from == "" && $to == "" && $rt_value == "" &&
        $rt_metric == "" && $date_from == "" && $date_to == "");
elgg_load_js('custom_css.globalize');
elgg_load_js('custom_css.chartjs');
elgg_load_js('custom_css.simple_chart.js');

elgg_load_library('elgg.draw_app_lib');

$run_url = elgg_get_site_url() . '/draw_app/view/' . $vars['model_guid'] . '/runs/';

elgg_load_library('draw_app.cdo_parser');

$socket_res = cdo_socket_client(get_entity($vars['model_guid'])->appname);
//cdo_socket_client_port_9010(get_entity($vars['model_guid'])->appname);

$socket_res_json = json_decode($socket_res);
$app = get_entity($vars['model_guid']);
// store number of runs
$app->runs = count($socket_res_json) == 1 ? 0 : count($socket_res_json);
$app->save();

$form_params = array(
    'model_guid' => $vars['model_guid'],
    'graph' => $vars['graph'],
    'data' => $socket_res_json
);

$view = elgg_view_form(
    'models/filter-metrics', 
    array('class' => 'form-horizontal'), 
    $form_params
);

$unset_queue = array();
$unset_count = -1;

$providers_a = array();
if ($providers != "") {
    $providers_a = explode('_', $providers);
}
foreach ($socket_res_json as $key) {
    $jkey = json_decode($key);
    if ($from !== "" && $jkey->{'cost'} < $from) {
        $unset_queue[] = $unset_count;
        $unset_count++;
        continue;
    } if ($to !== "" && $jkey->{'cost'} > $to) {
        $unset_queue[] = $unset_count;
        $unset_count++;
        continue;
    } if ($date_from !== "" && $jkey->{'start_time'}) {
        $exec_start = new DateTime($jkey->{'start_time'});
        $form_start = new DateTime($date_from);
        if ($exec_start < $form_start) {
            $unset_queue[] = $unset_count;
            $unset_count++;
            continue;
        }
    } if ($date_to !== "" && $jkey->{'start_time'}) {
        $exec_stop = new DateTime($jkey->{'start_time'});
        $form_stop = new DateTime($date_to);
        if ($exec_stop > $form_stop) {
            $unset_queue[] = $unset_count;
            $unset_count++;
            continue;
        }
    } if (count($providers_a) != 0) {
        foreach ($jkey->{'vms'} as $vm) {
            if (in_array($vm->{'provider'} , $providers_a) == FALSE) {
                $unset_queue[] = $unset_count;
                $unset_count++;
                continue;
            }
        }
    }

    $unset_count++;
}
foreach ($unset_queue as $index) {
    unset($socket_res_json[$index]);
}


$string_for_chart = $socket_res;

$view .= elgg_view(
        'page/elements/model-charts', array(
    'data' => $socket_res_json,
    'form_options' => $has_form_options
        ) + $vars
);
$view .= elgg_view(
        'page/elements/model-table', array(
    cdo_res => $socket_res_json,
    model_guid => $vars['model_guid'],
    'form_options' => $has_form_options
        )
);

echo $view;


/*
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// get the platform endpoint of the specific user
$currentUser = elgg_get_logged_in_user_entity();
$platformEndpoint = $currentUser->platformEndpoint;
$emailEndpoint = $currentUser->emailEndpoint;
$passwordEndpoint = $currentUser->passwordEndpoint;
$tenantEndpoint = $currentUser->tenantEndpoint;


// use the name of the model in the social network as name of the paasage resource 
$model_guid = $vars['model_guid'];
$model = get_entity($model_guid);
$name = $model->appname;


// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint);


// 1.5)retrieve the state of the paasage resource
$state = get_resource_state($platformEndpoint, $name, $token, $user_id);
//echo ("<script> console.log('" . $state . "'); </script>");


// functions

function authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint) {
    
    //* a POST request to PaaSage platform API to retrieve: 
    //*
    //* 1) a token
    //* 2) a userId
    //*

    // create the url for the API call
    $url = $platformEndpoint . "api/login";

    // this is the payload
    $data = array (
        'email' => $emailEndpoint,
        'password' => $passwordEndpoint,
        'tenant' => $tenantEndpoint
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    // execute the request
    $output = curl_exec($ch);

    // output the information - includes the header
    //echo($output);

    // retrieve token from the response
    $position = strpos($output, "token");
    $token = substr($output, $position+8, 205);
    //echo($token);

    // retrieve user_id from the response
    $position2 = strpos($output, "userId");
    $user_id = substr($output, $position2+8, 1);
    //echo($user_id);

    // close curl resource to free up system resources
    curl_close($ch);

    return array($token, $user_id);
}

function get_resource_state($platformEndpoint, $resource_name, $token, $user_id) {
    
    //* a GET request to PaaSage platform API to retrieve the state 
    //*
    //* of a resource with name $resource_name
    //*

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    echo ("<script> console.log('" . $output . "'); </script>");

    // retrieve resource primary key from the response
    $position = strpos($output, "state");
    $position2 = strpos($output, "subState");
    $state_length = ($position2-3) - ($position+8);
    $state = substr($output, $position+8, $state_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

*/
