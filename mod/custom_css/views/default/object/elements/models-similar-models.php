<?php

/**
 * 
 * @uses $vars['graph'] The metric for the graphs. uptime | rt | throughput | cost
 * @uses $vars['model_guid'] The model GUID.
 * @uses $vars['providers'] Filter based on providers seperated by comma
 */

//code for displaying the related models

// get the current app
$current_app = get_entity($vars['model_guid']);
$current_app_tags = $current_app->getAnnotations('models_tags');

// get all Elgg apps
$options = array(
  'type' => 'object',
  'subtype' => 'draw_application',
  'full_view' => false,
  'limit' => false
);
$allapps = elgg_get_entities($options);

// get related apps
$related_apps = array();
$indexX = 0;

foreach ($allapps as $app) {

    // do not include tags of current app
    if($app == $current_app) {
        continue;
    }

    $similar = 0;

    $indexY = 3;

    // get all the tags of each app
    $tags = $app->getAnnotations('models_tags');
    foreach ($tags as $tag) {
        //echo "<script> console.log('tag: ".$tag->value."'); </script>";

        foreach ($current_app_tags as $current_app_tag) {
            //echo "<script> console.log('current tag: ".$current_app_tag->value."'); </script>";

            if($current_app_tag->value == $tag->value) {
                $similar = 1;
                $related_apps[$indexX][$indexY] = $tag->value;
                $indexY++;
                //echo "<script> console.log('bingo'); </script>";
            }
        }
    }

    if ($similar == 1) {
        $related_apps[$indexX][0] = $indexY - 3;
        $related_apps[$indexX][1] = $current_app->appname;
        $related_apps[$indexX][2] = $app;
        $indexX++;
    }
}

// find the max number of same tags between two apps
$maxNumberOfTags = $related_apps[0][0];
for ($i = 1; $i <= count($related_apps); $i++) {
    if ($maxNumberOfTags < $related_apps[$i][0]) {
        $maxNumberOfTags = $related_apps[$i][0];
    }
}
//echo "<script> console.log('maxNumberOfTags: ".$maxNumberOfTags."'); </script>";

// show the similar apps according to the number of same tags
for ($numberOfTags = $maxNumberOfTags; $numberOfTags >= 1; $numberOfTags--) {
    $appsRow = array();
    $appsRowIndex = 0;

    // put all the same tags in the $appRow array
    for ($i = 0; $i <= count($related_apps); $i++) {
        if ($related_apps[$i][0] == $numberOfTags) {
            $appsRow[$appsRowIndex] = $related_apps[$i][2];
            //echo "<script> console.log('appsRow$numberOfTags: ".$appsRow[$appsRowIndex]."'); </script>";
            $appsRowIndex++;
        }
    }

    // prepare the array $oprtions that is needed for the list-models view
    $options = array();
    $options['full_view'] = false;
    if (!is_int($offset)) {
        $offset = (int) get_input('offset', 0);
    }
    $defaults = array(
        'items' => $appsRow,
        'list_class' => 'elgg-list-entity',
        'full_view' => true,
        'pagination' => true,
        'limit' => false,
        'list_type' => $list_type,
        'list_type_toggle' => false,
        'offset' => $offset,
    );
    $options = array_merge($defaults, $options);

    // call the list-models view
    if (count($appsRow) != 0) {
        echo '<div class="row-fluid stats-topbar"> matching ';
        if ($numberOfTags == 1) {
            echo N2L($numberOfTags);
            echo ' tag </div>';
        } else {
            echo N2L($numberOfTags);
            echo ' tags </div>';
        }

        echo '<div class="row-fluid search-content" id="applications-views">';
        echo elgg_view('page/components/list-models', $options);
        echo '</div>';
    }
}


if($related_apps == "") {
    $results = "No Related Models to {$page[0]}";
}


function N2L($number)
{
    $result = array();
    $tens = floor($number / 10);
    $units = $number % 10;

    $words = array
    (
        'units' => array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eightteen', 'Nineteen'),
        'tens' => array('', '', 'Twenty', 'Thirty', 'Fourty', 'Fifty', 'Sixty', 'Seventy', 'Eigthy', 'Ninety')
    );

    if ($tens < 2)
    {
        $result[] = $words['units'][$tens * 10 + $units];
    }

    else
    {
        $result[] = $words['tens'][$tens];

        if ($units > 0)
        {
            $result[count($result) - 1] .= '-' . $words['units'][$units];
        }
    }

    if (empty($result[0]))
    {
        $result[0] = 'Zero';
    }

    return trim(implode(' ', $result));
}

?>





