<?php
/**
 * Called by draw_app\views\default\object\draw_application
 * 
 * View of expanded application list
 * 
 * @uses $vars['entity_guid'] The entity GUID of the application
 * 
 * @author Christos Papoulas
 */
$site = elgg_get_site_url();
$app = get_entity($vars['entity_guid']);
$watch = elgg_get_entities_from_relationship(array(
    relationship => 'watching', 
    relationship_guid => $vars['entity_guid'],
    inverse_relationship => TRUE,
    count => TRUE
  ));
$use = isset($app->use) ? $app->use : 0;
$shares = isset($app->shares) ? $app->shares  : 0;
?>

<div class="model-description">
	<?= elgg_get_excerpt($app->description, 280); ?>
</div>

<div class="cloud-providers-info">
	
</div>

<div class="models-info-details-big">
	<a href="<?php echo $site . 'draw_app/editor/' . $vars['entity_guid']; ?>">
		<img src ="<?php echo $site; ?>_graphics/models/models-features-play-big.png">
	</a>
	<div class="specific-details-model-expand">
		<h4><?= $app->runs; ?> Runs</h4>
		<p>0 Configs</p>
	</div>
</div>

<div class="models-info-details-big">
	<img src ="<?php echo $site; ?>_graphics/models/models-features-eye-big.png">
	<div class="specific-details-model-expand">
		<h4><?= $watch; ?></h4>
		<p>Watches</p>
	</div>
</div>

<div class="models-info-details-big">
	<img src ="<?php echo $site; ?>_graphics/models/models-features-cart-big.png">
	<div class="specific-details-model-expand">
		<h4><?= $use; ?></h4>
		<p>Uses</p>
	</div>
</div>

<div class="models-info-details-big">
	<img src ="<?php echo $site; ?>_graphics/models/models-features-share-big.png">
	<div class="specific-details-model-expand">
		<h4><?= $shares; ?></h4>
		<p>Shares</p>
	</div>
</div>

