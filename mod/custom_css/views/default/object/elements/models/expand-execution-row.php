<?php
/**
 * 
 * @uses $vars['model_guid'] The model GUID.
 * @uses $vars['vms'] The VM types and providers array
 * @uses $vars['providers'] String | The providers list
 * @uses $vars['id'] Int | For jquery show / hide.
 * @uses $vars['id_cdo'] String The execution name from CDO.
 * 
 */
$vms = cdo_get_the_vms($vars['vms']);

$cdo_id =  $vars['id_cdo'];
?>

<tr class="expand-execution-table-row" id="etr<?= $vars['id']; ?>" style="display: none;">
  <td colspan="11">

    <div class="row-fluid">
      <div class="span3">  
        <h4><?= $vars['providers']; ?> Configuration</h4><br/>

        <h4>Runs(1)</h4>
        <h4>Deployments(1)</h4>

      </div>
      <div class="span3">
        <?php echo $vms; ?>
        <!--<div class="execution-vm-type">S</div> 1x Amazon <br/>
        <div class="execution-vm-type">M</div> <br/>
        <div class="execution-vm-type">L</div> 2x Amazon <br/>
        <div class="execution-vm-type">XL</div> 1x Amazon <br/>-->
      </div>
      <div class="span3"> Execution id: <?= $cdo_id; ?></div>
      <div class="span3">
        <div class="use-this-wrapper">
          <?php
					echo elgg_view_form('execution/configuration/save', array(), array(
						'execution_id' => $cdo_id,
						'model_id' => $vars['model_guid']
					));
          /*echo elgg_view('output/url', array(
              'href' => "draw_app/editor/${vars['model_guid']}",
              'text' => 'Use this Configuration'
          ));*/
          ?>
					<button type="submit" class="btn btn-info">contact execution contributor</button>
        </div>
      </div>
    </div>
  </td>
</tr>