<?php
/**
 * 
 * @uses $vars['rate_number'] The ration of the model
 * @uses $vars['model_guid'] 
 */
$site = elgg_get_site_url();

$rate_number = number_format($vars['rate_number'], 2);
$app = get_entity($vars['model_guid']);
$ace = $app->avgCostEffectiveness;
?>

<div class="models-all-scores">
	<?php if ($rate_number > 3) { ?>
		<img src="<?php echo $site; ?>_graphics/models/models-score-star.png" title="reviews"> <?php echo $rate_number; ?>
	<?php } ?>
	<?php if ($ace > 0) { ?>
		<img src="<?php echo $site; ?>_graphics/models/models-score-cup.png" class="badge-image" title="Cost Effectiveness"> <?= $ace; ?> 
	<?php } ?>
	<?php if ($app->runs > 1) { ?>
		<img src="<?php echo $site; ?>_graphics/models/models-score-time.png"> 20 ms 
	<?php } ?>
</div>

