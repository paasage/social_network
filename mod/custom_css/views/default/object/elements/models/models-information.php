<?php
/**
 * 
 * @uses $vars['entity_guid'] The entity GUID of the application
 * @uses $vars['summary'] The summary of the model
 */
$entity_guid = $vars['entity_guid'];
$summary = $vars['summary'];
$app = get_entity($entity_guid);

//if($app->appname == "Flight Scheduling" || $app->appname == "Milk Bank") {
  //echo "<script type='text/javascript'>alert('$app->runs');</script>";
  //$runs = 0;
  //$runs = $app->runs;
//} else {
  $runs = $app->runs;
//}
$uses = $app->use;
$watch = elgg_get_entities_from_relationship(array(
    relationship => 'watching', 
    relationship_guid => $vars['entity_guid'],
    inverse_relationship => TRUE,
    count => TRUE
  ));

if ($runs == NULL) {
  $runs = 0;
}
if ($uses == NULL) {
  $uses = 0;
}
if ($watch == NULL) {
  $watch = 0;
}
$site = elgg_get_site_url();

$view = elgg_view('object/elements/full', array(
    'summary' => $summary,
    'icon' => '<img src="' . $site . '_graphics/models/models-general.png">',
    'body' => elgg_view('object/elements/models-expanded-info', array('entity_guid' => $entity_guid))
        ));
$watching = check_entity_relationship(elgg_get_logged_in_user_guid(), 'watching', $entity_guid);
?>

<div class="models-info">	
  <span class="models-info-details"> <?=$uses;?> uses</span>
  <span class="models-info-details"> <?=$runs; ?> runs</span>
  <span class="models-info-details"> <?= $watch; ?> watches</span>
  <div class="badge addtocart" data-uid="<?php echo $entity_guid; ?>">
    <a>
      <img src="<?php echo $site; ?>_graphics/models/models-features-cart.png"> use
    </a>
  </div>
  <div class="badge apprun" id="<?php echo $entity_guid; ?>">
    <a href="<?php echo elgg_get_site_url(); ?>draw_app/view/<?= $vars['entity_guid']; ?>/models">
      <img src="<?php echo $site; ?>_graphics/models/models-features-play.png"> related
    </a>
  </div>
  <div class="badge appwatch" data-uid="<?php echo $entity_guid; ?>">
    <?php if($watching === FALSE) { ?>
    <img src="<?php echo $site; ?>_graphics/models/models-features-eye-big-tick.png" style="display: none;" class="watching">
    <img src="<?php echo $site; ?>_graphics/models/models-features-eye-big.png"  class="notwatching">watch
    <?php } else {?>
    <img src="<?php echo $site; ?>_graphics/models/models-features-eye-big-tick.png"  class="watching">
    <img src="<?php echo $site; ?>_graphics/models/models-features-eye-big.png" style="display: none;" class="notwatching">watch
    <?php } ?>
  </div>
  <span class="model-expand-button">
    <img src="<?php echo $site; ?>_graphics/models/models-more-details.png">
  </span>
</div> 

<div class="model-expand-info">
  <?php echo $view; ?>
  <span class="model-collapse-button"> 
    <img src="<?php echo $site; ?>_graphics/models/models-less-details.png">
  </span>
</div>