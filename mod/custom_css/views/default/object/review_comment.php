<?php
/**
 * 
 * @uses $vars['entity'] The display entity
 */
$reviewer = $vars['entity']->owner_guid;
$reviewer_icon = get_entity($reviewer)->getIconURL('small');
$name = get_entity($reviewer)->name;

$review_guid = $vars['entity']->getGUID();

$rating = elgg_view('output/rate', array('checked' => $vars['entity']->rating,
    'review_guid' => $review_guid));
$description = elgg_view('output/longtext', array('value' => $vars['entity']->description));

$can_review = can_vote_helpful($review_guid, elgg_get_logged_in_user_guid());
$count_helpful = count_helpful($review_guid);
$can_edit = FALSE;
if($reviewer == elgg_get_logged_in_user_guid() || elgg_is_admin_logged_in()){
  $can_edit = TRUE;
}
?>

<div class="review-wrapper">
  <div class="review-helpful-ratio"> <?php echo $count_helpful; ?>  users found this review helpful </div>

  <div class="review-owner-wrapper">
    <?php echo '<img src="' . $reviewer_icon . '"><br/>' . $name; ?>
  </div>
  <?php if($can_edit) { ?>
  <div class="edit-review">
    <span><?= elgg_view('output/url', array(is_action => TRUE, href => "reviews/edit/{$review_guid}/{$vars['entity']->application_reference}", text => 'Edit'));?></span>
    <span><a href="#" data-uid="<?=$review_guid?>" class="delete-review">x</a></span>
  </div>
  <?php } ?>
  <div class="review-body-wrapper">
    <?php 
    echo $rating . $description;
    ?>
    <?php if ($can_review) { ?>
      <div class="review-helpful-wrapper">
        Was this review helpful?
        <button class="helpful" data-uid="<?php echo $review_guid; ?>">Yes</button>
      </div>
    <?php } //end if ?>
  </div>
</div>