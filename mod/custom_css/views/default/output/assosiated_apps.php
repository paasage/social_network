<?php
/**
 * 
 * @uses $vars['tags'] ElggRelationship Array | The application to view
 */

$apps = array();
$executions = array();
$site = elgg_get_site_url();
foreach ($vars['tags'] as $tag) {
  if($tag->get('relationship') == 'question_associated_app') {
    $apps[] = get_entity($tag->get('guid_two'));
  } else if($tag->get('relationship') == 'question_associated_execution') {
    $guid = $tag->get('guid_two');
    $executions[] = get_entity( $guid );
  }
}
?>
<div class="tags-wrapper">
  <?php
  foreach ($apps as $app) { ?>
    <a href="<?=$site;?>draw_app/view/<?php echo $app->getGUID();?>/runs"><?= $app->appname; ?></a>
  <?php } 
  foreach ($executions as $exec) { ?>
    <a href="<?=$site;?>draw_app/view/<?php echo $exec->appguid;?>/runs/<?= $exec->execid; ?>"><?= $exec->execid; ?></a>
  <?php } ?>
    
</div>
