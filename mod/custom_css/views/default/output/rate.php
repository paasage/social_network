<?php
/**
 * 
 * @uses $vars['checked'] The checked value
 */
$checked = $vars['checked'];
$img = '<img src="' . elgg_get_site_url() . '_graphics/models/star-filled.png">';

?>

<div class="rating-wrap">
	<?php 
	for ($i = 0; $i < $checked; $i++) {
		echo $img;
	}
	?>
</div>

