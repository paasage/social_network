<?php
/**
 * 
 * add paasage platform credentials
 * 
 */

?>
    <form class="form-horizontal" style="background-color: #ffffff; padding: 20px;" >
    <div class="control-group">
        <label class="control-label" for="endpoint"></label>
        <div class="controls" id="cloudProviderChoice">
            
            <label class="control-label" for="inputResponse">
                Choose cloud provider:
            </label>
            <select name="cloudProvider" id="cloudProvider">
                <option value="Flexiant">None</option>
                <option value="Flexiant">Flexiant</option>
                <option value="GWDG">GWDG</option>
                <option value="AmazonEC2">AmazonEC2</option>
                <option value="Omistack">Omistack</option>
            </select>

            <div>
                <input type='file' name='upload'>
                <br>
                <button name='uploadProviderModel' id='uploadProviderModel' type='button' class='btn'>Upload</button>
                <div id="upload_xmi_gif" name="upload_xmi_gif_hidden"><img id="upload_xmi_gif" src="<?php elgg_get_root_path();?>/_graphics/ajax_loader.gif"/></div>
            </div>

        </div>
    </div>

    </form> 


<script type="text/javascript">
/*
// when we select a cloud provider we change the id of the upload button in order to upload different provider model
    $("#cloudProvider").change(function (e) {

        if (document.getElementById("cloudProvider").value == "Flexiant") {

            document.getElementsByName("uploadProviderModel")[0].id = 'uploadFlexiantProviderModel';

        } else if (document.getElementById("cloudProvider").value == "GWDG") {

            document.getElementsByName("uploadProviderModel")[0].id = 'uploadGWDGProviderModel';

        } else if (document.getElementById("cloudProvider").value == "AmazonEC2") {

            document.getElementsByName("uploadProviderModel")[0].id = 'uploadAmazonEC2ProviderModel';
                
        } else if (document.getElementById("cloudProvider").value == "Omistack") {

            document.getElementsByName("uploadProviderModel")[0].id = 'uploadOmistackProviderModel';

        } else if (document.getElementById("cloudProvider").value == "None"){

            document.getElementsByName("uploadProviderModel")[0].id = 'uploadProviderModel';

        }

    });
*/
</script>


