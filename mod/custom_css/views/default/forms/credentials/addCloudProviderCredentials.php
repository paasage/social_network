<?php
/**
 * 
 * add paasage platform credentials
 * 
 */

$currentUser = elgg_get_logged_in_user_entity();

// get the cloud provider credentials from the user profile
$FlexiantUsername = $currentUser->FLEXIANT_USERNAME;
$FlexiantPassword = $currentUser->FLEXIANT_PASSWORD;
$FlexiantEndpoint = $currentUser->FLEXIANT_ENDPOINT;

$GWDGUsername = $currentUser->GWDG_USERNAME;
$GWDGPassword = $currentUser->GWDG_PASSWORD;
$GWDGEndpoint = $currentUser->GWDG_ENDPOINT;

$OMISTACKUsername = $currentUser->OMISTACK_USERNAME;
$OMISTACKPassword = $currentUser->OMISTACK_PASSWORD;
$OMISTACKEndpoint = $currentUser->OMISTACK_ENDPOINT;

$AmazonEC2Username = $currentUser->EC2_USERNAME;
$AmazonEC2Password = $currentUser->EC2_PASSWORD;
$AmazonEC2Endpoint = $currentUser->EC2_ENDPOINT;

?>
    <form class="form-horizontal" style="background-color: #ffffff; padding: 20px;" >

    <!-- Flexiant -->
    <div class="control-group">
        <label class="control-label" for="FlexiantUsername">Flexiant username:</label>
        <div class="controls">
            <input type="text" id="FlexiantUsername" name="FlexiantUsername" value=" <?php echo $FlexiantUsername ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="FlexiantPassword">Flexiant password:</label>
        <div class="controls">
            <input type="password" name="FlexiantPassword" id="FlexiantPassword" value=" <?php echo $FlexiantPassword ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="FlexiantEndpoint">Flexiant endpoint:</label>
        <div class="controls">
            <input type="text" id="FlexiantEndpoint" name="FlexiantEndpoint" value=" <?php echo $FlexiantEndpoint ?>">
        </div>
    </div>
    <hr style="width:50%">

    <!-- OMISTACK -->
    <div class="control-group">
        <label class="control-label" for="OMISTACKUsername">OMISTACK username:</label>
        <div class="controls">
            <input type="text" id="OMISTACKUsername" name="OMISTACKUsername" value=" <?php echo $OMISTACKUsername ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="OMISTACKPassword">OMISTACK password:</label>
        <div class="controls">
            <input type="password" name="OMISTACKPassword" id="OMISTACKPassword" value=" <?php echo $OMISTACKPassword ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="OMISTACKEndpoint">OMISTACK endpoint:</label>
        <div class="controls">
            <input type="text" id="OMISTACKEndpoint" name="OMISTACKEndpoint" value=" <?php echo $OMISTACKEndpoint ?>">
        </div>
    </div>
    <hr style="width:50%">

    <!-- GWDG -->
    <div class="control-group">
        <label class="control-label" for="GWDGUsername">GWDG username:</label>
        <div class="controls">
            <input type="text" id="GWDGUsername" name="GWDGUsername" value=" <?php echo $GWDGUsername ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="GWDGPassword">GWDG password:</label>
        <div class="controls">
            <input type="password" name="GWDGPassword" id="GWDGPassword" value=" <?php echo $GWDGPassword ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="GWDGEndpoint">GWDG endpoint:</label>
        <div class="controls">
            <input type="text" id="GWDGEndpoint" name="GWDGEndpoint" value=" <?php echo $GWDGEndpoint ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="GWDGFilter1">GWDG filters:</label>
        <div class="controls">
            <!-- <input type="text" id="cloudCredentialsEndpoint" name="cloudCredentialsEndpoint" value="<?php echo $tenantEndpoint ?>"> -->
            1. <input type="text" id="GWDGFilterKey1" name="GWDGFilterKey1"> = <input type="text" id="GWDGFilterValue1" name="GWDGFilterValue1">
        </div>
        <div class="controls">
            <!-- <input type="text" id="cloudCredentialsEndpoint" name="cloudCredentialsEndpoint" value="<?php echo $tenantEndpoint ?>"> -->
            2. <input type="text" id="GWDGFilterKey2" name="GWDGFilterKey2"> = <input type="text" id="GWDGFilterValue2" name="GWDGFilterValue2">
        </div>
    </div>
    <hr style="width:50%">

    <!-- AmazonEC2 -->
    <div class="control-group">
        <label class="control-label" for="AmazonEC2Username">AmazonEC2 username:</label>
        <div class="controls">
            <input type="text" id="AmazonEC2Username" name="AmazonEC2Username" value=" <?php echo $AmazonEC2Username ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="AmazonEC2Password">AmazonEC2 password:</label>
        <div class="controls">
            <input type="password" name="AmazonEC2Password" id="AmazonEC2Password" value=" <?php echo $AmazonEC2Password ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="AmazonEC2Endpoint">AmazonEC2 endpoint:</label>
        <div class="controls">
            <input type="text" id="AmazonEC2Endpoint" name="AmazonEC2Endpoint" value=" <?php echo $AmazonEC2Endpoint ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="GWDGFilter1">AmazonEC2 filters:</label>
        <div class="controls">
            <!-- <input type="text" id="cloudCredentialsEndpoint" name="cloudCredentialsEndpoint" value="<?php echo $tenantEndpoint ?>"> -->
            1. <input type="text" id="AmazonEC2FilterKey1" name="AmazonEC2FilterKey1"> = <input type="text" id="AmazonEC2FilterValue1" name="AmazonEC2FilterValue1">
        </div>
        <div class="controls">
            <!-- <input type="text" id="cloudCredentialsEndpoint" name="cloudCredentialsEndpoint" value="<?php echo $tenantEndpoint ?>"> -->
            2. <input type="text" id="AmazonEC2FilterKey2" name="AmazonEC2FilterKey2"> = <input type="text" id="AmazonEC2FilterValue2" name="AmazonEC2FilterValue2">
        </div>
    </div>
    <!--
    <div class="control-group">
        <label class="control-label" for="AmazonEC2Filters">AmazonEC2 filters:</label>
        <div class="controls">
            <textarea id="AmazonEC2Filters" name="AmazonEC2Filters" rows="5" cols="100"></textarea>
        </div>
    </div>
    -->
    
    <div>
        <button id="addCloudProviderCredentials" type="button" class="btn">Save credentials</button>
    </div>
    </form> 


