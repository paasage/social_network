<?php
/**
 * Discussion topic add/edit form body show on groups/profile/GUID/name
 * 
 */
$title = elgg_extract('title', $vars, '');
$desc = elgg_extract('description', $vars, '');
$status = elgg_extract('status', $vars, '');
$tags = elgg_extract('tags', $vars, '');
$access_id = elgg_extract('access_id', $vars, ACCESS_DEFAULT);
$container_guid = elgg_get_page_owner_guid(); //elgg_extract('container_guid', $form_vars);
$guid = elgg_extract('guid', $vars, null);

//elgg load JS library for suggest to user.
elgg_load_js('elgg.autosuggest.to.topic');
elgg_load_js('custom_css.suggestions.js');
//elgg load all other necessary js & css libraries 
elgg_load_js('elgg.modals');
elgg_load_js('elgg.jquery.impromptu');

?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<div class="body-form-discussion">
  <h4>
    ASK A QUESTION
  </h4>
  <div>
    <?php echo elgg_view('input/text', array('name' => 'title', 'placeholder' => elgg_echo('title'))); ?>
  </div>
  <div class="main">
    <?php
    echo elgg_view('input/div_textarea', array('class' => 'textarea-div', 'id' => 'editablediv'));
    echo elgg_view('input/longtext', array(
        'name' => 'description',
        'id' => 'typing_area',
        'class' => 'discussion-topic-body',
        'style' => 'display: none'));
    ?>
  </div>
  <div class="elgg-foot">
    <?php
    echo elgg_view('input/hidden', array('name' => 'container_guid', 'value' => $container_guid));
    echo elgg_view('input/hidden', array('name' => 'access_id', 'value' => 2)); //ACCESS_PUBLIC
    if ($guid) {
      echo elgg_view('input/hidden', array('name' => 'topic_guid', 'value' => $guid));
    }
    ?>
    <?=elgg_view('page/elements/groups/discussion/associations');?>
		<input type="submit" value="Ask" class="disc-button-submit">

  </div>
</div>

