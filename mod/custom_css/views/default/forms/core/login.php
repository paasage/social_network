<?php
/**
 * 
 * <button class="btn btn-warning" style="" type="button">
 */
?>

<div style="position: relative;">
	<?php 
		echo elgg_view('input/text', array(
			'name' => 'username',
			'class' => 'elgg-autofocus login-field',
			'placeholder' => elgg_echo('loginusername')
		));
		?>
		<?php
		echo elgg_view('input/password', array(
			'name' => 'password',
			'class' => 'login-field',
			'placeholder' => elgg_echo('password')
		));
		?>
	
	<input type="submit" value="Log in" class="sm-login-bt">
        <?php
            echo elgg_view('output/url', array(
                'href' => 'forgotpassword',
                'text' => 'reset password',
                'class' => '',
                'style' =>   'display: -webkit-box;margin-top: -9px;overflow: hidden;'
            ));
        ?>
</div>