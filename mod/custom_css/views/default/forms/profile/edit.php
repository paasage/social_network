<?php
/**
 * Edit profile form
 *
 * @uses vars['entity']
 * 
 * overrides views/default/forms/profile/edit.php
 */

?>
<div class="profile-form-edit">
	<h3>General Settings</h3>
	<div class="control-group">
		<label class="control-label"><?php echo elgg_echo('user:name:label'); ?></label>
		<div class="controls"><?php echo elgg_view('input/text', array('name' => 'name', 'placeholder' => $vars['entity']->name)); ?></div>
	</div>
	<?php

	$sticky_values = elgg_get_sticky_values('profile:edit');

	$profile_fields = elgg_get_config('profile_fields');
	if (is_array($profile_fields) && count($profile_fields) > 0) {
		foreach ($profile_fields as $shortname => $valtype) {
			$metadata = elgg_get_metadata(array(
				'guid' => $vars['entity']->guid,
				'metadata_name' => $shortname,
				'limit' => false
			));
			if ($metadata) {
				if (is_array($metadata)) {
					$value = '';
					foreach ($metadata as $md) {
						if (!empty($value)) {
							$value .= ', ';
						}
						$value .= $md->value;
						$access_id = $md->access_id;
					}
				} else {
					$value = $metadata->value;
					$access_id = $metadata->access_id;
				}
			} else {
				$value = '';
				$access_id = ACCESS_DEFAULT;
			}

			// sticky form values take precedence over saved ones
			if (isset($sticky_values[$shortname])) {
				$value = $sticky_values[$shortname];
			}
			if (isset($sticky_values['accesslevel'][$shortname])) {
				$access_id = $sticky_values['accesslevel'][$shortname];
			}

	?>
	<div class="control-group">
		<label class="control-label"><?php echo elgg_echo("profile:{$shortname}") ?></label>
		<?php
			$params = array(
				'name' => $shortname,
				'value' => $value,
			);
			echo '<div class="controls">' . elgg_view("input/{$valtype}", $params);
			$params = array(
				'name' => "accesslevel[$shortname]",
				'value' => $access_id,
			);
			echo elgg_view('input/access', $params) . '</div>';
		?>
	</div>
	<?php
		} //for end
	}

	elgg_clear_sticky_form('profile:edit');

	?>
	<div class="elgg-foot">
	<?php
		echo elgg_view('input/hidden', array('name' => 'guid', 'value' => $vars['entity']->guid));
	?>
		<div class="control-group">
			<div class="controls">
				<?php
					echo elgg_view('input/submit', array('value' => elgg_echo('save')));
				?>
			</div>
		</div>
	</div>
</div>