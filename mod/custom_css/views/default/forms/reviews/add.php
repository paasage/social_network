<?php
/**
 * Elgg Reviews add/edit form
 * 
 * @package Elgg
 *
 * @uses ElggEntity $vars['entity'] The entity to comment on
 * @uses bool       $vars['inline'] Show a single line version of the form?
 * @uses $vars['review_guid'] if is edit
 */
$char_limit = 300;
$value = "Review content up to {$char_limit} characters";
if (isset($vars['review_guid'])) {
  $review = get_entity($vars['review_guid']);
  $value = $review->description;
}

if (isset($vars['entity']) && elgg_is_logged_in()) {
  ?>
  <div class="review-form">

  <?php
  echo '<h3>' . 'Add a Review' . '</h3>';
  echo elgg_view('input/rate');
  echo elgg_view('input/longtext', array('name' => 'generic_review',
      'placeholder' =>	$value,
			 'maxlength'	=>	$char_limit));
  echo elgg_view('input/submit', array('value' => 'ADD', 'class' => 'review-button'));
  echo elgg_view('input/hidden', array(
      'name' => 'entity_guid',
      'value' => $vars['entity']->getGUID()
  ));
  if (isset($vars['review_guid'])) {
    echo elgg_view('input/hidden', array(
      'name' => 'review_guid',
      'value' => $vars['review_guid']
  ));
  }
  ?>
  </div>
    <?php
  }
  ?>
		
