<?php
/**
 * The user entity view for /groups/my_invite
 * 
 * @uses $vars['entity'] The User Entity.
 * @uses $vars['group_entity'] The Group Entity.
 */
$user = $vars['entity'];
$user_guid = $user->getGUID();

$name = $user->name;

$name_link = elgg_view( 'output/url', array(
					'href' => $user->getURL(),
					'text' => $name
				) );

$icon = elgg_view( 'output/img', array(
					'src' => $user->getIconURL('medium'),
					'class' => 'img-suggested img-invitation'
				) );

$action_icon = elgg_view( 'output/img', array(
					'src' => elgg_get_site_url() . '_graphics/groups/invite.png',
					'class' => 'invite'
				) );

$invite_link = elgg_view( 'output/url', array(
					'href' => '#',//'action/groups/invite?user_guid=' . $user->getGUID() . '&group_guid=' . $vars['group_entity']->getGUID(),
					'text' => $action_icon . 'Invite',
				) );

$options = array(
			'count' => TRUE,
			'owner_guids' => array($user_guid),
			'types' => array('object'),
			'subtypes' => array('draw_application')
		);
$models = elgg_get_entities($options);

$options['subtypes'] = array('groupforumtopic');
$questions = elgg_get_entities($options);

$options_annotations = array(
		'annotation_owner_guids' => array($user_guid),
		'annotation_calculation' => 'count',
		'annotation_names' => array('group_topic_post')
		);
$replies = elgg_get_annotations($options_annotations);

?>

<div class="user-view-invitation">
	<?php echo $icon;?>	
	<div class="invitations-contributions">
		<div class="name"><?php echo $name_link;?></div>
		<?php echo "{$models} Models {$questions} Questions {$replies} Replies"; ?>
	</div>
	<!--invitation-->
	<div class="invite-link">
		<span class="invitation-action action_color" group-uid="<?php echo $vars['group_entity']->getGUID();?>" user-uid="<?php echo $user_guid;?>">
			<?php echo $invite_link;?>
		</span>
	</div>
	
	<div class="invitation-sent">
		Invitation Sent
	</div>	
</div>