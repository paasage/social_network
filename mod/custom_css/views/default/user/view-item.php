<?php
/**
 * 
 * @uses $vars['entity'] The User Entity.
 * @uses $vars['group_entity'] The Group Entity.
 */
$user = $vars['entity'];

$name = $user->name;

$name_link = elgg_view( 'output/url', array(
					'href' => $user->getURL(),
					'text' => $name
				) );
$icon = elgg_view( 'output/img', array(
					'src' => $user->getIconURL('small'),
					'class' => 'img-suggested'
				) );
$action_icon = elgg_view( 'output/img', array(
					'src' => elgg_get_site_url() . '_graphics/groups/invite.png',
					'class' => 'invite'
				) );
$invite_link = elgg_view( 'output/url', array(
					'href' => 'action/groups/invite?user_guid=' . $user->getGUID() . '&group_guid=' . $vars['group_entity']->getGUID(),
					'text' => $action_icon . 'Invite',
					'is_action' => TRUE
				) );
?>

<div class="user-view-item">
	<?php echo $icon . $name_link;?>
	<span class="invitation action_color"><?php echo $invite_link;?></span>
</div>