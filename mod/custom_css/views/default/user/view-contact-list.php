<?php
/**
 * The user entity view for /friends/{$username}
 * 
 * @uses $vars['entity'] The User Entity.
 * @uses $vars['details'] additional details.
 */
$user = $vars['entity'];
if($user == NULL) {
	$users = elgg_get_admins();//[0];
	$user = $users[0];
}
$user_guid = $user->getGUID();
$site = elgg_get_site_url();
$name = $user->name;

$name_link = elgg_view('output/url', array(
    'href' => $user->getURL(),
    'text' => $name
        ));

$icon = elgg_view_entity_icon($user, $size, array('img_class' => 'img-suggested img-invitation'));

$remove_icon = elgg_view('output/img', array(
    'src' => $site . '_graphics/user/remove-user-profile.png',
    'class' => 'invite'
        ));

$message_icon = elgg_view('output/img', array(
    'src' => $site . '_graphics/user/message-profile.png',
    'class' => 'invite'
        ));

//remove action
$remove = elgg_view('output/url', array(
    'href' => '#',
    'text' => $remove_icon
        ));

//message action 
$message = elgg_view('output/url', array(
    //'href' => "messages/compose?send_to={$user_guid}",
    'href' => '#', //"#send-message",
    /* 'role'				=>	"button", 
      'data-toggle' =>	"modal", */
    'text' => $message_icon
        ));
$options = array(
    'count' => TRUE,
    'owner_guids' => array($user_guid),
    'types' => array('object'),
    'subtypes' => array('draw_application')
);
$models = elgg_get_entities($options);

$options['subtypes'] = array('groupforumtopic');
$questions = elgg_get_entities($options);
if (!$questions) {
  $questions = 0;
}

$options_annotations = array(
    'annotation_owner_guids' => array($user_guid),
    'annotation_calculation' => 'count',
    'annotation_names' => array('group_topic_post')
);
$replies = elgg_get_annotations($options_annotations);
if (!$replies) {
  $replies = 0;
}
?>

<div class="user-view-invitation">
  <?php echo $icon; ?>	

  <div class="invitations-contributions">
    <div class="name"><?php echo $name_link; ?></div>
    <?= $vars['details']; ?>
  </div>
  <!--invitation-->
  <div class="invite-link">
    <span class="invitation-action action_color send-message" user-uid="<?php echo $user_guid; ?>">
      <?php echo $message; ?>
    </span>
  </div>
  <br>
  <div class="invite-link">
    <span class="invitation-action action_color remove_friend" user-uid="<?php echo $user_guid; ?>" style="padding: 3px 14px;">
      <?php echo $remove; ?>
    </span>
  </div>

  <div class="invitation-sent">
    Friend Removed
  </div>	
</div>
