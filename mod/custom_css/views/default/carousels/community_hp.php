<?php
/**
 * 
 */
function group_view_entities_in_carousel($entities, $vars = array(), $offset = 0, $limit = 10, $full_view = true,
$list_type_toggle = true, $pagination = true) {

	if (!is_int($offset)) {
		$offset = (int)get_input('offset', 0);
	}

	if (is_array($vars)) {
		// new function
		$defaults = array(
			'items' => $entities,
			'list_class' => 'elgg-list-entity',
			'full_view' => true,
			'pagination' => true,
			'list_type' => $list_type,
			'list_type_toggle' => false,
			'offset' => $offset,
		);

		$vars = array_merge($defaults, $vars);

	} 
	
	return elgg_view('page/components/group/carousel-list', $vars);
}

$count_content = elgg_get_entities(array(
						'type' => 'group',
						'full_view' => false,
						'limit' => (int) 6,
						'count' => TRUE
					));

$content = elgg_list_entities(array(
						'type' => 'group',
						'full_view' => false,
						'limit' => (int) 6,
						'count_px' => $count_content
					), 'elgg_get_entities', 'group_view_entities_in_carousel');


if (!$content) {
	$content = elgg_echo('groups:none');
}

elgg_load_js('custom_css.groups.js');

?>
<ul class="elgg-menu elgg-breadcrumbs"><li>Community</li></ul>
<div class="row-fluid">
	
	<div class="community-carousel">
		<div class="community-wrapper">
			<h4>Suggested Groups</h4>
			
			<div class="community-inner-wrapper" style="margin-left: 0px; width: <?php echo 390*$count_content+80;?>px;">;
				<?php echo $content;?>
			</div>
			
		</div>
		
		<span class="controls arrow-left">&lt;</span>
		<span class="controls arrow-right">&gt;</span>
	</div>

</div>