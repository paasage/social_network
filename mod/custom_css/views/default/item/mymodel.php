<?php
/**
 * 
 * @use $vars['model'] The model Entity.
 */

$name = $vars['model']->appname;
$configs = isset($vars['model']->configs) ? $vars['model']->configs : 0;
$uses = isset($vars['model']->uses) ? $vars['model']->uses : 0; 
$visibility = $vars['model']->access_id;
?>
<div class="my-application">
	<span class="my-app-name"><?php echo $name; ?> </span>
	
	<div class="my-application-submenu">
	<?php if( $visibility == ACCESS_PUBLIC ) { ?>
		<div class="my-application-submenu-item">
			<select>
				<option>Public</option>
				<option>Private</option>
			</select>
			<select>
				<option>Action</option>
			</select>
		</div>
	<?php } ?>
	</div>
</div>