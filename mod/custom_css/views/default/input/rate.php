<?php
/**
 * 
 * the rate view
 * 
 */
?>
<div class="rating-wrap">
	<div class="radio-wrap">
		<input type="radio" name="rating" value="1"></input>
		<div class="rating-fill"></div>

		<input type="radio" name="rating" value="2"></input>
		<div class="rating-fill"></div>

		<input type="radio" name="rating" value="3"></input>
		<div class="rating-fill"></div>

		<input type="radio" name="rating" value="4"></input>
		<div class="rating-fill"></div>

		<input type="radio" name="rating" value="5"></input>
		<div class="rating-fill"></div>
	</div>
	<div class="rating-blank"></div>
</div>