<?php
/**
 * 
 * @uses $vars['value'] The value of div.
 * @uses $vars['class'] Class
 * @uses $vars['id'] Id
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
?>
<div class="<?php echo $vars['class']; ?>" id="<?php echo $vars['id']; ?>" contenteditable>
	<?=$vars['value'];?>
</div>
<div id="suggestions" class="suggestions-list"></div>
<div id="tips">
  <div class="arrow"></div>
</div>
<!--	Question Description (associate a model or component by typing @Name or @URL)-->