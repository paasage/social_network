<?php
/**
 * The span3 content of the models full view.
 * 
 * @author Christos Papoulas
 * 
 * @uses $vars['page-submenu'] The submenu option: reviews, models, component
 * default is discussion.
 * @uses $vars['model_guid'] The guid of the model.
 * @uses $vars['graph'] The metric for the graphs. uptime | rt | throughput | cost
 * @uses $vars['users_rates'] The number of users rates
 * @uses $vars['rating_mean'] The mean of user rates.
 * @uses $vars['providers'] Filter based on providers seperated by comma
 */
$submenu = $vars['page-submenu'];
$models_guid = $vars['model_guid'];
$url_model = elgg_get_site_url() . 'draw_app/view/' . $models_guid;
$run_filter_param = array('graph' => $vars['graph'], 'model_guid' => $vars['model_guid']);
?>
<div class="span-width-components">
  <div class="application-submenu">
    <div class="application-submenu-item">
      <a href="<?php echo $url_model . '/runs/cost'; ?>">Runs</a>
      <?php if ($vars['page-submenu'] == 'runs') { ?>
        <div class="arrow-up"></div>
      <?php } ?>
    </div>

    <!--
    <div class="application-submenu-item">
      <a href="<?php echo $url_model . '/components'; ?>">Components</a>
      <?php if ($submenu == 'components') { ?>
        <div class="arrow-up"></div>
      <?php } ?>
    </div>
    -->

    <div class="application-submenu-item">
      <a href="<?php echo $url_model . '/discussion'; ?>">Discussions</a>
      <?php if ($submenu == 'discussion') { ?>
        <div class="arrow-up"></div>
      <?php } ?>
    </div>

    <div class="application-submenu-item">
      <a href="<?php echo $url_model . '/reviews'; ?>">Reviews</a>
      <?php if ($submenu == 'reviews') { ?>
        <div class="arrow-up"></div>
      <?php } ?>
    </div>

    <div class="application-submenu-item">
      <a href="<?php echo $url_model . '/models'; ?>">Similar Models</a>
      <?php if ($submenu == 'models') { ?>
        <div class="arrow-up"></div>
      <?php } ?>
    </div>
  </div>
  <br>

  <?php
  if ($submenu == 'discussion') {
    echo elgg_view_comments(get_entity($models_guid));
  } elseif ($submenu == 'runs') {
    echo elgg_view('object/elements/models-run-filter', $run_filter_param);
  } elseif ($submenu == 'reviews') {
    echo elgg_view('object/elements/models-reviews', $vars);
  } elseif ($submenu == 'models') {
    echo elgg_view('object/elements/models-similar-models', $run_filter_param); 
  } elseif ($submenu == 'components') {
    ?>
    <div class="content clearfix" id="components-views" style="min-width: 1270px;
				 background-color: transparent">
			<?php 
			if(get_entity($models_guid)->appname == "JEnterprise") {
				echo elgg_view('page/elements/components/specific_app', $vars);
			} else {
				echo "<h4>The are no components for this model.</h4>";
			}
		}
		?>
    </div>
</div>