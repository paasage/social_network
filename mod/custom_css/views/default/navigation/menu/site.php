<?php
/**
 * This file overrides the Menu site view.
 * @author Christos Papoulas
 * @uses $vars['menu']['default']
 * @uses $vars['menu']['more']
 */

$default_items = elgg_extract('default', $vars['menu'], array());
$more_items = elgg_extract('more', $vars['menu'], array());

$site = elgg_get_site_url();
echo '<ul class="nav" style="margin-top: 5px;">';		// <- List starts here
echo '<a class="brand" href="#"><img id="passage-logo" src="'. elgg_get_site_url() . '_graphics/passage-logo.png' .'"></a>';
echo '<li><a href="' .$site. 'mystats/all"><span class="glyphicon glyphicon-home"></span></a></li>';

// If the user is logged in
if(elgg_is_logged_in()){
	$bootstrap_menu = '<li><a href="' . $site . 'draw_app/all' . '" id="models_topmenu">Models</a></li>'
	/*.' <li><a href="' . $site . 'components/all' . '">Components</a></li>'*/;
	$bootstrap_menu .= '<li><a href="' . $site . 'groups/all?filter=discussion' . '" id="community_topmenu">Community</a></li>';
} else {
	$bootstrap_menu = '<li><a href="' . $site . '" id="models_topmenu">Models</a></li>'
	/*.' <li><a href="' . $site . 'components/all' . '">Components</a></li>'*/;
	$bootstrap_menu .= '<li><a href="' . $site . '" id="community_topmenu">Community</a></li>';
}

echo $bootstrap_menu;

// More menu choices
foreach ($default_items as $item) {
	$link_class = 'elgg-menu-closed';
	if ($item->getSelected()) {
		$link_class = 'elgg-menu-opened';
	}

	$children = $item->getChildren();
	if ($children) {
		$item->addLinkClass($link_class);
		$item->addLinkClass('elgg-menu-parent');
	}

	$item_class = $item->getItemClass();
	if ($item->getSelected()) {
		$item_class = "$item_class elgg-state-selected";
	}
	if (isset($vars['item_class']) && $vars['item_class']) {
		$item_class .= ' ' . $vars['item_class'];
	}

	echo "<li class=\"$item_class\">";
	$content = $item->getContent();
	echo $content;

	if ($children) {
		echo elgg_view('navigation/menu/elements/section', array(
			'items' => $children,
			'class' => 'elgg-menu elgg-child-menu',
		));
	}
	echo '</li>';
}