<?php
/**
 * 
 * 
 */
elgg_load_library('myarea.library');
elgg_load_library('infinite.scroll.river_library');

$user_guid = elgg_get_logged_in_user_guid();


// get the models of a user
$options = array(
	'type' => 'object',
	'subtype' => 'draw_application',
	'owner_guid' => $user_guid,
	'full_view' => false,
	'limit' => 3
);
$models1 = elgg_get_entities($options);

// get the models that a user is contributor
$options = array(
	'relationship' => 'is_contributor_to',
	'relationship_guid' => $user_guid,
	'inverse_relationship' => FALSE
);
$models2 = elgg_get_entities_from_relationship($options);

// add all models in single variable
$allmodels = array_merge($models1, $models2); 



// functions
function cdo_socket_client($model_name) {
  error_log("cdo_socket_client called with param: " + $model_name);
  $address = '127.0.0.1';
  $port = 9009;
  $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket == false) {
    error_log("socket_create failed: " . socket_strerror(socket_last_error()) . "\n");
    return false;
  }
  $result = socket_connect($socket, $address, $port);
  if ($result === false) {
    error_log("socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n");
    return false;
  }

  $in = "GET /$model_name HTTP/1.1\r\n";
  $out = '';
  socket_write($socket, $in, strlen($in));
  socket_write($socket, NULL, strlen(EOF));
  while ($res = socket_read($socket, 2048)) {
    $out .= $res;
  }

  socket_close($socket);
  error_log("cdo_socket_client finished");
  
  return $out;
}

function cdo_get_providers($key) {
  $providers = array();
  $str = '';
  foreach ($key['vms'] as $vms) {
    $providers[] = $vms['provider'];
  }
  $providers_u = array_unique($providers);
  foreach ($providers_u as $s) {
    $str .= $s . " ";
  }
  return $str;
}

function cdo_get_uptime($start_t, $end_t) {
  $s_t = strtotime($start_t);
  $e_t = strtotime($end_t);
  return ($e_t - $s_t);
}

/**
 * 
 * @param type $vms Must be an array but is string
 * @return string
 */
function cdo_get_the_vms($vms) {
  $str = '';
  /*  foreach ($vms as $vm) {
    $str = $str . $vm['type'] . " ";
    if($vm['heapsize']) {
    $str = $str . 'app server heap: ' . $vm['heapsize'] . "<br>";
    }
    if($vm['buffer']) {
    $str = $str . 'db buffer size: ' . $vm['buffer'] . "<br>";
    }
    }
   */
  return $str;
}

function cdo_get_the_geography($vms) {
  $geography = array();
  foreach ($vms as $vm) {
    $geography[] = $vm['geography'];
  }
  $geography_u = array_unique($geography);
  return implode(' ', $geography_u);
}

function cdo_get_the_providers_rates($res) {
  $providers = array();
  $amazon = 0;
  $azure = 0;
  $google = 0;
  foreach ($res as $k) {
    foreach ($k['vms'] as $vm) {
      switch ($vm['provider']) {
        case 'Amazon':
          $amazon++;
          break;
        case 'Azure':
          $azure++;
          break;
        case 'Google':
          $google++;
          break;
        default:
          break;
      }
    }
  }
  $total = $amazon + $azure + $google;
  $providers['amazon'] = ($amazon / $total) * 100;
  $providers['azure'] = ($azure / $total) * 100;
  $providers['google'] = ($google / $total) * 100;
  return $providers;
}

function json_get_providers($jkey) {
  $vms = $jkey->{'vms'};
  $providers = array();
  foreach ($vms as $vm) {
    $providers[] = $vm->{'provider'};
  }
  return get_string_with_unique_elements($providers);
}

function json_get_geography($jkey) {
  $vms = $jkey->{'vms'};
  $geography = array();
  foreach ($vms as $vm) {
    $geography[] = $vm->{'location'};
  }
  return get_string_with_unique_elements($geography);
}

function json_get_providers_rates($jarray) {
  $providers = array();
  $amazon = 0;
  $azure = 0;
  $google = 0;
  foreach ($jarray as $k) {
    $jk = json_decode($k);
    foreach ($jk->{'vms'} as $vm) {
      switch ($vm->{'provider'}) {
        case 'Amazon':
          $amazon++;
          break;
        case 'Azure':
          $azure++;
          break;
        case 'Google':
          $google++;
          break;
        default:
          break;
      }
    }
  }
  $total = $amazon + $azure + $google;
  if ($total > 0) {
    $providers['amazon'] = ($amazon / $total) * 100;
    $providers['azure'] = ($azure / $total) * 100;
    $providers['google'] = ($google / $total) * 100;
  } else {
    $providers['amazon'] = 0;
    $providers['azure'] = 0;
    $providers['google'] = 0;
  }
  return $providers;
}

function get_string_with_unique_elements($ar) {
  $str = '';
  $ar_u = array_unique($ar);
  foreach ($ar_u as $s) {
    $str .= $s . " ";
  }
  return $str;
}
?>

<!-- show the results in a table -->
<div class="row-fluid color-body mymodels-view-apps">
  <h4>Active Runs</h4>
  <div class="row-fluid models-run-table" style="background-color: #fff;">
    
  <?php 
  foreach ($allmodels as $model) {

    // communicate with cdo
    $model_name = $model->appname;

    //////////////////////////////////////////////
    // Additional code for the review 4/12/15.  //
    // It parses a local xmi file and retrieves //
    // info from the execution models.          //
    //////////////////////////////////////////////
    if($model_name == "Service Management Application2") {   

      $txt_file    = file_get_contents('/var/www/elgg-min/mod/draw_app/views/default/page/elements/FullDeploymentBewan2ExecutionModels.xmi');
      $rows        = explode("\n", $txt_file);
      array_shift($rows);

      $executionModels = 0;
      $inExecutionModel = 0;
      $executionModelsArray = array();
      $executionModelsArrayIndex = 0;

      $executionContexts = 0;
      $inExecutionContext = 0;
      $executionContextsArray = array();

      $measurements = 0;
      $inMeasurement = 0;

      $countries = 0;
      $inCountries = 0;

      $provider = 0;
      $inProvider = 0;

      foreach($rows as $row => $data)
      {
    
        if(strpos($data, "executionModels")) {
          $executionModels += 1;
          $inExecutionModel = 1;
        }
        if(strpos($data, "/>")) {
          $inExecutionModel = 0;
        }
    
    
        // retrive name, startTime, endTime and totalCost of an executionContext
        if(strpos($data, "executionContexts")) {
          $executionContexts += 1;
          $inExecutionContext = 1;
        }
        if(strpos($data, "/>")) {
          $inExecutionContext = 0;
        }
        if($inExecutionContext == 1 && strpos($data, "name")){
          $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $nameArray = array();
          $nameArray[0] = "executionContext";
          $nameArray[1] = substr($data, strpos($data, "\"")+1, $name_length);
          $executionContextsArray[0] = $nameArray;
        
        }
        if($inExecutionContext == 1 && strpos($data, "startTime")){
          $startTime_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $startTimeArray = array();
          $startTimeArray[0] = "startTime";
          $startTimeArray[1] = substr($data, strpos($data, "\"")+1, $startTime_length);
          $executionContextsArray[1] = $startTimeArray;
        
        }
        if($inExecutionContext == 1 && strpos($data, "endTime")){
          $endTime_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $endTimeArray = array();
          $endTimeArray[0] = "endTime";
          $endTimeArray[1] = substr($data, strpos($data, "\"")+1, $endTime_length);
          $executionContextsArray[2] = $endTimeArray;
        
        }
        if($inExecutionContext == 1 && strpos($data, "totalCost")){
          $totalCost_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $totalCostArray = array();
          $totalCostArray[0] = "totalCost";
          $totalCostArray[1] = substr($data, strpos($data, "\"")+1, $totalCost_length);
          $executionContextsArray[3] = $totalCostArray;
        
        }
    
        $executionContextsArray[4][0] = "status";
        $executionContextsArray[4][1] = "undefined";    
    
        // retrieve geography from the first loactionModel
        if(strpos($data, "<countries")) {
          $countries += 1;
          $inCountries = 1;
        }
        if(strpos($data, "/>")) {
          $inCountries = 0;
        }
        if($inCountries == 1 && strpos($data, "id") && $countries == 1){
          $countries_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $countriesArray = array();
          $countriesArray[0] = "geography";
          $countriesArray[1] = substr($data, strpos($data, "\"")+1, $countries_length);
          for ($x = 0; $x < count($executionModelsArray); $x++) {
            $executionModelsArray[$x][5] = $countriesArray;
          }
        
        }
    
        // retrieve cloudProvider from the last organisationModel
        if(strpos($data, "<provider")) {
          $provider += 1;
          $inProvider = 1;
        }
        if(strpos($data, ">")) {
          $inProvider = 0;
        }
        if($inProvider == 1 && strpos($data, "name")){
          $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $nameArray = array();
          $nameArray[0] = "cloudProvider";
          $nameArray[1] = substr($data, strpos($data, "\"")+1, $name_length);
          for ($x = 0; $x < count($executionModelsArray); $x++) {
            $executionModelsArray[$x][6] = $nameArray;
          } 
        
        }
    
        // retrieve all the measurements of an executionContext
        if(strpos($data, "measurements")) {
          $measurements += 1;
          $index = 6 + $measurements;
          $inMeasurement = 1;
        }
        if(strpos($data, "/>")) {
          $inMeasurement = 0;
        }
        if($inMeasurement == 1 && strpos($data, "name")){
          $name_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $measurementsArray = array();
          $measurementsArray[0] = substr($data, strpos($data, "\"")+1, $name_length);
        }
        if($inMeasurement == 1 && strpos($data, "value")){
          $value_length = strrpos($data, "\"") - strpos($data, "\"") - 1;
          $measurementsArray[1] = substr($data, strpos($data, "\"")+1, $value_length);
          $executionContextsArray[$index] = $measurementsArray;
        
        }
    
        if (strpos($data, "</executionModels>")) {
          $executionModelsArray[$executionModelsArrayIndex] = $executionContextsArray;
          $executionModelsArrayIndex++;
          $measurements = 0;
        }

      }
      echo "</br><h4> $model_name </h4>";
          echo "<table id='app-runs-table'>
            <thead>
              <tr>
                <th title='Start Time'>Start</th>
                <th title='End Time'>End</th>
                <th title='Uptime'>Uptime</th>";
                $measurementsNames = array();
                for ($x = 7; $x < count($executionModelsArray[0]); $x++) {
                  $measurementsNames[$x] = $executionModelsArray[0][$x][0];
                }

                foreach ($measurementsNames as $value) {
                  echo "<th title='blabla'>" . $value . "</th>";
                }
          echo "<th title='Cost'>Cost</th>
                <th title='Cloud Providers'>";
          echo elgg_get_excerpt("Cloud Providers", 10);
          echo "</th>
                <th title='Geography'>Geography</th>
                <th title='Cost Effectiveness'>";
          echo elgg_get_excerpt("Cost Effectiveness", 10);
          echo "</th>
                <th title='Status'>Status</th>
            </tr>
            </thead>
            <tbody>";
                $no_entries = 1;
                foreach ($executionModelsArray as $executionContext) {

                  //***************** if execution is active *****************
                  if (!$executionContext[2][1]) {
                    $no_entries = 0;
                    $executionContextName = $executionContext[0][1];
                    $startTime = $executionContext[1][1];
                    $endTime = $executionContext[2][1];
                    $cost = $executionContext[3][1];
                    if ($endTime == "") {
                      $uptime = "TBA";
                      $costEffectiveness = "TBA";
                      $status = "running";
                    } else {
                      $uptime = strtotime($endTime) - strtotime($startTime);
                      $uptime = $uptime . " sec";
                      $costEffectiveness = $uptime / $cost;
                      $status = "stopped";
                    }
                    $geography = $executionContext[5][1];
                    $provider = $executionContext[6][1];

                    $view .= '<tr id="tr0" class="blabla">'
                        . '<td>' . $startTime . '</td>'
                        . '<td>' . $endTime . '</td>'
                        . '<td>' . $uptime . '</td>';

                    $measurementsValues = array();
                    for ($x = 7; $x < count($executionContext); $x++) {
                        $measurementsValues[$x] = $executionContext[$x][1];
                        $view .= "<td>$measurementsValues[$x]</td>";
                    }

                    

                    $view .='<td>' . $cost . '</td>'
                        . '<td>' . $provider . '</td>'
                        . '<td>' . $geography . '</td>'
                        . '<td>' . $costEffectiveness . '</td>'
                        . '<td>' . $status . '</td>'
                        . '</tr>';
                  } 
                }
                if ($no_entries == 0) {
                  echo $view;
                } else {
                  echo "<tr><td> no entries</td></tr>";
                }
      echo "</tbody>
          </table>";
          
  } 
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
  else {
    $socket_res = cdo_socket_client($model_name);
    $socket_res_json = json_decode($socket_res);

    echo "</br><h4> $model_name </h4>";
    echo "<table id='app-runs-table'>
            <thead>
              <tr>
                <th title='Start Time'>Start</th>
                <th title='End Time'>End</th>
                <th title='Uptime'>Uptime</th>";
                foreach ($socket_res_json as $key) {
                    $jkey = json_decode($key);
                    foreach ($jkey->{'measurements'} as $key => $value) {
                        echo "<th title='$key'>" . elgg_get_excerpt($key, 10) . "</th>";
                    }
                    break;
                }
          echo "<th title='Cost'>Cost</th>
                <th title='Cloud Providers'>";
          echo elgg_get_excerpt("Cloud Providers", 15);
          echo "</th>
                <th title='Geography'>Geography</th>
                <th title='Cost Effectiveness'>";
          //echo elgg_get_excerpt("Cost Effectiveness", 8);
          echo "Cost Effectiveness";
          echo "</th>
                <th title='Status'>Status</th>
            </tr>
            </thead>
            <tbody>";

            $view = "";
            $i = 0;
            $avgCostEffect = 0;
            $uptime = array();
            $provider_rates = json_get_providers_rates($socket_res_json);
            $no_entries = 1;
            foreach ($socket_res_json as $key) {
              $jkey = json_decode($key);

              //***************** if execution is active *****************
              if (!$jkey->{'end_time'}) {  
                $no_entries = 0;
                $providers = json_get_providers($jkey);

                $geography = json_get_geography($jkey);
                $uptime[] = cdo_get_uptime($jkey->{'start_time'}, $jkey->{'end_time'});
                $cost = $jkey->{'cost'} === 0.0 ? 0.03 : $jkey->{'cost'};
                $view .= '<tr id="tr' . $i . '" class="' . $jkey->{'id'} . '">'
                //. '<td>' . $i . '</td>'
                . '<td>' . $jkey->{'start_time'} . '</td>'
                . '<td>' . $jkey->{'end_time'} . '</td>'
                . '<td>' . $uptime[$i] . ' sec</td>';


                foreach ($jkey->{'measurements'} as $key => $value) {
                    $view .= "<td>$value</td>";
                }


                //if ($model_guid == 1837) { // Scalarm
                //    $cost_effectiveness = round($jkey->{'measurements'}->{'Meas'} / $cost, 2);
                //} else {
                    $cost_effectiveness = round($jkey->{'measurements'}->{AVGthroughput} / $cost, 2);
                //}
                if ($avgCostEffect == 0) {
                    $avgCostEffectg = $cost_effectiveness;
                } else {
                    $avgCostEffectg = ($avgCostEffectg + $cost_effectiveness) / 2;
                }
                $view .= '<td>' . $cost . '</td>'
                . '<td>' . $providers . '</td>'
                . '<td>' . $geography . '</td>'
                . '<td>' . $cost_effectiveness . '</td>'
                . '<td>' . (isset($jkey->{'end_time'}) ? "stopped" : "undef") . '</td>'
                //. '<td class="expand-execution-row"><span onclick="execution_row_toggle($(this))">&gt;</span></td>'
                . '</tr>';

                $view .= elgg_view('page/elements/execution-modal', 
                    array(
                        id => $i, execution => $jkey->{'id'},
                        providers => $providers, vms => $jkey->{'vms'},
                        //model_guid => $model_guid
                   ));
                $i++;
              } 
            }

            //$app = get_entity($model_guid);
            //$app->avgCostEffectiveness = $avgCostEffectg;
            //$app->save(); 
            if ($no_entries == 0) {
                  echo $view;
            } else {
                  echo "<tr><td> no entries</td></tr>";
            }

      echo "</tbody>
          </table>";
  }

  }
  ?>
</div>
</div>

