<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
elgg_load_css('custom_css_my_area');
?>

<!--
<div class="color-body mymodels-view-apps">
    <h4>Cloud Credentials</h4>
</div>
-->
<div class="row-fluid">
    <?php
    //echo elgg_view('myarea/action/addcredentials');
    ?>

    <!--
    <div class="color-body mymodels-view-apps alignment">
        <h4>Existing CDO servers</h4>

        <ul style="margin-top: 10px">
            <li style="list-style: inherit;">public1</li>
        </ul>
    </div>
    -->
</div>

<div class="color-body mymodels-view-apps">
    <h4>1) PaaSage Platform</h4>

    <?php
    echo elgg_view_form('credentials/addPlatformCredentials');
    //echo elgg_view('myarea/action/addcredentials');
    ?>

    <!--
    <form class="form-horizontal" style="background-color: #ffffff; padding: 20px;" >
    <div class="control-group">
        <label class="control-label" for="endpoint">Communication endpoint</label>
        <div class="controls">
            <input type="text" id="endpoint" name="endpoint" placeholder="communication endpoint">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="usernameendpoint">username</label>
        <div class="controls">
            <input type="text" id="usernameendpoint" name="usernameendpoint" placeholder="use name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="pwdendpoint">password</label>
        <div class="controls">
            <input type="password" name="pwdendpoint" id="pwdendpoint">
        </div>
    </div>
    <div>
        <button type="button" class="btn">Add</button>
    </div>
    </form>
    -->
</div>

<div class="color-body mymodels-view-apps">
    <h4>2) Cloud provider credentials</h4>
    <p>(fill in only the boxes that are necessary for your deployment)</p>
    <?php
    echo elgg_view_form('credentials/addCloudProviderCredentials');
    //echo elgg_view('myarea/action/addcredentials');
    ?>
</div>

<div class="color-body mymodels-view-apps">
    <h4>3) Upload provider model </h4>
    <p>(it should be done only once for each cloud provider for the lifetime of a PaaSage platform)</p>
    <?php
    echo elgg_view_form('credentials/uploadProviderModel');
    //echo elgg_view('myarea/action/addcredentials');
    ?>
</div>

