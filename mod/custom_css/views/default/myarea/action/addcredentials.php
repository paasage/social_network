<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 
<form class="form-horizontal alignment" style="background-color: #ffffff; padding: 20px;" >
    <h4>Add a CDO server</h4>
    <div class="control-group">
        <label class="control-label" for="hostname">Hostname</label>
        <div class="controls">
            <input type="text" id="hostname" name="hostname" placeholder="host name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="port">Communication port</label>
        <div class="controls">
            <input type="text" name="port" id="port" placeholder="port">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="username">username</label>
        <div class="controls">
            <input type="text" id="username" name="username" placeholder="use name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="pwd">password</label>
        <div class="controls">
            <input type="password" name="pwd" id="pwd">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <select>
                <option>Public</option>
                <option>Private</option>
            </select>
        </div>
    </div>
    <div>
        <button type="button" class="btn">Add</button>
    </div>
</form>
