<?php
/**
 * 
 * 
 */
elgg_load_library('myarea.library');
elgg_load_library('infinite.scroll.river_library');

$user_guid = elgg_get_logged_in_user_guid();

$running_models = elgg_list_entities(array(
	'type' => 'object',
	'subtype' => 'draw_application',
	'owner_guid' => $user_guid,
	'full_view' => false,
	'limit' => 2,
	'metadata_name_value_pairs' => array(
					'running' => false
	)
), 'elgg_get_entities_from_metadata', 'user_stats_top_rated_apps');

?>

<div class="row-fluid color-body mymodels-view-apps">
  <h4>Active Models</h4>
  <?php  if($running_models != ""){
          echo "<hr>" . $running_models;
  } ?>
</div>

