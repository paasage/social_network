<?php

/**
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
$user_guid = elgg_get_logged_in_user_guid();

$apps = elgg_get_entities_from_relationship(array(
	'relationship' => 'watching',
	'relationship_guid' => $user_guid,
	'inverse_relationship' => FALSE
	));
echo '<div class="clearfix row-fluid watching-apps-view">';
foreach ($apps as $a) {
	echo '<div class="app-entity-wrapper">';
	echo elgg_view_entity($a, array(
		view_source => 'watching'
	));
	echo '</div>';	
}
echo '</div>';
