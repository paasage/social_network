<?php
/**
 * 
 * @uses $vars['link'] The active submenu as String.
 * 
 */

$topbar_items = array('Configurations', 'Active Runs', 'My Models', 'Credentials', 'Apps Watched');
$topbar_links = array('configuration', 'activeruns', 'mymodels', 'credentials', 'watching')
?>
<div class="row-fluid">
	<div class="span-width-components">
		
	<div class="application-submenu">
		<?php $i = 0;
			foreach ($topbar_items as $item) {
				$active = $vars['link'] == $topbar_links[$i];
				echo elgg_view('myarea/topbar-item', array(
						'text' => $item, 
						'active' => $active,
						'href' => 'myarea/' . $topbar_links[$i++]
						));
		} ?>
	</div>
	<br>
	
	</div>
</div>