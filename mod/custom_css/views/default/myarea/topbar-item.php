<?php
/**
 * 
 * @uses $vars['text'] The displaying text. 
 * @uses $vars['href']
 * @uses $vars['active'] If the specific link is active.
 */

?>

<div class="application-submenu-item">
	<?php 
		echo elgg_view('output/url', $vars);
		if($vars['active']) { ?>
			<div class="arrow-up"></div>
		<?php } ?>
</div>