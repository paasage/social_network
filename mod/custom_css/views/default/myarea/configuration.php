<?php
/**
 * 
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$options = array(
	'type' => 'object',
	'subtype' => 'execution_of_app',
	'owner_guids' => array(elgg_get_logged_in_user_guid()),
	'count' => TRUE
);
$count = elgg_get_entities($options);
$options['count'] = FALSE;
$entities = elgg_list_entities($options);
?>

<div class="color-body mymodels-view-apps">
	<?php if($count > 0) { 
		echo "<h4>Saved executions ($count)</h4>";
		echo $entities;
	 }else {  ?>
	<h4>Configuration of application model's executions</h4>
	<?php } ?>
</div>
