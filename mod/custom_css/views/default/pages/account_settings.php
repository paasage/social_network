<?php
/**
 * Elgg user account settings.
 *
 * Overrides /pages/seetings/account.php
 * 
 * @package Elgg
 * @subpackage Core
 */

// Only logged in users
gatekeeper();

// Make sure we don't open a security hole ...
$page_owner = elgg_get_page_owner_entity();
if ((!$page_owner) || (!elgg_get_page_owner_entity()->canEdit())) {
	register_error(elgg_echo('noaccess'));
	forward('/');
}

//$title = elgg_echo('usersettings:user');

$content = elgg_view('core/settings/account');

$params = array(
	'content' => $content,
	'sidebar' => elgg_view('page/sidebars/profile/settings', array('page' => 'settings')),
	'title'		=> $title,
);
$body = elgg_view_layout('one_sidebar', $params);

echo elgg_view_page($title, $body);
