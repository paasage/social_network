<?php
/**
 * Elgg friends page
 * 
 * overrides /pages/friends/index.php
 *
 * @package Elgg.Core
 * @subpackage Social.Friends
 * 
 * @uses $vars['reverse'] View reverse relationship.
 */

$owner = elgg_get_page_owner_entity();
if (!$owner) {
	// unknown user so send away (@todo some sort of 404 error)
	forward();
}

$title = elgg_echo("friends:owned", array($owner->name));

$options = array(
	'relationship' => 'friend',
	'relationship_guid' => $owner->getGUID(),
	'inverse_relationship' => FALSE,
	'type' => 'user',
	'full_view' => FALSE
);

if($vars['reverse']) {
	$options['inverse_relationship'] = TRUE;
}

$content = elgg_view('page/elements/profile/user-header', array(
		'guid' => $owner->getGUID(),
		'icon' => elgg_view_entity_icon($owner, 'medium', $vars)
));
$friends = elgg_get_entities_from_relationship($options);

$friends_view = elgg_view('object/elements/friends-list', array(
	'friends' => $friends,
	'view_friendsOf_link' => !$vars['reverse']
	));
$content .= $friends_view;


$params = array(
	'content' => $content,
	'title' => $title,
);
$body = elgg_view_layout('one_column', $params);

echo elgg_view_page($title, $body);


