<?php
/**
 * Upload and crop an avatar page.
 * 
 * overrides pages/avatar/edit.php
 */

// Only logged in users
gatekeeper();

elgg_set_context('profile_edit');

$entity = elgg_get_logged_in_user_entity();
if (!elgg_instanceof($entity, 'user') || !$entity->canEdit()) {
	register_error(elgg_echo('avatar:noaccess'));
	forward(REFERER);
}

$content = elgg_view('pages/profile/avatar/upload', array('entity' => $entity));

$params = array(
	'content' => $content,
	'sidebar' => elgg_view('page/sidebars/profile/settings', array('page' => 'avatar'))
);
$body = elgg_view_layout('one_sidebar', $params);

echo elgg_view_page($title, $body);
