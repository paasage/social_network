<?php 
/**
 * Edit profile page
 * 
 * Overrides pages/profile/edit.php
 * 
 * @uses $vars['page'] The current page.
 */

gatekeeper();

$user = elgg_get_logged_in_user_entity();
if (!$user) {
	register_error(elgg_echo("profile:notfound"));
	forward();
}

// check if logged in user can edit this profile
if (!$user->canEdit()) {
	register_error(elgg_echo("profile:noaccess"));
	forward();
}

elgg_set_context('profile_edit');

$content = elgg_view_form('profile/edit', array('class' => 'form-horizontal'), array('entity' => $user));

$params = array(
	'content' => $content,
	'sidebar' => elgg_view('page/sidebars/profile/settings', $vars)
);
$body = elgg_view_layout('one_sidebar', $params);

echo elgg_view_page($title, $body);
