$(document).ready(function () {

  console.log('addPlatformCredentials.js');

  // when the download button is clicked
  $('button#addPlatformCredentials').on('click', function () {
    var endpoint = document.getElementById('endpoint').value;
    var emailendpoint = document.getElementById('emailendpoint').value;
    var pwdendpoint = document.getElementById('pwdendpoint').value;
    var usernameendpoint = document.getElementById('usernameendpoint').value;
    //var model_name = $(this).attr('name');

    elgg.action('credentials/addPlatformCredentials', {
      data: {
        endpoint: endpoint,
        emailendpoint: emailendpoint,
        pwdendpoint: pwdendpoint,
        usernameendpoint: usernameendpoint
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");

        console.log(resultText.output);

        if (resultText.output != '') {
          
          elgg.system_message("Platform credentials saved successfully!");
          //setTimeout(myFunction, 3000);
        } else {
          location.reload();
          elgg.system_message("Something went wrong while storing Platform credentials!");
        }
      }
    });
  });

  function myFunction() {
    location.reload();
  }

});