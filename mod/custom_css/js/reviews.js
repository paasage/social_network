/**
 * 
 * @param {type} param
 * 
 */

$( document ).ready(function () {
	$('button.helpful').on('click', function () {
		var guid = $(this).attr('data-uid');
		var $helpful_wrapper = $(this);
		console.log('click ' + guid);
		elgg.action('reviews/ishelpful', {
			data: {
				guid: guid
			},
			success: function () {
				;
			},
			beforeSend: function(){
				$helpful_wrapper.parent().remove();
      }
		});
	});
});

