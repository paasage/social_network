/**
 * elgg library: custom_css.view.friend.js
 * The hover actions for view friend list.
 * 
 */

$('document').ready(function () {
  console.log('view-friend.js');

  // hover display actions
  $('.user-view-invitation').hover(
    function () {
      $(this).children('.invite-link').children('.invitation-action').css('visibility', 'visible');
      $(this).find('.contributor').css('visibility', 'visible');
    },
    function () {
      $(this).children('.invite-link').children('.invitation-action').css('visibility', 'hidden');
      $(this).find('.contributor').css('visibility', 'hidden');
    }
  );

  // execute actions on click
  // remove friend
  $('.remove_friend').on('click', function () {
    var uid = $(this).attr('user-uid');
    var $actionRemoveDiv = $(this).parent().next();
    var $removeDiv = $(this).parent().parent();
    elgg.action('action/friends/remove', {
      data: {
        friend: uid,
      },
      success: function () {
        $actionRemoveDiv.show();
        setTimeout(function () {
          $removeDiv.hide('slow', function () {
            $removeDiv.remove();
          });
        }, 1800);
      }
    }
    );
  });

  // send message
  $('.send-message').on('click', function () {
    var uid = $(this).attr('user-uid');

    $.ajax({
      datatype: 'json',
      data: {'recipient_guid': uid},
      url: '/elgg-mit/modals/send-message',
      success: function (d) {
        console.log(d);
        $('body').append(d);
        $('#send-message-' + uid).modal('show');
      }
    });
  });
  
  // add contibutor
  $('.contributor.add').on('click', function (){
		uid = $(this).attr('uid');
		appid = $(this).attr('appid');
		$userview = $(this).parent().parent();
		$(this).remove();
    elgg.action('action/contributors/add', {
      data: {
        user: uid,
        app: appid
      },
      success: function () {
				$userview.remove();
				$( '#contibutors' ).append($userview);
      }
    });
  });
	// remove a contributor
	$('.contributor.remove').on('click', function (){
		uid = $(this).attr('uid');
		appid = $(this).attr('appid');
		$userview = $(this).parent().parent();
		$(this).remove();
    elgg.action('action/contributors/remove', {
      data: {
        user: uid,
        app: appid
      },
      success: function () {
				$userview.remove();
				$( '#friends' ).append($userview);
      }
    });
  });
});
