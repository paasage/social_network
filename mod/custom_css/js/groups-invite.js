/**
 * For group invitation page
 */

$( document ).ready(function () {
	console.log('groups-invite.js');
	
	
	// hover on user entity view
	$( '.user-view-invitation' ).hover(
		function () {
			$(this).children('.invite-link').children('.invitation-action').css('visibility', 'visible');
		}, 
		function () {
			$(this).children('.invite-link').children('.invitation-action').css('visibility', 'hidden');
		}
	);
	
	// action when press invite button
	$( '.invitation-action' ).on('click', function () {
		var group = $(this).attr('group-uid');
		var user = $(this).attr('user-uid');
		
		console.log('invite ' + user + ' user to group ' + group);
		console.log('this ' + $(this));
		var $actionDiv = $(this).parent().next();
		var $removeDiv = $(this).parent().parent();
		
		elgg.action('action/groups/invite', {
			data: {
				user_guid: user,
				group_guid: group
			},
			success: function () {
				$actionDiv.show();
				setTimeout(function() {
						$removeDiv.hide('slow', function(){ $removeDiv.remove(); });
				}, 1800);
			}
		});
	});
	
});

