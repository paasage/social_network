$(document).ready(function () {

  console.log('addPlatformCredentials.js');

  // read the xmi file
  var file;
  var reader;
  $(':file').change(function(){
      file = this.files[0];
      reader = new FileReader();
      reader.readAsText(file, 'UTF-8');
  });

  // when the addPlatformCredentials button is clicked
  $('button#addPlatformCredentials').on('click', function () {
    var endpoint = document.getElementById('endpoint').value;
    var emailendpoint = document.getElementById('emailendpoint').value;
    var pwdendpoint = document.getElementById('pwdendpoint').value;
    var usernameendpoint = document.getElementById('usernameendpoint').value;
    //var model_name = $(this).attr('name');

    elgg.action('credentials/addPlatformCredentials', {
      data: {
        endpoint: endpoint,
        emailendpoint: emailendpoint,
        pwdendpoint: pwdendpoint,
        usernameendpoint: usernameendpoint
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");

        console.log(resultText.output);

        if (resultText.output != '') {
          
          elgg.system_message("Platform credentials saved successfully!");
          //setTimeout(myFunction, 3000);
        } else {
          location.reload();
          elgg.system_message("Something went wrong while storing Platform credentials!");
        }
      }
    });
  });

  function myFunction() {
    location.reload();
  }

  // when the addCloudProviderCredentials button is clicked
  $('button#addCloudProviderCredentials').on('click', function () {

    // flexiant
    var FlexiantUsername = document.getElementById('FlexiantUsername').value;
    var FlexiantPassword = document.getElementById('FlexiantPassword').value;
    var FlexiantEndpoint = document.getElementById('FlexiantEndpoint').value;

    // GWDG
    var GWDGUsername = document.getElementById('GWDGUsername').value;
    var GWDGPassword = document.getElementById('GWDGPassword').value;
    var GWDGEndpoint = document.getElementById('GWDGEndpoint').value;

    var GWDGFilterKey1 = document.getElementById('GWDGFilterKey1').value;
    var GWDGFilterValue1 = document.getElementById('GWDGFilterValue1').value;
    var GWDGFilterKey2 = document.getElementById('GWDGFilterKey2').value;
    var GWDGFilterValue2 = document.getElementById('GWDGFilterValue2').value;

    // OMISTACK
    var OMISTACKUsername = document.getElementById('OMISTACKUsername').value;
    var OMISTACKPassword = document.getElementById('OMISTACKPassword').value;
    var OMISTACKEndpoint = document.getElementById('OMISTACKEndpoint').value;

    // Amazon
    var AmazonEC2Username = document.getElementById('AmazonEC2Username').value;
    var AmazonEC2Password = document.getElementById('AmazonEC2Password').value;
    var AmazonEC2Endpoint = document.getElementById('AmazonEC2Endpoint').value;
    
    var AmazonEC2FilterKey1 = document.getElementById('AmazonEC2FilterKey1').value;
    var AmazonEC2FilterValue1 = document.getElementById('AmazonEC2FilterValue1').value;
    var AmazonEC2FilterKey2 = document.getElementById('AmazonEC2FilterKey2').value;
    var AmazonEC2FilterValue2 = document.getElementById('AmazonEC2FilterValue2').value;
    
    //var model_name = $(this).attr('name');

    elgg.action('credentials/addCloudProviderCredentials', {
      data: {
        FlexiantUsername: FlexiantUsername,
        FlexiantPassword: FlexiantPassword,
        FlexiantEndpoint: FlexiantEndpoint,
        GWDGUsername: GWDGUsername,
        GWDGPassword: GWDGPassword,
        GWDGEndpoint: GWDGEndpoint,
        GWDGFilterKey1: GWDGFilterKey1,
        GWDGFilterValue1: GWDGFilterValue1,
        GWDGFilterKey2: GWDGFilterKey2,
        GWDGFilterValue2: GWDGFilterValue2,
        OMISTACKUsername: OMISTACKUsername,
        OMISTACKPassword: OMISTACKPassword,
        OMISTACKEndpoint: OMISTACKEndpoint,
        AmazonEC2Username: AmazonEC2Username,
        AmazonEC2Password: AmazonEC2Password,
        AmazonEC2Endpoint: AmazonEC2Endpoint,
        AmazonEC2FilterKey1: AmazonEC2FilterKey1,
        AmazonEC2FilterValue1: AmazonEC2FilterValue1,
        AmazonEC2FilterKey2: AmazonEC2FilterKey2,
        AmazonEC2FilterValue2: AmazonEC2FilterValue2
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");

        console.log(resultText.output);

        if (resultText.output != '') {
          
          elgg.system_message("Cloud provider credentials saved and sent to PaaSage Platform successfully!");
          //setTimeout(myFunction, 3000);
        } else {
          //location.reload();
          elgg.system_message("Something went wrong while storing cloud provider credentials!");
        }
      }
    });
  });


  // when the upload button is clicked
  $('button#uploadProviderModel').on('click', function () {
    var model_guid = $(this).attr('data-uid');
  
    var cloudProvider = document.getElementById("cloudProvider").value
    
    // show the loading image
    var iconDiv = document.getElementsByName("upload_xmi_gif_hidden");
    iconDiv[0].setAttribute("name","upload_xmi_gif_shown");

    elgg.action('credentials/uploadFlexiantProviderModel', {
      data: {
        val: reader.result,
        model_guid: model_guid,
        cloudProvider: cloudProvider
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");

        console.log(resultText.output);

        if (resultText.output == 'READY_TO_REASON') {

          // hide the loading image
          var iconDiv = document.getElementsByName("upload_xmi_gif_shown");
          iconDiv[0].setAttribute("name","upload_xmi_gif_hidden");
          
          elgg.system_message("Provider model uploaded successfully!");
          //setTimeout(myFunction, 3000);
        } else {
          location.reload();
          elgg.system_message("Something went wrong while uploading provider model! Please try again!");
        }
      }
    });
  });


  

});