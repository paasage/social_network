$(document).ready(function () {

  console.log('download_abstract_model.js');

  // when the download button is clicked
  $('a#download_abstract_model').on('click', function () {
    var model_guid = $(this).attr('data-uid');
    var model_name = $(this).attr('name');

    elgg.action('download_xmi/download_abstract_model', {
      data: {
        model_guid: model_guid
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");
        //$('.model-specific-sidebar-API').html(resultText.output);
        //var myWindow = window.open("", "MsgWindow", "top=500, left=500, width=400, height=200");
        //myWindow.document.write("<p>" + resultText.output + "</p>");

        //alert(resultText.output);

        if (resultText.output) {
          var interval = setInterval(function() {
            saveInFile(resultText.output, interval, model_name);
          }, 1000);
        }
      }
    });
  });

  function saveInFile(outtext, interval, model_name) {
      //  create a new Blob (html5 magic) that conatins the data from your form feild
      var textFileAsBlob = new Blob([outtext], {type:'text/plain'});
        
      // Specify the name of the file to be saved
      var fileNameToSaveAs = model_name.concat(".xmi");
        
      // create a link for our script to 'click'
      var downloadLink = document.createElement("a");
      downloadLink.download = fileNameToSaveAs;
      downloadLink.innerHTML = "My Hidden Link";
        
      // Create the link Object.
      window.URL = window.URL || window.webkitURL;
      downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
      downloadLink.style.display = "none";
      document.body.appendChild(downloadLink);
        
      // click the new link
      downloadLink.click();
    
      clearInterval(interval);
  };

});