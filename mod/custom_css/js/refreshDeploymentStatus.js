$(document).ready(function () {

  console.log('refreshDeploymentStatus.js');

  // when the refreshDeploymentStatus button is clicked
  $('button#refresh_status').on('click', function () {

    var model_name = $(this).attr('modelName');

    elgg.action('refresh_deployment_status/refreshDeploymentStatus', {
      data: {
        model_name: model_name
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");

        console.log(resultText.output);

        // retrieve resource primary key from the response
        var position = resultText.output.indexOf(";");
        var state = resultText.output.slice(0,position);
        var substate = resultText.output.slice(position+1,resultText.output.length);

        // change the values on the page (state, substate and time)
        document.getElementById("state_"+model_name).innerHTML = "state: " + state;
        document.getElementById("substate_"+model_name).innerHTML = "subState: " + substate;
        var d = new Date();
        var now = d.getTime();
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var currentDate = d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear() + " @ " + d.getHours() + ":" + d.getMinutes();
        document.getElementById("time_"+model_name).innerHTML = "information as of: " + currentDate;

        //if (resultText.output != '') {
        //  elgg.system_message("Platform credentials saved successfully!");
        //} else {
        //  elgg.system_message("Something went wrong while storing Platform credentials!");
        //}

      }
    });

  });

});