/*
 * lib: custom.css.menu.js
 */

$( document ).ready(function() {
    var pathname = window.location.pathname; 
    if (pathname.indexOf("draw_app") >= 0) {
        $("#models_topmenu").css('color', '#ffffff');
    } else if(pathname.indexOf("groups") >= 0 || pathname.indexOf("discussion") >= 0) {
        $(".elgg-menu-item-groups a").css('color', '#ffffff');
    } else if(pathname.indexOf("mystats") >= 0){
        $("li a span.glyphicon-home").css('color', '#ffffff');
    } else if(pathname.indexOf("myarea") >= 0 || pathname.indexOf("my_profile") >= 0 ||
            pathname.indexOf("friends") >= 0 || pathname.indexOf("messages") >= 0 ||
            pathname.indexOf("settings") >= 0) {
        ; //do nothing
    } else if(pathname.indexOf("components") >= 0) {
        $("#components_topmenu").css('color', '#ffffff');
    }
});