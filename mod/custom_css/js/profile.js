/**
 * 
 * library name: custom_css.profile.js
 */


$(document).ready(function () {
  console.log('profile.js');
  //delete an interest
  $('body').on('click', '#interest-tags-container .skill-tag .skill-tag-delete-btn', function () {
    var id = $(this).attr('data-uid');
    var $interest = $(this);
    elgg.action('tags/delete/interest', {
      data: {
        id: id
      },
      success: function () {
        $interest.parent().remove();
      },
      beforeSend: function () {
        $interest.parent().css('background-color', 'red');
      }
    });
  });

  //delete a skill
  $( 'body' ).on('click', '#skill-tags-container .skill-tag .skill-tag-delete-btn', function () {
    var id = $(this).attr('data-uid');
    var $skill = $(this);
    elgg.action('tags/delete/skill', {
      data: {
        id: id
      },
      success: function () {
        $skill.parent().remove();
      },
      beforeSend: function () {
        $skill.parent().css('background-color', 'red');
      }
    });
  });
});

function add_interest() {
  var interest = $('.add-interest').val();
	$('.add-interest').val("");
  elgg.action('tags/add/interest', {
    data: {
      interest: interest
    },
    success: function (resultText, success, xhr) {
      console.log('annotation this ' + this);
      $('#interest-tags-container').append('<div class="skill-tag" >' +
        '<span class="skill-tag-name">' + interest + '</span>' +
        '<span class="skill-tag-delete-btn" data-uid="' + resultText.output + '">&times;</span>' +
        '</div>');
      $('#interest-form').css('visibility', 'visible');
      $('.loading_icon').remove();
    }, beforeSend: function () {
      $('#interest-form').parent().append('<img src="../_graphics/ajax_loader_bw.gif" class="loading_icon">');
      $('#interest-form').css('visibility', 'hidden');

    }
  });
  return false;
}

function add_skill() {
  var skill = $('.add-skill').val();
	$('.add-skill').val("");
  elgg.action('tags/add/skill', {
    data: {
      skill: skill
    },
    success: function (resultText, success, xhr) {
      $('#skill-tags-container').append('<div class="skill-tag" >' +
        '<span class="skill-tag-name">' + skill + '</span>' +
        '<span class="skill-tag-delete-btn" data-uid="' + resultText.output + '">&times;</span>' +
        '</div>');
      $('#skills-form').css('visibility', 'visible');
      $('.loading_icon').remove();
    }, beforeSend: function () {
      $('#skills-form').parent().append('<img src="../_graphics/ajax_loader_bw.gif" class="loading_icon">');
      $('#skills-form').css('visibility', 'hidden');

    }
  });
  return false;
}