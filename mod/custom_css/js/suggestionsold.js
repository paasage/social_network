/**
 * library: custom_css.suggestions.js
 */

// our global vars
window.currentkey = "";
window.selecteditem = 0;
window.totalitems = 0;
window.flag = false;


$('document').ready(function () {
  console.log('suggestions.js');

  // FOR INSIDE SUGGESTIONS
  var $edit = $('#editablediv');
  var $sug = $('#suggestions');
  // for autocomplete from elgg model.

  $edit.keydown(function (event) {

    if (event.shiftKey && event.keyCode === 50) {
      window.flag = true;
      window.currentkey = "";
      var pos = getCaretPixelPos($edit[0], -16.53, 97);
      $sug.css('display', 'block');
      t = Math.round(parseFloat(pos.top)) + 17 - 100 + window.scrollY + $('#editablediv').offset().top;
      l = Math.round(parseFloat(pos.left)) + $('#editablediv').offset().left;
      $sug.css('top', t);
      $sug.css('left', l);
    } else if (event.keyCode === 32) { //space
      removeSuggestionList($sug);
    } else if (window.flag) {
      if (bindKeyboardArrows(event, $sug)) {
        return;
      }
      if (event.keyCode === 8) {  //backspace
        if (window.currentkey.length === 1) {
          window.currentkey = "";
          $sug.html('');
        } else if (window.currentkey.length === 0) { // stop suggestion
          removeSuggestionList($sug);
        } else {
          window.currentkey = window.currentkey.slice(0, -1);
          sendGetRequestAndshowSuggestions($sug);
        }
      } else if (event.keyCode !== 16) { //shift
        if ((c = letterOrNumber(event)) !== '') {
          tmp = window.currentkey + letterOrNumber(event);
          window.currentkey = tmp;
          sendGetRequestAndshowSuggestions($sug);
        } else {
          removeSuggestionList($sug);
        }
      }
    }
  });
  // for associate execution
  $('#execution-hint img').on('click', function (e) {
    $('#execution-association').toggle();
  });

  $('#execution-association input').on('keydown', function (e) {
    if (e.which === 13) { // enter
      e.stopPropagation();
      e.preventDefault();
      associate();
      return false;
    }
  });

  // FOR OUTSIDE SUGGESTIONS WITH BUTTONS
  $('#application-hint img').on('click', function () {
    $('#suggest-app-out-form').toggle();
  });

  $('#suggest-app-out-form input').on('keyup', function (e) {
    if (!e.shiftKey) {
      sendGetRequestAndshowSuggestions($('#suggest-app-out-form .suggestions-list'), $('#suggest-app-out-form input').val());
    }
  });
  $(document).on('click', '.suggestions-list .suggestion-item-out', function () {
    var name = $(this).attr('data-uid');
    var url = $(this).attr('data-href');
    var guid = $(this).attr('data-guid');

    if ($('div.tags-wrapper > a[data-uid=' + name + ']').size() === 0) {
      $('.tags-wrapper').append('<a href="' + url + '" data-uid="' + name + '" target="_blank" data-guid="' + guid + '">' + name + '<span class="delete">&#215;</span></a>');
    }
  });
  $(document).on('mouseover', '.tags-wrapper a', function () {
    console.log('mouseover');
    $(this).children().css({'visibility': 'visible'});
  });
  $(document).on('mouseout', '.tags-wrapper a', function () {
    console.log('mouseout');
    $(this).children().css({'visibility': 'hidden'});
  });
  $(document).on('click', '.tags-wrapper span.delete', function (e) {
    $(this).parent().remove();
    e.preventDefault();
    e.stopPropagation();
  });
});

function associate() {
  var execution = $('#execution-association input').val();
  var guid = $('#execution-association select option:selected').attr('value');
  $('.tags-wrapper').append('<a href="../../../draw_app/view/' + guid + '/runs/' + execution + '" target="_blank" data-guid="' +
    guid + '" data-execution="' + execution + '">'
    + execution + '<span class="delete">&#215;</span></a>');
  $('#execution-association').hide();
  return false;
}


var getCaretPixelPos = function ($node, offsetx, offsety) {
  offsetx = offsetx || 0;
  offsety = offsety || 0;

  var nodeLeft = 0,
    nodeTop = 0;
  if ($node) {
    nodeLeft = $node.offsetLeft;
    nodeTop = $node.offsetTop;
  }

  var pos = {left: 0, top: 0};

  if (document.selection) {
    var range = document.selection.createRange();
    pos.left = range.offsetLeft + offsetx - nodeLeft + 'px';
    pos.top = range.offsetTop + offsety - nodeTop + 'px';
  } else if (window.getSelection) {
    var sel = window.getSelection();
    var range = sel.getRangeAt(0).cloneRange();
    try {
      range.setStart(range.startContainer, range.startOffset - 1);
    } catch (e) {
    }
    var rect = range.getBoundingClientRect();
    if (range.endOffset == 0 || range.toString() === '') {
      // first char of line
      if (range.startContainer == $node) {
        // empty div
        if (range.endOffset == 0) {
          pos.top = '0px';
          pos.left = '0px';
        } else {
          // firefox need this
          var range2 = range.cloneRange();
          range2.setStart(range2.startContainer, 0);
          var rect2 = range2.getBoundingClientRect();
          pos.left = rect2.left + offsetx - nodeLeft + 'px';
          pos.top = rect2.top + rect2.height + offsety - nodeTop + 'px';
        }
      } else {
        pos.top = range.startContainer.offsetTop + 'px';
        pos.left = range.startContainer.offsetLeft + 'px';
      }
    } else {
      pos.left = rect.left + rect.width + offsetx - nodeLeft + 'px';
      pos.top = rect.top + offsety - nodeTop + 'px';
    }
  }
  return pos;
}

function getCaretPosition(editableDiv) {
  var caretPos = 0, containerEl = null, sel, range;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.rangeCount) {
      range = sel.getRangeAt(0);
      if (range.commonAncestorContainer.parentNode == editableDiv) {
        caretPos = range.endOffset;
      }
    }
  } else if (document.selection && document.selection.createRange) {
    range = document.selection.createRange();
    if (range.parentElement() == editableDiv) {
      var tempEl = document.createElement("span");
      editableDiv.insertBefore(tempEl, editableDiv.firstChild);
      var tempRange = range.duplicate();
      tempRange.moveToElementText(tempEl);
      tempRange.setEndPoint("EndToEnd", range);
      caretPos = tempRange.text.length;
    }
  }
  return caretPos;
}

function letterOrNumber(event) {
  var inp = String.fromCharCode(event.keyCode);
  if (/[a-zA-Z0-9-_ ]/.test(inp)) {
    return inp;
  } else {
    return '';
  }
}

function sendGetRequestAndshowSuggestions($sug, text) {
  if (text) {
    window.currentkey = text;
  }
  $sug.html('');
  $.getJSON('http://139.91.70.76/elgg-mit/services/api/rest/json/?method=applications.get&appname=' + window.currentkey, function (data) {
    if (data.result) {
      res = JSON.parse(data.result);
      window.totalitems = res.length;
      $.each(res, function (i, v) {
        $sug.prepend(
          '<div class="suggestion-item-out" data-uid="' + v.name + '" data-href="' + v.url + '" data-guid="' + v.guid + '">' +
          v.name +
          '</div>'
          );
      });
    }
  });
}

function removeSuggestionList($sug) {
  $sug.css('display', 'none');
  window.flag = false;
  window.currentkey = "";
  window.selecteditem = 0;
}

function putSelected() {
  var appname = $('.suggestions-list').find('.active').attr('data-uid');
}

function bindKeyboardArrows(e, $sug) {
  switch (e.keyCode) {
    case 37: // left
    case 38: // up
      e.preventDefault();
      e.stopPropagation();
      $('div.suggestions-list :nth-child(' + (window.selecteditem + 1) + ')').removeClass('active');
      if (window.selecteditem === 0) {
        window.selecteditem = window.totalitems - 1;
      } else {
        window.selecteditem = (window.selecteditem - 1);
      }
      $('div.suggestions-list :nth-child(' + (window.selecteditem + 1) + ')').addClass('active');
      return true;
      break;
    case 39: // right
    case 40: // down
      e.preventDefault();
      e.stopPropagation();
      $('div.suggestions-list :nth-child(' + (window.selecteditem + 1) + ')').removeClass('active');
      window.selecteditem = (window.selecteditem + 1) % window.totalitems;
      $('div.suggestions-list :nth-child(' + (window.selecteditem + 1) + ')').addClass('active');
      return true;
      break;
    case 13: //enter
      e.preventDefault();
      e.stopPropagation();
      putSelected();
      removeSuggestionList($sug);
    default:
      return false; // exit this handler for other keys
  }
}


function getContextFromDiv() {
  var div_val = document.getElementById("editablediv").innerHTML;
  document.getElementById("typing_area").value = div_val;
  console.log("content of div: " + div_val);
  if (div_val === '') {
    return false;
  } else {
    $('.tags-wrapper a').each(function () {
      var guid = $(this).attr('data-guid');
      var execution_id = $(this).attr('data-execution');
      if(execution_id) {
        $('.body-form-discussion .main').append('<input name="execution-guids[]" type="hidden" value="' + guid + '">');
        $('.body-form-discussion .main').append('<input name="execution-ids[]" type="hidden" value="' + execution_id + '">');
      } else if(guid) {
        $('.body-form-discussion .main').append('<input name="app-guids[]" type="hidden" value="' + guid + '">');
      }
      
    });
    return true;
  }
}