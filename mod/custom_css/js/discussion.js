$('document').ready(function() {
	console.log('discussion.js');
	$('.votes-stasts-wrapper').on({
		mouseenter: function() {
			$(this).children('.info').css('display', 'none');
			$(this).children('.action').css('display', 'block');
		},
		mouseleave: function() {
			$(this).children('.action').css('display', 'none');
			$(this).children('.info').css('display', 'block');
		}
	});

	$('.votes-stasts-wrapper .action .upvote').on('click', function() {
		var topic = $(this).attr('data-guid');
		elgg.action('votes/up', {
			data: {
				topic: topic
			},
			success: function(json) {
				var up = parseInt($( '.votes-stasts-wrapper .info #up-' + topic ).text());
				var down = parseInt($( '.votes-stasts-wrapper .info #down-' + topic ).text());
				$( '.votes-stasts-wrapper .info #up-' + topic ).text(up + json.output.upvote);
				$( '.votes-stasts-wrapper .info #down-' + topic ).text(down + json.output.downvote);
			}
		});
	});

	$('.votes-stasts-wrapper .action .downvote').on('click', function() {
		var topic = $(this).attr('data-guid');
		elgg.action('votes/down', {
			data: {
				topic: topic
			},
			success: function(json) {
				var up = parseInt($( '.votes-stasts-wrapper .info #up-' + topic ).text());
				var down = parseInt($( '.votes-stasts-wrapper .info #down-' + topic ).text());
				$( '.votes-stasts-wrapper .info #up-' + topic ).text(up + json.output.upvote);
				$( '.votes-stasts-wrapper .info #down-' + topic ).text(down + json.output.downvote);
			},
			error: function () {
				console.log("error");
			}
		});
	});

});
