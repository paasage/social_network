<?php

elgg_register_event_handler('init', 'system', 'custom_css_init', 1000);

function custom_css_init() {
    elgg_extend_view('css/elgg', 'custom_css/css');
    register_bootstrap();

    elgg_unregister_menu_item('site', 'activity');

    load_bootstrap(elgg_get_context());
    register_and_load_my_css(
            'sidebar_css', 'mod/custom_css/vendors/bootstrap/css/simple-sidebar.css'
    );
    elgg_extend_view('css/elgg', 'custom_css/elggclasses');

    custom_css_register_actions();

    register_chart_js();

    elgg_register_entity_url_handler('user', 'all', 'user_entity_set_url');

    custom_css_register_page_handlers();

    custom_css_register_js();

    custom_css_register_libraries();

    custom_css_api_init();

    $css_url = 'mod/custom_css/views/default/css/myarea.css';
    elgg_register_css('custom_css_my_area', $css_url);
    
    elgg_load_js('custom.css.menu.js');
}

function custom_css_register_libraries() {
    $base = elgg_get_plugins_path();

    $l1 = '/custom_css/lib/reviews.php';
    elgg_register_library('custom_css.reviews.lib', $base . $l1);

    $l2 = '/custom_css/lib/groups.php';
    elgg_register_library('custom_css.groups.lib', $base . $l2);

    $l3 = '/custom_css/lib/friends.php';
    elgg_register_library('custom_css.friends.lib', $base . $l3);

    $l4 = '/custom_css/lib/contacts.php';
    elgg_register_library('custom_css.contacts.lib', $base . $l4);
}

function custom_css_register_js() {
    $profile_js = 'mod/custom_css/js/profile.js';
    elgg_register_js('custom_css.profile.js', $profile_js, 'footer');

    $reviews_js = 'mod/custom_css/js/reviews.js';
    elgg_register_js('custom_css.reviews.js', $reviews_js, 'footer');

    $groups_js = 'mod/custom_css/js/group.js';
    elgg_register_js('custom_css.groups.js', $groups_js, 'footer');

    $groups_invite_js = 'mod/custom_css/js/groups-invite.js';
    elgg_register_js('custom_css.groups.invite.js', $groups_invite_js, 'footer');

    $view_friend_js = 'mod/custom_css/js/view-friend.js';
    elgg_register_js('custom_css.view.friend.js', $view_friend_js, 'footer');

    $suggest = 'mod/custom_css/js/suggestions.js';
    elgg_register_js('custom_css.suggestions.js', $suggest, 'footer');

    $discussion = 'mod/custom_css/js/discussion.js';
    elgg_register_js('custom_css.discussion.js', $discussion, 'footer');
    
    $menu = 'mod/custom_css/js/menu.js';
    elgg_register_js('custom.css.menu.js', $menu, 'footer');
}

function register_bootstrap() {
    register_bootstrap_css();
    register_bootstrap_js();
}

function register_bootstrap_css() {
    $bootCSS = 'mod/custom_css/vendors/bootstrap/css/bootstrap.css';
    elgg_register_css('bootstrap_css', $bootCSS, 600);
    $bootCSSResp = 'mod/custom_css/vendors/bootstrap/css/bootstrap-responsive.css';
    elgg_register_css('bootstrap_css_resp', $bootCSSResp, 600);
}

function register_bootstrap_js() {
    $bootstrap_js = 'mod/custom_css/vendors/bootstrap/js/';
    elgg_register_js('bootstrap_js', $bootstrap_js . 'bootstrap.js', 'footer');
    elgg_register_js('bootstrap.modals.js', $bootstrap_js . 'bootstrap-modal.js');
}

function load_bootstrap($get_context) {
    if ($get_context != 'admin') {
        elgg_load_js('bootstrap_js');
        //elgg_load_js('bootstrap.modals.js');
        elgg_load_css('bootstrap_css');
        elgg_load_css('bootstrap_css_resp');
    }
}

function register_and_load_my_css($name, $loc, $priority = 10) {
    elgg_register_css($name, $loc, $priority);
    if ($get_context != 'admin') {
        elgg_load_css($name);
    }
}

/**
 * Register all actions.
 * 
 * action/all/search
 */
function custom_css_register_actions() {
    $base = elgg_get_plugins_path();
    elgg_register_action('reviews/add', $base . 'custom_css/actions/reviews/add.php');
    elgg_register_action('reviews/ishelpful', $base . 'custom_css/actions/reviews/ishelpful.php');
    elgg_register_action('reviews/delete', $base . 'custom_css/actions/reviews/delete.php');

    elgg_register_action('area-of-interest/add-interest', $base . 'custom_css/actions/area-of-interest/add-interest.php');
    elgg_register_action('area-of-interest/delete-interest', $base . 'custom_css/actions/area-of-interest/delete-interest.php');

    elgg_register_action('user_interface/all/search');

    elgg_register_action('skills/add', $base . 'custom_css/actions/skills/add.php');
    elgg_register_action('skills/delete-skill', $base . 'custom_css/actions/skills/delete-skill.php');

    elgg_register_action('votes/up', $base . 'custom_css/actions/votes/up.php');
    elgg_register_action('votes/down', $base . 'custom_css/actions/votes/down.php');

    elgg_register_action('topbar/search', $base . 'custom_css/actions/topbar_search.php');
}

/**
 * registers the JS libraries for charts.
 */
function register_chart_js() {
    $js_dir = 'mod/custom_css/vendors/js/';
    elgg_register_js('custom_css.globalize', $js_dir . 'globalize.min.js', 'footer');
    elgg_register_js('custom_css.chartjs', $js_dir . 'dx.chartjs.js', 'footer');
    elgg_register_js('custom_css.simple_chart.js', $js_dir . 'chart.js', 'footer');
}

/**
 * Redirect the user profile to specific functions.
 * 
 * @param type $page
 * @return boolean true on success
 * 
 */
function user_entity_set_url($user) {
    return elgg_get_site_url() . "my_profile/" . $user->guid;
}

function profile_page_handler($guid) {
    if (isset($guid[0]) && !isset($guid[1])) { //profile view
        elgg_load_js('custom_css.profile.js');
        elgg_load_js('custom_css.simple_chart.js');

        $body = elgg_view_layout('content/user_profile', array('guid' => $guid));
        echo elgg_view_page('User Profile', $body);
        return true;
    } elseif ($guid[1] == 'edit') { // profile edit
        echo elgg_view('pages/profile/edit', array('page' => 'edit'));
        return true;
    }
}

function avatar_page_handler($page) {
    if ($page[0] == 'edit') {
        echo elgg_view('pages/profile/avatar');
    } else {
        global $CONFIG;

        require $CONFIG->path . 'pages/avatar/view.php';
    }
}

/**
 * 
 * @param string $page /settings/user/${username}
 * @return boolean
 * 
 */
function settings_page_handler($page) {
    if ($page[0] == 'user') {
        elgg_set_page_owner_guid(elgg_get_logged_in_user_guid());
        echo elgg_view('pages/account_settings', array('user' => $page[1]));
    }
    return FALSE;
}

function my_friends_page_handler($segments, $handler) {
    elgg_set_context('friends');

    if (isset($segments[0]) && $user = get_user_by_username($segments[0])) {
        elgg_set_page_owner_guid($user->getGUID());
    }
    if (elgg_get_logged_in_user_guid() == elgg_get_page_owner_guid()) {
        collections_submenu_items();
    }

    switch ($handler) {
        case 'friends':
            echo elgg_view('pages/friends/index', array(reverse => FALSE));
            //require_once(dirname(dirname(dirname(__FILE__))) . "/pages/friends/index.php");
            break;
        /*  case 'friendsof':
          echo elgg_view('pages/friends/index', array('reverse' => TRUE));
          //require_once(dirname(dirname(dirname(__FILE__))) . "/pages/friends/of.php");
          break; */
        default:
            return false;
    }
    return true;
}

function my_friendsof_page_handler($segments, $handler) {
    elgg_set_context('friends');

    if (isset($segments[0]) && $user = get_user_by_username($segments[0])) {
        elgg_set_page_owner_guid($user->getGUID());
    }
    if (elgg_get_logged_in_user_guid() == elgg_get_page_owner_guid()) {
        collections_submenu_items();
    }

    switch ($handler) {
        case 'friendsof':
            echo elgg_view('pages/friends/index', array(reverse => TRUE));
            //require_once(dirname(dirname(dirname(__FILE__))) . "/pages/friends/index.php");
            break;
        default:
            return false;
    }
    return true;
}

function modals_page_handler($page) {
    gatekeeper();
    switch ($page[0]) {
        case 'send-message':
            $recipient = get_input('recipient_guid');
            echo elgg_view('modals/send-message', array('to' => $recipient, 'from' => elgg_get_logged_in_user_guid()));
            break;
    }
    return TRUE;
}

/**
 * @todo Move the following code to another plugin.
 * 
 */
function API_GetApplication($string) {
    $options = array(
        'type' => 'object',
        'subtype' => 'draw_application',
        'full_view' => false,
        'pagination' => true,
        'limit' => (int) 100000
    );
    $entities = elgg_get_entities($options);
    $result = array();
    foreach ($entities as $entity) {
        $appname = $entity->appname;
        $pos = strpos(strtolower($appname), strtolower($string));
        if ($pos !== false) {
            $result[] = array(
                'name' => $entity->appname,
                'url' => $entity->getURL(),
                'guid' => $entity->getGUID()
            );
        }
    }
    return json_encode($result);
}

function custom_css_api_init() {
    expose_function(
            "applications.get", "API_GetApplication", array("appname" => array('type' => 'string')), 'Returns all applications similar in name with the key', 'GET', false, false
    );
}

function custom_css_register_page_handlers() {
    elgg_register_page_handler('my_profile', 'profile_page_handler');

    elgg_register_page_handler('my_avatar', 'avatar_page_handler');

    elgg_register_page_handler('settings', 'settings_page_handler');

    elgg_register_page_handler('friends', 'my_friends_page_handler');

    elgg_register_page_handler('friendsof', 'my_friendsof_page_handler');

    elgg_register_page_handler('modals', 'modals_page_handler');

    elgg_register_page_handler('contact', 'contact_page_handler');

    elgg_register_page_handler('reviews', 'reviews_page_handler');

    elgg_register_page_handler('search', 'search_page_handler');
}

/**
 * pages:
 * * reviews/edit
 * 
 * @param type $page the page
 */
function reviews_page_handler($page) {
    switch ($page[0]) {
        case 'edit':
            if ($page[1] && $page[2]) {
                $title = "edit review";

                $content = elgg_view_form('reviews/add', array(), array('review_guid' => $page[1], 'entity' => get_entity($page[2])));

                $sidebar = '';

                // layout the page
                $body = elgg_view_layout('one_sidebar', array(
                    'content' => $content,
                    'sidebar' => $sidebar,
                ));

                echo elgg_view_page($title, $body);
            }
            break;

        default:
            break;
    }
}

/**
 * pages:
 * * contact/list
 * 
 * @param String $page The requested page
 */
function contact_page_handler($page) {
    switch ($page) {
        case 'list':
        default:
            elgg_load_library('custom_css.contacts.lib');
            show_contact_list();
            break;
    }
}

/**
 * 
 * @param type $page
 */
function search_page_handler($page) {
    if ($page[0]) {
        $title = "Search Results";

        $options = array();
        $options['metadata_names'] = array('appname');
        $options['metadata_values'] = array($page[0]);
        $options['full_view'] = false;
        elgg_load_library('draw_app.all_apps');
        $results_apps = elgg_list_entities(
                $options, 'elgg_get_entities_from_metadata'
        );
        $params = array(
            title => 'Models',
            results => elgg_list_entities($options, 'elgg_get_entities_from_metadata', 'draw_app_view_entity_list'
            ),
            key => $page[0]
        );
        $content = elgg_view("page/elements/search/section", $params);

        $params[title] = 'Users';
        $params[results] = "";
        $content .= elgg_view("page/elements/search/section", $params);

        $params[title] = 'Groups';
        $params[results] = "";
        $content .= elgg_view("page/elements/search/section", $params);

        // layout the page
        $body = elgg_view_layout('one_column', array(
            'content' => $content
        ));

        echo elgg_view_page($title, $body);
    }
}
