<?php
/**
* Read a message page
*
* @package ElggMessages
*/

gatekeeper();

$message = get_entity(get_input('guid'));
if (!$message || !elgg_instanceof($message, "object", "messages")) {
	forward('messages/inbox/' . elgg_get_logged_in_user_entity()->username);
}

// mark the message as read
$message->readYet = true;

elgg_set_page_owner_guid($message->getOwnerGUID());
$page_owner = elgg_get_page_owner_entity();

$title = $message->title;

$inbox = false;
if ($page_owner->getGUID() == $message->toId) {
	$inbox = true;
	//elgg_push_breadcrumb(elgg_echo('messages:inbox'), 'messages/inbox/' . $page_owner->username);
} /*else {
	elgg_push_breadcrumb(elgg_echo('messages:sent'), 'messages/sent/' . $page_owner->username);
}
elgg_push_breadcrumb($title);*/

$content = elgg_view_entity($message, array('full_view' => true));
if ($inbox) {
	$form_params = array(
		'id' => 'messages-reply-form',
		'class' => 'mtl form-disp',
		'action' => 'action/messages/send',
	);
	$body_params = array('message' => $message);
	$content .= elgg_view_form('messages/reply', $form_params, $body_params);
	$from_user = get_user($message->fromId);
	
}

$body = elgg_view_layout('one_sidebar', array(
	'content' => $content,
	'sidebar' => '',
));

echo elgg_view_page($title, $body);
