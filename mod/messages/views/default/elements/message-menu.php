<?php
/**
 * 
 * The message submenu.
 * 
 * @uses $vars['active'] String The viewing menu page. values: inbox | sent
 */
$message_icon = elgg_view('output/img', array('src' =>'_graphics/icons/create_essage.jpg'));
$add_message = elgg_view('output/url', array('href' => 'messages/add', 'text' => $message_icon . 'New Message'));
$active_menu = "<div class='arrow-up'></div>"
?>

<div class="row-fluid">
	<div class="span-width-components">
		<div class="application-submenu">
			<div class="application-submenu-item">
				<?php 
					echo elgg_view('output/url', array('text' => 'Messages', 'href' => 'messages/inbox'));
					if($vars['active'] == 'inbox') {
						echo $active_menu;
					}
				?>
			</div>
			
			<div class="application-submenu-item">
				<?php 
					echo elgg_view('output/url', array('text' => 'Sent', 'href' => 'messages/sent'));
					if($vars['active'] == 'sent') {
						echo $active_menu;
					}
				?>
			</div>
			<div class="application-submenu-action">
				<?php echo $add_message; ?>
			</div>
		</div>
	</div>
</div>