<?php
/**
 * 
 * @uses $vars['entity'] The message entity.
 */
?>

<div class="message-reply">
	<?php echo elgg_view('output/url', array(
			'text'	=> 'Reply',
			'href'	=> "messages/read/{$vars['entity']->guid}",
			'class' => 'reply-link')
			); 
	?>
</div>
<div class="message-delete" data-uid="<?php echo $vars['entity']->guid?>">
	delete
</div>