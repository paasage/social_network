/**
 * 
 * The messages Js
 */

$( 'document' ).ready(function(){
		console.log('messages.js');
		$('.message').hover(function () {
			$(this).children('.elgg-body').children('.messages-actions').css('visibility', 'visible');
		}, function () {
			$(this).children('.elgg-body').children('.messages-actions').css('visibility', 'hidden');
		});
		
		$( '.message-delete' ).on('click', function () {
			var mess_guid = $(this).attr('data-uid');
			var $action_div = $(this).parent().parent().parent().parent().children('.messages-delete-notification');
			var $remove_div = $(this).parent().parent().parent();
			elgg.action('action/messages/delete', {
				data: {
					guid: mess_guid
				},
				beforeSend: function () {
					$(this).remove();
				},
				success: function () {
					$action_div.show();
					setTimeout(function() {
						$remove_div.hide('slow', function(){ 
							$remove_div.remove(); 
							$action_div.remove(); 
						});
				}, 1800);
				}
			});
		});
});

