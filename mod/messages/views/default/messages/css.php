<?php
/**
 * Elgg Messages CSS
 * 
 * @package ElggMessages
 */
?>

.messages-container {
	min-height: 200px;
}
.message.unread a {
	color: #d40005;
}
.messages-buttonbank {
	text-align: right;
}
.messages-buttonbank input {
	margin-left: 10px;
}

/*** message metadata ***/
.messages-owner {
	float: left;
	width: 20%;
	margin-right: 2%;
}
.messages-subject {
	float: left;
	width: 45%;
	margin-right: 2%;
}
.messages-timestamp {
	float: left;
	width: 14%;
	margin-right: 2%;
}
.messages-delete {
	float: left;
	width: 5%;
}
/*** topbar icon ***/
.messages-new {
	color: white;
	background-color: red;
	
	-webkit-border-radius: 10px; 
	-moz-border-radius: 10px;
	border-radius: 10px;
	
	-webkit-box-shadow: -2px 2px 4px rgba(0, 0, 0, 0.50);
	-moz-box-shadow: -2px 2px 4px rgba(0, 0, 0, 0.50);
	box-shadow: -2px 2px 4px rgba(0, 0, 0, 0.50);
	
	position: absolute;
	text-align: center;
	top: 0px;
	left: 26px;
	min-width: 16px;
	height: 16px;
	font-size: 10px;
	font-weight: bold;
}

.messages-actions {
	float: left;
	width: 15%;
	visibility: hidden;
}

.message-reply {
	background-color: #f47264;
	padding: 10px;
}

.message-delete {
	background-color: #455272;
	padding: 5px;
}

.message-reply, .message-delete {
	color: #fff;
	font-size: 1.3em;
	text-align: center;
	cursor: pointer;
	margin-top: 2px;
}

.messages-delete-notification {
	color: #fff;
	text-align: center;
	background-color: #485172;
	height: 100%;
	width: 100%;
	position: absolute;
	top: 0;
	left: 0;
	z-index: 2;
	padding: 30px;
	font-size: 1.78em;
	display: none;
}

.messages-container .elgg-list-entity .elgg-item {
	position: relative;
}

.application-submenu-action {
	position: absolute;
	right: 0;
	padding: 12px;
	height: 100%;
	background-color: #f47264;

	text-transform: uppercase;
	text-decoration: underline;
	cursor: pointer;
	text-align: center;
}

.application-submenu-action a {
	color: #fff;
}

.application-submenu-action img {
	margin-right: 8px;
}

.reply-link {
	color: #fff;
}