<?php
/**
 * Compose message form
 *
 * @package ElggMessages
 * @uses $vars['friends']
 * @uses $vars['to'] if is ajax, this var has the recipient_guid.
 */

$recipient_guid = elgg_extract('recipient_guid', $vars, 0);
$subject = elgg_extract('subject', $vars, '');
$body = elgg_extract('body', $vars, '');

$recipients_options = array();
if(!$recipient_guid) {
	$recipient_guid = get_input('recipient_guid');
}
if(!$vars['friends']) {
	$options = array(
		'relationship' => 'friend',
		'relationship_guid' => elgg_get_logged_in_user_guid(),
		'inverse_relationship' => FALSE,
		'type' => 'user',
		'full_view' => FALSE
	);
	$vars['friends'] = elgg_get_entities_from_relationship($options);
}
foreach ($vars['friends'] as $friend) {
	$recipients_options[$friend->guid] = $friend->name;
}

if (!array_key_exists($recipient_guid, $recipients_options)) {
	$recipient = get_entity($recipient_guid);
	if (elgg_instanceof($recipient, 'user')) {
		$recipients_options[$recipient_guid] = $recipient->name;
	}
}

$recipient_drop_down = elgg_view('input/dropdown', array(
	'name' => 'recipient_guid',
	'value' => $recipient_guid,
	'options_values' => $recipients_options,
	'id' => 'inputTo'
));

?>
<div class="control-group">
	<label class="control-label" for="inputTo">
		<?php echo elgg_echo("messages:to"); ?>: 
	</label>
	<div class="controls">
		<?php echo $recipient_drop_down; ?>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputTitle">
		<?php echo elgg_echo("messages:title"); ?>: 
	</label>
	<div class="controls">
		<?php echo elgg_view('input/text', array(
			'name' => 'subject',
			'value' => $subject,
			'id' => 'inputTitle'
		));
		?>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="inputbody">
		<?php echo elgg_echo("messages:message"); ?>:
	</label>
	<div class="controls">
		<?php echo elgg_view("input/longtext", array(
			'name' => 'body',
			'value' => $body,
			'id' => 'inputbody'
		));
		?>
	</div>
</div>

<div class="elgg-foot control-group">
	<div class="controls">
		<?php echo elgg_view('input/submit', array('value' => elgg_echo('messages:send'))); ?>
	</div>
</div>
