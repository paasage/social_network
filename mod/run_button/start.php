<?php

/**
* Run Button plugin
*/

elgg_register_event_handler('init', 'system', 'run_button_init');

function run_button_init() {
  
  // register the test action
  $base = elgg_get_plugins_path() . 'run_button/actions/run_button';
  elgg_register_action("run_button/test", "$base/test.php", "logged_in");

  // register the action that makes the request to the platform API
  elgg_register_action("run_button/callPlatformAPI", "$base/callPlatformAPI.php", "logged_in");

  // register the action that makes the request to the platform API 
  // in order to deploy an application
  elgg_register_action("run_button/callPlatformAPI_deploy", "$base/callPlatformAPI_deploy.php", "logged_in");

  // register the javascript files
  $url1 = 'mod/run_button/js/run_button/click_run.js';
  elgg_register_js('run_button.click_run.js', $url1, 'footer');
  elgg_load_js('run_button.click_run.js');

}
