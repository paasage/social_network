<?php 
/**
 * 
 * @uses $vars['model_guid'] The model guid
 *
 */
elgg.system_message("callPlatformAPI.php run successfully! (from callPlatformAPI.php)");

// get the platform endpoint of the specific user
$currentUser = elgg_get_logged_in_user_entity();
$platformEndpoint = $currentUser->platformEndpoint;
//echo $platformEndpoint;

// use the name of the model in the social network as name of the paasage resource 
$model_guid = get_input('model_guid');
$model = get_entity($model_guid);
$name = $model->appname;
//$name = "test17";
//echo $name;

// open the xmi file
//$xmifile = fopen("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/Manos_App.xmi", "r") or die("Unable to open file!");
//$xmi = fread($xmifile, filesize("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/Manos_App.xmi"));
//$xmi_encoded = base64_encode($xmi);
//fclose($xmifile);

$xmifile = fopen("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi", "r") or die("Unable to open file!");
$xmi = fread($xmifile, filesize("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi"));
$xmi_encoded = base64_encode($xmi);
fclose($xmifile);

// store the chosen provider model locally with the appropriate name
$target_path = elgg_get_plugins_path() . "draw_app/lib/XMIsToBeUploaded/";
$target_path = $target_path . $name ."ProviderModel.xmi";
$data = $_REQUEST['val'];
$fp = fopen($target_path,'w'); //Prepends timestamp to prevent overwriting
fwrite($fp, $data);
fclose($fp);



// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint);
//echo $user_id;

// for testing...
//echo resource_state($platformEndpoint, $name, "CREATED", $token, $user_id);
//if (resource_state($platformEndpoint, $name, "CREATED", $token, $user_id) == false ) {
//    echo $token;
//} else {
//    echo $user_id;
//}

// 2)create the paasage resource 
//if (resource_state($platformEndpoint, $name, "CREATED", $token, $user_id) == false
//    && resource_state($platformEndpoint, $name, "READY_TO_REASON", $token, $user_id) == false
//    && resource_state($platformEndpoint, $name, "REASONING", $token, $user_id) == false) {

    //echo create_resource($platformEndpoint, $name, $token, $user_id);
//} esle {
//    echo $user_id;
//}

// 3)retrieve the primary key of the paasage resource
$resource_pk = get_resource_pk($platformEndpoint, $name, $token, $user_id);
//echo($resource_pk);
//echo("a CDO resource is with the name of your application has been created"); 

// 4)upload an xmi file to the paasage resource
//if (resource_state($platformEndpoint, $name, "READY_TO_REASON", $token, $user_id) == false) {
    //upload_xmi($platformEndpoint, $resource_pk, $token, $user_id, $xmi_encoded);
    //echo $xmi_encoded;
//}

// 5)start the reasonning 
echo start_reasonning($platformEndpoint, $resource_pk, $token, $user_id);



////if (resource_state($name, "REASONING", $token, $user_id)){
//	echo("The reasoning has started...");
////}

/*
// 6)start the deployment
do {
    //$ready_to_deploy = resource_state($name, "READY_TO_DEPLOY", $token, $user_id);
    $ready_to_deploy = resource_state($name, "REASONING", $token, $user_id);

} while ($ready_to_deploy == FALSE);
echo("The deployment starts...");
//start_deployment($resource_pk, $token, $user_id);
*/

// functions
function authentication($platformEndpoint) {
    /**
    * a POST request to PaaSage platform API to retrieve: 
    *
    * 1) a token
    * 2) a userId
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/login";

    // this is the payload
    $data = array (
        'email' => 'john.doe@example.com',
        'password' => 'admin',
        'tenant' => 'admin'
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    // execute the request
    $output = curl_exec($ch);

    // output the information - includes the header
    //echo($output);

    // retrieve token from the response
    $position = strpos($output, "token");
    $token = substr($output, $position+8, 205);
    //echo($token);

    // retrieve user_id from the response
    $position2 = strpos($output, "userId");
    $user_id = substr($output, $position2+8, 1);
    //echo($user_id);

    // close curl resource to free up system resources
    curl_close($ch);

    return array($token, $user_id);
}

function resource_state($platformEndpoint, $resource_name, $state, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to check 
    *
    * if paasage-resource with name $resource_name is in state $state
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve state from the response
    $position = strpos($output, $state);

    // close curl resource to free up system resources
    curl_close($ch);

    return $position;
}

function create_resource($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a POST request to PaaSage platform API to create paasage-resource 
    *
    * with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model";
    
    // this is the payload
    $data = array (
        "name" => $resource_name, 
        "state" => "CREATED", 
        "subState" => "empty", 
        "action" => "CREATE", 
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token, 
        'X-Auth-UserId: ' . $user_id, 
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function get_resource_pk($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the primary key 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;
    $url2 = $platformEndpoint . "api/model/";

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, $url2);
    $position2 = strpos($output, "rel");
    $resource_pk_length = ($position2-3) - ($position+38);
    $resource_pk = substr($output, $position+38, $resource_pk_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $resource_pk;
}

function upload_xmi($platformEndpoint, $resource_pk, $token, $user_id, $xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload an xmi file
    *
    */
    
    // open the xmi file
    //$xmifile = fopen("/var/www/elgg-min/mod/run_button/actions/run_button/enterprise-service-application.xmi", "r") or die("Unable to open file!");
    //$xmi = fread($xmifile, filesize("/var/www/elgg-min/mod/run_button/actions/run_button/enterprise-service-application.xmi"));
    //$xmi_encoded = base64_encode($xmi);
    //fclose($xmifile);

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'UNCHANGED',
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_reasonning($platformEndpoint, $resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the reasonning
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "START_REASONNING",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_deployment($resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the deployment
    *
    */

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "DEPLOY",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://109.231.126.102:9999/api/model/" . $resource_pk);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

?>

