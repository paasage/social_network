package eu.paasage.camel.cdo.client;

import java.util.Iterator;

import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.json.simple.JSONArray;

import eu.paasage.camel.CamelModel;
import eu.paasage.camel.CamelPackage;
import eu.paasage.camel.deployment.DeploymentModel;
import eu.paasage.camel.deployment.DeploymentPackage;
import eu.paasage.camel.deployment.VMInstance;
import eu.paasage.camel.deployment.VM;
import eu.paasage.camel.execution.ExecutionContext;
import eu.paasage.camel.execution.ExecutionModel;
import eu.paasage.camel.execution.ExecutionPackage;
import eu.paasage.camel.execution.Measurement;
import eu.paasage.camel.organisation.OrganisationPackage;
import eu.paasage.camel.scalability.ScalabilityPackage;
import eu.paasage.mddb.cdo.client.CDOClient;

public class SocketAPI {
	
	public static JSONArray queryHistory(String appNamespace) {
	    //        Util.printOut("[queryHistory] start v1.0.1");

	    //Create the CDOClient
	    CDOClient cl = new CDOClient();
	    JSONArray res = new JSONArray();
//	        Util.printOut("[queryHistory] spot 1");

	    //Register the required packages
	    cl.registerPackage(CamelPackage.eINSTANCE);
	    cl.registerPackage(OrganisationPackage.eINSTANCE);
	    cl.registerPackage(DeploymentPackage.eINSTANCE);
	    cl.registerPackage(ScalabilityPackage.eINSTANCE);
	    cl.registerPackage(ExecutionPackage.eINSTANCE);

//	        Util.printOut("[queryHistory] spot 2");  
	    CDOView view = cl.openView();

	    //query = view.createQuery("hql", "select cm from CamelModel cm");
//		List<EObject> results = query.getResult(EObject.class);
	    EList<EObject> results = null;
	    try {
	      results = view.getResource(appNamespace).getContents();
	    } catch (Exception e) {
	      JSONArray err = new JSONArray();
	      err.add("no such application");

	      return err;
	    }
//	        Util.printOut("The results of the query are: " + results);
//	        Util.printOut("results size: " + results.size());

	    // Parse all the CAMEL models that are present
	    Iterator camelModel_iter = results.iterator();
	    while (camelModel_iter.hasNext()) {
	      System.out.println("start");
	      Application2Json execution;
	      CamelModel camel = (CamelModel) camelModel_iter.next();

	      if(camel.getExecutionModels().size() < 1) {
	    	  continue;
	      }
	      // Assume one ExecutionModel (each one can have multiple exec Contexts)
	      ExecutionModel execModel = camel.getExecutionModels().get(0);
	      System.out.println("executionModels: " + camel.getExecutionModels().size());
	      
	      
	      for (int ei = 0; ei < execModel.getExecutionContexts().size(); ei++) {
	        String line = "";
	        String debug_line = "";
	        
	        
		    // parse each executionContext of the executionModel
		    // from the executionContext we get the:
		    // 	1) startTime
		    // 	2) endTime
		    //	3) executionContextName
		    // 	4) cost
		    //	5) costUnit
		    // 	6) deploymentModelName
	        ExecutionContext exec = execModel.getExecutionContexts().get(ei);

	        debug_line += "[debug] App: " + exec.getApplication().getName()
	                + "\tExecution Context: " + exec.getName() + "\n";
	        debug_line += "[debug] start: " + exec.getStartTime() + "\tend: "
	                + exec.getEndTime() + "\n";
	        debug_line += "[debug] Cost: " + exec.getTotalCost()
	                + "\tcost units: " + exec.getCostUnit().getName() + "\n";

	        line = exec.getName() + ", " + exec.getStartTime() + ", "
	                + exec.getEndTime() + ", " + exec.getTotalCost() + ", "
	                + exec.getCostUnit().getUnit().getName();
	        
	        // printMeasurementlList(measurementList);
	        
	        execution = new Application2Json(
	                exec.getStartTime().toString(),
	                exec.getEndTime().toString(),
	                exec.getName(),
	                exec.getTotalCost(),
	                exec.getCostUnit().getUnit().getName(),
	                exec.getDeploymentModel().getName()
	        );
	        System.out.println(ei + " executioncontext of execution with id: " + exec.getName() + " parsed");
	        
	        
	        // parse each measurement of the executionModel
	        // from each measurement we get the:
	        // 	1) measurementName
	        //	2) value
	        EList<Measurement> measurementList = (EList<Measurement>) execModel.getMeasurements();
	        Iterator<Measurement> im = (Iterator<Measurement>) measurementList.iterator();
	        debug_line += "[debug] Measurements::\n";
	        while (im.hasNext()) {
	          Measurement m = im.next();
	          // if the measurement is not associated with this execution
	          if (!m.getExecutionContext().getName().equals(exec.getName())) {
	            continue;
	          }
	          
	          if(m.getName().contains("WorkloadMix")) {
	        	  line = line + ", " + m.getName() + ", " + m.getValue();
	          } else {
	        	  line = line + ", " + m.getMetricInstance().getMetric().getName() + ", " + m.getValue();
	          }
	          
	          //line = line + ", " + m.getName() + ", " + m.getValue();
	          debug_line += "[debug] \t" + m.getName() + " = " + m.getValue() + "\n";
	          
	          if(m.getName().contains("WorkloadMix")) {
	        	  execution.addMeasurement(m.getName(), m.getValue());
	          } else {
	        	  execution.addMeasurement(m.getMetricInstance().getMetric().getName(), m.getValue());
	          }
	        }
	        System.out.println("Measurements of " + ei + " executioncontext of execution with id: " + exec.getName() + " parsed");
	        
	        
	        // parse the deployment model that is mentioned in each executionContext 
	        // from the deployment model we get the:
	        //	1) VmInstances
	        DeploymentModel dm = exec.getDeploymentModel();

	        if (dm.getVmInstances().size() > 0) {  // As VMInstance
	          EList<VMInstance> vmInstances = dm.getVmInstances();
	          // printVMInstancesList(vmInstances); 

	          line = line + ", " + vmInstances.size();
	          debug_line += "[debug] VM instances: " + vmInstances.size() + "\n";

	          Iterator<VMInstance> i = vmInstances.iterator();
	          while (i.hasNext()) {
	            VMInstance vmi = i.next();
	            if (!vmi.getName().endsWith(exec.getName())){
	            	continue;
	            }
	            line = line + ", " + vmi.getType().getName() + ", "
	                    //+ ((VM) vmi.getType()).getProvider().getName() + ", "
	                    //+ ((VM) vmi.getType()).getLocation().getCountry();
	                    + ((VM) vmi.getType()).getVmRequirementSet().getProviderRequirement().getProviders().get(0).getName() + ", "
	                    + ((VM) vmi.getType()).getVmRequirementSet().getLocationRequirement().getLocations().get(0).getId(); 
	            debug_line += "[debug] \t" + vmi.getType().getName() + ", "
	                    //+ ((VM) vmi.getType()).getProvider().getName() + ", "
	                    //+ ((VM) vmi.getType()).getLocation().getCountry() + "\n";
	            		+ ((VM) vmi.getType()).getVmRequirementSet().getProviderRequirement().getProviders().get(0).getName() + ", "
	                    + ((VM) vmi.getType()).getVmRequirementSet().getLocationRequirement().getLocations().get(0).getId(); 
	            //execution.addVM(vmi.getType().getName(), ((VM) vmi.getType()).getProvider().getName(), ((VM) vmi.getType()).getLocation().getName());
	            
	            String provider = ((VM) vmi.getType()).getVmRequirementSet().getProviderRequirement().getProviders().get(0).getName();
	            
	            if (provider.contains("Amazon")) provider = "Amazon";
	            if (provider.contains("Flexiant")) provider = "Flexiant";
	            if (provider.contains("Azure")) provider = "Azure";
	            if (provider.contains("GWDG")) provider = "GWDG";
	            if (provider.contains("Openstack")) provider = "Openstack";
	            
	            execution.addVM(vmi.getType().getName(), 
	            				provider, 
	            				((VM) vmi.getType()).getVmRequirementSet().getLocationRequirement().getLocations().get(0).getId()
	            				);
	          }
	        }
	        System.out.println("VmInstances of " + ei + " executioncontext of execution with id: " + exec.getName() + " parsed");
	        /*
	        else {  // As External Component Instance
	          EList<ExternalComponentInstance> externalCompInstances = dm.getExternalComponentInstances();
	          Iterator<ExternalComponentInstance> eci = externalCompInstances.iterator();

	          line = line + ", " + externalCompInstances.size() + ", ";
	          debug_line += "[debug] External Component instance (VMinstances): " + externalCompInstances.size() + "\n";
	          while (eci.hasNext()) {
	            ExternalComponentInstance ecinst = eci.next();
	            //                    debug_line += "name: " + ecinst.getName() + "\n";
	            VMInstance vmi = (VMInstance) ecinst;
	            line += vmi.getType().getName() + ", "
	                    + ((VM) vmi.getType()).getProvider().getName() + ", "
	                    + ((VM) vmi.getType()).getLocation().getCountry();
	            debug_line += "[debug] \t" + vmi.getType().getName() + ", "
	                    + ((VM) vmi.getType()).getProvider().getName() + ", "
	                    + ((VM) vmi.getType()).getLocation().getCountry() + "\n";
	            execution.addVM(vmi.getType().getName(), ((VM) vmi.getType()).getProvider().getName(), ((VM) vmi.getType()).getLocation().getCountry());
	          
	          }
	        }*/

	       
	        res.add(execution.toJSON());
	        //System.out.println(debug_line);
	        System.out.println(line);

	        System.out.println("end");
	      }

	    }

	    view.close();

	    //Close the CDOSession once you are done
	    cl.closeSession();
	    System.out.println("\n#####################################################\nres: " + res.toString());
	    return res;
	  }

}
