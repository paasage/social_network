<?php 
/**
 * 
 * @uses $vars['model_guid'] The model guid
 *
 */
elgg.system_message("callPlatformAPI.php run successfully! (from callPlatformAPI.php)");

// get the platform endpoint of the specific user
$currentUser = elgg_get_logged_in_user_entity();
$platformEndpoint = $currentUser->platformEndpoint;
$emailEndpoint = $currentUser->emailEndpoint;
$passwordEndpoint = $currentUser->passwordEndpoint;
$tenantEndpoint = $currentUser->tenantEndpoint;
//echo $platformEndpoint;
//echo("\n");
//echo $emailEndpoint;
//echo("\n");
//echo $passwordEndpoint;
//echo("\n");
//echo $tenantEndpoint;
//echo("\n");

// use the name of the model in the social network as name of the paasage resource 
$model_guid = get_input('model_guid');
$model = get_entity($model_guid);
$name = $model->appname;

$xmifile = fopen("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi", "r") or die("Unable to open file!");
$xmi = fread($xmifile, filesize("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi"));
$xmi_encoded = base64_encode($xmi);
fclose($xmifile);

// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint);
//echo("1: ");
//echo $token;
//echo("\n");
//echo $user_id;
//echo("\n");

// 1.5)retrieve the state of the paasage resource
$state = get_resource_state($platformEndpoint, $name, $token, $user_id);
//echo("1.5: ");
//echo($state);
//echo("\n");

// if this is the first time the Reason button is clicked 
if (((strcmp($state, "REASONING") != 0) && (strcmp($state, "READY_TO_REASON") != 0)) && 
    ((strcmp($state, "CREATED") != 0) && (strcmp($state, "READY_TO_DEPLOY") != 0))) {

    // 2)create the paasage resource
    //echo("2: ");
    create_resource($platformEndpoint, $name, $token, $user_id);
    //echo("Resource created");
    //echo("\n");

    // 3)retrieve the primary key of the paasage resource
    $resource_pk = get_resource_pk($platformEndpoint, $name, $token, $user_id);
    //echo("3: ");
    //echo($resource_pk);
    //echo("\n");

    // 4)upload an application xmi file to the paasage resource
    //echo("4: ");
    upload_xmi($platformEndpoint, $resource_pk, $name, $token, $user_id, $xmi_encoded);
    //echo("\n");

    // 5) get the resourse state every 30 seconds until its value is not "UPLOADING_XMI"
    do {
        sleep(4);

        //echo("5: ");
        $state2 = get_resource_state($platformEndpoint, $name, $token, $user_id);
        //echo($state2);
        //echo("\n");
    } while (strcmp($state2, "UPLOADING_XMI") == 0);

}


// 3)retrieve the primary key of the paasage resource
$resource_pk = get_resource_pk($platformEndpoint, $name, $token, $user_id);
//echo("3: ");
//echo($resource_pk);
//echo("\n");

// 5)start the reasonning 
//echo("5: ");
start_reasoning($platformEndpoint, $resource_pk, $token, $user_id);
//echo("\n");

// get the resourse state until its value is not "REASONING"
do {

    sleep(30);

    //echo("6: ");
    $state2 = get_resource_state($platformEndpoint, $name, $token, $user_id);
    //echo($state2);
    //echo("\n");

} while (strcmp($state2, "REASONING") == 0);

echo($state2);



//////////////////////////////////////////////////////////////////////// New code

//
echo get_resoning_info2($platformEndpoint, $name, $token, $user_id);

// get the reasoning info from the CDO model
list ($rootFeatureNames, $vmNames, $attributes) = get_resoning_info($platformEndpoint, $name, $token, $user_id);

//$returnSrting = "READY_TO_DEPLOY\n";
$returnSrting = $state2 . "\n";


for ($i=0; $i < count($rootFeatureNames); $i++) { 
    $returnSrting .= "VM #" . ($i+1) . ": cloud provider is " . $rootFeatureNames[$i] . ", VM flavour is " . $vmNames[$i] . " ("; 

    for ($k=0; $k < count($attributes); $k++) {
        if ($k == count($attributes)-1) {
            $returnSrting .= $attributes[$k][$i];
        } else {
            $returnSrting .= $attributes[$k][$i] . ", ";
        }
    }

    $returnSrting .= ")\n";
}


echo $returnSrting;

////////////////////////////////////////////////////////////////////////


// functions
function authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint) {
    /**
    * a POST request to PaaSage platform API to retrieve: 
    *
    * 1) a token
    * 2) a userId
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/login";

    // this is the payload
    $data = array (
        'email' => $emailEndpoint,
        'password' => $passwordEndpoint,
        'tenant' => $tenantEndpoint
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    // execute the request
    $output = curl_exec($ch);

    // output the information - includes the header
    //echo($output);

    // retrieve token from the response
    $position = strpos($output, "token");
    $token = substr($output, $position+8, 205);
    //echo($token);

    // retrieve user_id from the response
    $position2 = strpos($output, "userId");
    $user_id = substr($output, $position2+8, 1);
    //echo($user_id);

    // close curl resource to free up system resources
    curl_close($ch);

    return array($token, $user_id);
}

function get_resource_state($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the state 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, "state");
    $position2 = strpos($output, "subState");
    $state_length = ($position2-3) - ($position+8);
    $state = substr($output, $position+8, $state_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $state;
}

function resource_state($platformEndpoint, $resource_name, $state, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to check 
    *
    * if paasage-resource with name $resource_name is in state $state
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve state from the response
    $position = strpos($output, $state);

    // close curl resource to free up system resources
    curl_close($ch);

    return $position;
}

function create_resource($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a POST request to PaaSage platform API to create paasage-resource 
    *
    * with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model";
    
    // this is the payload
    $data = array (
        "name" => $resource_name, 
        "state" => "CREATED", 
        "subState" => "empty", 
        "action" => "CREATE", 
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token, 
        'X-Auth-UserId: ' . $user_id, 
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function get_resource_pk($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the primary key 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;
    $url2 = $platformEndpoint . "api/model/";

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, $url2);
    $position2 = strpos($output, "rel");
    $resource_pk_length = ($position2-3) - ($position+strlen($url2));
    $resource_pk = substr($output, $position+strlen($url2), $resource_pk_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $resource_pk;
}

function upload_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload a provider xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'upperware-models/fms/Flexiant',
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $provider_xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function upload_xmi($platformEndpoint, $resource_pk, $resource_name, $token, $user_id, $xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload an xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => $resource_name,
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_reasoning($platformEndpoint, $resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the reasonning
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "START_REASONING",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_deployment($platformEndpoint, $resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the reasonning
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "DEPLOY",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}



function get_resoning_info($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the state 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve the model from the response
    $position = strpos($output, "xmiModelEncoded");
    $position2 = strpos($output, "link");
    $model_length = ($position2-3) - ($position+18);
    $model_encoded = substr($output, $position+18, $model_length);
    $model_decoded = base64_decode($model_encoded);

    // close curl resource to free up system resources
    curl_close($ch);

    // returned values
    $rootFeatureNames = array();
    $vmNames = array();
    $attributes = array();

    // Find the positions of all vmTypes in the model
    $vmTypesNumber = substr_count($model_decoded,"vmType="); 

    $vmInstancePositionsStart[0] = array();
    $vmTypePositionsStart[0] = array();
    $vmTypePositionsEnd[0] = array();

    $vmInstancePositionsStart[0] = strpos($model_decoded, "<vmInstances name=");
    $vmTypePositionsStart[0] = strpos($model_decoded, "vmType=");
    $vmTypePositionsEnd[0] = strpos($model_decoded, "vmTypeValue=");

    // Discover all the vmTypes
    $vmTypes = array();
    for ($x = 0; $x < $vmTypesNumber; $x++) {

        // take the name of the vm
        $vmInstancePositionsStart[$x] = strpos($model_decoded, "<vmInstances name=", $vmTypePositionsStart[$x-1]+1);
        $typePositionsStart = strpos($model_decoded, "@vms.", $vmInstancePositionsStart[$x]);
        $typePositionsEnd = strpos($model_decoded, "vmType=", $typePositionsStart);
        $type_length = ($typePositionsEnd-2) - ($typePositionsStart+5);
        $type = substr($model_decoded, $typePositionsStart+5, $type_length);

        $vmPossitionStart = strposX($model_decoded, "<vms name=", $type+1);
        $vmPossitionEnd = strpos($model_decoded, "vmRequirementSet=", $vmPossitionStart);
        $vmName_length = ($vmPossitionEnd-2) - ($vmPossitionStart+11);
        $vmName = substr($model_decoded, $vmPossitionStart+11, $vmName_length);
        //echo ("vmName: ");
        //echo $vmName;
        //echo ("\n");
        $vmNames[$x] = $vmName;

        // take the number of the providerModel, subFeature and attribute
        $vmTypePositionsStart[$x] = strpos($model_decoded, "vmType=", $vmTypePositionsStart[$x-1]+1);
        $vmTypePositionsEnd[$x] = strpos($model_decoded, "vmTypeValue=", $vmTypePositionsEnd[$x-1]+1);
        $vmType_length = ($vmTypePositionsEnd[$x]-2) - ($vmTypePositionsStart[$x]+9);
        $vmTypes[$x] = substr($model_decoded, $vmTypePositionsStart[$x]+9, $vmType_length);

        $vmTypesSplitted = explode("/@",$vmTypes[$x]);

        $vmTypesSplitted[1] = str_replace("providerModels.","",$vmTypesSplitted[1]);
        $vmTypesSplitted[2] = str_replace("rootFeature","",$vmTypesSplitted[2]);
        $vmTypesSplitted[3] = str_replace("subFeatures.","",$vmTypesSplitted[3]);
        $vmTypesSplitted[4] = str_replace("attributes.","",$vmTypesSplitted[4]);

        // take the name of the providerModel
        $providerModelPossitionStart = strposX($model_decoded, "<providerModels name=", $vmTypesSplitted[1]+1);
        $providerModelPossitionEnd = strpos($model_decoded, "<constraints ", $providerModelPossitionStart);
        $providerModelName_length = ($providerModelPossitionEnd-7) - ($providerModelPossitionStart+22);
        $providerModelName = substr($model_decoded, $providerModelPossitionStart+22, $providerModelName_length);

        // take the rootFeature name
        $rootFeaturePossitionStart = strpos($model_decoded, "<rootFeature name=", strposX($model_decoded, "<rootFeature name=", $vmTypesSplitted[2]) + $providerModelPossitionStart);
        $rootFeaturePossitionEnd = strpos($model_decoded, ">", $rootFeaturePossitionStart);
        $rootFeatureName_length = ($rootFeaturePossitionEnd-1) - ($rootFeaturePossitionStart+19);
        $rootFeatureName = substr($model_decoded, $rootFeaturePossitionStart+19, $rootFeatureName_length);
        //echo ("rootFeatureName: ");
        //echo $rootFeatureName;
        //echo ("\n");
        $rootFeatureNames[$x] = $rootFeatureName;

        // take the subFeature position
        $subFeaturePossitionStart = strpos($model_decoded, "<subFeatures name=", strposX($model_decoded, "<subFeatures name=", $vmTypesSplitted[3]) + $providerModelPossitionStart);

        // take the attribute name
        $attributePossitionStart = strpos($model_decoded, "<attributes name=", strposX($model_decoded, "<attributes name=", $vmTypesSplitted[4]) + $subFeaturePossitionStart);
        $attributePossitionEnd = strpos($model_decoded, "valueType=", $attributePossitionStart);
        $attributeName_length = ($attributePossitionEnd-2) - ($attributePossitionStart+18);
        $attributeName = substr($model_decoded, $attributePossitionStart+18, $attributeName_length);

        // take the attribute value name
        $attributeValuePossitionStart = strpos($model_decoded, "<value ", $attributePossitionStart);
        $attributeValuePossitionStart2 = strpos($model_decoded, "name=", $attributeValuePossitionStart);
        $attributeValuePossitionEnd = strpos($model_decoded, "\"", $attributeValuePossitionStart2+6);
        $attributeValueName_length = ($attributeValuePossitionEnd) - ($attributeValuePossitionStart2+6);
        $attributeValueName = substr($model_decoded, $attributeValuePossitionStart2+6, $attributeValueName_length);

        // Find the attribute values of the Implies constraint
        $impliesConstraintPossitionStart = strpos($model_decoded, $attributeValueName, $providerModelPossitionStart);
        $impliesConstraintPossitionEnd = strpos($model_decoded, "</constraints>", $impliesConstraintPossitionStart);
        $impliesConstraint_length = $impliesConstraintPossitionEnd - $impliesConstraintPossitionStart;

        // Find the number of the attributeConstraints in the Implies constraint
        $attributeConstraintsNumber = substr_count($model_decoded,"<attributeConstraints", $impliesConstraintPossitionStart, $impliesConstraint_length); 
        
        for ($y = 0; $y < $attributeConstraintsNumber; $y++) { 
            // get the values 
            $toValuePossitionStart = strpos($model_decoded, "<toValue ", $impliesConstraintPossitionStart);
            $valuePossitionStart = strpos($model_decoded, "value=", $toValuePossitionStart);
            $valuePossitionEnd = strpos($model_decoded, "/>", $valuePossitionStart+6);
            $value_length = ($valuePossitionEnd-1) - ($valuePossitionStart+7);
            $value = substr($model_decoded, $valuePossitionStart+7, $value_length);

            // the values are stored here
            $attributes[$y][$x] = $value;

            // in order to find the next value
            $impliesConstraintPossitionStart = $valuePossitionStart;
        }


    }
    return array($rootFeatureNames, $vmNames, $attributes);
}

/**
 * Find the position of the Xth occurrence of a substring in a string
 * @param $haystack
 * @param $needle
 * @param $number integer > 0
 * @return int
 */
function strposX($haystack, $needle, $number){
    if($number == '1'){
        return strpos($haystack, $needle);
    }elseif($number > '1'){
        return strpos($haystack, $needle, strposX($haystack, $needle, $number - 1) + strlen($needle));
    }else{
        return error_log('Error: Value for parameter $number is out of range');
    }
}


function get_resoning_info2($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the state 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve the model from the response
    $position = strpos($output, "xmiModelEncoded");
    $position2 = strpos($output, "link");
    $model_length = ($position2-3) - ($position+18);
    $model_encoded = substr($output, $position+18, $model_length);
    $model_decoded = base64_decode($model_encoded);

    // close curl resource to free up system resources
    curl_close($ch);

    // returned values
    return $model_decoded;
}
?>

