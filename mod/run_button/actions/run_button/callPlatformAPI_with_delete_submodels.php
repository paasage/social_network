<?php 
/**
 * 
 * @uses $vars['model_guid'] The model guid
 *
 */
elgg.system_message("callPlatformAPI.php run successfully! (from callPlatformAPI.php)");


// use the name of the model in the social network as name of the paasage resource 
$model_guid = get_input('model_guid');
$model = get_entity($model_guid);
$name = $model->appname;

// these are the uncheckedValues
$checkedValues = get_input('checkedValues');
for ($i=0; $i < sizeof($checkedValues); $i++) { 
    $checkedValues[$i] = $checkedValues[$i] . "\"";
}

// create an array ($result_array) with the contents of the xmi file minus the checkedValues
//$xmi3 = simplexml_load_file("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi");
//$json_string = json_encode($xmi3);
//$result_array = json_decode($json_string, TRUE);
//foreach ($checkedValues as $value) {
//    $result_array = removeElementWithValue($result_array, "name", $value);
//}
//echo print_r($result_array);


// open the xmi file
$xmifile = fopen("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi", "r") or die("Unable to open file!");
$xmi = fread($xmifile, filesize("/var/www/elgg-min/mod/draw_app/lib/XMIsToBeUploaded/" . $name . ".xmi"));
$xmi_encoded = base64_encode($xmi);
fclose($xmifile);

// find all the deploymentModels and the requirementModels in the xmi file
$needle = "<deploymentModels";
$needle2 = "</deploymentModels>";
$needle3 = "<requirementModels";
$needle4 = "</requirementModels>";
    
$positions = array();
$positions2 = array();
$positions3 = array();
$positions4 = array();

$lastPos = 0;
while (($lastPos = strpos($xmi, $needle, $lastPos))!== false) {
    $positions[] = $lastPos;
    $lastPos = $lastPos + strlen($needle);
}
$lastPos = 0;
while (($lastPos = strpos($xmi, $needle2, $lastPos))!== false) {
    $positions2[] = $lastPos;
    $lastPos = $lastPos + strlen($needle2);
}
$lastPos = 0;
while (($lastPos = strpos($xmi, $needle3, $lastPos))!== false) {
    $positions3[] = $lastPos;
    $lastPos = $lastPos + strlen($needle3);
}
$lastPos = 0;
while (($lastPos = strpos($xmi, $needle4, $lastPos))!== false) {
    $positions4[] = $lastPos;
    $lastPos = $lastPos + strlen($needle4);
}

// find all the checkedValues in the xmi file
$positions5 = array();
foreach ($checkedValues as $value) {
    $positions5[] = strpos($xmi, $value);
}

/*  print all the positions
    foreach ($positions as $value) {
        echo "positions: ".$value."\n";
    }
        foreach ($positions2 as $value) {
        echo "positions2: ".$value."\n";
    }
        foreach ($positions3 as $value) {
        echo "positions3: ".$value."\n";
    }
        foreach ($positions4 as $value) {
        echo "positions4: ".$value."\n";
    }
        foreach ($positions5 as $value) {
        echo "positions5: ".$value."\n";
    }
*/

// put the submodels to be deleted according to the checkedValues to array $removeThese
$removeThese = array();
$removeTheseIndex = 0;
foreach ($positions5 as $positions5index => $checkedValue) {
    foreach ($positions as $index => $deploymentModel) {
        if($checkedValue > $deploymentModel && $checkedValue < $positions2[$index]) {
            $removeThis = iconv_substr($xmi, $deploymentModel, ($positions2[$index]+19-$deploymentModel));
            $removeThese[$removeTheseIndex] = $removeThis;
            $removeTheseIndex++;
            //echo "removeThisDeploymentModel: ".$removeThis."\n";
        }
    }
    foreach ($positions3 as $index => $requirementModel) {
        if($checkedValue > $requirementModel && $checkedValue < $positions4[$index]) {
            $removeThis = iconv_substr($xmi, $requirementModel, ($positions4[$index]+19-$requirementModel));
            $removeThese[$removeTheseIndex] = $removeThis;
            $removeTheseIndex++;
            //echo "removeThis: ".$removeThis."\n";
        }
    }
}

// delete all the submodels form the xmi
foreach ($removeThese as $value) {
    $xmi = str_replace($value, "", $xmi);
}

//echo $xmi;

//////////////////////////////////////////////
// 1)get token and user_id for authentication
//list ($token, $user_id) = authentication();

// 2)create the paasage resource 
//if (!resource_state($name, "CREATED", $token, $user_id) 
//    && !resource_state($name, "READY_TO_REASON", $token, $user_id)
//    && !resource_state($name, "REASONING", $token, $user_id)) {

//        create_resource($name, $token, $user_id);
//}

// 3)retrieve the primary key of the paasage resource
//$resource_pk = get_resource_pk($name, $token, $user_id);

// 4)upload an xmi file to the paasage resource
//if (!resource_state($name, "READY_TO_REASON", $token, $user_id)) {
//    upload_xmi($resource_pk, $token, $user_id);
//}

// 5)start the reasonning 
//start_reasonning($resource_pk, $token, $user_id);
echo("The reasoning has started...");

/*
// 6)start the deployment
do {
    //$ready_to_deploy = resource_state($name, "READY_TO_DEPLOY", $token, $user_id);
    $ready_to_deploy = resource_state($name, "REASONING", $token, $user_id);

} while ($ready_to_deploy == FALSE);
echo("The deployment starts...");
//start_deployment($resource_pk, $token, $user_id);
*/

// functions
function authentication() {
    /**
    * a POST request to PaaSage platform API to retrieve: 
    *
    * 1) a token
    * 2) a userId
    */

    // this is the payload
    $data = array (
        'email' => 'john.doe@example.com',
        'password' => 'admin',
        'tenant' => 'admin'
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/login");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    // execute the request
    $output = curl_exec($ch);

    // output the information - includes the header
    //echo($output);

    // retrieve token from the response
    $position = strpos($output, "token");
    $token = substr($output, $position+8, 205);
    //echo($token);

    // retrieve user_id from the response
    $position2 = strpos($output, "userId");
    $user_id = substr($output, $position2+8, 1);
    //echo($user_id);

    // close curl resource to free up system resources
    curl_close($ch);

    return array($token, $user_id);
}

function resource_state($resource_name, $state, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to check 
    *
    * if paasage-resource with name $resource_name is in state $state
    */

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/model/name/" . $resource_name);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve state from the response
    $position = strpos($output, $state);

    // close curl resource to free up system resources
    curl_close($ch);

    return $position;
}

function create_resource($resource_name, $token, $user_id) {
    /**
    * a POST request to PaaSage platform API to create paasage-resource 
    *
    * with name $resource_name
    */
    
    // this is the payload
    $data = array (
        "name" => $resource_name, 
        "state" => "CREATED", 
        "subState" => "empty", 
        "action" => "CREATE", 
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/model");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token, 
        'X-Auth-UserId: ' . $user_id, 
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function get_resource_pk($resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the primary key 
    *
    * of a resource with name $resource_name
    */

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/model/name/" . $resource_name);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, "http://develop-aws.paasage.cetic.be:9000/api/model/");
    $position2 = strpos($output, "rel");
    $resource_pk_length = ($position2-3) - ($position+51);
    $resource_pk = substr($output, $position+51, $resource_pk_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $resource_pk;
}

function upload_xmi($resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to upload an xmi file
    *
    */
    
    // open the xmi file
    $xmifile = fopen("/var/www/elgg-min/mod/run_button/actions/run_button/enterprise-service-application.xmi", "r") or die("Unable to open file!");
    $xmi = fread($xmifile, filesize("/var/www/elgg-min/mod/run_button/actions/run_button/enterprise-service-application.xmi"));
    $xmi_encoded = base64_encode($xmi);
    fclose($xmifile);

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'UNCHANGED',
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/model/" . $resource_pk);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_reasonning($resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the reasonning
    *
    */

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "START_REASONNING",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/model/" . $resource_pk);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_deployment($resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the deployment
    *
    */

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "DEPLOY",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://develop-aws.paasage.cetic.be:9000/api/model/" . $resource_pk);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

/////////////////////////////////////////////
// in order to remove the submodel that we do not need from the CAMEL model
function removeElementWithValue($array, $key, $value){
     foreach($array as $subKey => $subArray){
        foreach($subArray as $subKey2 => $subArray2){
            foreach($subArray2 as $subKey3 => $subArray3){
                if($subArray3[$key] == $value){
                    unset($array[$subKey][$subKey2][$subKey3]);
                }
            }
        }
     }
     return $array;
}
/////////////////////////////////////////////

?>
