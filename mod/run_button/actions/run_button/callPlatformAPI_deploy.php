<?php 
/**
 * 
 * @uses $vars['model_guid'] The model guid
 *
 */
elgg.system_message("callPlatformAPI_deploy.php run successfully! (from callPlatformAPI.php)");

//echo("The deployment starts...");

// get the platform endpoint of the specific user
$currentUser = elgg_get_logged_in_user_entity();
$platformEndpoint = $currentUser->platformEndpoint;
$emailEndpoint = $currentUser->emailEndpoint;
$passwordEndpoint = $currentUser->passwordEndpoint;
$tenantEndpoint = $currentUser->tenantEndpoint;

// use the name of the model in the social network as name of the paasage resource 
$model_guid = get_input('model_guid');
$model = get_entity($model_guid);
$name = $model->appname;


// 1)get token and user_id for authentication
list ($token, $user_id) = authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint);
//echo $token;

// 1.5)retrieve the state of the paasage resource
$state = get_resource_state($platformEndpoint, $name, $token, $user_id);
//echo("1.5: ");
//echo($state);
//echo("\n");

// 3)retrieve the primary key of the paasage resource
$resource_pk = get_resource_pk($platformEndpoint, $name, $token, $user_id);
//echo("3: ");
//echo($resource_pk);
//echo("\n");


if (strcmp($state, "READY_TO_DEPLOY") === 0) {
    start_deployment($platformEndpoint, $resource_pk, $token, $user_id);
    $state2 = get_resource_state($platformEndpoint, $name, $token, $user_id);
    echo($state2);
} else {
    echo ("The application needs to go through the reasoning phase first!");
} 



// wait 35 minutes
//sleep(2100);

// get the resourse state until its value is not "DEPLOYING"
//do {
    
//    echo("6: ");
//    $state2 = get_resource_state($platformEndpoint, $name, $token, $user_id);
//    echo($state2);
//    echo("\n");

    // wait 5 minutes
//    sleep(300);

//} while (strcmp($state2, "DEPLOYING") == 0);

//echo($state2);




// functions
function authentication($platformEndpoint, $emailEndpoint, $passwordEndpoint, $tenantEndpoint) {
    /**
    * a POST request to PaaSage platform API to retrieve: 
    *
    * 1) a token
    * 2) a userId
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/login";

    // this is the payload
    $data = array (
        'email' => $emailEndpoint,
        'password' => $passwordEndpoint,
        'tenant' => $tenantEndpoint
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    // execute the request
    $output = curl_exec($ch);

    // output the information - includes the header
    //echo($output);

    // retrieve token from the response
    $position = strpos($output, "token");
    $token = substr($output, $position+8, 205);
    //echo($token);

    // retrieve user_id from the response
    $position2 = strpos($output, "userId");
    $user_id = substr($output, $position2+8, 1);
    //echo($user_id);

    // close curl resource to free up system resources
    curl_close($ch);

    return array($token, $user_id);
}

function get_resource_state($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the state 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, "state");
    $position2 = strpos($output, "subState");
    $state_length = ($position2-3) - ($position+8);
    $state = substr($output, $position+8, $state_length);

    // retrieve the model from the response
    //$position = strpos($output, "xmiModelEncoded");
    //$position2 = strpos($output, "link");
    //$model_length = ($position2-3) - ($position+18);
    //$model_encoded = substr($output, $position+18, $model_length);
    //$model_decoded = base64_decode($model_encoded);

    // close curl resource to free up system resources
    curl_close($ch);

    return $state;
}

function resource_state($platformEndpoint, $resource_name, $state, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to check 
    *
    * if paasage-resource with name $resource_name is in state $state
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve state from the response
    $position = strpos($output, $state);

    // close curl resource to free up system resources
    curl_close($ch);

    return $position;
}

function create_resource($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a POST request to PaaSage platform API to create paasage-resource 
    *
    * with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model";
    
    // this is the payload
    $data = array (
        "name" => $resource_name, 
        "state" => "CREATED", 
        "subState" => "empty", 
        "action" => "CREATE", 
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token, 
        'X-Auth-UserId: ' . $user_id, 
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function get_resource_pk($platformEndpoint, $resource_name, $token, $user_id) {
    /**
    * a GET request to PaaSage platform API to retrieve the primary key 
    *
    * of a resource with name $resource_name
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/name/" . $resource_name;
    $url2 = $platformEndpoint . "api/model/";

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // retrieve resource primary key from the response
    $position = strpos($output, $url2);
    $position2 = strpos($output, "rel");
    $resource_pk_length = ($position2-3) - ($position+strlen($url2));
    $resource_pk = substr($output, $position+strlen($url2), $resource_pk_length);

    // close curl resource to free up system resources
    curl_close($ch);

    return $resource_pk;
}

function upload_provider_xmi($platformEndpoint, $resource_pk, $token, $user_id, $provider_xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload a provider xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => 'upperware-models/fms/Flexiant',
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $provider_xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function upload_xmi($platformEndpoint, $resource_pk, $resource_name, $token, $user_id, $xmi_encoded) {
    /**
    * a PUT request to PaaSage platform API to upload an xmi file
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        'name' => 'UNCHANGED',
        'state' => 'UNCHANGED',
        'subState' => $resource_name,
        'action' => 'UPLOAD_XMI',
        'xmiModelEncoded' => $xmi_encoded
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        //'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_reasoning($platformEndpoint, $resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the reasonning
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "START_REASONING",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

function start_deployment($platformEndpoint, $resource_pk, $token, $user_id) {
    /**
    * a PUT request to PaaSage platform API to start the reasonning
    *
    */

    // create the url for the API call
    $url = $platformEndpoint . "api/model/" . $resource_pk;

    // this is the payload
    $data = array (
        "name" => "UNCHANGED",
        "state" => "UNCHANGED",
        "subState" => "UNCHANGED",
        "action" => "DEPLOY",
        "xmiModelEncoded" => "UNCHANGED"
    );
    $data_string = json_encode($data);

    // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'accept: application/json',
        'Content-Length: ' . strlen($data_string),
        'X-Auth-Token: ' . $token,
        'X-Auth-UserId: ' . $user_id,
        'X-Tenant: admin'
    ));

    // execute the request
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

?>
