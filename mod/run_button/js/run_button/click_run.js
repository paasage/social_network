$(document).ready(function () {

  console.log('click_run.js');

  // read the xmi file
  //var file;
  //var reader;
  //$(':file').change(function(){
  //    file = this.files[0];
  //    reader = new FileReader();
  //    reader.readAsText(file, 'UTF-8');
  //});

  // when the reason button is clicked
  $('a#run').on('click', function (event) {

    //event.preventDefault();
    var model_guid = $(this).attr('data-uid');
    //var ch = $("#xmi-components-modal .modal-body input:not(:checked)").map(function () {
    //        return this.value
    //}).get();//console.log(ch);

    // show the loading image
    //var iconDiv = document.getElementsByName("upload_xmi_gif_hidden");
    //iconDiv[0].setAttribute("name","upload_xmi_gif_shown");
    

    elgg.action('run_button/callPlatformAPI', {
      data: {
        //val: reader.result,
        model_guid: model_guid,
        //checkedValues: ch
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");
        //setTimeout(myFunction, 5000);

        console.log(resultText.output);



        if (resultText.output.indexOf("READY_TO_DEPLOY") >= 0) {

          // change the title
          //var modal_header = document.getElementById("modal-header");
          //modal_header.innerHTML = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="myModalLabel">Reasoning phase</h3>';    
        
          // remove the upload
          //var inputUpload = document.getElementById("inputUpload");
          //inputUpload.innerHTML = '';

          // hide the loading image
          var iconDiv = document.getElementsByName("upload_xmi_gif_shown");
          iconDiv[0].setAttribute("name","upload_xmi_gif_hidden");

          // show the message to the user
          var returnedString1 = resultText.output.substring(0, 14);

          // reasoning information for the user
          var returnedStrings = [];
          var indices = getIndicesOf("VM #", resultText.output);
          
          ////
          var newHTML = "<p style='color:#0088cc'>The reasoning phase is finished and you may proceed with the deployment of the application. Please close this message and click on the Deploy button.</p> <p>REASONING INFO</p>";
          
          for (var i = 0; i < indices.length; i++) {
            if(i == indices.length - 1) {
              returnedStrings[i] = resultText.output.substring(indices[i], resultText.output.length);
            } else {
              returnedStrings[i] = resultText.output.substring(indices[i], indices[i+1]-1);
            }
            
            ////
            newHTML = newHTML + "<p style='color:#0088cc'>" + returnedStrings[i] + "</p>";
            
          };

          var message_to_user = document.getElementById("message_to_user");
          
          ////
          message_to_user.innerHTML = newHTML;
          
          //message_to_user.innerHTML = "<p style='color:#0088cc'>The reasoning phase is finished and you may proceed with the deployment of the application. Please close this message and click on the Deploy button.</p> <p>REASONING INFO</p> <p style='color:#0088cc'>" + returnedStrings[0] +"</p> <p style='color:#0088cc'>" + returnedStrings[1] +"</p>";    
        
          // remove the reason button
          //var modal_footer = document.getElementsByName("modal-footer-shown");
          //modal_footer[0].setAttribute("name","modal-footer-hidden");
        } else {

          // change the title
          //var modal_header = document.getElementById("modal-header");
          //modal_header.innerHTML = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="myModalLabel">Reasoning phase</h3>';    
        
          // remove the upload
          //var inputUpload = document.getElementById("inputUpload");
          //inputUpload.innerHTML = '';

          // remove the message
          var message_to_user_deploy1 = document.getElementById("message_to_user_deploy1");
          message_to_user_deploy1.innerHTML = '';

          // hide the loading image
          var iconDiv = document.getElementsByName("upload_xmi_gif_shown");
          iconDiv[0].setAttribute("name","upload_xmi_gif_hidden");

          // show the message to the user
          var message_to_user = document.getElementById("message_to_user");
          message_to_user.innerHTML = "<p style='color:red'>The reasoning phase is not finished successfully. Please close this message and try again.</p>";    
        
          // remove the reason button
          //var modal_footer = document.getElementsByName("modal-footer-shown");
          //modal_footer[0].setAttribute("name","modal-footer-hidden");

        }

        

        return true;
      }
    });  
  });

  function getIndicesOf(searchStr, str, caseSensitive) {
    var searchStrLen = searchStr.length;
    if (searchStrLen == 0) {
        return [];
    }
    var startIndex = 0, index, indices = [];
    if (!caseSensitive) {
        str = str.toLowerCase();
        searchStr = searchStr.toLowerCase();
    }
    while ((index = str.indexOf(searchStr, startIndex)) > -1) {
        indices.push(index);
        startIndex = index + searchStrLen;
    }
    return indices;
  }

  function myFunction() {
    alert('The application is ready to be deployed.');
  }

  // when the deploy button is clicked
  $('button#deployXMI').on('click', function () {
    var model_guid = $(this).attr('data-uid');

    // show the loading image
    var iconDiv = document.getElementsByName("upload_xmi_gif_hidden");
    if(iconDiv.length > 1) {
      iconDiv[1].setAttribute("name","upload_xmi_gif_shown");
    } else {
      iconDiv[0].setAttribute("name","upload_xmi_gif_shown");
    }
    

    elgg.action('run_button/callPlatformAPI_deploy', {
      data: {
        model_guid: model_guid
      },
      success: function (resultText) {
        //elgg.system_message("test.php run successfully!");
        console.log(resultText.output);

        $('#xmi-components-modal-deploy').modal('hide');
        /*
        if (resultText.output.indexOf("RUNNING") >= 0) { 

          // remove the message
          var message_to_user_deploy1 = document.getElementById("message_to_user_deploy1");
          message_to_user_deploy1.innerHTML = '';
           
          // hide the loading image
          var iconDiv = document.getElementsByName("upload_xmi_gif_shown");
          iconDiv[0].setAttribute("name","upload_xmi_gif_hidden");

          // show the message to the user
          var message_to_user2 = document.getElementById("message_to_user_deploy");
          message_to_user2.innerHTML = "<p style='color:#0088cc'>The deployment phase is finished.</p>";    
        
          // remove the deploy button
          var modal_footer = document.getElementsByName("modal-footer-deploy-shown");
          modal_footer[0].setAttribute("name","modal-footer-deploy-hidden");
        } else {

        }
        */
      }
    });
  });

});
