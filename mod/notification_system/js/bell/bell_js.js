/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
  console.log('bell_js.js');
  // show bell 
  $('.bell-topbar-img').on('click', function (e) {
    e.stopPropagation();
    if ($('.bell-topbar-container').html() === '') {
      elgg.action('bell/show', {
        success: function (resultText, success, xhr) {
          $('.bell-topbar-container').html(resultText.output);
          $('.bell-topbar-container').show();
          $('li.elgg-menu-item-bell').css('background-color', '#475071');
          $('div.notification-number').remove();
        }
      });
    } else {
      hideBell();
    }

  });

  // if click anywhere else make sure to hide the bell div
  $(document).on('click', function (evt) {
    if (evt.target.classList[0] !== "list" && evt.target.classList[0] !== "bell-topbar-img" &&
      evt.target.classList[0] !== "notification-item") {
      hideBell();
    }
  });

  function hideBell() {
    $('.bell-topbar-container .list').remove();
    $('li.elgg-menu-item-bell').css('background-color', '#2b3042');
  }
	
	// for delete notification
	$( document ).on('click', ".elgg-list-entity .elgg-item .notification-content span.delete-notification", function (e) {
		e.stopPropagation();
		var $toremove = $(this).parent().parent();
		var id = $(this).attr("data-uid");
		elgg.action('notification/delete', {
			data: {
				guid: id
			},
			success: function () {
				$toremove.remove();
			}
		});
	})
});
