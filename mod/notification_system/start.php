<?php

/**
 * 
 * 
 */
elgg_register_event_handler('init', 'system', 'initialize_notification_system_plugin');

function initialize_notification_system_plugin() {
  elgg_register_page_handler('cart', 'cart_page_handler');

  notification_system_register_actions();

  notification_system_register_library();

  notification_system_register_topbar_icon();

  notification_system_register_js();

  elgg_register_event_handler('notification', 'notification', 'notification_handler', 0);

  elgg_load_js('notification_system.bell.js');
}

function notification_system_register_actions() {
  $base = elgg_get_plugins_path() . 'notification_system/actions/';

  elgg_register_action('bell/show', $base . 'bell/show.php', 'logged_in');
	elgg_register_action('notification/delete', $base . 'notification/delete.php', 'logged_in');
}

function notification_system_register_library() {
  ;
}

function notification_system_register_topbar_icon() {

  $text = elgg_view('topbar/content-bell');

  $params = array(
      'name' => 'bell',
      'text' => $text,
      'href' => '#',
      'link_class' => 'cart-link-no-display'
  );
  elgg_register_menu_item('topbar', $params);
}

function notification_system_register_js() {
  $url1 = 'mod/notification_system/js/bell/bell_js.js';
  elgg_register_js('notification_system.bell.js', $url1, 'footer');
}

function notification_handler($hook, $type, $value, $params) {
  if ($value) {
    if (elgg_instanceof($value, 'object', 'review_comment')) {
      $reviewer = $value->guid; //@todo what is that?
      $app = $value->application_reference;
      $href = elgg_view('output/url', array(href => "draw_app/view/{$app}/reviews", text => get_entity($app)->appname));
      $watchers = elgg_get_entities_from_relationship(array(
          'relationship' => 'watching',
          'relationship_guid' => $app,
          'inverse_relationship' => TRUE
      ));
      foreach ($watchers as $w) {
        $notif = new ElggNotificationItem();
				$notif->type = 'object';
        $notif->item = $value->getGUID();
//        $notif->container_guid = $w->getGUID(); //add as relationship
        $notif->descrition = "A review added to $href";
        $guid = $notif->save();
				if(add_entity_relationship($guid, 'notification-not-seen', $w->getGUID()) === FALSE){
					print_r("fail to add relationship");
				}
				if( !check_entity_relationship($guid, 'notification-not-seen', $w->getGUID()) ){
					print_r("Cant add relationship");
				}
      }
    }
  }
  return true;
}
