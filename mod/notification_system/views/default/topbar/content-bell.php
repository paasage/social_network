<?php
/**
 * 
 */
if(!elgg_is_logged_in()){
  return ; 
}
$topbar_icon = elgg_get_site_url() . '_graphics/models-features-bell-topbar.png';
$count = count(groups_get_invited_groups(elgg_get_logged_in_user_guid(), TRUE));
$re = elgg_get_entities_from_relationship(array(
						'relationship'					=> 'notification-not-seen',
						'relationship_guid'			=> elgg_get_logged_in_user_guid(),
						'inverse_relationship'	=>	TRUE,
						));
$count += count($re); 

?>
<!-- hack the a href with the following close and open another to the end -->
</a>
<span>
	<img src="<?php echo $topbar_icon;?>" class="bell-topbar-img" title="My Notifications"> 
<?php if($count) { ?>
	<div class="notification-number"><?php echo $count;?></div>
<?php } ?>
</span>
<div class="bell-topbar-container"></div>
  
<a>