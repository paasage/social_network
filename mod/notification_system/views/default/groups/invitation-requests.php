<?php
/**
 * A user's group invitations
 *
 * @uses $vars['invitations'] Array of ElggGroups
 */

if (!empty($vars['invitations']) && is_array($vars['invitations'])) {
	$user  = elgg_get_logged_in_user_entity();
?>
<div class='notification-list'>
	<?php foreach ($vars['invitations'] as $group) { 
		$icon = elgg_view_entity_icon($group, 'tiny', array('use_hover' => 'true'));

		$group_title = elgg_view('output/url', array(
			'href' => $group->getURL(),
			'text' => $group->name,
			'is_trusted' => true,
		));

		$url1 = elgg_add_action_tokens_to_url(elgg_get_site_url()."action/groups/join?user_guid={$user->guid}&group_guid={$group->guid}");
		$accept_button = elgg_view('output/url', array(
			'href' => $url1,
			'text' => elgg_echo('accept'),
			'class' => 'accept-invitation',
			'is_trusted' => true,
		));

		$url2 = "action/groups/killinvitation?user_guid={$user->getGUID()}&group_guid={$group->getGUID()}";
		$delete_button = elgg_view('output/url', array(
				'href' => $url2,
				'text' => elgg_echo('delete'),
				'class' => 'delete-invitation',
				'is_trusted' => true,
				'is_action' => true
		));
	?>
	<div class="notification-item">
		<?php echo $icon . "You have invited to join ". $group_title . " Group ". $accept_button . " ". $delete_button;?>
	</div>
	<?php } //end for?> 
</div>
<?php
} //end if