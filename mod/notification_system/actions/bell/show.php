<?php

/**
 * 
 */
$user_guid = elgg_get_logged_in_user_guid();
$user_entity = elgg_get_logged_in_user_entity();
$annotations = $user_entity->getAnnotations(
  'cart'
);
$site = elgg_get_site_url();

$content = "<div class='list'>"
        . "<h4>Notifications</h4>";

$invitations = groups_get_invited_groups($user_guid);
$content .= elgg_view('groups/invitation-requests', array('invitations' => $invitations));
$content1 = elgg_get_entities_from_relationship(array(
	'relationship' => 'notification-not-seen', 
	'relationship_guid' => $user_guid, 
	'inverse_relationship' => TRUE
	));
$content2 = elgg_get_entities_from_relationship(array(
	'relationship' => 'notification-seen', 
	'relationship_guid' => $user_guid, 
	'inverse_relationship' => TRUE
	));
$content .= "</div>";
echo $content; // send reply for fast response

$entities = elgg_get_entities_from_relationship(array(
	'relationship_guid' => $user_guid, 
	'relationship' => 'notification-not-seen', 
	'inverse_relationship' => TRUE)
	);

foreach ($entities as $e) {
	if(check_entity_relationship($e->getGUID(), 'notification-not-seen', $user_guid)){
		remove_entity_relationship($e->getGUID(), 'notification-not-seen', $user_guid);
		add_entity_relationship($e->getGUID(), 'notification-seen', $user_guid);
	}
}