<?php
/**
 * action: notification/delete
 *
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */

$notif_guid = (int) get_input('guid');

$notif = get_entity($notif_guid);
if (!$notif || !$notif->getSubtype() == "notification-item") {
	echo FALSE;
}

$container = $notif->getContainerEntity();

$result = $notif->delete();
if ($result) {
	system_message(elgg_echo('discussion:topic:deleted'));
} else {
	register_error(elgg_echo('discussion:error:notdeleted'));
}

echo TRUE;