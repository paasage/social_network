<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ElggNotificationItem
 *
 * @author Christos Papoulas <papoulas@ics.forth.gr>
 */
class ElggNotificationItem extends ElggObject {
	protected function initializeAttributes() {
		parent::initializeAttributes();
		
		$this->attributes['subtype'] = "notification-item";
	}
	
	/**
	 * Update container entity last action on successful save.
	 *
	 * @param bool $update_last_action Update the container entity's last_action field
	 * @return bool|int
	 */
	public function save($update_last_action = true) {
		$result = parent::save();
		if ($result && $update_last_action) {
			update_entity_last_action($this->container_guid, $this->time_updated);
		}
		return $result;
	}
}
