<?php
/**
 * Short summary of the action that occurred
 *
 * @vars['item'] ElggRiverItem
 */

$item = $vars['item'];

$subject = $item->getSubjectEntity();
$object = $item->getObjectEntity();
$target = $object->getContainerEntity();

$subject_link = elgg_view('output/url', array(
	'href' => $subject->getURL(),
	'text' => $subject->name,
	'class' => 'elgg-river-subject',
	'is_trusted' => true,
));

$object_text = $object->title ? $object->title : $object->name;
if($object_text == "") {
	// if the object is a comment
	if($object->getSubtype() == 'draw_application') {
		$object_link = elgg_view('output/url', array(
			'href' => $object->getURL(), //$object->getURL(),
			'text' => $object->Name, //'here',
			'class' => 'elgg-river-object',
			'is_trusted' => true,
		));
		
	} 
	// if the container of the object is a topic
	elseif ($target->getSubtype() == 'groupforumtopic') {
		$object_link = elgg_view('output/url', array(
			'href' => $target->getURL(), //$object->getURL(),
			'text' => $target->title,
			'class' => 'elgg-river-object',
			'is_trusted' => true,
		));
		$object_link = elgg_echo('river:reply', array($object_link));
	}

} else {
$object_link = elgg_view('output/url', array(
	'href' => $object->getURL(),
	'text' => elgg_get_excerpt($object_text, 100),
	'class' => 'elgg-river-object',
	'is_trusted' => true,
));
}

$action = $item->action_type;
$type = $item->type;
$subtype = $item->subtype ? $item->subtype : 'default';

$container = $object->getContainerEntity();
if ($container instanceof ElggGroup) {
	$params = array(
		'href' => $container->getURL(),
		'text' => $container->name,
		'is_trusted' => true,
	);
	$group_link = elgg_view('output/url', $params);
	$group_string = elgg_echo('river:ingroup', array($group_link));
}

// check summary translation keys.
// will use the $type:$subtype if that's defined, otherwise just uses $type:default
$key = "river:$action:$type:$subtype";
$summary = elgg_echo($key, array($subject_link, $object_link));

if ($summary == $key) {
	$key = "river:$action:$type:default";
	$summary = elgg_echo($key, array($subject_link, $object_link));
}

echo $summary;