<?php
/**
 * Elgg primary CSS view
 *
 * @package Elgg.Core
 * @subpackage UI
 */

/* 
 * Colors:
 *  #4690D6 - elgg light blue
 *  #0054A7 - elgg dark blue
 *  #e4ecf5 - elgg very light blue
 */

// check if there is a theme overriding the old css view and use it, if it exists
$old_css_view = elgg_get_view_location('css');
if ($old_css_view != elgg_get_config('viewpath')) {
	echo elgg_view('css', $vars);
	return true;
}


/*******************************************************************************

Base CSS
 * CSS reset
 * core
 * helpers (moved to end to have a higher priority)
 * grid

*******************************************************************************/
echo elgg_view('css/elements/reset', $vars);
echo elgg_view('css/elements/core', $vars);
echo elgg_view('css/elements/grid', $vars);


/*******************************************************************************

Skin CSS
 * typography     - fonts, line spacing
 * forms          - forms, inputs
 * buttons        - action, cancel, delete, submit, dropdown, special
 * navigation     - menus, breadcrumbs, pagination
 * icons          - icons, sprites, graphics
 * modules        - modules, widgets
 * layout_objects - lists, content blocks, notifications, avatars
 * layout         - page layout
 * misc           - to be removed/redone

*******************************************************************************/
echo elgg_view('css/elements/typography', $vars);
echo elgg_view('css/elements/forms', $vars);
echo elgg_view('css/elements/buttons', $vars);
echo elgg_view('css/elements/icons', $vars);
echo elgg_view('css/elements/navigation', $vars);
echo elgg_view('css/elements/modules', $vars);
echo elgg_view('css/elements/components', $vars);
echo elgg_view('css/elements/layout', $vars);
echo elgg_view('css/elements/misc', $vars);


// included last to have higher priority
echo elgg_view('css/elements/helpers', $vars);


// in case plugins are still extending the old 'css' view, display it
echo elgg_view('css', $vars);


/*my code begins here */
?>
.window
		{
			background-color: white;
			border: 3px solid #346789;
			color: black;
			font-family: helvetica;
			font-size: 0.8em;
			height: 60px;
			opacity: 0.8;
			padding: 0.5em;
			position: absolute;
			width: 60px;
			z-index: 20;
			text-align: center;
		}

		.vmwindow
		{
			background-color: #FFFFD1;
			border: 3px solid #946789;
			color: black;
			font-family: helvetica;
			font-size: 0.8em;
			height: 60px;
			opacity: 0.8;
			padding: 0.5em;
			position: absolute;
			width: 60px;
			z-index: 20;
			text-align: center;
		}

		.slowindow 
		{
			background-color: #F00FD1;
			border: 3px solid #946789;
			color: black;
			font-family: helvetica;
			font-size: 0.8em;
			height: 35px;
			opacity: 0.8;
			padding: 0.5em;
			position: absolute;
			width: 70px;
			z-index: 20;
			text-align: center;
		}

		.dragActive
		{
			border: 10px solid red;
		}
		
		.composition_area
		{
		    width: 700px;
		    height: 600px;
		    float:left;
		    border: solid 2px;
		    overflow:scroll;
		}
		
		.suggestions
		{
			width: 15em;
			height: 37.5em;
			border: 2px solid black;
			float: left;
			background-color:#aaffff;
			word-wrap: break-word;
			overflow:scroll;
		}

		.mddbfd
		{
			color: #000000;
			background-color: #5F9EA0;

		}
		.json_data
		{
			position: relative;
		}

		.suggestfromMDDB 
		{
			border:2px solid #a1a1a1;
			padding:10px 10px; 
			background:#dddddd;
			border-radius:20px;
		}